## C 语言设计模式

#### 单一职责

单一职责：只专注于做一件事和仅有一个引起它变化的原因。接口、实现、函数级别的单一职责很容易，但很可能会忽略返回值、参数。接口永远是给别人使用的，一定要把使用者当成傻瓜，才能设计出好的接口。往往就是某些不符合单一职责原则的设计，导致很难发现的BUG。比如标准库函数`realloc()`就是返回值不单一：

```c
/* void *realloc(void *mem_address, unsigned newsize); // 给一个指针，赋予新的内存空间。如果重新分配成功则返回指向被分配内存的指针，否则返回NULL */
pBuf = (byte*)realloc( pBuf, size);
if( pbBuf != NULL ) {
       // do something
} //Bug：如果内存分配失败，pBuf被赋值NULL，原空间就成了泄露的内存
```

如果改善`realloc()`的设计，让返回值「单一职责」，就会带来更简单、无隐患的调用方式：

```c
boolean realloc(void **ppmen_addr, unsigned size) {
  boolean flag = true;
  if (succeed())
      **ppmen_addr = new_addr; // 分配成功，指针就指向新内存。返回true
  else
      boolean flag = false;// 分配失败，指针指向原内存。返回false
  return flag;
}
/* 更简单的、无隐患的 调用方式 */
if (realloc(&pBuf, size)) {// do something
} else { // failed. do something special
}
```

#### 接口隔离

接口隔离：客户端不应该依赖它不需用的接口（只提供外部接口，隐藏内部接口）。

在C语言中，一般将头文件作为模块的接口。但在实际的项目中，往往头文件不符合接口隔离原则。

- 例1：头文件包含模块内部接口（内部类型定义、内部接口声明）和外部接口（外部接口声明）

  ```c
  /* moudle.h */
  struct s_point { int a, b; }; // 内部接口
  typedef struct s_point Point; // 内部接口
  void func(void); // 外部接口

  /* moudle.c */
  #include "moudle.h"
  void func(void) {  // 接口的实现
      Point p = {0, 0};
      // do something
  }
  ```

  改善：将内部接口放在`moudle.inc`文件，而外部接口放在`moudle.h`文件

  ```c
  /* moudle.h */
  void func(void);

  /* moudle.inc */
  struct s_point { int a, b; };
  typedef struct s_point Point;
  ```

- 例2：千万别用「万能头文件」——一个头文件包含项目所有的接口

  ```c
  /* global.h */
  #inlcude "moudle1.h"
  #inlcude "moudle2.h"
  ....
  #inlcude "moudlen.h"
  ```

内外隔离、头文件层次分明的好处：

- 缩减编译时间。大项目、头文件混乱，头文件处理会耗费大量的编译时间
- 头文件层次分明，能让调用者清楚模块的依赖关系，并且减少误操作。比如一个数据获取模块有两个子模块：网络获取、本地缓存获取。后台使用时，调用网络；前台则调用本地缓存。如果两个子模块的头文件混合，维护者可能不知道这样的模块划分，前台使用网络就会出现性能问题；后台使用本地缓存则是bug。

#### 依赖于抽象，不依赖于具体

依赖倒置：依赖于抽象，而不是依赖于具体；让调用者对抽象进行编程，而不是对实现进行编程，从而降低调用者与实现模块间的耦合。

例如，对应不同的终端，显示不同的内容

```c
/* 调用者的代码 */
if (type == terminal_1) { // 判断究竟使用哪种终端。if switch都可
  T1_display();
} else if (type == terminal_2) {
  T2_display();
}
```

```mermaid
graph TD;
A(user)-->B;
subgraph Terminal
B(display)
end;
B-->C; B-->D;
subgraph terminal_2
D(T2_display)
end;
subgraph terminal_1
C(T1_display)
end;

c(user)-->b; c-->a;
subgraph terminal_2
b(T2_display)
end;
subgraph terminal_1
a(T1_display)
end;
```

这样做，调用者依赖了实现，业务依赖了底层，完全耦合。如果新增一种终端，就必须改动调用的代码，增加一个判断分支。如果系统非常复杂，那么工程中就会遍布这种判断，开发维护都很难。

改进：提供一个稳定的接口。在面向对象的语言比如 JAVA 中，接口很容易实现。其实C也可以实现。

1. 底层

   ```c
   #include "Terminal.h"
   void T1_display() {
       printf("Terminal1_display \r\n");
   }
   Terminal init_Terminal() {
       Terminal t;
       t.display = T1_display; // 在这里可以增加判断语句
       return t;
   }
   ```

2. 接口

   ```c
   /*Terminal.h*/
   typedef struct Terminal_s Terminal;
   Terminal init_Terminal();
   struct Terminal_s
   {
       void (*display)();
   };
   ```

3. 调用者

   ```c
   Terminal t;
   t = init_Terminal();
   t.display();
   ```


#### 实现「面向对象」

不建议用 C 实现”面向对象“语法（抽象、封装、继承、多态）。而是建议使用面向对象的思想：将数据和操作绑定，用指针变换操作方式。

- 用定义实现类。
- 将类的属性（数据和方法）组成一个`struct`。如果是`private`的属性，就使用不透明指针。将这个`struct`和相关的操作方法的声明，放在同一个头文件。
- 每一个函数的第一参数，都是`int func(MyStruct *mystruct_t);`这种形式。而这个指针，可以这么声明`typedef struct Mystruct *mystruct_t;`。
- 函数的实现，放在相关的`*.c`文件里。公开的函数，就把声明放在`*.h`文件里。而私有的函数，只在`*.c`里定义，而且定义为`static`避免命名冲突。而且命名的时候，尽量`mystruct_func()`比较好区分。另外，内存管理严格注意，可能需要构造函数 `mystruct_t *create()` 以及析构函数 `void destroy(mystruct_t *target)` 。

[博文](http://blog.csdn.net/besidemyself/article/category/959727)



- 抽象：将数据与操作，组织为一个类

  ```java
  // java 代码
  class Square { // 类
      int side; // 成员变量
      public Square(int side) { // 成员函数
          this.side = side;
      }
      public void draw() {
          System.out.println("Square len: " + this.side);
      }
  }
  public class Main {
      public static void main(String[] args){
          Square s = new Square(12);
          s.draw();
      }
  }
  ```

  用C，则用结构体和指针实现

  ```c
  typedef struct square_s { // tag _square 不可以省略，为了继承
      int side; // 用结构成员，表示 类的成员变量
      void (*draw) (void *); // 用结构成员(指针)，表示 类的成员方法
  } Square;
  static void draw(void *sq_obj) {    // 需要手动传入自身的地址
      Square *sp = (Square *) sq_obj;
      printf("Square len is %d\n", sp->side);
  }
  int main(void) {
      Square s = {12, draw}; // 初始化
      s.draw(&s);  // 函数调用
      return 0;
  }
  ```

- 封装

  ```java
  // java 代码
  class Square { // 类
      private int side; // 用 private 等访问关键字，就可封装
      ...
  }
  ```



## *"Patterns in C"* series

*"Patterns in C"* series by [Adam Petersen](http://adampetersen.se):

- [State](http://adampetersen.se/Patterns%20in%20C%202,%20STATE.pdf)
- [Strategy](http://www.adampetersen.se/Patterns%20in%20C%203,%20STRATEGY.pdf)
- [Observer](http://www.adampetersen.se/Patterns%20in%20C%204,%20OBSERVER.pdf)
- [Reactor](http://www.adampetersen.se/Patterns%20in%20C%205,%20REACTOR.pdf)

软件开发越来越复杂，所以产生了：

1. 组件/模块化开发，更容易设计、实现、编译、维护。
   - 将模块切分，然后命名，就像 JAVA class。别仿造OO语言的语法（特别是继承），这会非常难以维护，组合还是可以使用的。
   - 将接口和实现分开。只把必须的接口暴露给其他模块（C语言没有命名空间，这样也可以降低标识符冲突）。
   - 隐藏全局变量，而用`read() write()`之类的函数去读写。
   - 维护方面，给代码写注释和文档，并且工程里写上测试代码。
2. 设计模式。（方便沟通，并且可以将软件设计的更好）

设计模式，是软件工程界借用的建筑学词汇，它有利于正确、精确地表达和交流。所以，「设计模式」更像是一种沟通工具，而不是软件工程的解决方案。所谓模式，是如何在不同的上下文中、解决重复性出现的问题的简单而优雅的公式。

「设计模式」分为3类：

1. *惯用法Idiom*。语言层面，某种特定语言的经验用法，无法迁移到其他语言。

   - 常用的优秀表达式。表现力强，而且健壮。

     ```c
     void strcpy(char *s, char *t)
     {
     	while (*s++ = *t++);
     }
     ```

   - First-Class ADT。*ADT，数据抽象Abstract Data Type。数据类型可以分为三级：*

     | First-Class     一级数据类型 | 可作为函数参数        | 可作为返回值        | 可赋值给变量        |
     | ---------------------- | -------------- | ------------- | ------------- |
     | Second-Class 二级数据类型    | 可作为函数参数        | ***不可***作为返回值 | ***不可***赋值给变量 |
     | Third-Class     三级数据类型 | ***不可***作为函数参数 | ***不可***作为返回值 | ***不可***赋值给变量 |

2. *设计Design*。组件、模块的设计方法。

   - 状态机模式。
   - 策略模式。封装了算法簇，对扩展开放，对修改关闭。
   - 观察者模式。松耦合实现了通知机制。

3. *架构Architecture*。软件整体架构法则。最基本的法则，确定有多少个子系统、各组件功能、组件相互关系。

   - Reactor反应堆架构：减弱了事件驱动程序的职责


#### First-Class ADT

下面的代码导致”一个客户只能有42个订单“。当然，给`MAX_ORDERS`赋个更大的值可以改善，却不能根治。

```c
#define MAX_ORDERS 42
/* Internal representation of a customer. */
typedef struct {
  const char* name;
  Address address;
  size_t noOfOrders;
  Order orders[MAX_ORDERS];
} Customer;
void initCustomer(Customer* man, const char* name, const Address* addr);
void placeOrder(Customer* man, const Order* od);
/* A lot of other related functions and struct... */
```

采用链表可以解决，但如果变换数据结构，那么相关的函数每个地方都得跟着修改，别说是可能引入新的bug，就是所有client重新编译，都是大把时间，怎么办？程序设计的理想状况是：一个组件对扩展开放，对修改关闭，封装内部实现，只向上层client提供唯一的接口，这样只需要修改这个模块。使用First-Class ADT：

- 松耦合：把接口和实现分离
- 强封装：隐藏细节
- 更可控：构造函数和析构函数
- 增加抽象：所有的数据操作，都包裹了一个抽象层
- 动态内存：动态分配内存

ADT/Abstract Data Type，是数据和操作数据的集合。First-Class，当我们需要考虑很多独特场景时，ADT是首要考虑的。

C语言语法：当size无需考虑的时候，允许声明不完整类型的对象。比如下例，这个无需`int`之类指定size的声明语句，Customer没有结构体、不是完整的类型，然而还是可以声明指向这类对象的指针（当然指针是完整类型）。只有编译器遇到下面的标识符，相同的tag、完整的结构体声明，才会认为结构体Customer是完整的。

```c
typedef struct Customer* CustomerPtr;//这时候Customer并不完整
struct Customer {
  const char* name;
  Address address;
  size_t noOfOrders;
  Order orders[42];
};//这时候Customer才完整
```

用这种方式，指针作为访问ADT的句柄，就可以强制调用者使用提供的接口，因为没有办法获取内部信息。只要接口不增不减不变，无论内部怎么变化，调用者都无需改变——松耦合、强封装。当调用者无法干涉内部机制的时候，对象的初始化、销毁都可以让ADT自己管理——更可控、动态内存。

- 对外提供的接口`Customer.h`

  ```c
  typedef struct Customer* CustomerPtr; // 句柄
  CustomerPtr createCustomer(const char* name, const Address* addr); // 构造函数
  void destroyCustomer(CustomerPtr man); // 析构函数
  ```

- 内部的实现`Customer.c`

  ```c
  #include "Customer.h"
  #include "Order.h"  // 外部提供的一种数据类型。
  #include <stdlib.h>
  struct Customer {
    const char* name;
    Address address;
    size_t noOfOrders;
    Order orders[42]; // 使用者无法访问，所以可以任意更改实现方案
  };
  CustomerPtr createCustomer(const char* name, const Address* addr) {
    CustomerPtr customer = malloc(sizeof *customer);
    if(customer) {
      /* Initialize each field in the customer... */
    }
    return customer;
  }
  void destroyCustomer(CustomerPtr man) {
    free(man); /*clean internal data if necessary. */
  }
  ```

  上面的代码用malloc获取内存空间。有些嵌入式编程不能这么做。如果说数量有个上限，那么也很容易用数组实现：

  ```c
  #define MAX_CUSTOMERS 42
  static struct Customer objectPool[MAX_CUSTOMERS];
  static size_t numberOfObjects = 0;
  CustomerPtr createCustomer(const char* name, const Address* addr) {
    CustomerPtr customer = NULL;
    if(numberOfObjects < MAX_CUSTOMERS) {
      customer = &objectPool[numberOfObjects++];
      /* Initialize the object... */
    }
    return customer;
  }
  void destroyCustomer(CustomerPtr man) {
    /* 这里需要很强的内存管理算法*/
  }
  ```

- 其他问题：

  拷贝：如果只是拷贝指针（就像Python），这会提高运行性能，但是要注意，如果实体已经销毁，指针是非法的。如果想深层拷贝实体，那就必须提供接口、实现接口。——增加抽象。

- 应用案例：

  C语言标准库的 FILE 结构体：虽然FILE是完整类型，但原则是一样的，FILE内部各平台的实现可能不同，不可移植，但接口相同。

  Sockets：Uinx 和 Win的实现肯定不同，但是提供了相同的接口（`*.h`文件），所以客户端用相同的代码，跨平台调用。

  算法-C语言实现 Robert Sedgewick 使用了很多ADT。



#### 状态机State Machine

编程中（特别是过程式语言），尽量将状态和副作用最小化，这些都很容易出问题。状态很难进行逻辑推理和跟踪：

- 多个因素共同影响：输入、当前的状态，影响下一个状态
- 状态变量大多是隐藏的：化身为不同的变量名，隐藏在设计中

状态机就是为了解决这类问题而生的：

- 将共同的状态算法（相关的函数）归为一组
- 显式、规则地表达状态

##### 最简单的例子：数字秒表

- 用分支语句、First-Class ADT 来实现，单个文件

  ```c
  #include <stdio.h>
  #include <time.h>
  #include <stdlib.h>
  #ifdef _WIN32   /* windows ************ */
  #include <windows.h>
  #define sleep(n)  Sleep((n))  /* _Sleep函数 milliseconds 毫秒， 1/1000 s  */
  #else   /* Unix && Linux ************ */
  #include <unistd.h> /* usleep函数 microseconds 微秒， 1/1000 000 s  */
  #define sleep(n)  usleep(1000 * (n)) /* 改造为_Sleep函数 1/1000 s  */
  #endif
  typedef struct DigitalStopWatch* DigitalStopWatchPtr;
  typedef enum { stopped, started } State;
  typedef void (*WatchDisplay)(DigitalStopWatchPtr instance);
  struct DigitalStopWatch {
      State state;
      int starttime, finishtime;
      WatchDisplay display;
  };
  void startWatch(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:  /* Already started -> do nothing. */
          break;
      case stopped:
          instance->state = started;
          instance->starttime = clock();
          break;
      default:
          fprintf(stderr, "Illegal state");
          exit(0); // error("Illegal state");     break;
      }
  }
  void stopWatch(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:
          instance->state = stopped;
          instance->finishtime = clock();
          break;
      case stopped: /* Already stopped -> do nothing. */
          break;
      default:
          fprintf(stderr, "Illegal state");
          exit(0); // error("Illegal state");     break;
      }
  }

  void display(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:
          printf("\t%d\n", (clock() - instance->starttime));
          break;
      case stopped: /* Already stopped -> do nothing. */
          printf("\t%d\n", (instance->finishtime - instance->starttime));
          break;
      default:
          fprintf(stderr, "Illegal state");   exit(0);
      }
  }
  int main(void) {
      struct DigitalStopWatch watch = {stopped, 0, 0, display};
      startWatch(&watch);
      watch.display(&watch);
      sleep(3000);
      watch.display(&watch);
      sleep(7000);
      stopWatch(&watch);
      watch.display(&watch);
      return 0;
  }
  ```

  这种实现的缺点：

  - 冗余。所有与状态有关的函数，内部都差不多是“状态判断，执行语句”，逻辑几乎是一样的。冗余意味着维护容易出错，比如要增加一个状态，那么所有的功能都要改变，万一有个函数改错了……
  - 高度耦合。每一个函数，对象的状态 与 操作语句混合在一起，没有将行为抽象为一个有限的状态机，导致可读性太差。
  - 不能扩大规模。大型的状态机，一页页的复杂缠绕，同时又单调乏味的分支逻辑，维护绝对是噩梦。

- 用转换表 transition tables 来实现

  易读，没有冗余，扩展规模非常方便。缺点是：除了切换状态之外，很难添加别的函数（display()就没法用这种方式重构），也不易读。

  ```c
  /* 新增代码 */
  #define NUM_OF_STATES 2
  #define NUM_OF_EVENTS 2
  typedef enum { stopped, started } State;
  typedef enum { stopEvent, startEvent } Event;
  static State TransitionTable[NUM_OF_STATES][NUM_OF_EVENTS] = {
      { stopped, started },
      { stopped, started }
  };

  /* 重构的代码 */
  void startWatch(DigitalStopWatchPtr instance) {  // 避开了 分支语句
      const State currentState = instance->state;
      instance->state = TransitionTable[currentState][startEvent];
  }
  void stopWatch(DigitalStopWatchPtr instance) {
      const State currentState = instance->state;
      instance->state = TransitionTable[currentState][stopEvent];
  }
  ```

- State Machine Pattern

  转换表，本质是关注状态的转换，所以这种设计不太贴合。

  State Machine Pattern，本质是针对’‘特定模式的方法“进行建模。

  ​

  ​

  ​

  ​

  1

- 1




## UML

#### plantUML

VScode 安装 `plantuml/jeb`。如果需要预览，2种方法：

- 使用server预览（以markdown为例）：

  写在markdown里，VScode 安装`markdown-all-in-one/yzh`

  ```plantuml
  Duck <|-right- RedheadDuck
  ```

  `Ctrl Shift V`预览Markdown，联网就可预览。如果需要本地预览，则必须设立本地[plantUML Server](http://plantuml.com/server)：

  - 下载 [plantuml.war](http://sourceforge.net/projects/plantuml/files/plantuml.war/download) 和 tomcat。将`*.war`放在apps目录，比如`..\Tomcat 8.5\webapps\`，重启tomcat，自动会解压，生成`Tomcat 8.5\webapps\plantuml`。浏览器打开`http://localhost:8080/plantuml`就可使用
  - 在VScode里面，设置settings.json，`"plantuml.server": "http://localhost:8080/plantuml","plantuml.render": "PlantUMLServer",`就可以预览markdown里面的UML

- 使用 `plantuml.jar`和`graphviz` 预览（以`*.puml`文件为例）

  - 一般来说，文件名后缀`puml`，文件内容以`@startuml`起头，以`@enduml`结尾。
  - VScode预览：安装JDK，下载[plantuml.jar](http://sourceforge.net/projects/plantuml/files/plantuml.jar/download) ，默认要放在`jdk/jre/ext`，下载 [Graphviz](http://www.graphviz.org)，默认放在`C:/Program Files/graphviz/bin/dot.exe`。右击选择预览，或者Ctrl D
  - 输出图片文件：`"cd $workspaceRoot &&java -DGRAPHVIZ_DOT=\"C:/Program Files/graphviz/bin/dot.exe\"  -jar F:/Note/plantuml.jar $fullFileName"`（请注意，jar包、graphviz路径都在命令行设置了，如果没有设置，请参考上一条的默认路径）

#### UML 六大关系

继承和实现是is-a关系（有时统称继承），聚合和组合是has-a关系（有时统称组合）。

上下关系：继承和实现

平行关系：依赖 --->--- 关联 --->--- 聚合 --->--- 组合

- 继承Extension：`Super <|-- Sub`，子类继承父类。

  java代码略

  plantUML：

  ```plantuml
  class Super {
  }
  class Sub extends Super {
  }
  ```

- 实现Realization：`InterFaceA <|.. B`，子类实现抽象类、接口

  java代码略

  plantUML：

  ```plantuml
  interface InterFaceA { 
  	public void quack() 
  }
  class B implements InterFaceA {
      public void quack()
  }
  ```

- 依赖Dependency：`A <.. B`，类A偶然、临时地用到了类B，B类的变化可能会影响A类。比如，类B是类A方法的一个参数。

  Java代码：

  ```java
  A a = new A();
  B b = new B();
  a.method_m(b); 
  ```


  plantUML：

  ```plantuml
  A <.left. B
  ```


- 关联Association：`A <-- B`，两个类（或者类与接口）之间长期稳定的强依赖关系。比如，被关联的类B，是类A的类属性。

  ```java
  class A {
    static B b;
  }
  ```


  plantUML：

  ```plantuml
    class A {
      static B   b;
    }
    A <-left- B
  ```

- 聚合Aggregation：`Family o--> Child`，一个类是另一个类的组成部分，但是也可以分离。比如孩子与家庭。代码中，一般类B是类A的成员属性

  ```java
  class Family {
    Child alice = new Girl();
  }
  class Family {}
  class Girls extends Family {}
  ```
  plantUML：

  ```plantuml
    class Family {
      Child alice
      getName()
    }
    Family o-left-> Child
    Child <-- Girl
  ```

- 组合Composition：强聚合。比如，脑器官与人体。有可能是内部类，但聚合与组合不是从语法区分，而是从逻辑区分的。

  ```java
  class Body {
    Brain b = new Brain();
  }
  class Brain {}
  ```
  plantUML：

  ```plantuml
    class Body {
      Brain b;
    }
    class Brain
    Body *-right-> Brain
  ```

  ​


## Head First Design Patterns

模式成长之路：简单的模式——组合模式——模式创造

4个基本object oriented概念：

- 抽象：将数据与操作结合。成员变量与成员方法、静态变量与静态方法、构造器
- 封装：访问关键字、作用域
- 继承：父类、子类
- 多态：组合、接口、抽象类、泛化

不管什么公司、什么工程，什么编程语言，在软件开发里，唯一不变的就是变更。不管软件当初设计多好，一段时间后，总是需要成长和改变，否则软件就会“死亡”。变更原因有：改变需求或新增功能；兼容新的数据库、硬件、软件；出现新技术；主动重构。

软件开发完成后的维护、变更，比软件开发完成前的创造，所花的时间会更多。所以，应该提高可维护性、可扩展性。

因此，知道OO概念，并且也熟悉语法，并不能写出好的面向对象程序。需要建立弹性的设计，提高复用性、扩展性、维护性，才可以应对各种变更。

要诀：随时都能预见系统以后可能需要的变化、应对变化的原则和方法。

大部分设计原则和设计模式，都是掌控这种变化，都是为了让系统的局部改变独立于其他部分，而把系统中会变化的部分抽出来封装。



9个OO原则Principle：P491

1. 封装变化：这是总纲，将「变化」与「不变」分开，让系统中的某部分发生改变不会影响其他部分。找出程序中可能需要变化的部分，独立出来，不要混在一起。如果每次有新需求，就会让某部分代码产生变化，那么这部分就抽取出来，和其他稳定的代码区分开来。
2. 开放-关闭：对扩展开放，对修改关闭。允许代码容易扩展，不修改现有代码，只是通过新增代码来增加新的功能。通过业务分析，预测可能的变更，合理规划，建立弹性的设计架构，足以应付改变，才不容易引入新的bug。但是，不可能让每部分都遵循开放-关闭原则，这样会浪费很多时间，并且代码很复杂，不易读。
3. 多用组合，少用继承。继承和组合都是一种扩展形式。继承时，子类的属性是编译时决定的，只能继承父类，或者自己覆盖。而组合+委托 可以在运行时实现和继承相同的效果，却更加有弹性，更灵活。这样可以动态扩展对象的行为，甚至不用修改原代码，就给代码添加了新的功能。
4. 针对接口编程，而不是针对实现编程。在java 中，是“使用多态语法，针对超类/接口编程”。比如在策略模式使用  组合+接口（多态语法），非常灵活。
5. 依赖抽象，而不是依赖具体。
6. 让交互对象松耦合。让对象之间的相互依赖降到最低，就能建立弹性的系统，能够应对变化。
7. 最少知识原则：只和你的密友谈话
8. 别调用「我们」，「我们」来调用你
9. 一个类只有一个改变的理由

设计原则是目标，设计模式是途径，而OO概念与语法是具体实现。

各种语言的惯用法、函数与类、库、框架，是“代码复用”；设计模式，是层次更高的“经验复用”，是历经验证的OO设计经验，是针对某类设计问题的通用解决方案，以此可以构建具有良好设计质量的系统。

使用设计模式的专业词汇，有利于沟通。当然，不能过分使用，比如写个 Hello World 都使用模式……







### 策略模式 Strategy Pattern

任务：创建很多鸭子类，抽象类Duck，绿头的野鸭MallardDuck，红头的RedheadDuck， 不会飞、叫声不一样的模型鸭ModelDuck，橡皮鸭RubberDuck

##### 混乱的实现：

这设计现在看来也还行：父类规划了3个方法，抽象方法`display()`由子类实现，而其他方法让子类定制，比如ModelDuck自己覆盖了`Quack()`。

```plantuml
@startuml
abstract class Duck {
    + void Quack()
    + void swim()
    + {abstract}abstract void display()
}
class MallardDuck野鸭 extends Duck {
    + void display(){//绿头}
}
Duck "Super" <|-- "Sub" RedheadDuck红头鸭 : Generalization
class RedheadDuck红头鸭 {
    + void display(){//红头}
}
class ModelDuck模型鸭 extends Duck {
    + void display(){//模型鸭}
    + void Quack(){//叫声不同}
}
@enduml
```
如果来了新需求：1. 要增加一个`fly()`方法，但是有些鸭子不会飞。2. 增加一个橡皮鸭`RubberDuck`类，这种鸭子不会飞也不会叫。

设计父类的时候，很难知道所有子类的情况。如果一味使用继承，会导致：

- 不继承的代码，会在多个子类中重复。比如`ModelDuck`和`RubberDuck`都要覆盖父类的`Quack()`方法，如果子类更多，问题就更严重
- 继承的代码，父类改变，子类全部被动改变，或许子类并不需要。
- 只能使用类中绑定的方法。相对于策略模式，运行时不能动态改变方法。

可以看出`fly() quack()`两个方法会随着鸭子种类的不同而改变，就提取出来（封装变化原则）。那么，究竟怎样实现这两个行为呢？

##### 策略模式，算法族

策略模式，就是定义算法族，分门别类封装起来，让一个族内的算法可以互相替换，这样，算法的变化就独立于算法的用户。

将这2种放在分开的类中，并且使用2个接口统领（原则：针对接口编程，而不是针对实现编程）。

```plantuml
abstract class Duck {
    QuackBehavior quackBehavior
    FlyBehavior flyBehavior
    + Duck()
    + {abstract}abstract void display()
    + void performQuack()
    + void performFly()
    + void setFlyBehavior()
    + void setQuackBehavior()
    + void swim()
}
class MallardDuck extends Duck{
    + MallardDuck ()
    + void display()
}
class RedheadDuck extends Duck {
    + void display() {}
}
class RubberDuck extends Duck {
    + ModelDuck()
    + void display()
}
class ModelDuck extends Duck {
    + ModelDuck()
    + void display()
}
Duck -left-* FlyBehavior
Duck -right-* QuackBehavior

interface FlyBehavior {
    + void fly()
}
class FlyWithWings  {
    + void fly()
}
class FlyNoWay  {
    + void fly()
}
class FlyRocketPowered {
    + void fly()
}
FlyRocketPowered .down.|> FlyBehavior
FlyNoWay .down.|> FlyBehavior
FlyWithWings .down.|> FlyBehavior

interface QuackBehavior {
    + void quack();
}
class Quack {
    + void quack()
}
class MuteQuack {
    + void quack()
}
class Squeak {
    + void quack()
}
Quack .down.|> QuackBehavior
MuteQuack .down.|> QuackBehavior
Squeak .down.|> QuackBehavior
```

将不变的部分，留在Duck类族里；而变化的那些方法，抽取出来成为算法族。不一定是开发后才知道需要分离，有经验的开发者，在设计时就会预先考虑到这种变化，于是在代码中预先加入这些弹性。将不变的设计为继承的类族，而经常改变的设计为接口，并且使用接口QuackBehavior而不是使用具体的子类Quack（依赖接口原则）。

将FlyBehavior组合Composition为Duck，而Duck将fly委托Delegation给FlyBehavior。“依赖于接口”，是用**父类+多态**语法实现的，这样就可以在运行时动态切换。

```java
FlyBehavior flyBehavior = new FlyNoWay();//声明为接口类型，而不是子类。有时这里会用getFly()的方式
flyBehavior.fly(); // 父类泛化为子类，运行时才知道究竟是哪个子类
flyBehavior = new FlyRocketPowered(); // 运行时动态切换
flyBehavior.fly(); // 切换成功
```



### 观察者模式Observer Pattern

设计任务：建立一个气象站。WeatherData数据类负责从各种传感器生成数据，汇总、发送。DisplayElement展示板类（现状、统计、预测）负责展示数据。WeatherStation类是主入口，运行整个程序。

##### 高度耦合的实现

如果不进行设计，随便一写，是以下实现：

```java
abstract class DisplayElement_A {
    float temperature, humidity, pressure;
    abstract public void update();
    abstract public void display();
}
class CurrentDisplay_A extends DisplayElement_A { // 会有其他方法和数据。其他展示板差不多也这样
    public void update(float temperature, float humidity, float pressure){
        this.temperature = temperature;  this.humidity = humidity;
    }
    public void display() { System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");}
}
class StatisticsDisplay_A extends DisplayElement_A { // 最大、最小、平均值
}
class ForecastDisplay_A extends DisplayElement_A { //根据气压显示预报
}

class WeatherData_A {
    float temperature, humidity, pressure;
    DisplayElement_A currentDisplay, statisticsDisplay, forecastDisplay;
    public void measurementChanged() {
        this.temperature=getTmp();  //取最新的数据
        this.humidity=getHum(); this.pressure=getPre();
        currentDisplay.update(temperature, humidity, pressure);//取数据后，依次更新各个展示板
        statisticsDisplay.update(temperature, humidity, pressure);
        forecastDisplay.update(temperature, humidity, pressure);
    }
}
public class WeatherStation_A {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentDisplay currentDisplay = new CurrentDisplay;
        weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
        weatherData.update();
        currentDisplay.display();// 依次展示数据
    }
}
```

上面的实现，有巨大的缺陷：每次新增一个展示板，都必须更改WeatherData、WeatherStation（差不多就是整个工程）；另外，无法在运行时动态地增加、删除展示板。

违反的设计原则是：针对具体实现编程，而不是针对接口编程；没有将变化封装。

```plantuml
abstract class DisplayElement_A {
    float temperature, humidity, pressure;
    {abstract} update(float temperature, float humidity, float pressure){}
    {abstract} display();
}
class CurrentDisplay_A extends DisplayElement_A {
    update()
    display()
}
class StatisticsDisplay_A extends DisplayElement_A { 
    update()
    display()
}
class ForecastDisplay_A extends DisplayElement_A {
    update()
    display()
}

class WeatherData_A {
    float temperature, humidity, pressure;
    DisplayElement_A currentDisplay;
    DisplayElement_A statisticsDisplay;
    DisplayElement_A forecastDisplay;
    measurementChanged(){}
    {method}//取得数据，然后依次更新各展示板
}
class WeatherStation_A {
    main(String[] args) {}
}
WeatherData_A -left-> DisplayElement_A
WeatherStation_A -down-> WeatherData_A
WeatherStation_A -down-> DisplayElement_A
```



##### 观察者模式，建立松耦合的一对多关系

这个模式中，会改变的是：主题的状态，观察者的数目和类型。

使用接口编程，而不是使用实现编程，松耦合的关系：观察者利用主题的接口进行注册或注销，主题利用观察者的接口进行通知。

使用组合：在运行时，动态地将观察者装入观察者体内。

| 媒体与用户（比如报社与订阅者）    | 观察者模式                  |
| ------------------ | ---------------------- |
| 报社：出版发行报纸          | 主题对象：管理数据              |
| 如果订阅了报纸，有新报就会递送给用户 | 数据一旦改变，就会通知观察者，传递数据    |
| 未订阅、取消订阅的用户，不会递送   | 未注册、取消注册的观察者，不会通知、传递数据 |

观察者模式定义了对象之间**一对多**依赖关系，当一个对象改变状态时，它的所有依赖者都会收到**通知并自动更新**。

主题对象：具有数据，并且控制数据的一个对象。由一个对象控制数据，比让多个对象控制数据，要更干净、易于掌控。

观察者对象：使用主题对象的数据、依赖主题对象，多个对象。

主题接口，一般都有`registerObserver/removeObserver/notifyObserver` 3类函数，注册、撤销注册、通知或更新观察者。而接口继承者，某个具体的主题类，则有自己的数据、设置数据和从数据源获取数据的方法。

观察者接口，一般都有`update`函数，与主题类交互，以更新自己的数据。继承的、具体的某个观察者类，必须注册某个具体主题类，以便接收数据。

UML如下：

```plantuml
interface Subject {
    registerObserver();
    removeObserver();
    notifyObserver();
}
class Broadcast  implements Subject {
    registerObserver(){}
    removeObserver(){}
    notifyObserver(){}
    --
    getState(){}
    setState(){}
}
interface Observer {
    update()
}
class Listener_A implements Observer {
    update(){}
}
class Listener_B implements Observer {
    update(){}
}
class Listener_* implements Observer {
    update(){}
}
Observer --> Broadcast
```

两个对象松耦合，可以流畅交互，但又不太清楚彼此的细节，所以互不干扰。

观察者模式，就是让主题和观察者之间松耦合，只要遵循各自的接口，就互不影响：

- 主题对象只知道观察者对象遵循“订阅协议”（实现了Observer接口），而不知道他是谁、有什么、做了什么（具体的类、字段、方法）。
- 主题对象只需要一个对象列表，所以可以动态**增、删、改**观察者对象
- 改变主题对象，也不会影响观察者对象（除了必要的通知和更新）。

##### **重新实现气象站工程：**

```java
import java.util.ArrayList;
interface Observer {
    public void update(float temperature, float humidity, float pressure); // 主题调用这个接口，参数就是传递给观察者的数据
}

interface DisplayElement { // 这个更适合写成抽象类。
    public void display();
}

class CurrentConditionDisplay implements Observer, DisplayElement {
    private float temperature;
    private float humidity;
    private Subject weatherData;
    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData = weatherData; // 在一个主题对象里注册自己
        weatherData.registerObserver(this);
    }
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature; // 当update()被调用时，就更新自己的数据
        this.humidity = humidity;
        display(); // 同时刷新自己的展示板
    }
    public void display() { //展示
        System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");
    }
    public void trunDown() { // 在主题对象里，注销自己
        weatherData.removeObserver(this);
    }
}


public interface Subject {
    public void registerObserver(Observer ob); // 传入观察者变量，注册 观察者
    public void removeObserver(Observer ob); // 传入观察者变量，注销 观察者
    public void notifyObservers(); //主题状态有变，就调用这个方法，通知所有的观察者
}

class WeatherData implements Subject {
    private ArrayList observers; // 用列表记录 已注册的观察者
    private float temperature;
    private float humidity;
    private float pressure;
    public WeatherData() {
        observers = new ArrayList(); // 构造器里建立观察者列表
    }
    public void registerObserver(Observer ob) { // 注册的实现：增加列表项
        observers.add(ob);
    }
    public void removeObserver(Observer ob) {// 注销的实现：删除列表项
        int i = observers.indexOf(ob);
        if (i >= 0) {
            observers.remove(i);
        }
    }
    public void notifyObservers() { // 通知每一个观察者
        for (int i = 0; i < observers.size(); i++) { //实现：遍历列表
            Observer ob = (Observer)observers.get(i); // 给每个观察者对象发送数据
            ob.update(temperature, humidity, pressure);// 发送数据：调用update()
        }
    }
    public void measurementsChanged() { // 一旦数据更新，就通知观察者
        notifyObservers();
    }
    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature; // 获取数据（这里是主动设置数据）
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }
}

public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherData);
        weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
        currentDisplay.trunDown();
        weatherData.setMeasurements(5.0f, 4.0f, 3.4f);
    }
}
```

UML图：

```plantuml
interface Subject {
    registerObserver(Observer ob);
    removeObserver(Observer ob);
    notifyObservers();
}
class WeatherData implements Subject {
    private ArrayList observers;
    private float temperature,humidity,pressure;
    registerObserver(Observer ob){}
    removeObserver(Observer ob) {}
    notifyObservers(){}
    measurementsChanged(){}
    setMeasurements(){}
}
class WeatherStation {
    public static void main(String[] args) {}
}

interface Observer {
    update();
}
interface DisplayElement {
    display();
}
class CurrentDisplay implements Observer, DisplayElement {
    float temperature,humidity;
    private Subject weatherData;
    update(){}
    display(){}
    trunDown(){}
}
class StatisticsDisplay implements Observer, DisplayElement {
} 
class ForecastDisplay implements Observer, DisplayElement {
}
Observer --> WeatherData
WeatherStation -up- WeatherData
CurrentDisplay -- WeatherStation
StatisticsDisplay -- WeatherStation
ForecastDisplay -- WeatherStation
```

##### 深入探讨观察者模式：Push & Pull

其实观察者模式有2种传递数据的方式：push由主题对象推送数据、pull由观察者对象提取数据。

|              | 优点                         | 缺点                                       |
| ------------ | -------------------------- | ---------------------------------------- |
| **主题对象推送数据** | 主动推送，数据更新非常及时              | 有时候，观察者并不需要数据更新，造成干扰                     |
|              | 能一次性将数据更新，而不是一个数据项调用一次获取方法 | 有时候，观察者不需要全部数据，只需要部分数据。如果主题要扩展，一次性更新更多的数据项，那么所有的观察者都要改update()方法 |
| **观察者提取数据**  | 主动提取，观察者可以自由执行自己内部的操作      | 不知道究竟何时才能更新。如果频繁询问，过度占用主题接口（特别是观察者特别多的系统）；如果询问太少，会错过更新 |
|              | 只获取需要的数据项，避免不需要的数据         | 如果需要的又多又繁，则必须写一堆的getxxx()特别繁琐。           |

一般来说，Push的方式更常见。

其实JAVA 内置了观察者模式。

```java
import java.util.Observable; import java.util.Observer; // 都在这个包里
public interface Observer { // 观察者，是一个接口，只有一个方法
    void update(Observable o, Object arg); // arg: argument passed to the notifyObservers()
}
public class Observable { //主题。是一个类：不能多继承。有些接口是protected：只能继承，不能组合。
    private boolean changed = false; //数据是否更新的 flag
    private Vector<Observer> obs; // 观察者列表
    public Observable() {obs = new Vector<>();}
    public synchronized void addObserver(Observer o) {}
    public synchronized void deleteObserver(Observer o) {}
    public synchronized void deleteObservers() {} // 清空所有的观察者
    public synchronized int countObservers() {}
    public void notifyObservers() {}
    protected synchronized void setChanged() { changed = true; } // 设置数据更新flag
    protected synchronized void clearChanged() { changed = false; }
    public synchronized boolean hasChanged() {} // 得到是否已更新的flag
    public void notifyObservers(Object arg) {
        if (!changed)
            return; // 没有数据更新，就不执行任何操作
        clearChanged(); // 清除更新 flag
        for (int i = obs.length-1; i>=0; i--) // 遍历观察者列表
            ((Observer)obs[i]).update(this, arg); // 每个观察者调用update()
    }    
}
```

主题的实现`Observable` ，是一个类，不能多继承；有些接口是protected：只能继承，不能组合。所以有很多限制，可以自己写一个，去替代它。

设立`changed`标志位，并且有`setChanged/clearChanged/hasChanged` 相关方法，可以建立更灵活的通知机制：

- 以气象站为例，感受器的数据几乎每秒都有微弱的数据更新，如果每次微弱更新都通知，就太频繁而且没有必要。可以设立一个尺度，比如大于半度的更新，才调用`setChanged()`从而通知观察者。

- 如果是单纯的pull方式，观察者主动从主题对象获取数据，那么先调用`hasChanged()`，看看是否有重大数据更新，会更适合。

- 一般使用混合模式：主题有重大更新，才调用`setChanged()`——给观察者发送通知——观察者提取数据。以此重新实现气象站：

  ```java
  import java.util.Observer; import java.util.Observable; // 导入
  class WeatherData_P extends Observable {
      private float temperature, humidity, pressure;
      // Observable已有： 构造方法里新建观察者列表；注册、注销、通知等方法。所以不需要实现这些了
      public void setMeasurements(float tmp, float hum, float pre) {
          this.temperature = tmp; this.humidity = hum; this.pressure = pre;
          measurementsChanged();
      }
      public void measurementsChanged() {
          setChanged(); // 不管什么通知，之前都必须设置标志位 changed = true
          notifyObservers(); // 这里用pull方式，所以不调用 notifyObservers(info)
      }
      // 使用pull 的方式，观察者调用这些方法获取数据
      public float getTemperature() { return temperature; }
      public float getHumidity() { return humidity; }
      public float getPressure() { return pressure; }
  }
  abstract class DisplayElement_P {
      protected float temperature, humidity, pressure;
      abstract public void display();
  }
  class CurrentDisplay_P extends DisplayElement_P implements Observer {
      Observable observable; // 与原来的差不多
      public CurrentDisplay_P(Observable observable) { // 与原来的差不多
          this.observable = observable; // 记住 主题对象
          observable.addObserver(this); // 在这个主题对象注册
      }
      public void update(Observable ob, Object arg) {
          if (ob instanceof WeatherData_P) {
              WeatherData_P weatherData = (WeatherData_P) ob;
              this.temperature = weatherData.getTemperature();
              this.humidity = weatherData.getHumidity();
          }
          display();
      }
      public void display() {
          System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");
      }
      public void trunDown() { observable.deleteObserver(this); }
  }
  public class WeatherStationPlus {
      public static void main(String[] args) {
          WeatherData_P weatherData = new WeatherData_P();
          CurrentDisplay_P CurrentDisplay_P = new CurrentDisplay_P(weatherData);
          weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
          CurrentDisplay_P.trunDown();
          weatherData.setMeasurements(5.0f, 4.0f, 3.4f);
      }
  }
  ```

#####  JDK UI 包的观察者模式

观察者模式被广泛运用：GUI、JavaBeans、RMI、MVC

Java 的UI包，JButton作为主题，而ActionListener作为倾听者。

```java
import javax.swing.*;    //  JButton;  JFrame;
import java.awt.event.*; //  ActionListener; ActionEvent;
import java.awt.BorderLayout;
public class SwingExample {
	JFrame frame;
	public static void main(String[] args) {
		SwingExample example = new SwingExample();
		example.go();
	}
	public void go() {
		frame = new JFrame();
		JButton button = new JButton("Should I do it?");
		// Without lambdas 这种内部类的方式，可以用lambda表达式取代
		button.addActionListener(new AngelListener());
		button.addActionListener(new DevilListener());
		// // With lambdas 用lambda表达式取代内部类
		// button.addActionListener(event -> System.out.println("Don't do it, you might regret it!"));
        // button.addActionListener(event -> System.out.println("Come on, do it!") );

        frame.getContentPane().add(BorderLayout.CENTER, button);
		// Set frame properties
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(BorderLayout.CENTER, button);
		frame.setSize(300,300);
		frame.setVisible(true);
	}
	/* Remove inner classes，use lambda expressions instead.如果用 lambda，就去掉内部类 */
	class AngelListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
            System.out.println("Don't do it, you might regret it!");
		}
	}
	class DevilListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			System.out.println("Come on, do it!");
		}
	}
}
```



### 装饰者模式

设计任务：给一个主营咖啡的饮品店设计订单系统，核心是：饮料种类、计价

##### 繁琐冗余的设计

- 类爆炸的设计：抽象类Beverage，第一次泛化为不同类别的咖啡，第二次加上不同的调料。UML只是示意部分

  ```plantuml
  abstract class Beverage {
  	protected String description = "抽象饮料类";
  	public String getDescription() {return description;}
  	public abstract double cost();
  }
  Beverage  <|-- "深度烘焙咖啡" DarkRoast
  DarkRoast : cost()
  Beverage  <|-- "均衡风味咖啡" Blend
  Blend : cost()
  Beverage  <|-- "无咖啡因 咖啡" Decaf
  Decaf : cost()
  Beverage  <|-- "意式咖啡" Espresso
  Espresso : cost()
  Blend  <|-- "加奶" Blend_Milk
  Blend_Milk : cost()
  Blend  <|-- "加巧克力" Blend_Mocha
  Blend_Mocha : cost()
  Blend  <|-- "加豆浆" Blend_Soy
  Blend_Soy : cost()
  Blend  <|-- "加奶泡" Blend_Foam
  Blend_Foam : cost()
  ```

  缺点：类太多了。而且一旦有任何变动，改动会很大、很复杂。几乎是指数级的维护难度：

  - 某种调料的价格有变化，那么所有的子类都要改变；
  - 如果新增一种调料，则必须增加很多子类。
  - 调料的配方很难修改，比如客户要双倍奶、双倍咖啡……怎么办？

- 改进：各种调料设布尔值flag，放在基类Beverage，加上相关的方法。父类计算「基本价格+调料价格」，各个子类则在此基础上，加上咖啡豆的价格，得到售价。

  ```plantuml
  abstract class Beverage {
      protected String description = "抽象饮料类";
      protected Boolean milk, mocha, soy, foam;
      {fild} 各种调料...
  	public String getDescription() {return description;}
      abstract public double cost();
      protected void setMilk(// 控制有没有牛奶);
      protected void hasMilk(// 计价时，看看有没有加奶);
      {method} // 其他各种方法...
  }
  Beverage  <|-- "深度烘焙咖啡" DarkRoast
  DarkRoast : cost()
  Beverage  <|-- "均衡风味咖啡" Blend
  Blend : cost()
  Beverage  <|-- "无咖啡因 咖啡" Decaf
  Decaf : cost()
  Beverage  <|-- "意式咖啡" Espresso
  Espresso : cost()
  ```

  这种设计虽然很不错，不是「类爆炸」和指数级维护量了。但是依然会有很大的问题：

  - 调料的价格改变，就必须改变Beverage。
  - 调料的种类改变，必须改变Beverage 和 某些子类。
  - 调料的配方仍然很难修改，比如客户要双倍奶、双倍咖啡……怎么办？
  - 如果开发新饮料，比如不适合加调料的绿茶，却仍然会继承Beverage的调料及method

##### 装饰者模式

装饰者模式动态地将责任附加到对象身上，提供了丰富的扩展。一些基本组件与一堆的装饰者类，它们都继承自同一个接口或者父类，然后用装饰者包裹基本组件。

- 继承是为了类型匹配：装饰者和被装饰者   继承自同一个基类，这就保证了它们的类型相同，对于使用者，无论包裹与否都能匹配类型。所以，可以任意次数包裹对象，也可以不作任何包裹，还可以在运行时动态地包裹对象。
- 组合是为了行为灵活：如果是继承，行为只能是从父类继承、覆盖父类。用组合，装饰者可以在被装饰者的行为前后增加自己的行为，也可能完全取代，从而达到特定的目的。

```plantuml
abstract class AbstractComponent {
    method_A();
    method_B();
}
class Component extends AbstractComponent {
   {field} 需要装饰的对象，就没有 wrappedObj
    method_A();
    method_B();
}
abstract class Decorator extends AbstractComponent {
    {method} 装饰者可以用一个抽象类统领
    method_A();
    method_B();
}
class Decorator_A extends Decorator {
    AbstractComponent wrappedObj 装饰者必需有这个变量;
    {field} new_data 扩展的数据
    method_A();
    method_B();
}
class Decorator_B extends Decorator {
    AbstractComponent wrappedObj;
    method_A();
    method_B();
    {method}new_method() 扩展的方法
}
```

卡布奇诺，是`Whip + Mocha +DarkRoast`一层层包裹起来的。算钱的时候，装饰者先委托被装饰对象计算出价钱，然后加上自己的价钱返回，一层层调用，一层层返回。

```plantuml
HowMuch -right-> Whip
state Whip {
  whip.cost -right-> Mocha
  state Mocha {
    mocha.cost -right-> DarkRost
    state DarkRost {
    darkRoast.cost -right-> [*]
    }
  }
}
```

JAVA代码：（搭配工厂模式，或者生成器模式，会有更好的实现）。

如果要把`DarkRoast Mocha Mocha Whip`改为`DarkRoast, double Mocha, Whip`，可以写一个最终装饰者PrettyPrint。如果让超类`getName()`返回的是一个ArrayList，那么字符串处理会更方便。

```java
abstract class Beverage {
    protected String name = "抽象饮料类";
	public String getName() { return name; }
    abstract public double cost();
}
class DarkRoast extends Beverage { // 深度烘焙咖啡
    public DarkRoast() { name = "DarkRoast"; }
    public double cost(){ return 1.99; }
}
class Blend extends Beverage { // 均衡风味咖啡
    public Blend() { name = "House Blend Coffee"; }
    public double cost(){ return 0.89; }
}
abstract class Decorator extends Beverage { // 装饰者基类
}
class Mocha extends Decorator {
    Beverage bev;
    public Mocha(Beverage b) { this.bev = b; }
    public String getName() { return bev.getName() + " Mocha"; }
    public double cost(){ return 5.0 + bev.cost(); }
}
class Whip extends Decorator {
    Beverage bev;
    public Whip(Beverage b) { this.bev = b; }
    public String getName() { return bev.getName() + " Whip"; }
    public double cost(){ return 5.0 + bev.cost(); }
}
public class StarBuzz {
    public static void main(String[] args) {
        Beverage bev = new Blend(); // 均衡口味的 黑咖啡
        System.out.println(bev.getName() + " $" + bev.cost());
        Beverage bev2 = new DarkRoast(); // 深度烘焙的黑咖啡
        bev2 = new Mocha(bev2); // 加 巧克力
        bev2 = new Mocha(bev2); // 加 巧克力
        bev2 = new Whip(bev2);  // 加 奶泡
        System.out.println(bev2.getName() + " $" + bev2.cost());
    }
}
```

##### 深入探讨装饰者模式：

包 `java.io`里面大部分都是装饰者模式的设计。

- 装饰者模式可以带来弹性的设计，缺点是会形成纷繁的小类，这让别人很难理解设计意图，代码不易读。
- 装饰者的包裹层可以无限层级，使用者很难进行拆解，同时调用层次太深也有性能损耗
- 如果使用者A依赖原型X，使用者B却需要变种X1，如果忽然采用装饰者类的解决方案，A很可能使用X1，并且能够成功运行，但是却在某个时候出现严重错误。

```plantuml
abstract class InputStream
InputStream "抽象类" <|-- "可被装饰" FileInputStream
InputStream <|-- "可被装饰" StringBufferInputStream
abstract class FilterInputStream
InputStream <|-- "抽象装饰器" FilterInputStream
InputStream <|-- "可被装饰" ByteArrayInputStream
FilterInputStream <|-- "装饰器" PushbackInputStream
FilterInputStream <|-- "装饰器" BufferedInputStream
FilterInputStream <|-- "装饰器" DataInputStream
FilterInputStream <|-- "装饰器" LineNumberInputStream
```

使用案例：将大写字母转为小写字母。继承，扩展read方法，就可以了。

```java
// LowerCaseInput.java
import java.io.*;
class LowerCaseInputStream extends FilterInputStream {
    public LowerCaseInputStream(InputStream in) { super(in); }
    public int read() throws IOException {
        int c = super.read();
        return (c == -1 ? c : Character.toLowerCase((char)c) );
    }
    public int read(byte[] b, int offset, int len) throws IOException {
        int result = super.read(b, offset, len);
        for (int i = offset; i < offset+result; i++) {
            b[i] = (byte) Character.toLowerCase((char)b[i]);
        }
        return result;
    }
}
/* 如果继承自 BufferedInputStream ，就可以这么写
class LowerCaseInputStream extends BufferedInputStream {
	....
		InputStream in = new LowerCaseInputStream(System.in);*/
public class LowerCaseInput {
    public static void main(String[] args) {
        int c;
        try {
            InputStream in = new LowerCaseInputStream(
                new BufferedInputStream(
                    System.in // new FileInputStream("abc.txt") 从文件读取
                    ));
            while ((c = in.read()) >= 0) { System.out.print((char) c); }
            in.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
```



### 工厂模式

### 单件模式

### 命令模式

### 适配器与外观模式

### 模板方法模式

### 迭代器与组合模式

### 状态模式

### 代理模式

### 复合模式















