[TOC]



## Head First Design Patterns

模式成长之路：简单的模式——组合模式——模式创造

4个基本object oriented概念：

- 抽象：将数据与操作结合。成员变量与成员方法、静态变量与静态方法、构造器
- 封装：访问关键字、作用域
- 继承：父类、子类
- 多态：组合、接口、抽象类、泛化

不管什么公司、什么工程，什么编程语言，在软件开发里，唯一不变的就是变更。不管软件当初设计多好，一段时间后，总是需要成长和改变，否则软件就会“死亡”。变更原因有：改变需求或新增功能；兼容新的数据库、硬件、软件；出现新技术；主动重构。

软件开发完成后的维护、变更，比软件开发完成前的创造，所花的时间会更多。所以，应该提高可维护性、可扩展性。

因此，知道OO概念，并且也熟悉语法，并不能写出好的面向对象程序。需要建立弹性的设计，提高复用性、扩展性、维护性，才可以应对各种变更。

要诀：随时都能预见系统以后可能需要的变化、应对变化的原则和方法。

大部分设计原则和设计模式，都是掌控这种变化，都是为了让系统的局部改变独立于其他部分，而把系统中会变化的部分抽出来封装。



9个OO原则Principle：P491

1. 封装变化：这是总纲，将「变化」与「不变」分开，让系统中的某部分发生改变不会影响其他部分。找出程序中可能需要变化的部分，独立出来，不要混在一起。如果每次有新需求，就会让某部分代码产生变化，那么这部分就抽取出来，和其他稳定的代码区分开来。一般来说，关键在于抽象，将一个功能的通用部分和实现细节部分清晰的分离开来。

2. 开放-关闭原则：对扩展开放，对修改关闭。允许代码容易扩展，不修改现有代码，只是通过新增代码来增加新的功能。通过业务分析，预测可能的变更，合理规划和抽象，建立弹性的设计架构，足以应付改变，才不容易引入新的bug。但是，不可能让每部分都遵循开放-关闭原则，这样会浪费很多时间，并且代码很复杂，不易读。

3. 黑盒原则：多用组合，少用继承。继承和组合语法都是一种扩展形式。继承时，子类的属性是编译时决定的，只能继承父类，或者自己覆盖。而组合+委托 可以在运行时实现和继承相同的效果，却更加有弹性，更灵活。这样可以动态扩展对象的行为，甚至不用修改原代码，就给代码添加了新的功能。

4. 针对接口编程，而不是针对实现编程。在java 中，是“使用多态语法，针对超类/接口编程”。如果代码使用大量的具体的类，一旦具体的类发生任何改变，都必须改变代码；而如果针对超类编程，通过多态语法就可以操作任何子类，从而隔离以后可能出现的变更。比如在策略模式使用  组合+接口（多态语法），非常灵活。

5. 依赖倒置原则：依赖抽象，而不是依赖具体。

   > 抽象不应该依赖于细节。细节应该依赖于抽象。程序中所有的依赖关系都应该终止于抽象类和接口。针对接口而非实现编程。任何变量都不应该持有一个指向具体类的指针或引用。任何类都不应该从具体类派生。任何方法都不应该覆写他的任何基类中的已经实现了的方法。

6. 迪米特法则：让交互对象松耦合。让对象之间的相互依赖降到最低，就能建立弹性的系统，能够应对变化。

   > 如果两个类不必彼此通信，那么这两个类就不应该发生直接的相互作用。如果其中一个类需要调用另一个类的某个方法，可以通过第三者转发这个调用。其本意是，设计中要注意松耦合。

7. 最少知识原则Least Knowledge：也叫德墨忒尔Demeter。只和你的密友谈话，减少对象之间的交互。不要让太多的类耦合在一起，否则修改系统中的一部分，就会影响其他部分。如果太多类之间相互依赖，这个系统就会脆弱易碎，难以维护，也难以理解。比如**外观模式**。

8. 好莱坞原则：别调用「我们」，「我们」来调用你。在**模板方法模式**。

9. 单一职责原则 ：就一个类而言，应该仅有一个引起它变化的原因。职责即为“变化的原因”。

>**理氏替换原则 **
>
>子类型必须能替换掉他们的基本类型。
>
>**接口隔离原则 **
>
>不应该强迫客户依赖于他们不用的方法。接口属于客户，不属于他所在的类层次结构。多个面向特定用户的接口胜于一个通用接口。
>
>**重用发布等价原则 **
>
>重用的粒度就是发布的粒度。
>
>**共同重用原则 **
>
>一个包中的所有类应该是共同重用的。如果重用了包中的一个类，那么就要重用包中的所有类。相互之间没有紧密联系的类不应该在同一个包中。
>
>**共同封闭原则 **
>
>包中的所有类对于同一类性质的变化应该是共同封闭的。一个变化若对一个包影响，则将对包中的所有类产生影响，而对其他的包不造成任何影响。
>
>**无依赖原则 **
>
>在包的依赖关系中不允许存在环。细节不应该被依赖。
>
>**稳定依赖原则 **
>
>朝着稳定的方向进行依赖。应该把封装系统高层设计的软件（比如抽象类）放进稳定的包中，不稳定的包中应该只包含那些很可能会改变的软件（比如具体类）。
>
>**稳定抽象原则 **
>
>包的抽象程度应该和其他稳定程度一致。一个稳定的包应该也是抽象的，一个不稳定的包应该是抽象的。
>
>**缺省抽象原则**
>
>在接口和实现接口的类之间引入一个抽象类,这个类实现了接口的大部分操作。
>
>**接口设计原则 **
>
>规划一个接口而不是实现一个接口。
>
>**不构造具体的超类原则 **
>
>避免维护具体的超类。

设计原则是目标，设计模式是途径，而OO概念与语法是具体实现。

各种语言的惯用法、函数与类、库、框架，是“代码复用”；设计模式，是层次更高的“经验复用”，是历经验证的OO设计经验，是针对某类设计问题的通用解决方案，以此可以构建具有良好设计质量的系统。

使用设计模式的专业词汇，有利于沟通。当然，不能过分使用，比如写个 Hello World 都使用模式……







### 策略模式 Strategy

任务：创建很多鸭子类，抽象类Duck，绿头的野鸭MallardDuck，红头的RedheadDuck， 不会飞、叫声不一样的模型鸭ModelDuck，橡皮鸭RubberDuck

##### 混乱的实现：

这设计现在看来也还行：父类规划了3个方法，抽象方法`display()`由子类实现，而其他方法让子类定制，比如ModelDuck自己覆盖了`Quack()`。

```plantuml
@startuml
abstract class Duck {
    + void Quack()
    + void swim()
    + {abstract}abstract void display()
}
class MallardDuck野鸭 extends Duck {
    + void display(){//绿头}
}
Duck "Super" <|-- "Sub" RedheadDuck红头鸭 : Generalization
class RedheadDuck红头鸭 {
    + void display(){//红头}
}
class ModelDuck模型鸭 extends Duck {
    + void display(){//模型鸭}
    + void Quack(){//叫声不同}
}
@enduml
```
如果来了新需求：1. 要增加一个`fly()`方法，但是有些鸭子不会飞。2. 增加一个橡皮鸭`RubberDuck`类，这种鸭子不会飞也不会叫。

设计父类的时候，很难知道所有子类的情况。如果一味使用继承，会导致：

- 不继承的代码，会在多个子类中重复。比如`ModelDuck`和`RubberDuck`都要覆盖父类的`Quack()`方法，如果子类更多，问题就更严重
- 继承的代码，父类改变，子类全部被动改变，或许子类并不需要。
- 只能使用类中绑定的方法。相对于策略模式，运行时不能动态改变方法。

可以看出`fly() quack()`两个方法会随着鸭子种类的不同而改变，就提取出来（封装变化原则）。那么，究竟怎样实现这两个行为呢？

##### 策略模式，算法族

策略模式，就是定义算法族，分门别类封装起来，让一个族内的算法可以互相替换，这样，算法的变化就独立于算法的用户。

将这2种放在分开的类中，并且使用2个接口统领（原则：针对接口编程，而不是针对实现编程）。

```plantuml
abstract class Duck {
    QuackBehavior quackBehavior
    FlyBehavior flyBehavior
    + Duck()
    + {abstract}abstract void display()
    + void performQuack()
    + void performFly()
    + void setFlyBehavior()
    + void setQuackBehavior()
    + void swim()
}
class MallardDuck extends Duck{
    + MallardDuck ()
    + void display()
}
class RedheadDuck extends Duck {
    + void display() {}
}
class RubberDuck extends Duck {
    + ModelDuck()
    + void display()
}
class ModelDuck extends Duck {
    + ModelDuck()
    + void display()
}
Duck -left-* FlyBehavior
Duck -right-* QuackBehavior

interface FlyBehavior {
    + void fly()
}
class FlyWithWings  {
    + void fly()
}
class FlyNoWay  {
    + void fly()
}
class FlyRocketPowered {
    + void fly()
}
FlyRocketPowered .down.|> FlyBehavior
FlyNoWay .down.|> FlyBehavior
FlyWithWings .down.|> FlyBehavior

interface QuackBehavior {
    + void quack();
}
class Quack {
    + void quack()
}
class MuteQuack {
    + void quack()
}
class Squeak {
    + void quack()
}
Quack .down.|> QuackBehavior
MuteQuack .down.|> QuackBehavior
Squeak .down.|> QuackBehavior
```

将不变的部分，留在Duck类族里；而变化的那些方法，抽取出来成为算法族。不一定是开发后才知道需要分离，有经验的开发者，在设计时就会预先考虑到这种变化，于是在代码中预先加入这些弹性。将不变的设计为继承的类族，而经常改变的设计为接口，并且使用接口QuackBehavior而不是使用具体的子类Quack（依赖接口原则）。

将FlyBehavior组合Composition为Duck，而Duck将fly委托Delegation给FlyBehavior。“依赖于接口”，是用**父类+多态**语法实现的，这样就可以在运行时动态切换。

```java
FlyBehavior flyBehavior = new FlyNoWay();//声明为接口类型，而不是子类。有时这里会用getFly()的方式
flyBehavior.fly(); // 父类泛化为子类，运行时才知道究竟是哪个子类
flyBehavior = new FlyRocketPowered(); // 运行时动态切换
flyBehavior.fly(); // 切换成功
```



### 观察者模式 Observer

设计任务：建立一个气象站。WeatherData数据类负责从各种传感器生成数据，汇总、发送。DisplayElement展示板类（现状、统计、预测）负责展示数据。WeatherStation类是主入口，运行整个程序。

##### 高度耦合的实现

如果不进行设计，随便一写，是以下实现：

```java
abstract class DisplayElement_A {
    float temperature, humidity, pressure;
    abstract public void update();
    abstract public void display();
}
class CurrentDisplay_A extends DisplayElement_A { // 会有其他方法和数据。其他展示板差不多也这样
    public void update(float temperature, float humidity, float pressure){
        this.temperature = temperature;  this.humidity = humidity;
    }
    public void display() { System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");}
}
class StatisticsDisplay_A extends DisplayElement_A { // 最大、最小、平均值
}
class ForecastDisplay_A extends DisplayElement_A { //根据气压显示预报
}

class WeatherData_A {
    float temperature, humidity, pressure;
    DisplayElement_A currentDisplay, statisticsDisplay, forecastDisplay;
    public void measurementChanged() {
        this.temperature=getTmp();  //取最新的数据
        this.humidity=getHum(); this.pressure=getPre();
        currentDisplay.update(temperature, humidity, pressure);//取数据后，依次更新各个展示板
        statisticsDisplay.update(temperature, humidity, pressure);
        forecastDisplay.update(temperature, humidity, pressure);
    }
}
public class WeatherStation_A {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentDisplay currentDisplay = new CurrentDisplay;
        weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
        weatherData.update();
        currentDisplay.display();// 依次展示数据
    }
}
```

上面的实现，有巨大的缺陷：每次新增一个展示板，都必须更改WeatherData、WeatherStation（差不多就是整个工程）；另外，无法在运行时动态地增加、删除展示板。

违反的设计原则是：针对具体实现编程，而不是针对接口编程；没有将变化封装。

```plantuml
abstract class DisplayElement_A {
    float temperature, humidity, pressure;
    {abstract} update(float temperature, float humidity, float pressure){}
    {abstract} display();
}
class CurrentDisplay_A extends DisplayElement_A {
    update()
    display()
}
class StatisticsDisplay_A extends DisplayElement_A {
    update()
    display()
}
class ForecastDisplay_A extends DisplayElement_A {
    update()
    display()
}

class WeatherData_A {
    float temperature, humidity, pressure;
    DisplayElement_A currentDisplay;
    DisplayElement_A statisticsDisplay;
    DisplayElement_A forecastDisplay;
    measurementChanged(){}
    {method}//取得数据，然后依次更新各展示板
}
class WeatherStation_A {
    main(String[] args) {}
}
WeatherData_A -left-> DisplayElement_A
WeatherStation_A -down-> WeatherData_A
WeatherStation_A -down-> DisplayElement_A
```



##### 观察者模式，建立松耦合的一对多关系

这个模式中，会改变的是：主题的状态，观察者的数目和类型。

使用接口编程，而不是使用实现编程，松耦合的关系：观察者利用主题的接口进行注册或注销，主题利用观察者的接口进行通知。

使用组合：在运行时，动态地将观察者装入观察者体内。

| 媒体与用户（比如报社与订阅者）    | 观察者模式                  |
| ------------------ | ---------------------- |
| 报社：出版发行报纸          | 主题对象：管理数据              |
| 如果订阅了报纸，有新报就会递送给用户 | 数据一旦改变，就会通知观察者，传递数据    |
| 未订阅、取消订阅的用户，不会递送   | 未注册、取消注册的观察者，不会通知、传递数据 |

观察者模式定义了对象之间**一对多**依赖关系，当一个对象改变状态时，它的所有依赖者都会收到**通知并自动更新**。

主题对象：具有数据，并且控制数据的一个对象。由一个对象控制数据，比让多个对象控制数据，要更干净、易于掌控。

观察者对象：使用主题对象的数据、依赖主题对象，多个对象。

主题接口，一般都有`registerObserver/removeObserver/notifyObserver` 3类函数，注册、撤销注册、通知或更新观察者。而接口继承者，某个具体的主题类，则有自己的数据、设置数据和从数据源获取数据的方法。

观察者接口，一般都有`update`函数，与主题类交互，以更新自己的数据。继承的、具体的某个观察者类，必须注册某个具体主题类，以便接收数据。

UML如下：

```plantuml
interface Subject {
    registerObserver();
    removeObserver();
    notifyObserver();
}
class Broadcast  implements Subject {
    registerObserver(){}
    removeObserver(){}
    notifyObserver(){}
    --
    getState(){}
    setState(){}
}
interface Observer {
    update()
}
class Listener_A implements Observer {
    update(){}
}
class Listener_B implements Observer {
    update(){}
}
class Listener_* implements Observer {
    update(){}
}
Observer --> Broadcast
```

两个对象松耦合，可以流畅交互，但又不太清楚彼此的细节，所以互不干扰。

观察者模式，就是让主题和观察者之间松耦合，只要遵循各自的接口，就互不影响：

- 主题对象只知道观察者对象遵循“订阅协议”（实现了Observer接口），而不知道他是谁、有什么、做了什么（具体的类、字段、方法）。
- 主题对象只需要一个对象列表，所以可以动态**增、删、改**观察者对象
- 改变主题对象，也不会影响观察者对象（除了必要的通知和更新）。

**重新实现气象站工程：**

```java
import java.util.ArrayList;
interface Observer {
    public void update(float temperature, float humidity, float pressure); // 主题调用这个接口，参数就是传递给观察者的数据
}

interface DisplayElement { // 这个更适合写成抽象类。
    public void display();
}

class CurrentConditionDisplay implements Observer, DisplayElement {
    private float temperature;
    private float humidity;
    private Subject weatherData;
    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData = weatherData; // 在一个主题对象里注册自己
        weatherData.registerObserver(this);
    }
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature; // 当update()被调用时，就更新自己的数据
        this.humidity = humidity;
        display(); // 同时刷新自己的展示板
    }
    public void display() { //展示
        System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");
    }
    public void trunDown() { // 在主题对象里，注销自己
        weatherData.removeObserver(this);
    }
}


public interface Subject {
    public void registerObserver(Observer ob); // 传入观察者变量，注册 观察者
    public void removeObserver(Observer ob); // 传入观察者变量，注销 观察者
    public void notifyObservers(); //主题状态有变，就调用这个方法，通知所有的观察者
}

class WeatherData implements Subject {
    private ArrayList observers; // 用列表记录 已注册的观察者
    private float temperature;
    private float humidity;
    private float pressure;
    public WeatherData() {
        observers = new ArrayList(); // 构造器里建立观察者列表
    }
    public void registerObserver(Observer ob) { // 注册的实现：增加列表项
        observers.add(ob);
    }
    public void removeObserver(Observer ob) {// 注销的实现：删除列表项
        int i = observers.indexOf(ob);
        if (i >= 0) {
            observers.remove(i);
        }
    }
    public void notifyObservers() { // 通知每一个观察者
        for (int i = 0; i < observers.size(); i++) { //实现：遍历列表
            Observer ob = (Observer)observers.get(i); // 给每个观察者对象发送数据
            ob.update(temperature, humidity, pressure);// 发送数据：调用update()
        }
    }
    public void measurementsChanged() { // 一旦数据更新，就通知观察者
        notifyObservers();
    }
    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature; // 获取数据（这里是主动设置数据）
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }
}

public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherData);
        weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
        currentDisplay.trunDown();
        weatherData.setMeasurements(5.0f, 4.0f, 3.4f);
    }
}
```

UML图：

```plantuml
interface Subject {
    registerObserver(Observer ob);
    removeObserver(Observer ob);
    notifyObservers();
}
class WeatherData implements Subject {
    private ArrayList observers;
    private float temperature,humidity,pressure;
    registerObserver(Observer ob){}
    removeObserver(Observer ob) {}
    notifyObservers(){}
    measurementsChanged(){}
    setMeasurements(){}
}
class WeatherStation {
    public static void main(String[] args) {}
}

interface Observer {
    update();
}
interface DisplayElement {
    display();
}
class CurrentDisplay implements Observer, DisplayElement {
    float temperature,humidity;
    private Subject weatherData;
    update(){}
    display(){}
    trunDown(){}
}
class StatisticsDisplay implements Observer, DisplayElement {
}
class ForecastDisplay implements Observer, DisplayElement {
}
Observer --> WeatherData
WeatherStation -up- WeatherData
CurrentDisplay -- WeatherStation
StatisticsDisplay -- WeatherStation
ForecastDisplay -- WeatherStation
```

##### 深入探讨 Push & Pull

其实观察者模式有2种传递数据的方式：push由主题对象推送数据、pull由观察者对象提取数据。

|              | 优点                         | 缺点                                       |
| ------------ | -------------------------- | ---------------------------------------- |
| **主题对象推送数据** | 主动推送，数据更新非常及时              | 有时候，观察者并不需要数据更新，造成干扰                     |
|              | 能一次性将数据更新，而不是一个数据项调用一次获取方法 | 有时候，观察者不需要全部数据，只需要部分数据。如果主题要扩展，一次性更新更多的数据项，那么所有的观察者都要改update()方法 |
| **观察者提取数据**  | 主动提取，观察者可以自由执行自己内部的操作      | 不知道究竟何时才能更新。如果频繁询问，过度占用主题接口（特别是观察者特别多的系统）；如果询问太少，会错过更新 |
|              | 只获取需要的数据项，避免不需要的数据         | 如果需要的又多又繁，则必须写一堆的getxxx()特别繁琐。           |

一般来说，Push的方式更常见。

其实JAVA 内置了观察者模式。

```java
import java.util.Observable; import java.util.Observer; // 都在这个包里
public interface Observer { // 观察者，是一个接口，只有一个方法
    void update(Observable o, Object arg); // arg: argument passed to the notifyObservers()
}
public class Observable { //主题。是一个类：不能多继承。有些接口是protected：只能继承，不能组合。
    private boolean changed = false; //数据是否更新的 flag
    private Vector<Observer> obs; // 观察者列表
    public Observable() {obs = new Vector<>();}
    public synchronized void addObserver(Observer o) {}
    public synchronized void deleteObserver(Observer o) {}
    public synchronized void deleteObservers() {} // 清空所有的观察者
    public synchronized int countObservers() {}
    public void notifyObservers() {}
    protected synchronized void setChanged() { changed = true; } // 设置数据更新flag
    protected synchronized void clearChanged() { changed = false; }
    public synchronized boolean hasChanged() {} // 得到是否已更新的flag
    public void notifyObservers(Object arg) {
        if (!changed)
            return; // 没有数据更新，就不执行任何操作
        clearChanged(); // 清除更新 flag
        for (int i = obs.length-1; i>=0; i--) // 遍历观察者列表
            ((Observer)obs[i]).update(this, arg); // 每个观察者调用update()
    }
}
```

主题的实现`Observable` ，是一个类，不能多继承；有些接口是protected：只能继承，不能组合。所以有很多限制，可以自己写一个，去替代它。

设立`changed`标志位，并且有`setChanged/clearChanged/hasChanged` 相关方法，可以建立更灵活的通知机制：

- 以气象站为例，感受器的数据几乎每秒都有微弱的数据更新，如果每次微弱更新都通知，就太频繁而且没有必要。可以设立一个尺度，比如大于半度的更新，才调用`setChanged()`从而通知观察者。

- 如果是单纯的pull方式，观察者主动从主题对象获取数据，那么先调用`hasChanged()`，看看是否有重大数据更新，会更适合。

- 一般使用混合模式：主题有重大更新，才调用`setChanged()`——给观察者发送通知——观察者提取数据。以此重新实现气象站：

  ```java
  import java.util.Observer; import java.util.Observable; // 导入
  class WeatherData_P extends Observable {
      private float temperature, humidity, pressure;
      // Observable已有： 构造方法里新建观察者列表；注册、注销、通知等方法。所以不需要实现这些了
      public void setMeasurements(float tmp, float hum, float pre) {
          this.temperature = tmp; this.humidity = hum; this.pressure = pre;
          measurementsChanged();
      }
      public void measurementsChanged() {
          setChanged(); // 不管什么通知，之前都必须设置标志位 changed = true
          notifyObservers(); // 这里用pull方式，所以不调用 notifyObservers(info)
      }
      // 使用pull 的方式，观察者调用这些方法获取数据
      public float getTemperature() { return temperature; }
      public float getHumidity() { return humidity; }
      public float getPressure() { return pressure; }
  }
  abstract class DisplayElement_P {
      protected float temperature, humidity, pressure;
      abstract public void display();
  }
  class CurrentDisplay_P extends DisplayElement_P implements Observer {
      Observable observable; // 与原来的差不多
      public CurrentDisplay_P(Observable observable) { // 与原来的差不多
          this.observable = observable; // 记住 主题对象
          observable.addObserver(this); // 在这个主题对象注册
      }
      public void update(Observable ob, Object arg) {
          if (ob instanceof WeatherData_P) {
              WeatherData_P weatherData = (WeatherData_P) ob;
              this.temperature = weatherData.getTemperature();
              this.humidity = weatherData.getHumidity();
          }
          display();
      }
      public void display() {
          System.out.println("Current Condition: " + temperature + "F, " + humidity + "% humidity");
      }
      public void trunDown() { observable.deleteObserver(this); }
  }
  public class WeatherStationPlus {
      public static void main(String[] args) {
          WeatherData_P weatherData = new WeatherData_P();
          CurrentDisplay_P CurrentDisplay_P = new CurrentDisplay_P(weatherData);
          weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
          CurrentDisplay_P.trunDown();
          weatherData.setMeasurements(5.0f, 4.0f, 3.4f);
      }
  }
  ```

#####  JDK UI 包的观察者模式

观察者模式被广泛运用：GUI、JavaBeans、RMI、MVC

Java 的UI包，JButton作为主题，而ActionListener作为倾听者。

```java
import javax.swing.*;    //  JButton;  JFrame;
import java.awt.event.*; //  ActionListener; ActionEvent;
import java.awt.BorderLayout;
public class SwingExample {
	JFrame frame;
	public static void main(String[] args) {
		SwingExample example = new SwingExample();
		example.go();
	}
	public void go() {
		frame = new JFrame();
		JButton button = new JButton("Should I do it?");
		// Without lambdas 这种内部类的方式，可以用lambda表达式取代
		button.addActionListener(new AngelListener());
		button.addActionListener(new DevilListener());
		// // With lambdas 用lambda表达式取代内部类
		// button.addActionListener(event -> System.out.println("Don't do it, you might regret it!"));
        // button.addActionListener(event -> System.out.println("Come on, do it!") );

        frame.getContentPane().add(BorderLayout.CENTER, button);
		// Set frame properties
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(BorderLayout.CENTER, button);
		frame.setSize(300,300);
		frame.setVisible(true);
	}
	/* Remove inner classes，use lambda expressions instead.如果用 lambda，就去掉内部类 */
	class AngelListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
            System.out.println("Don't do it, you might regret it!");
		}
	}
	class DevilListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			System.out.println("Come on, do it!");
		}
	}
}
```



### 装饰者模式 Decorator

设计任务：给一个主营咖啡的饮品店设计订单系统，核心是：饮料种类、计价

##### 繁琐冗余的实现

- 类爆炸的设计：抽象类Beverage，第一次泛化为不同类别的咖啡，第二次加上不同的调料。UML只是示意部分

  ```plantuml
  abstract class Beverage {
  	protected String description = "抽象饮料类";
  	public String getDescription() {return description;}
  	public abstract double cost();
  }
  Beverage  <|-- "深度烘焙咖啡" DarkRoast
  DarkRoast : cost()
  Beverage  <|-- "均衡风味咖啡" Blend
  Blend : cost()
  Beverage  <|-- "无咖啡因 咖啡" Decaf
  Decaf : cost()
  Beverage  <|-- "意式咖啡" Espresso
  Espresso : cost()
  Blend  <|-- "加奶" Blend_Milk
  Blend_Milk : cost()
  Blend  <|-- "加巧克力" Blend_Mocha
  Blend_Mocha : cost()
  Blend  <|-- "加豆浆" Blend_Soy
  Blend_Soy : cost()
  Blend  <|-- "加奶泡" Blend_Foam
  Blend_Foam : cost()
  ```

  缺点：类太多了。而且一旦有任何变动，改动会很大、很复杂。几乎是指数级的维护难度：

  - 某种调料的价格有变化，那么所有的子类都要改变；
  - 如果新增一种调料，则必须增加很多子类。
  - 调料的配方很难修改，比如客户要双倍奶、双倍咖啡……怎么办？

- 改进：各种调料设布尔值flag，放在基类Beverage，加上相关的方法。父类计算「基本价格+调料价格」，各个子类则在此基础上，加上咖啡豆的价格，得到售价。

  ```plantuml
  abstract class Beverage {
      protected String description = "抽象饮料类";
      protected Boolean milk, mocha, soy, foam;
      {fild} 各种调料...
  	public String getDescription() {return description;}
      abstract public double cost();
      protected void setMilk(// 控制有没有牛奶);
      protected void hasMilk(// 计价时，看看有没有加奶);
      {method} // 其他各种方法...
  }
  Beverage  <|-- "深度烘焙咖啡" DarkRoast
  DarkRoast : cost()
  Beverage  <|-- "均衡风味咖啡" Blend
  Blend : cost()
  Beverage  <|-- "无咖啡因 咖啡" Decaf
  Decaf : cost()
  Beverage  <|-- "意式咖啡" Espresso
  Espresso : cost()
  ```

  这种设计虽然很不错，不是「类爆炸」和指数级维护量了。但是依然会有很大的问题：

  - 调料的价格改变，就必须改变Beverage。
  - 调料的种类改变，必须改变Beverage 和 某些子类。
  - 调料的配方仍然很难修改，比如客户要双倍奶、双倍咖啡……怎么办？
  - 如果开发新饮料，比如不适合加调料的绿茶，却仍然会继承Beverage的调料及method

##### 装饰者模式

装饰者模式动态地将责任附加到对象身上，提供了丰富的扩展。一些基本组件与一堆的装饰者类，它们都继承自同一个接口或者父类，然后用装饰者包裹基本组件。

- 继承是为了类型匹配：装饰者和被装饰者   继承自同一个基类，这就保证了它们的类型相同，对于使用者，无论包裹与否都能匹配类型。所以，可以任意次数包裹对象，也可以不作任何包裹，还可以在运行时动态地包裹对象。
- 组合是为了行为灵活：如果是继承，行为只能是从父类继承、覆盖父类。用组合，装饰者可以在被装饰者的行为前后增加自己的行为，也可能完全取代，从而达到特定的目的。

```plantuml
abstract class AbstractComponent {
    method_A();
    method_B();
}
class Component extends AbstractComponent {
   {field} 需要装饰的对象，就没有 wrappedObj
    method_A();
    method_B();
}
abstract class Decorator extends AbstractComponent {
    {method} 装饰者可以用一个抽象类统领
    method_A();
    method_B();
}
class Decorator_A extends Decorator {
    AbstractComponent wrappedObj 装饰者必需有这个变量;
    {field} new_data 扩展的数据
    method_A();
    method_B();
}
class Decorator_B extends Decorator {
    AbstractComponent wrappedObj;
    method_A();
    method_B();
    {method}new_method() 扩展的方法
}
```

卡布奇诺，是`Whip + Mocha +DarkRoast`一层层包裹起来的。算钱的时候，装饰者先委托被装饰对象计算出价钱，然后加上自己的价钱返回，一层层调用，一层层返回。

```plantuml
HowMuch -right-> Whip
state Whip {
  whip.cost -right-> Mocha
  state Mocha {
    mocha.cost -right-> DarkRost
    state DarkRost {
    darkRoast.cost -right-> [*]
    }
  }
}
```

JAVA代码：（搭配工厂模式，或者生成器模式，会有更好的实现）。

如果要把`DarkRoast Mocha Mocha Whip`改为`DarkRoast, double Mocha, Whip`，可以写一个最终装饰者PrettyPrint。如果让超类`getName()`返回的是一个ArrayList，那么字符串处理会更方便。

```java
abstract class Beverage {
    protected String name = "抽象饮料类";
	public String getName() { return name; }
    abstract public double cost();
}
class DarkRoast extends Beverage { // 深度烘焙咖啡
    public DarkRoast() { name = "DarkRoast"; }
    public double cost(){ return 1.99; }
}
class Blend extends Beverage { // 均衡风味咖啡
    public Blend() { name = "House Blend Coffee"; }
    public double cost(){ return 0.89; }
}
abstract class Decorator extends Beverage { // 装饰者基类
}
class Mocha extends Decorator {
    Beverage bev;
    public Mocha(Beverage b) { this.bev = b; }
    public String getName() { return bev.getName() + " Mocha"; }
    public double cost(){ return 5.0 + bev.cost(); }
}
class Whip extends Decorator {
    Beverage bev;
    public Whip(Beverage b) { this.bev = b; }
    public String getName() { return bev.getName() + " Whip"; }
    public double cost(){ return 5.0 + bev.cost(); }
}
public class StarBuzz {
    public static void main(String[] args) {
        Beverage bev = new Blend(); // 均衡口味的 黑咖啡
        System.out.println(bev.getName() + " $" + bev.cost());
        Beverage bev2 = new DarkRoast(); // 深度烘焙的黑咖啡
        bev2 = new Mocha(bev2); // 加 巧克力
        bev2 = new Mocha(bev2); // 加 巧克力
        bev2 = new Whip(bev2);  // 加 奶泡
        System.out.println(bev2.getName() + " $" + bev2.cost());
    }
}
```

##### 深入探讨装饰者模式：

包 `java.io`里面大部分都是装饰者模式的设计。

- 装饰者模式可以带来弹性的设计，缺点是会形成纷繁的小类，这让别人很难理解设计意图，代码不易读。
- 装饰者的包裹层可以无限层级，使用者很难进行拆解，同时调用层次太深也有性能损耗
- 如果使用者A依赖原型X，使用者B却需要变种X1，如果忽然采用装饰者类的解决方案，A很可能使用X1，并且能够成功运行，但是却在某个时候出现严重错误。

```plantuml
@startuml
abstract class InputStream
InputStream "抽象类" <|-- "可被装饰" FileInputStream
InputStream <|-- "可被装饰" StringBufferInputStream
abstract class FilterInputStream
InputStream <|-- "抽象装饰器" FilterInputStream
InputStream <|-- "可被装饰" ByteArrayInputStream
FilterInputStream <|-- "装饰器" PushbackInputStream
FilterInputStream <|-- "装饰器" BufferedInputStream
FilterInputStream <|-- "装饰器" DataInputStream
FilterInputStream <|-- "装饰器" LineNumberInputStream
@enduml
```

使用案例：将大写字母转为小写字母。继承，扩展read方法，就可以了。

```java
// LowerCaseInput.java
import java.io.*;
class LowerCaseInputStream extends FilterInputStream {
    public LowerCaseInputStream(InputStream in) { super(in); }
    public int read() throws IOException {
        int c = super.read();
        return (c == -1 ? c : Character.toLowerCase((char)c) );
    }
    public int read(byte[] b, int offset, int len) throws IOException {
        int result = super.read(b, offset, len);
        for (int i = offset; i < offset+result; i++) {
            b[i] = (byte) Character.toLowerCase((char)b[i]);
        }
        return result;
    }
}
/* 如果继承自 BufferedInputStream ，就可以这么写
class LowerCaseInputStream extends BufferedInputStream {
	....
		InputStream in = new LowerCaseInputStream(System.in);*/
public class LowerCaseInput {
    public static void main(String[] args) {
        int c;
        try {
            InputStream in = new LowerCaseInputStream(
                new BufferedInputStream(
                    System.in // new FileInputStream("abc.txt") 从文件读取
                    ));
            while ((c = in.read()) >= 0) { System.out.print((char) c); }
            in.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
```



### 工厂模式 Factory

> 工厂模式：将对象的创建过程封装起来，以便将代码从具体类解耦。各种工厂模式，都减少了应用代码与具体类直接的依赖，从而得到松耦合、更弹性的设计；都符合依赖倒置原则、针对抽象而不是针对具体原则。

任务：构建一个Pizza店

##### 高度耦合的实现

```java
class PizzaStore {
    Pizza orderPizza(String flavour) {
        Pizza pizza = null; // Pizza类有很多子类。按照口味，实例化不同的子类
        if (flavour.equals("cheese")) { pizza = new CheesePizza();
        } else if (flavour.equals("greek")) { pizza = new GreekPizza();
        } else if (flavour.equals("pepperoni")) { pizza = new PepperroniPizza();
        }
        pizza.prepare(); // 每种Pizza都有面粉、烘烤、切开、装盒操作
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
```

最近有些新口味非常流行，新增ClamPizza生蚝比萨、VeggiePizza素食比萨；而有些旧口味客户量太少，减去GreekPizza希腊比萨。那么，那些 `if () {} else` 都必须改变。

实例化对象如果总是用`Pizza p = new Pizza();`这种公开的方式进行，会造成耦合，让代码更脆弱，更缺乏弹性。这是针对实现编程，而不是针对接口编程。new操作符并不是问题，new操作符的使用方式，以及变更才是问题。

一旦有变化或者扩展，就必须重新打开这段代码进行检查和修改。通常这样修改过的代码将会造成部分系统更难维护和更新，而且容易犯错。很容易地，我们会想到把`if () {pizza = new CheesePizza();}`这种代码都放在一起，与其他代码分来：简单工厂。

##### 简单工厂类

简单工厂更像是惯用法而不是设计模式。

- 如果要增减产品Pizza的种类，仍然要修改工厂SimplePizzaFactory类，这样有什么意义吗？首先，这样将变更都集中到一处了；其次，工厂类会有很多的用户，这些用户，就已经不依赖具体的实现了。
- 工厂类的生产方法，可以是静态的。优点：无需实例化工厂对象（如下例）。缺点：静态方法不能使用继承和多态语法。

```java
class SimplePizzaStore {
    Pizza orderPizza(String flavour) {
        Pizza pizza = SimplePizzaFactory.creatPizza(flavour);
        pizza.prepare(); // 每种Pizza都有面粉、烘烤、切开、装盒操作
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
    public static void main(String[] args) {
        SimplePizzaStore store = new SimplePizzaStore();
        store.orderPizza("clam");
        System.out.println("Sir, your pizza is OK.");
    }
}
class SimplePizzaFactory {
    public static Pizza creatPizza(String flavour) { // 可以设为静态的，好处是不需要实例化
        Pizza pizza = null; // 缺点是，静态方法 不能 java多态
        if (flavour.equals("cheese")) { pizza = new CheesePizza();
        } else if (flavour.equals("clam")) { pizza = new ClamPizza();
        } else if (flavour.equals("veggie")) { pizza = new VeggiePizza();
        } else if (flavour.equals("pepperoni")) { pizza = new PepperroniPizza();
        }
        return pizza;
    }
}
class Pizza {
    public void prepare(){};
    public void bake(){};
    public void cut(){};
    public void box(){};
}
class CheesePizza extends Pizza {}
class ClamPizza extends Pizza {}
class VeggiePizza extends Pizza {}
class PepperroniPizza extends Pizza {}
```

UML图：

```plantuml
@startuml C
SimplePizzaStore .right.> SimplePizzaFactory
SimplePizzaFactory .right.> Pizza
Pizza <|-- CheesePizza
Pizza <|-- ClamPizza
Pizza <|-- VeggiePizza
Pizza <|-- PepperroniPizza
SimplePizzaStore : 使用者类
SimplePizzaFactory : 工厂类
Pizza : 产品类
CheesePizza : 具体产品类
@enduml
```

##### 加盟比萨店——工厂方法

比萨店很火爆，因此开了很多加盟店，NewYork、Chicago、California三个地区3个比萨工厂。根据上面的实现，很简单地会想到下面的实现：
```plantuml
NewYork ..> PizzaStore
Chicago ..> PizzaStore
California ..> PizzaStore
PizzaStore .right.> PizzaFactory
PizzaFactory <|-- NewYorkFactory
PizzaFactory <|-- CaliforniaFactory
PizzaFactory <|-- ChicagoFactory
CaliforniaFactory ..> Pizza
NewYorkFactory ..> Pizza
ChicagoFactory ..> Pizza
Pizza <|-- CheesePizza
Pizza <|-- ClamPizza
Pizza <|-- VeggiePizza
Pizza <|-- PepperroniPizza
PizzaStore : 使用者类
PizzaFactory : 工厂类
Pizza : 产品类
CheesePizza : 具体产品类
```

java 代码：以纽约为例

```java
class PizzaFactory {
    public Pizza creatPizza(String flavour) {
        Pizza pizza = null;
        if (flavour.equals("cheese")) { pizza = new CheesePizza();
        } else if (flavour.equals("clam")) { pizza = new ClamPizza();
        } else if (flavour.equals("veggie")) { pizza = new VeggiePizza();
        } else if (flavour.equals("pepperoni")) { pizza = new PepperroniPizza();
        }
        return pizza;
    }
}
class NYPizzaFactory extends PizzaFactory {}// 在内部重写
class PizzaStore {
    PizzaFactory factory;
    public PizzaStore(PizzaFactory factory) { this.factory = factory; }
    Pizza orderPizza(String flavour) {
        Pizza pizza = factory.creatPizza(flavour);
        pizza.prepare(); // 每种Pizza都有面粉、烘烤、切开、装盒操作
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
class NewYork {
    public static void main(String[] args) {
        NYPizzaFactory nyFactory = new NYPizzaFactory();
        PizzaStore nyStore = new PizzaStore(nyFactory);
        nyStore.orderPizza("clam");
        System.out.println("Your pizza is OK.");
    }
}
```

这种方式，子类继承或者覆盖父类方法，所以PizzaFactory 的每个子类NYPizzaFactory 可能都要写很多分支语句，如果Pizza类改变，那么也不知道哪儿应该修改，哪儿不应该修改。而PizzaStore又将某些方法完全控制。所以这个方式并不好。

**工厂方法模式**

> 父类定义了一个实例化对象的接口（抽象方法），但将具体的创建行为封装在子类中，由子类决定要实例化的类是哪一个。虽然每个子类都可能用到这个接口创建的产品对象，但是父类只定义接口，只有子类真正实现并且创建产品。工厂方法让类把实例化推迟到子类，父类就与具体的对象创建过程解耦了。编写父类时，不需要知道具体创建的产品是哪一个，选择了一个子类，自然就知道实际创建的是什么。

父类设立抽象工厂方法，子类实现工厂方法；产品类由一个接口统领；具体的产品类与相关的子类的工厂方法挂钩，所以一旦选用了一个子类，就选择了相关产品。而父类提供了一个方法`orderPizza()`作为统领的核心，让各个子类继承，从而可以获取产品，然后向用户递交产品。

当然父类的工厂方法也可以是正常的方法，作为”默认工厂“。工厂方法也可以没有参数，作为”固定工厂“；如果字符串作为参数不太安全，比如"clam"写成"calm"引发运行时错误，那么可以使用静态常量、enum。主要是父类子类的继承关系与产品决定权。

```plantuml
USER -down- PizzaStore
USER : // 使用 PizzaStore 的工厂方法接口获得产品
PizzaStore <|-left- NYPizzaStore
PizzaStore <|-right- ChicagoPizzaStore
PizzaStore : //抽象创建者
PizzaStore : {abstract}creatPizza( 抽象工厂方法 )
PizzaStore : orderPizza( 提供给调用者的接口)
NYPizzaStore : //具体创建者，用产品的抽象接口操控产品
NYPizzaStore : creatPizza( 实现工厂方法 )
NYPizzaStore .. Pizza
ChicagoPizzaStore .. Pizza
NYCheesePizza : //具体产品
Pizza <|-- NYCheesePizza
Pizza <|-- NYClamPizza
Pizza <|-- NYVeggiePizza
Pizza <|-- NYPepperroniPizza
Pizza : 创建者依赖的是具体产品，而不是抽象类
```

Java代码：

```java
import java.util.ArrayList; import java.util.List;
abstract class PizzaStore {
    Pizza pizza = null;
    abstract Pizza creatPizza(String flavour); // 抽象工厂方法
    Pizza orderPizza(String flavour) {
        pizza = creatPizza(flavour);
        pizza.prepare(); pizza.bake();
        pizza.cut();     pizza.box();
        return pizza;
    }
}
class NYPizzaStore extends PizzaStore {
    Pizza creatPizza(String flavour) { // 子类负责实现工厂方法
        if (flavour.equals("cheese")) { pizza = new NYCheesePizza();
        } else if (flavour.equals("clam")) { pizza = new NYClamPizza();
        } else if (flavour.equals("veggie")) { pizza = new NYVeggiePizza();
        } else if (flavour.equals("pepperoni")) { pizza = new NYPepperroniPizza(); }
        return pizza;
    }
}
abstract class Pizza {
    String name, dough, sauce; // 名称、面团厚薄、酱料
    List<String> toppings = new ArrayList<String>(); // 比萨上面的佐料，比如芝士
    public void prepare() {
        System.out.println("preparing " + name + "\n"
            + "\tTossing: " + dough + "\n" + "\tAdding: " + sauce+ "\n"
            + "\tAdding: " + toppings.toString());
    }
    public void bake() { System.out.println("bake for 25 minutes."); }
    public void cut() { System.out.println("Cut into sectors."); }
    public void box() { System.out.println("put into box."); }
    public String getName() { return name; }
}
class NYCheesePizza extends Pizza { // Pizza 子类都和它差不多，所以下面省略不写
    public NYCheesePizza() {
        name = "NY style sauce and Cheese Pizza";
        dough = "Thin crust dough";
        sauce = "Marinara sauce";
        toppings.add("Grated Reggiano Cheese");
    }
}
class NYClamPizza extends Pizza {}
class NYVeggiePizza extends Pizza {}
class NYPepperroniPizza extends Pizza {}
public class FactoryMethod {
    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Order info: " + pizza.getName() + "\n");
    }
}
```

简单工厂类与工厂方法框架的对比

| 简单工厂类                                | 工厂方法框架                   |
| ------------------------------------ | ------------------------ |
| 主要使用 组合 语法                           | 主要使用 继承 语法               |
| 把所有的事情，都在一个工厂类内完成。如果类太多，难以管理，适合简单的软件 | 父类创建框架，子类各自实现。更有弹性，适合规模化 |

将创建对象的代码集中在一个对象（简单工厂类）或者方法（工厂方法）中，可以避免代码冗余，并且方便以后维护。这样，用户在使用这个模块、实例化对象时，依赖的是接口，而不是具体的类，是针对抽象编程，而不是针对实现编程。

当然，不管如何封装，最后对象创建还是依靠`Object ob = new Object()`来实现的，这个是不可回避的。但将实例化的过程，用栅栏围起来，才可以应对各种变更。

##### 依赖倒置原则

如果不用简单工厂和工厂方法，那么代码大概如下：

```java
class PizzaStore {
    Pizza orderPizza(String style,String flavour) {
        Pizza pizza = null;
        if (style.equals("NY") { // 先选 地域风格
            if (flavour.equals("cheese")) { // 再选口味
                pizza = new NYCheesePizza();
            } else if (flavour.equals("veggie")) {
                pizza = new NYVeggiePizza();
            } //。。。还有很多其他的口味
        } // 。。。还有很多地域风格
        pizza.prepare(); pizza.bake(); pizza.cut(); pizza.box();
        return pizza;
    }
    public static void main(String[] args) {
        PizzaStore nyStore = new PizzaStore();
        Pizza pizza = nyStore.orderPizza("cheese");
    }
}
```

类关系UML：可以看到PizzaStore依赖所有的、具体的Pizza类，因为它直接参与Pizza对象的创建。只要这些类有了些许的改动，PizzaStore就必须随之改动。而且，每增加一个Pizza类，PizzaStore又多了一个依赖项。

```plantuml
PizzaStore .up.> NYCheesePizza
PizzaStore .up.> NYClamPizza
PizzaStore .up.> NYVeggiePizza
PizzaStore .up.> NYPepperroniPizza
PizzaStore .right.> XXXXXXX_YYYY_Pizza
PizzaStore  ..>  ChicagoCheesePizza
PizzaStore ..> ChicagoClamPizza
PizzaStore ..> ChicagoVeggiePizza
PizzaStore ..> ChicagoPepperroniPizza
```

依赖倒置原则 Dependency Inversion Principle：要依赖抽象，不要依赖具体类。也就是不要像上面的那样，依赖于具体类。与「针对接口编程」很相似，但「依赖倒置」更强调抽象，高层底层的组件都必须依赖于抽象。

上面的PizzaStore，实例化的过程依赖于各种X_Pizza类——高层组件依赖于低层的具体类。用了工厂方法后，NYCheesePizza依赖于Pizza、NYPizzaStore也依赖于Pizza，高层低层组件都依赖于这个抽象；类似地，PizzaStore也是一个被两层依赖的抽象。而且，用了工厂方法后，从USER到NYCheesePizza，每次的交互，都是用统一的接口操控不同的子类，这就是依赖于抽象：

```plantuml
USER .right.> PizzaStore
PizzaStore <|-right- NYPizzaStore
NYPizzaStore .down.> Pizza
Pizza <|-- NYCheesePizza
Pizza <|-- NYClamPizza
Pizza <|-- NYVeggiePizza
Pizza <|-- NYPepperroniPizza
```

要做到依赖倒置，应该：

- 设计时，改变思考方式：如果要实现一个PizzaStore，首先会想到制作Pizza的步骤，和不同的X_Pizza。那么，不同的X_Pizza抽象为一个Pizza类，然后使用这个接口。
- 在编码时，尽量做到（但不是绝对要做到）：
  - 变量不可以声明为具体类 ，而声明为抽象类然后使用多态：`Pizza p;//对   ClamPizza p; //错`。如果无法做到，就使用工厂模式之类的解耦。
  - 继承时，使用`abstract class`或者`interface`作为父类（而不是普通的类），也不要覆盖父类中已经实现的方法，而用多态语法。

##### 抽象工厂

按照工厂方法，每个 PizzaFactory 都通过Pizza抽象接口，和很多子类挂钩，比如NYCheesePizza。如果工厂很多、产品也很多，那么产品的管理会非常困难（尽管使用了抽象Pizza类，依赖倒置），而且产品有了变动，工厂也要变动。

再抽象一层：Pizza是由很多原料（面团、酱料、芝士、蔬菜、香料、鱼肉蛋）组成的，设立原料供应的工厂。

```java
class Dough {}  class Sauce {}  class Cheese {} //各种原料抽象类
class Veggies {}    class Pepperroni {} class Clams {}
class ThinCrustDough extends Dough {} // 具体的原料类
class ThickCrustDough extends Dough {}
class MarinaraSauce extends Sauce{}
class PlumTomatoSauce extends Sauce{}
class ReggianoCheese extends Cheese{}
class MozzarellaCheese extends Cheese{}
class SlicedPepperroni extends Pepperroni{}
class FreshClams extends Clams{}
class FrozenClams extends Clams{}
class BlackOlives extends Veggies{}
class Spinach extends Veggies{}
class Eggplant extends Veggies{}
class Garlic extends Veggies{}
class Onion extends Veggies{}
class Mushroom extends Veggies{}
class RedPepper extends Veggies{}

interface PizzaIngredientFactory { // 原料工厂的接口
    public Dough createDough();
    public Sauce createSauce();
    public Cheese createCheese();
    public Veggies[] createVeggies();
    public Pepperroni createPepperroni();
    public Clams createClam();
}
class NYPizzaIngredientFactory implements PizzaIngredientFactory {
	public Dough createDough() { return new ThinCrustDough();}
	public Sauce createSauce() { return new MarinaraSauce();}
	public Cheese createCheese() {return new ReggianoCheese();}
	public Veggies[] createVeggies() {
		Veggies veggies[] = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
		return veggies;
	}
	public Pepperroni createPepperroni() { return new SlicedPepperroni(); }
	public Clams createClam() {return new FreshClams();}
}
class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {
	public Dough createDough() {return new ThickCrustDough();}
	public Sauce createSauce() {return new PlumTomatoSauce();}
	public Cheese createCheese() {return new MozzarellaCheese();}
	public Veggies[] createVeggies() {
		Veggies veggies[] = {new BlackOlives(), new Spinach(), new Eggplant()};
		return veggies;
	}
	public Pepperroni createPepperroni() {return new SlicedPepperroni();}
	public Clams createClam() {return new FrozenClams();}
}
abstract class Pizza {
    PizzaIngredientFactory ingredientFactory;
    String name; // 名字和各种原料
    Dough dough;    Sauce sauce;    Cheese cheese;
    Veggies veggies[]; Pepperroni pepperoni; Clams clam;
    abstract void prepare();
    public void bake() { System.out.println("bake for 25 minutes."); }
    public void cut() { System.out.println("Cut into sectors."); }
    public void box() { System.out.println("put into box."); }
    public void setName(String s) { name = s; }
    public String getName() { return name; }
}
class CheesePizza extends Pizza {
    public CheesePizza(PizzaIngredientFactory factory) {
        this.ingredientFactory = factory;
    }
    void prepare() { // 一步步创建 pizza ，向工厂索要原料
        System.out.println("prepare " + name);
        dough = ingredientFactory.createDough(); // 面向抽象接口编程
        sauce = ingredientFactory.createSauce(); // 不在乎究竟是什么工厂
        cheese = ingredientFactory.createCheese();
    }
}
class ClamPizza extends Pizza { // 类似地，其他 Pizza也这么写。省略
    public ClamPizza(PizzaIngredientFactory factory) {
        this.ingredientFactory = factory;
    }
    void prepare() {}
}
abstract class PizzaStore { // 不变。
    Pizza pizza = null;
    abstract Pizza createPizza(String flavour); // 抽象工厂方法
    Pizza orderPizza(String flavour) {
        pizza = createPizza(flavour);
        pizza.prepare(); pizza.bake();
        pizza.cut();     pizza.box();
        return pizza;
    }
}
class NYPizzaStore extends PizzaStore { // 传入原料工厂
    Pizza createPizza(String flavour) {
        PizzaIngredientFactory factory = new NYPizzaIngredientFactory();
        if (flavour.equals("cheese")) {
            pizza = new CheesePizza(factory); pizza.setName("NY Cheese Pizza");
        } else if (flavour.equals("clam")) {
            pizza = new ClamPizza(factory);   pizza.setName("NY clam Pizza");
        }
        return pizza;
    }
}
public class IngredientFactory {
    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Order info: " + pizza.getName() + "\n");
    }
}
```

类关系UML：

```plantuml
USER ()- PizzaStore
PizzaStore <|-up- NYPizzaStore
PizzaStore <|-up- ChicagoPizzaStore
PizzaStore .down.> Pizza
PizzaStore .down.> PizzaIngredientFactory
PizzaIngredientFactory <|-- ChicagoPizzaIngredientFactory
PizzaIngredientFactory <|-- NYPizzaIngredientFactory
Pizza .. PizzaIngredientFactory
Pizza <|-up- CheesePizza
Pizza <|-up- ClamPizza
Pizza <|-up- VeggiePizza
Pizza <|-up- PepperroniPizza
Pizza .. Dough
Pizza .. Sauce
Pizza .. Cheese
Pizza .. Clams
Clams <|-- FreshClams
Clams <|-- FrozenClams
```

使用工厂方法：user——NYPizzaStore——NYClamPizza

使用抽象工厂：user——NYPizzaStore——NYClamPizza——NYPizzaIngredientFactory——Clams等原料

> 抽象工厂模式：提供一个接口，用于创建相关或依赖的对象家族，而不需要明确指定具体类。

引入的抽象工厂PizzaIngredientFactory，提供了创建所有比萨原料的接口，使用者利用这个接口就可以制造出**整个酱料家族的产品**。因为使用的是接口，所以**可以替换**为不同的工厂NYPizzaIngredientFactory来实现，获得不同的实现，比如用大蒜酱取代番茄酱。但是，并不需要详细知道每种产品的制造过程，换个工厂就可以得到「一揽子解决方案」。

| 工厂方法              | 抽象工厂                          |
| ----------------- | ----------------------------- |
| 继承：父类把对象的创建委托给子类。 | 组合：父类定义接口，要创建一组对象，子类则实现创建的过程。 |
| 让子类决定如何创建对象。使用的是类 | 创建相关的对象家族，但不需要依赖具体类。使用的是对象组合  |
| 父类通常会用到子类创建的具体类型  | 父类通常用抽象方法定义接口，而子类实现——这很像工厂方法。 |



### 单件模式 Singleton

有些对象，只需要一个，甚至只能有一个，再多就会程序行为异常、资源使用过量、处理结果与预期不一致等等，引发各种故障。比如：线程池 threadpool、缓存 cache、对话框、处理设置/注册表/数据库的对象、日志对象、各种设备的驱动程序……

单件模式：确保一个类最多只有一个实例，并提供一个全局访问点。并且延迟实例化，这对资源敏感的对象特别重要。

##### 经典单件模式

最简单的实现方式，是静态变量+静态方法+访问修饰符

```java
class Singleton {
    private static Singleton unique; // static 将实例对象变为类属性
    private Singleton() { } // private 将构造器私有化
    public static Singleton getInstance() { // static 只提供类方法
        if (unique == null) { unique = new Singleton();}
        return unique; // 不调用，不new：延迟实例化。有对象，不new：单件
    }
}
```

糖果公司有一个巧克力锅炉，每次依次进行：倒入原料、煮沸、倒出巧克力液。本来经典单件模式，可以实现这个功能，但是，使用了多线程后，就会出现问题！因为多个线程同时进入`getInstance()`方法，几乎是同时实例化了一个对象。2个线程已经是经常出现问题了，如果有更多线程……

```java
class ChocolateStove {
    private static ChocolateStove unique; // 静态的
    private boolean empty, boiled;
    private ChocolateStove(){ empty=true; boiled=false;} // 私有的
    public static ChocolateStove getInstance() { // 静态的
        if (unique == null) { unique = new ChocolateStove();}
        return unique;
    }
    public void fill() { // 往锅炉加巧克力
        if (empty){
            empty = false; boiled = false;
            System.out.println("filling");
        }
    }
    public void heat() { // 给 巧克力 加热
        if (!empty && !boiled) {
            boiled = true;
            System.out.println("heating");
        }
    }
    public void pour() { // 从锅炉排出煮沸的巧克力
        if (!empty && boiled) {
            empty = true;
            System.out.println("pouring out");
        }
    }
}
class MakeChocolate extends Thread {
    @Override
    public void run() {
        ChocolateStove stove = ChocolateStove.getInstance();
        System.out.println(stove);
        stove.fill();
        stove.heat();
        stove.pour();
    }
    public static void main(String[] args){
        Thread thread_1 = new MakeChocolate(); // 创建2个线程对象
        Thread thread_2 = new MakeChocolate();
        thread_1.start(); // 两个线程同时跑
        thread_2.start();
    }
}
// ChocolateStove@6c6aa204     2个线程有2个炉子实例
// filling
// ChocolateStove@43e8772d
// filling  但实际上只有一个炉子。两次都导入同一个锅炉，爆满
// heating
// heating  重复加热，过热爆炸
// pouring out
// pouring out
```

##### 单件的同步问题

为了解决多线程的这种资源抢占问题，有3种解决方案：

1. 获得实例同步化，在`getInstance()`方法加上`synchronized `关键字就可。

   synchronized：每个线程进入时，必须等别的线程离开这个方法，从而保证不可能会有两个线程同时进入这个方法。缺点是每次同步会导致程序执行效率极速下降。如果经常要调用这个方法，则不建议使用。

   ```java
   class Singleton {
       private static Singleton unique;
       private Singleton() { }
       public static synchronized Singleton getInstance() { // 只要加关键字
           if (unique == null) { unique = new Singleton();}
           return unique;
       }
   }
   ```

2. 不延迟实例化。`synchronized `适用于不经常使用的单件对象。而要频繁使用的单件，或者创建、运行时资源占用不大的话，建议“急切实例化”

   ```java
   class Singleton {  // 程序一开始运行就创建
       private static Singleton unique = new Singleton();
       private Singleton() { }
       public static synchronized Singleton getInstance() {
           return unique;
       }
   }
   ```

3. 双重检查加锁（java > 1.4）。首先检查实例是否创建，如果尚未创建，才同步进行创建。延迟实例化，也保证了单件，而且还不造成同步无谓的等待。

   `volatile`确保这个变量在多线程时，恰当处理。

   ```java
   class Singleton {
       private volatile static Singleton unique = new Singleton();
       private Singleton() { }
       public static Singleton getInstance() {
           if (unique == null) { //如果实例不存在，才使用下面的 同步代码
               synchronized (Singleton.class) {
                   if (unique == null) { // 进入同步代码块后，再检查一次
                       unique = new Singleton();
                   }
               }
           }
           return unique;
       }
   }
   ```

考虑性能和资源的限制，和主要使用场景，然后选择合适的方案实现单件，以解决多线程的问题。

##### 其他

纯粹static的类（全局变量的类）：如果把一个类所有的方法和变量都定义为static，那就是把类当做一个单件了。但是，如果单件的初始化很复杂，或者类的关系很复杂，那么就会引发很微妙的bug。

使用多个类加载器时，有可能会出现多个对象并存，单件模式被破解了，要小心。

单件类不继承。

