

[TOC]



# 第一章   导言

变量与常量、运算符、控制流、函数、

基本输入、输出

指针、结构

C语言程序，就是由 函数 和 变量 组成的。函数中的语句，执行操作；变量则用于存储计算过程中使用的值。

main 是一个特殊的函数名，每个 C 程序必须包含 main 函数，而且都从 main 函数开始执行。

main 函数会调用其他函数，这些函数可能是自己编写的，也可能来自于函数库。

### 变量

变量声明`int a;` 说明了变量的属性：数据类型 和 变量名。



数据类型：基本类型（字符、各种长度的整型/浮点型），还可通过指针、数组、结构、联合派生出各种数据类型。

基本类型：

- 数字：整数 int、short、long；浮点数/小数 double、float
- 字符：char​

```c
int 16 位，也有 32位的
float 32 位，至少有6位有效数字
char 一个字节
```

表达式和运算符

表达式：由运算符和操作数组成。任何表达式，都可以是语句。

```c
printf("%d", 2);
printf("%d", 2 / var); /* 使用某种类型的常量的地方，就可以使用同类型的更复杂的表达式 */
```

没有幂运算 `**`



运算符：

如果有浮点数，就转换为浮点数，然后运算。

```c
float a, b = 10.0; /* 虽然 b = 10 也可以，但 10.0 强调浮点，更易读 */
a = 3.0 / 2;
b = b / 3;
```



函数：函数可以返回基本类型、结构、联合、指针。任何函数都可以递归调用。局部变量通常是「自动」的，每次调用重新创建。函数定义不可以嵌套，但函数声明可以嵌套。不同的函数可以出现在多个单独编译的不同源文件中。变量可以只在函数内部、函数外部且一个源文件内、整个程序不同的源文件内有效。





### 符号常量

编译的预处理阶段：#include包含其他源文件、#define宏替换，条件编译。

`#include` 表示包含哪些函数库

简单的宏定义`#define`，就是**文本替换**replace。所以，可以将一个符号名（符号常量）定义为一个特定的字符串。

```c
#include <stdio.h>
#define LOWER   0 /* 注意末尾没有 ; 分号  常量一般全大写*/
#define UPPER   300
#define STEP    20
int main(void)
{
    int fahr;
    for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP) {
        printf("\t%3d %7.1f\n", fahr, (5.0/9.0)*(fahr-32));
    }
    return 0;
}
```

宏定义和头包含语句，都是预处理指令，是在编译之前处理的。





### 控制流：

包括：语句组（语句块`{ }`），条件判断 if…else，多路选择 switch ，终止测试在顶部的循环 while | for ，终止测试在底部的循环 do ，跳转 continue break goto

while 语句执行步骤：

- 首先进行条件测试
- 如果为真，就执行语句块。执行完毕后，进行下一次 测试—语句块 的循环。
- 如果为假，就不执行语句块，执行 while 之后的语句。

while之后，既可以是单条语句，也可以是语句块。

如果某个运算符所有的操作数（常量或者表达式）都是整型，就执行整型运算。

```c
int a, b=10;
a = 3 / 2; /* 整数除法将会 舍位，也就是 截尾，舍弃小数部分 */
b = b / 2; 
```

##### for 语句

for 语句适合 初始化、步长递增、条件测试 逻辑相关的情形。这时候用 for，会比 while 更紧凑易读。

```c
int i;
for (i=0; i<10; i++) { // for(int i=0; i<10; i++)这是 C99 写法
    printf("%d", i);
}
/* 初始化语句 i=0 仅在进入循环前 执行一次
 * 条件测试 i<10 为真 就执行循环体，再执行 步长递增 i++
 * 循环后，再次条件测试：为真，则执行循环体，再步长递增；为假，则终止循环。*/
int i, j;
for (i=0, j=10; i<j; i++, j--) { ; } // 两个递增变量。逗号运算符
for (i=0, j=10; i<5, j>5; i++, j--) { } // 这种写法，编译器会警告
for (;;) {...} // 死循环
```

写循环体的时候，要特别注意边界条件。

### 字符输入输出

标准库提供的输入/输出模型非常简单：无论文本从何处输入、何处输出，输入输出都按照文本流的方式处理。文本流是由多行字符构成的字符序列（也就是字符数组char [ ]，以`\0` 结束），每行字符是由0或多个字符构成（文本行以`\n` 结束）。标准库处理了底层，使得所有的输入输出都遵循这个模型，比如Unix/Linux 换行是 LF，Win是CRLF，而C语言都是`\n`

##### 文件（文本流）复制

「文件」指的是 Unix 里面的文本流。Unix文本流可以来自输入、文件、管道。

```c
#include <stdio.h>
int main(void)
{
    int c; /* 因为 EOF 用 char 会溢出，所以只能用 int */
    c = getchar(); /* getchar()读取一个字符 */
    while (c != EOF) { /*如果不是 EOF/end of file就继续。EOF的定义在 stdio.h */
        putchar(c); /* putchar()输出一个字符 */
        c = getchar(); /* 读取下一个字符 */
    } /* Win 用 Ctrl Z 发送 EOF，Unix 用 Ctrl D*/
    printf("EOF is: %d", c); /* EOF == -1 */
    return 0;
}
```

可以写的更紧凑

```c
c = getchar(); while (c != EOF) {...} /* 将这两句合并。python不可这样合并 */
while ((c = getchar()) != EOF) {...} /* 可以合并为这样 */
c = getchar() != EOF /*不可以这样。根据运算符优先级，是 c = (getchar()!=EOF) */
```

##### 统计字符数

```c
#include <stdio.h>
int main(void)
{
    long nc = 0;
    while (getchar() != EOF) /* 如果不是 EOF ，就统计 */
        ++nc;
    /* for (nc=0; getchar() != EOF; ++nc) // 改写为for循环，里面是 空语句
     *     ;
     */
    printf("%ld", nc);
    return 0;
}
```

##### 行计数

```c
int i, j, k;
i = j = k = 0; /*赋值顺序：从右到左 k = 0; j = k; i = j;*/
```

##### printf() 函数的格式化

```c
int fahr;
for (fahr = 0; fahr <= 300; fahr = fahr + 20) {
  printf("%d\t%d\n", fahr, (5 * (fahr-32) / 9));// 制表符，左对齐
}
0       -17
20      -6
40      4
60      15
printf("%3d%7d\n", fahr, (5 * (fahr-32) / 9));// 指定宽度，右对齐
  0     -17
 20      -6
 40       4
 60      15
int fahr;
for (fahr = 0; fahr <= 300; fahr = fahr + 20) {
  printf("%3d\t%.1f\n", fahr, (5.0 * (fahr-32) / 9));// %.0d 不指定宽度，但是指定小数点后多少位
}
printf("%.0d%7d\n", fahr, celsius); // %.0d 不指定宽度，但是指定小数点后多少位
0  -17.8
20   -6.7
40    4.4
60   15.6
%0 表示八进制数，%x表示十六进制，%s表示字符串，%%表示百分号自身
```



### 数组

统计数字[0,9]、空白符、字母出现的频率。

```c
#include <stdio.h>
int main(void)
{
    int c, i, nwhite, nother;
    nwhite = nother = 0;
    int ndigit[10];
    for (i = 0; i < 10; ++i) {
        ndigit[i] = 0;
    } /* int ndigit[10] = {0, }; 就可以完成声明、初始化为0 */
    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9') {
            ++ndigit[c-'0']; /* 获得下标的方式非常讨巧 */
        } else if (c == ' ' || c == '\n' || c == '\t') {
            ++nwhite;
        } else {
            ++nother;
        }
    }
    for (i = 0; i < 10; ++i) {
        printf("\t%d:\t%d\n", i, ndigit[i]);
    }
    printf("\twhiteSpace: %d\nother: %d\n", nwhite, nother);
    return 0;
}
```





### 作用域

定义Define是创建变量或者分配存储单元。

声明Declaration 是说明变量的性质，但并不分配存储单元。

函数内部的变量，是私有变量、局部变量，其他函数不能访问。因为每个局部变量都是函数调用时建立，函数执行完毕时消失，所以也称为自动变量。

也正因为每次调用建立，所以自动变量不保存前一次调用时的赋值，每次进入的时候都要显式赋值，如果没有赋值，那就会是随机值。

外部变量：定义在所有函数外部的变量。

- 只能定义一次，定义后编译程序将会为它分配存储单元（自动变量是函数调用时，才分配存储单元）。

- 可以用`extern` 显式声明，也可以通过上下文隐式声明

  ```c
  /* 如果外部变量的定义，在函数使用它之前，就无需 extern 这也是最常见的做法*/
  int max = 0;
  int main(void){ max = 1;} 

  /* 如果外部变量的定义，在函数使用它之后，就一定要 extern */
  int main(void){ extern int max = 1;} 
  int max = 0;
  ```

  ​

- 1



## 数据类型、运算符、表达式

### 变量

变量和常量是程序的两种基本数据对象。声明语句说明变量的名字和类型，也可以指定初始值。变量的类型决定该对象可取值的范围，以及可以执行的操作。

- 变量声明：类型  变量名;

  ```c
  unsigned count;  /* unsigned int 无负号 */
  char c, line[100]; /* 多个变量同时声明。分开容易修改，但合在一起更紧凑 */
  int i = 0; /* 声明的同时初始化 */
  int i, j = 0; /* 语法可行，但不推荐。不易读，容易误解 */
  const double e = 2.718281828 /* const 限定符，此变量只能赋值一次，不能被修改 */
  ```

- 变量初始化

  自动变量（函数、语句块的局部变量，且没有static限定），可以用任何表达式初始化，每次进入函数/语句块都被初始化一次。未进行初始化，值为随机值（当时内存的电位而定）。

  非自动变量（外部变量、静态变量），能且只能在程序开始之前  初始化  一次，所以必须用常量表达式初始化。未进行初始化，默认为0。

  const 修饰后，只能初始化一次，没法修改。

### 常量

符号常量（待合入）

```c
#
```



枚举常量

```c
enum boolean {NO, YES}; /* 如果不指定，默认第一个枚举值为0，下一个为1，以此类推 */
enum months {Mon = 1, Tue, Wed, Thu, Fri, Sat, Sun} /* 指定第一个枚举值为1，下一个为2，以此类推 */
enum escapes { BELL = '\a', BACKSPACE = '\b', TAB = '\t',
               NEWLINE = '\n', VTAB = '\v', RETURN = '\r'} /* 全部设定枚举值 */
```

const 变量「固化」

```c
const double PI = 3.1415926;
```





### 数据类型和长度

仅有 2 大 基本类型：

- 整型：int（长度不定。加上限定符，可以衍生出很多种）、char（8 bit/1 byte）
- 浮点型：float、double、long double

short long 限定符：

- 组成了 4 种整数类型代表不同的大小：short int ≤ int ≤ long int ≤ long long int（可以缩写），但C标准只规定 bit 最小值 16, 16, 32, 64，所以实际上它们通常有重叠。现在的普通个人计算机一般是 16, 16/32, 32, 64。
- 普通情况，优先使用简单常见的 int，int是机器中整数的最自然长度（所以各平台可能不同）。
- 考虑大小和速度：如果为了更小的空间（数组占用内存太大、特殊变量要考虑寄存器），可以使用 short 甚至是 signed char / unsigned char ，否则不要使用。如果要表示更大的数据，优先考虑使用 unsigned int，而不是直接使用 long，因为会降低运算速度。
- 考虑移植性：因为 int 类型的长度各平台不定，所以如果一定需要32位，就一定要用long，需要64位就用long long

类似地，存储空间 float ≤ double ≤ long double 也可能重叠。一般来说，double 最常见，要节约就使用 float，但很少使用 long double。

signed/unsigned 限定符：所谓sign指的是负号，所以signed就是有负号的，unsigned就是没有负号的。

- 用于 short/int/long：所有整数（short/int/long）默认都是signed，取值范围 [-Min, Max] ，所以标明`signed` 没有实际作用，只为了强调有符号而已；如果标明`unsiged` 类型，没有负数，取值范围 [0, Max] 。结论：unsigned只用于非负值。
- 用于 char ：char 表示字符时，没有signed/unsigned的说法。但如果是用来表示整数，就必须加上signed/unsigned，因为每种编译器的char类型实现不同，声明`signed char a;`，那么 `a `取值范围 [-128, 127]；声明`unsigned char a;`，那么 `a `取值范围 [0, 255]。结论：用 char 表示整数，必须指定正负。

##### 类型转换

扩展转换Widening Conversion（自动转换）：从容纳空间小的类型，转向大的类型，不必显式（手动）转换。

1. 目标类型能与源类型兼容，且容量(字节数)大于源类型。如 double 型兼容 int 型，但是 char 型不能兼容 int 型；double 类型可以存放 int 类型的数据，但反过来就不可以了。

   ```mermaid
   graph LR;
   char-->int;int-->long;long-->float;float-->double;
   short-->int;
   ```

   ```c
   int a = 10; float b = 1.4;
   float sum = a + b; /* 1.自动把 a 转为 float 再运算 */
   char c = array[1.0]; /* 2.禁止使用无意义的表达式（比如浮点数用作下标），不存在这种转换问题 */
   char d = 798; short cut = 32.76; /* 3.超出限额的赋值，会损失信息，可能导致意外结果。编译器会警告，但表达式并不非法 */
   ```

2. 如果是 float 和 double ，将**不会自动转换**，这是考虑 存储空间 和 运算速度。高精度计算，比如`<math.h>`的函数，才用 double，一般都用float。

3. unsigned  的数据转换比较复杂。

   ```c
       long int a = -1;
       unsigned int b = 1;
       unsigned long int c = 1;
       if (a > b) /* 关系运算不正确。算术运算正确。 */
           printf("a: %d > b: %d; a+b: %d; a*b: %d\n", a, b, a+b, a*b);
       if (a > c) /* 关系运算不正确。算术运算正确。 */
           printf("a: %d > c: %d; a+c: %d; a*c: %d\n", a, c, a+c, a*c);
   ```

4. 赋值语句会隐性转换。

   ```c
   int i; char c = 'x';
   i = c; /* 这种赋值，隐含的数据转换不会丢失信息 */
   c = i; /* 这种赋值，隐含的数据转换很可能会丢失信息 */
   int i = 3; float f = 4.5; double d = 3.122423454777777786;
   i = f; /* float转为整数，会截尾 */
   f = d; /* double 转为 float，究竟是四舍五入，还是截尾，随编译器而定 */
   ```

5. 函数调用会进行转换。

6. 1

   ​

字符转换

- 将 char 强制转换为 int：char表示字符的时候，没有signed/unsigned的说法（都是正整数），不同的编译器可能实现不同，因此 `(int)c`可能得到一个负数（ANSI只是要求标准打印字符集一定非负）。因此，用 char 存储非字符数据，必须要指定 signed/unsigned。

- 将字符串`"12894"`转为整数`12894`，如上所述，加上 `'0'`的ASCII 码不是整数 0，所以不能用 `(int)c`这种方式

  ```c
  #include <stdio.h>
  int atoi(char s[]) /* 将 字符串转为数字，比如 "153" 转为 153 */
  {
      int i = n = 0;
      for (; s[i] >= '0' && s[i] <= '9'; ++i)
          n = 10 * n + (s[i] - '0');
      return n;
  }
  int main(void) /* 用法 */
  {
      char str[] = "1897333";
      printf("\t%d\n", atoi(str));
      return 0;
  }
  ```

- `<ctype.h>`有大小写转换函数，也有判断是不是大小写、字母数字等函数。但要注意：

  ```c
  d = (c >= '0' && c <= '9'); /* 判断 c 是不是数字，结果只会是 0 或 1 */
  d = isdigit(c); /* 用函数判断 c 是不是数字，结果可能是 0 或 其他非零值 */
  ```

  ​

- ​

- 1



##### 字面量的表示方法

- 整数：一般整数都默认是int，超过限额就默认是 long。强调类型，`l/L`表示long，`u/U`表示unsigned。

  ```c
  long a = 1000L; /* 最好是大写 L 易读 */
  unsigned long a = 1000UL; /* 最好是大写 U ，和 L 搭配 */
  /* 整数可以用 oct hex 表示 */
  int b = 31, c = 037, d = 0x1f, e = 0x1F; /* 31，分别用十进制、八进制、十六进制表示 */
  ```

- char 字符。

  ```c
  char a = '4'; /*字符、转义字符。单引号。计算机内部，实际是一个整数 */
  char b = '\n', c = '\012', d = '\x0a'; /* 可以用整数值对应的八进制、十六进制 表达 */
  char b = 'a', c = '\141', d = '\x61'; /* 可以用整数值对应的八进制、十六进制 表达 */
  ```

  转义序列 Escape Sequence，2个作用：普通字符转为特殊字符，比如`\n`；特殊字符转为普通字符，比如`\\`

  | 转义字符 | 意义                            | ASCII码值（十进制） | 转义字符 | 意义                   | ASCII码值（十进制） |
  | ---- | ----------------------------- | ------------ | ---- | -------------------- | ------------ |
  | \a   | 响铃(BEL)                       | 007          | \b   | 退格 Backspace         | 008          |
  | \f   | 换页 Form Feed                  | 012          | \n   | 换行 Line Feed         | 010          |
  | \r   | 回车 Carriage Return            | 013          | \t   | 水平制表 Horizon Tab     | 009          |
  | \v   | 垂直制表 Vertical Tab             | 011          | \\\  | 反斜线字符''\'  Backslash | 092          |
  | \'   | 单引号 Single Quote / Apostrophe | 039          | \"   | 双引号 Double Quote     | 034          |
  | \?   | 问号 Question Mark              | 063          | \0   | 空字符(NULL)            | 000          |

  Windows 和 很多网络应用层协议（比如HTTP）是用\r\n，而 Unix/Linux 是用 \n

  转义序列是在编译时候处理的，而转换说明是在运行时调用 printf 函数处理的。

- 浮点数：小数点、指数、或者小数点带指数

  ```c
  double a = 123.4, b = 1e-2, c = 1.2e-3;
  float d = 1.3f, e = 2.7F; /* f/F 表示 float */
  long double y = 12512353.23562345L; /* l/L 表示 long double，最好是大写 L */
  ```


- 字符串

  ```c
  char a = 'x'; char b[] = "x"; /*字符 和 字符串 不同。字符串是包含字符的、以\0结尾的数组*/
  char c[] = "you say: \"Hello, "
             "World!\"\n"; /* 字符串分行，编译器可以自动连接。可以包含转义字符*/
  char empty = ""; /* 空字符串 */
  ```

- 常量表达式：仅仅包含常量的表达式，在编译时求值，而不在运行时求值。

  ```c
  #define MAX 100
  char line[MAX + 1]; /* 常量表达式 */
  ```

- 1





### 运算符

运算符指定将要进行的操作。

##### 算术运算符

没有`**`幂运算

```c
+ -     * / %                /* + - 优先级比 * / % 更低 */
float a = 3 / 4; /* 3/4整数除法，截尾 */
float c = 3 % 4.0; /* 错！ % 不可以用于浮点数 */
int b = 5 / -3; /* 有负数的 / 运算，截尾方向取决于编译器的实现 */
int e = 5 % -3;  /* 有负数的 % 运算，结果取决于编译器的实现 */
```

##### 布尔运算：关系运算符 与 逻辑运算符

```c

>, >=, <, <=;   ==, !=;   !, &&, ||   /* 关系运算符 、相等性运算符、逻辑运算符*/
if (i < lim -1)  等价于 if (i < (lim-1))  /* 关系运算符 优先级 都比 算术运算符 低 */
if (i<lim-1  &&  (c=getchar()) != '\n'  &&  c != EOF) /* && 和 || 都存在短路运算*/
if ((c=getchar()) != EOF) /* 因为赋值运算符 优先级 更低，所以需要加括号 */
if (!valid) 等价于 if (valid == 0)
```









表达式吧变量与常量组合起来，生成新的值。











# settings  备份

```json
    // java 设置
    // 操作系统：系统变量  新建 JAVA_HOME       C:/Program Files/Java/jdk1.8.0_131
    // 操作系统：系统变量，新增一条 PATH        ;C:/Program Files/Java/jdk1.8.0_131/bin;
    // 操作系统：系统变量  新建 CLASSPATH     .;C:/Program Files/Java/jdk1.8.0_131/lib
    // 安装插件： redhat.java（仅需设置Javahome）    donjayamanne.javadebugger
    //"java.home": "C:/Program Files/Java/jdk1.8.0_131",

    // python 设置
    "window.zoomLevel": 0,
    //整个mac 的venv：/usr/local/bin/venv2/bin/python
    // "python.pythonPath": "${workspaceRoot}/.venv2/Scripts/python.exe", // "/Users/aChing/Documents/LinuxAndC/.venv2/bin/python",
    "python.venvPath": "${workspaceRoot}/.venv2",
    "python.envFile": "${workspaceRoot}/.venv2/Scripts/python.exe"//"${workspaceRoot}/.venv2/bin/python",
```



```c

```





