[TOC]

## 导言

#### 自然语言与形式语言

自然语言Natural Language，也就是人类的语言比如汉语，是自然进化而不是人为设计的。

形式语言Formal Language，为了特定目的而设计的语言，数学公式、化学方程式、编程语言。

自然语言与形式语言的**相同点**：

- 语法Syntax：
  - 符号Token：每个符号的意义（词汇的含义），词法规则 Lexical
  - 结构Structure：符号排列组合的方式（语句的结构），语法规则Grammar
- 语义Semantic：理解符号，解析语句结构，放入到上下文，就能理解语义Semantic。

自然语言与形式语言的**不同点**：

- 歧义性Ambiguity：自然语言充满歧义，而形式语言毫无歧义，每个语句都必须有确切的含义。
- 冗余性Redundancy：为了消除歧义，自然语言会有很多冗余。形式语言更紧凑，极少冗余。
- 字面一致Literal：自然语言会有俗语和隐喻。形式语言字面意义就是所有的意义。
- 细节影响很大。拼写和标点错误在自然语言中可以忽略，但在形式语言中可是大问题。


阅读程序的建议：

- 形式语言比自然语言更紧凑，所以要多花时间来读。
- 结构很重要，而不是读文章一样一下子翻页了，而是识别Token，分解Structure。
- 不要按照文件顺序从上到下阅读代码，而应该按照代码的执行顺序阅读代码，模仿计算机执行的过程，


#### 程序和编程语言

程序Program是告诉计算机如何完成一个计算任务。计算包括数学运算和信息运算（比如文字图像处理），但任何运算在计算机内部都是数学二进制运算。数据是二进制，指令也是二进制。

程序由一系列指令Instruction 构成，通常包括：输入Input（键盘、磁盘、扫描仪等等），输出Output（屏幕、磁盘、音响），基本运算（数据运算和I/O存取，数据结构），控制（顺序分支循环3大控制结构，与或非三种逻辑）。编程，就是把复杂的任务分解成子任务，再把子任务分解成更小的任务，直至最小单元——上面的几大种类的指令。

[注]: 上面说的这些都是图灵完备语言，核心都是指令，明确地指明每一步该怎么做，这是图灵机原理。但是函数式的语言Lisp、Haskell的核心是λ演算

学习编程的核心，无论什么，甚至学习数据结构和算法的真正目的，是学习把现实问题转化为计算机语言的方法。

编程语言分类program Language

1.  低级语言 low-level Language：用计算机指令编写程序。只有与硬件关系密切的少数程序才用低级语言。
   - 机器语言Machine Language，十六进制数字表示的二进制指令。1st Generation Programming Language/1GL
   - 汇编语言 Assembler Language，将二进制指令，一个个映射成助记符Mnemonic。用汇编写的程序，需要用汇编器 Assembler翻译成指令数字。2nd Generation Programming Language/2GL
2.  高级语言 high-level Language：高级语言的语句与低级语言的指令，并不是机器语言和汇编语言那样简单的一一对应的关系。读写维护更简单，而且与平台无关。
   - 3rd Programming Language/3GL，使用语句而不用指令编程，但是也分输入输出、运算、控制。
     - 编译Compile执行的C语言：需要编译为汇编语言或者机器语言。注意：某些C标准中不包含的语法特性，不同的编译器结果不同，所以尽量避免这种不可移植的语法。源代码 hello.c---编译为可执行文件 hello.out---操作系统执行。
     - 解释Interpret执行的bash shell：程序在解释器中执行，不需要编译为机器指令。
     - 编译解释相结合的Python/Java/Perl/Ruby：python源代码 hello.py---转化为平台无关的字节码 hello.pyc---PVM解释执行
   - 4th Programming Language/4GL，描述性地Declarative表达做什么，而不是一步步怎么做Imperative。比如SQL


平台是计算机体系结构Architecture，或者操作系统Operating System，或者开发平台，对应着一种指令集Instruction Set，一种机器语言。

SCIP说，学习一门编程语言，应该特别注意：

1. 有哪些原子概念primitive expressions，比如基本类型、基本运算符、表达式、语句。
2. 有哪些组合规则means of combination，基本类型如何组建复合类型、简单表达式和语句如何组建复杂的表达式和语句。
3. 有哪些抽象机制means of abstraction，数据如何抽象Data Abstraction，过程如何抽象Procedure Abstraction。函数是过程的抽象，面向对象的class是数据和过程的综合



## 开发环境的搭建

C语言标准：

1. K&R C（Old style C）
2. C89（ANSI 1990，最早的规范），也称C90、ANSI C，其他重要文献：Standard C: A Reference 和 the Standard C Library
3. C99，其他重要文献：Rationale for International Standard — Programming Language — C
4. C11，2011年

C语言难学的原因：需要编译原理、操作系统、计算机原理的支撑。学通C语言，其实是以C语言作为载体，理解计算机和程序的原理。建议在Linux 里面学习 C 语言。C语言是在Unix上开发的，编译器、Unix/Linux及其中的大部分程序，都是C语言开发的。

Linux/Unix：

- 编译器Compiler：Linux 用 gcc或者g++，Mac用 clang
- 头文件Header Files和库Libraries：glibc，Linux预装。如果要更新或者安装其他版本，最好是安装到/usr/local/lib下。修改gcc的spec文件（/usr/lib/gcc-lib/***/3.2.2/specs），更改ld-linux.so.2为/usr/local/lib下的新的共享库装载器。
- 调试器Debug：gbd
- 二进制文件处理工具链：Binutils toolchain
- 工程管理和构建：make

Windows

- gcc或者g++，glibc，gbd，Binutils toolchain，make，这些工具 都用 MinGW配置。但是MinGW 在线安装很麻烦，直接下载安装CodeBlocks就可以。或者下载并且配置 [gcc](http://tdm-gcc.tdragon.net/download)  或 [clang]( http://releases.llvm.org/download.html) 


编辑器，Linux/Unix shell 环境用 vim，UI 环境都使用 vs code

配置 VS Code（每个工程下的.vscode文件夹，git下载后覆盖就可）：

- launch.json，是debug的配置；
- tasks.json，这是被launch.json 调用的编译配置；
- c_cpp_properties.json，cpptools插件的配置；
- 新建settings.json文件，这个工作区的VS编辑器设置。




#### Hello World

源代码

```c
/* hello.c 第一个程序 */
#include <stdio.h>

/*main 入口 每个程序都必须的Boilerplate*/
int main(void){
    printf("Hello World!\n");
    return 0;
}
```

运行

```sh
# vim gcc 分步
$ vim ./hello.c # 在工作目录创建文件。i insert， :wq 保存退出

$ gcc ./hello.c # 编译，默认结果是 a.out 。-o hello.o 指定输出
$ ./a.out # 运行

# vim 不退出，vim 内部调试
$ vim ./hello.c # 编辑完毕后，:w 一定要保存，:!gcc hello.c -o hello.o && ./hello.o 编译并且运行。

# vs code 运行
$ cd ${fileDirname} && gcc hello.c -o hello.o && ./hello.o # 首先，切换到工作目录，看看能不能在命令行编译运行，如果不能，检查MinGW和PATH配置
# 如果命令行能够正常编译，且 tasks.json launch.json 都配置正确，F5 运行。
```





看到好多小伙伴不知道OJ是什么   我复制一下所谓OJ的概念
OJ是Online Judge系统的简称，用来在线检测程序[源代码**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/60376.htm)的正确性。著名的OJ有[TYVJ**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/3100833.htm)、[RQNOJ**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/1400437.htm)、URAL等。国内著名的题库有北京大学题库、[浙江大学**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/2548.htm)题库、[电子科技大学**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/4733.htm)题库、杭州电子科技大学等。国外的题库包括乌拉尔大学、瓦拉杜利德大学题库等。
Online Judge系统（简称OJ）是一个在线的判题系统。用户可以在线提交程序多种程序（如C、C++）[源代码**](http://link.zhihu.com/?target=http%3A//baike.baidu.com/view/60376.htm)，系统对源代码进行编译和执行，并通过预先设计的测试数据来检验程序源代码的正确性。
并在这里加上我常用的三个OJ网站
lintcode：[LintCode - 编程面试题在线训练平台**](http://link.zhihu.com/?target=http%3A//www.lintcode.com/zh-cn/problem/)
51nod：[登录 - 51Nod**](http://link.zhihu.com/?target=http%3A//www.51nod.com/index.html)
入门OJ：[入门OJ**](http://link.zhihu.com/?target=http%3A//begin.lydsy.com/JudgeOnline/problemset.php)



## Unix C 编码风格

Programs must be written for people to read, and only incidentally for machines to execute.代码主要是写给人看的，只是顺便能够用机器运行而已，如果只是给机器的，那写汇编好了，为什么要有高级语言呢？

#### 标识符命名

开头必须是下划线或者字母，`_a-zA-Z`，后面可以是下划线、字母、数字`_a-zA-Z0-9`

不可以使用 保留字 Reserved word （或称关键字 keyword）

```c
if else switch case do while for goto break continue default return 
int long char float double short signed unsigned void sizeof
const register typedef  struct volatile static  extern union enum  auto 
```

最好不要用下划线`_`作为开头，因为可能与 编译器 、C标准库 冲突。

禁止用 **汉语拼音** 作为标识符。

命名要清晰明了，见名知意。全局变量和全局函数，在整个项目中使用率很高，必须让使用者易懂，所以命名一定要详细，比如`radix_tree_insert`。局部变量或者只在一个源文件使用的变量，可以稍微简短，但也不能太短。`i; j; k` 只能用于循环体内。

可以使用 短单词 和 缩写。看多了代码，就会知道缩写的惯例。

```c
// 短单词用元音缩写
count / cnt，block / blk，length / len，window / win，message / msg
number / nr，temporary / tmp
// 长单词 用 头字母缩写
// 词根 用 近似元音缩写
transmit / xmit
```

大小写：

- Unix风格里，变量、函数、类型   都全小写、下划线。常量（宏定义、枚举常量等）全大写、下划线。
- 微软匈牙利命名法：`iCnt` 前缀i表示类型int，Cnt 是变量名。但这种命名不建议。
- Java 和 C++ 的大驼峰命名法，也可以考虑。



#### 注释

```c
/* 单行注释也建议写成这样，而不是用 // */

/* 块注释。注释的 界定符 和 文字 之间有空格
 * 注释
 * 释
 */

/* 注释 的主要应用场合：
 * 1. 整个源文件的顶部注释。文件信息（文件名、作者、版本历史等），不缩进
 * 2. 函数注释。说明函数的功能、参数、返回值、错误码等。在函数上方，缩进与函数名对齐
 * 3. 语句组注释。解释一组语句。写在语句上方，与该语句之间没有空行，缩进与该语句对齐
 * 4. 单行注释。解释一句语句。写在语句右侧，与语句至少要隔开一个空格
 * 5. 其他：尽量少用单行注释，只在必要的时候使用：写注释一般是为了说明做什么，而代码直接说
 * 了怎么做，所以没有必要说明怎么做。但复杂的宏定义、变量声明等，也可能要注释。
 */
```

#### 缩进、空格、换行

空格、tab、空行都不是C语言的 Token，无关语法，仅仅是为了易读。

- 空格

  ```c
  /* 关键字 for while if 与控制表达式 (1 > 0) 要有一个空格，()括号内部的表达式紧贴括号 */
  while (1) { }
  /* 双目运算符 有空格，单目运算符没有空格 */
  i = i + 1; i++; !(i < 1); -i;
  /* 后缀运算符不加空格，比如结构体/数组元素读取、函数调用、 */
  s.a
  a[i]
  foo(arg1)
  /* 按照英文书写习惯，逗号、分号后要加空格 */
  for (i = 1; i < 10; i++)
  foo(arg1, arg2)
  /* 空格并不是死规定，可以根据需要裁决，但一切为了易读 */
  for (i=1; i<10; i++) // 易读
  len = sqrt(x*x + y*y) // 易读
  a||b && C // 合法，但很难读，容易误导 运算符优先级
  /* Unix 标准终端 24行 80列，超过 80 就要分行。分行后用空格对齐 */
  if (sqrt(x*X + y*y) > 5.0 && x < 0.0 && y > 0.0)  // 实际上这句不要换行，仅表示‘如何对齐’
  if (sqrt(x*X + y*y) > 5.0 
      && x < 0.0 
      && y > 0.0)
  printf("This is such a long sentence that "
         "it cannot be held within a line.\n"); //长字符串换行。编译器会拼接相邻的字符串
  /* 变量名对齐会很好看（但很费事，不要求） */
  int     a, b;
  double  c;
  ```

- 缩进

  缩进是为了体现语句块的层次关系。只能用tab 缩进，一个 tab 等于 8 空格。

  ```c
  /* 控制流语句的 { } 不单独一行 */
  if (1 > 0) {
      printf();
  } else if (2 > 3) {
      printf();
  } else {
      printf();
  }

  switch (c) {
  case 'a': //case、default 不缩进，但下面的语句要缩进
      printf();
  case 'b':
      printf();
  default:
      printf();
  }

  if (a == 0) {
      goto error;
  }
  error:  // goto label 标签 一定不能有任何缩进 
      printf("divide by zero");

  int func_foo(void) /* 函数定义，大括号 { } 单独一行 */
  {
      printf();
  }
  ```

- 空行

  ```c
  每个函数尽可能简单，更容易维护
  1. 一个函数只是为了做好一件事，别想用途广泛、面面俱到，否则又臭又长、维护困难
  2. 缩进太多也表示复杂度太高。一般少于 4 层
  3. 长度不要超过 48 行（终端 2 屏）。但如果是概念简单，而只是长度很长，没有关系。比如 switch 有 n 多个 case
  4. 局部变量太多也说明太复杂。最好不要超过 10 个
  5. 函数名最好是一个动词。重要的函数上方应该加上注释。说明函数功能、参数、返回值、错误码等
  /* 函数：如果函数太长，内部也可能要加 空行 分隔。根据语句逻辑关系分段，一般：变量定义；执行语句；return语句 */
  int main(void)
  {
      int     a, b;
      double  c;
  
      printf("a");
      printf(0);
  
      return 0;
  }
  
  /* 一个源文件：每个逻辑段落用空行隔开。头文件、全局变量定义、函数定义、另一个函数定义... */
  #include <stdio.h>
  
  int g;
  double h;
  
  int func_foo(void) 
  {
      printf();
  }
  
  int main(void) 
  {
      func_foo();
      func_bar();
  }
  ```

  

#### 自动化格式代码 indent

Linux 有一个专门格式化 C 语言代码的工具。它会直接修改源文件，而不是打印到终端。不对空行做任何操作，不加不减。

```sh
$ indent -kr -i8 main.c # -kr 指定代码风格。-i8 指定一个tab等于多少空格
```



## 调试 和 gdb

程序的错误分为：

- 编译时错误Compile-time，语法错误，过不了编译检查。
- 运行时错误Run-time，编译生成了可执行文件，但在运行时出错。
- 与设计不符。编译和运行都没有问题，但是没有做该做的事。

编写程序，建议切分为小模块开始，每个模块都进行调试，步步为营，然后再扩大。
