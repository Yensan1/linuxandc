[TOC]


### 命令模式

设计任务：有个控制面板，有7 个卡槽可以接入各种设备，而每个卡槽都有ON/OFF按钮，最后还有个UNDO撤销按钮，撤销上一个按钮的操作。每个设备都是写好的一个个类，有着不同的接口，比如Light、CellingFan、TV等等。

```ditaa
+-------+-------+-------+-------+-------+-------+-------+--------+
| slot1 | slot2 | slot3 | slot4 | slot5 | slot6 | slot7 |        |
|  ON   |  ON   |  ON   |  ON   |  ON   |  ON   |  ON   | <UNDO> |
|  OFF  |  OFF  |  OFF  |  OFF  |  OFF  |  OFF  |  OFF  |        |
+-------+-------+-------+-------+-------+-------+-------+--------+
```

##### 简单实现

可能会写成这样（以Light类为例）：

```java
import java.util.LinkedList;
class Light {
    public void on(){ System.out.println("Light on--"); }
    public void off(){ System.out.println("Light off.."); }
}
class SimplePanel {
    LinkedList<int[]> historys = new LinkedList<int[]>(); // 记录操作历史
    public void press_ON(int slot){
        historys.addFirst(new int[] {slot, 1}); //记录历史，1代表ON
        switch (slot) { // 假设卡槽1是灯具，其余暂为空
            case 1: (new Light()).on(); break;
            default:                    break;
        } // 其他卡槽，写法相似，略去
    }
    public void press_OFF(int slot){
        historys.addFirst(new int[] {slot, 0});  //记录历史，0代表OFF
        switch (slot) {
            case 1: (new Light()).off(); break;
            default:                     break;
        }
    }
    public void press_UNDO(){
        if (historys.isEmpty()) {
            System.out.println("historys is Empty");
        } else {
            int hs[] = historys.removeFirst();
            if (hs[1] == 0) { press_ON(hs[0]); } else { this.press_OFF(hs[0]); }
            historys.removeFirst();
        }
    }
    public static void main(String[] args) {
        Panel panel = new Panel();
        panel.press_ON(1); // 按下 卡槽1 的 ON 按钮
        panel.press_OFF(1); // 按下 卡槽1 的 OFF 按钮
        panel.press_UNDO();  // 按下 UNDO 按钮
        panel.press_UNDO();
        panel.press_UNDO();
    }
}
```

可以看到，Panel类 直接操作 Light类，「发出动作请求的对象」与「接收和执行这些动作请求的对象」高度耦合。Panel类`press_ON()`和`press_OFF()`内部有7个case分支分别对应7个卡槽，而每个分支内部的逻辑都和插槽关联的设备操作流程有关，所以逻辑各不相同。一旦某个卡槽更改设备，或者这个设备的操作流程改变，那么Panel类必须改变，维护成本很高。

##### 命令模式

命令模式可以把「发出动作请求的对象」从「接收和执行这些动作请求的对象」解耦。解耦后，两者通过命令对象进行沟通。命令对象，就是`接受者 + 动作`的封装。

在餐厅中，食客、服务员、厨师一般通过菜单订单进行交流：食客从菜单选菜，写在订单里；服务员把菜单交给厨师；厨师做菜；把端出去。服务员把菜单交给厨师时，不需要更多沟通厨师就知道怎么做，因为菜单包含了“需要做哪些菜，怎么做”的命令——服务员和厨师是彻底解耦的。


```plantuml
Client -> Client : create( command )
Client -> Invoker : set( command )
Invoker -> Receiver : execute( command )
Receiver -> Receiver : executing。。。
```

把方法调用封装起来，也就是把一个运算块（操作流程）包装成一个命令对象，那么调用它的使用者不需要知道细节，只需要调用哪一个运算块。这样，就可以随心所欲地调用（替换不同的动作、执行者）、传递（队列）和存储（还原、撤销、日志）了。

```plantuml
Customer -> Customer : create( order )
Customer -> Waiter : take( order )
Waiter -> Chef : up( order )
Chef -> Chef : do( food )
```


调用者——服务员；命令对象——菜单；执行者——厨师

命令模式：把方法调用封装起来，也就是把一个运算块（操作流程）包装成一个命令对象，这样，调用者不需要知道细节，只需要调用这个运算块。从而，就可以随心所欲地调用、传递（队列）和存储（记录调用过程、还原、撤销）了。

命令模式可以把「发出动作请求的对象」从「接收和执行这些动作请求的对象」解耦。解耦后，两者通过命令对象进行沟通。命令对象封装了接受者、一个或者一组动作。


用命令模式重新实现控制面板：

```java
import java.util.LinkedList;
class Light {   // Receiver
    public void on(){ System.out.println("Light on--"); }
    public void off(){ System.out.println("Light off.."); }
}

interface Command { public void execute(); }  // Command
class DummyCmd implements Command { public void execute(){} }

interface Command { public void execute(); }
class DummyCmd implements Command { public void execute(){//Null对象} }

class LightONCommand implements Command {
    Light light;
    public LightONCommand(Light lt){ light = lt; }
    public void execute(){ light.on(); } // 命令对象将请求委托给接收者
}
class LightOFFCommand implements Command { // 命令对象：绑定 执行者 和 一组动作
    Light light; // 执行者对象、一组动作在代码中已经绑定
    public LightOFFCommand(Light lt){ light = lt; }
    public void execute(){ light.off(); } // 调用方法，就会调用执行者的一组动作
}
class Panel {
    private Command[][] commands; // 下标1是卡槽号，下标2用二进制表示ON/OFF
    LinkedList<int[]> historys; // 用堆栈记录历史，实现多步撤销
    public Panel() {
        historys = new LinkedList<int[]>();
        commands = new Command[8][2];  // 不存在卡槽0，所以要8个
        Command dummy = new DummyCmd(); // null object 空对象，什么也不做
        for (int i = 0; i <  commands.length; i++) {
            for (int j = 0; j < commands[i].length; j++) {
                commands[i][j] = dummy; } } // 占位，避免语法错误
    }
    public void setCommand(int slot, Command ON, Command OFF){ // Invoker 封装者调用者
        commands[slot][0] = OFF;  commands[slot][1] = ON; } // 将命令对象储存在调用者对象中
    public void pressButton(int slot, int bin){
        historys.addFirst(new int[] {slot, bin}); // 记录历史
        commands[slot][bin].execute(); // 调用命令对象，执行方法
    }
    public void pressUNDO(){
        if (historys.isEmpty()) {
            System.out.println("historys is Empty");
        } else {
            int hs[] = historys.removeFirst();
            if (hs[1] == 0) { pressButton(hs[0], 1); } else { pressButton(hs[0], 0); }
            historys.removeFirst();
        }
    }
    public static void main(String[] args) { // Client 用户
        Panel panel = new Panel();  Light light = new Light();
        LightONCommand lightON = new LightONCommand(light);  // 传入执行者对象
        LightOFFCommand lightOFF = new LightOFFCommand(light); //创建命令对象
        panel.setCommand(1, lightON, lightOFF); //动态设置一个卡槽的ON/OFF对应什么命令
        panel.pressButton(1, 1); // 调用命令对象的 execute()
        panel.pressButton(1, 0); // 请看，调用的过程完全与执行者解耦
        panel.pressUNDO(); panel.pressUNDO(); panel.pressUNDO();
    }
}
```

类图UML

```plantuml
Client ..> Invoker  : 1_创建调用者
Client ..> Receiver : 2_创建执行者
interface Command
Command : {abstract} abstract execute()
Command : {abstract} abstract undo()
Command <|-down- ConcreteCommand
ConcreteCommand : {field} 命令对象封装执行者 和 动作
ConcreteCommand : execute(){ }
ConcreteCommand : undo()
Receiver <.left. ConcreteCommand
Client ..> ConcreteCommand : 3_传入执行者，创建命令对象
Invoker .left.> Command : 4_将命令对象存进调用者
Client ..> Invoker : 5_调用者触发命令
```

**探讨**

- 接收者有必要单独存在吗？为什么不可以命令对象和接收者合为一体？就像下面那样？

  ```java
  class Light{
      String location;
      public Light(String location) { this.location = location; }
      public void on(){System.out.println(location + getClass().getName() + " on--"); }
      public void off(){System.out.println(location + getClass().getName() + " off..");}
  }
  class LightONCommand {
      Light light = new Light("Living Room");
      public void execute(){ light.on(); }
      public void undo(){ light.off(); }
  }
  public static void main(String[] args) { // 调用
    LightONCommand lightONCommand = new LightONCommand();
    lightONCommand.execute();
  }
  ```

  这样做，虽然使用命令对象会便利一点，但是高度耦合，就没法更换接收者了。

##### 状态撤销

CellingFan有4档，如果想把遥控器相邻两个槽位设置如下（假如设在1、2槽），并且实现撤销，那么会出现OFF——High——Low——Medium的操作流，按下撤销按钮，依次效果是Low——High——OFF。这用上面的代码是不能实现的。

```ditaa
| slot1 | slot2 |    | slot1 | slot2 |
|  ON   |  ON   |    | High  |  Low  |
|  OFF  |  OFF  |    | Medium|  OFF  |
```

状态撤销2个要素：1.在命令内部记录状态；2.在每个命令对象内部实现`undo()`

```java
import java.util.LinkedList;
class Light {
    String name;
    public Light(String s) { name = s; }
    public void on(){ System.out.println(name + "Light on--"); }
    public void off(){ System.out.println(name + "Light off.."); }
}
class Stereo {
    String name;
    public Stereo(String s) { name = s; }
    public void on(){ System.out.println("Stereo on--"); }
    public void off(){ System.out.println("Stereo off.."); }
    public void setCD(){ System.out.println("setCD"); }
    public void setVolume(){ System.out.println("setVolume"); }
}
class CeilingFan {
    public static final int High = 3, Medium = 2, Low = 1, Off = 0;
    String name;
    int speed;
    public CeilingFan(String s) { name = s; speed = Off; }
    public int getSpeed() { return speed; }
    public void off(){
        System.out.println(name + "CeilingFan off.."); speed = Off;}
    public void high(){
        System.out.println(name+"CeilingFan High speed"); speed = High;}
    public void medium(){
        System.out.println(name+"CeilingFan Medium speed"); speed = Medium;}
    public void low(){
        System.out.println(name+"CeilingFan Low speed"); speed = Low;}
}
class GarageDoor {
	String location;
	public GarageDoor(String location) {this.location = location;}
	public void up(){System.out.println(location + "Door is Up");}
	public void down(){System.out.println(location + "Door is Down");}
	public void stop(){System.out.println(location + "Door is Stopped");}
	public void lightOn(){System.out.println(location + "light is on");}
	public void lightOff(){System.out.println(location + "light is off");}
}
interface Command { void execute(); void undo();}
class DummyCmd implements Command {public void execute(){} public void undo(){} }
class LightONCommand implements Command {
    Light light;
    public LightONCommand(Light lt){ light = lt; }
    public void execute(){ light.on(); }
    public void undo(){ light.off(); }
}
class LightOFFCommand implements Command {
    Light light;
    public LightOFFCommand(Light lt){ light = lt; }
    public void execute(){ light.off(); }
    public void undo(){ light.on(); }
}
class StereoONwithCDcommand implements Command {
    Stereo stereo;
    public StereoONwithCDcommand(Stereo t) { stereo = t; }
    public void execute() {
        stereo.on(); stereo.setCD(); stereo.setVolume();
    }
    public void undo(){ stereo.off(); }
}
class StereoOFFcommand implements Command {
    Stereo stereo;
    public StereoOFFcommand(Stereo t) { stereo = t; }
    public void execute() {stereo.off();}
    public void undo(){
        stereo.on(); stereo.setCD(); stereo.setVolume();
    }
}
class CeilingFanCommand implements Command {
    CeilingFan fan; int prevSpeed;
    LinkedList<Integer> speeds = new LinkedList<Integer>();
    public CeilingFanCommand(CeilingFan ceilingFan) { fan = ceilingFan; }
    public void execute(){}
    protected void execute(int state) {
        speeds.addFirst(state);
        if (state == CeilingFan.Off   ) { fan.off();   }
        if (state == CeilingFan.Low   ) { fan.low();   }
        if (state == CeilingFan.Medium) { fan.medium();}
        if (state == CeilingFan.High  ) { fan.high();  }
    }
    public void undo() {
        int state = speeds.removeFirst();
        execute(state);
        speeds.removeFirst();
    }
}
class CeilingFanHighCommand extends CeilingFanCommand{
    public CeilingFanHighCommand(CeilingFan ceilingFan) {super(ceilingFan);}
    public void execute() { super.execute(3); }
}
class CeilingFanMediumCommand extends CeilingFanCommand{
    public CeilingFanMediumCommand(CeilingFan ceilingFan) {super(ceilingFan);}
    public void execute() { super.execute(2); }
}
class CeilingFanLowCommand extends CeilingFanCommand{
    public CeilingFanLowCommand(CeilingFan ceilingFan) {super(ceilingFan);}
    public void execute() { super.execute(1); }
}
class CeilingFanOffCommand extends CeilingFanCommand{
    public CeilingFanOffCommand(CeilingFan ceilingFan) {super(ceilingFan);}
    public void execute() { super.execute(0);}
}
class GarageDoorUpCommand implements Command {
    GarageDoor door;
    public GarageDoorUpCommand(GarageDoor garageDoor) {door = garageDoor;}
    public void execute() {door.up();}
    public void undo() { door.down(); }
}
class GarageDoorDownCommand implements Command {
    GarageDoor door;
    public GarageDoorDownCommand(GarageDoor garageDoor) {door = garageDoor;}
    public void execute() {door.down();}
    public void undo() {door.up();}
}
class FullPanel {
    private Command[][] commands;

class Panel {
    private Command[][] commands; //面板 ON OFF 按钮，对应一个命令对象

    LinkedList<int[]> historys;
    public FullPanel() {
        historys = new LinkedList<int[]>();

        commands = new Command[8][2];
        Command dummy = new DummyCmd();
        for (int i = 0; i <  commands.length; i++) {
            for (int j = 0; j < commands[i].length; j++) {
                commands[i][j] = dummy; } }
    }
    public void setCommand(int slot, Command OFFcmd, Command ONcmd){
        commands[slot][0] = OFFcmd;  commands[slot][1] = ONcmd; }
    public String toString() {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("\nControl Panel on the wall:\n");
        for (int i = 1; i < commands.length; i++) {
            strBuf.append("\t[ slot " + i + " ] " +
                    commands[i][0].getClass().getName() +"\t"+
                    commands[i][1].getClass().getName() + "\n"); }
        return strBuf.toString();

        commands = new Command[8][2]; //前下标是卡槽号，后下标是1/ON 2/OFF
        for(Command[] cmds : commands) {
            for(Command c : cmds) { c = new DummyCmd(); }
        }

    }
    public void pressButton(int slot, int bin){
        historys.addFirst(new int[] {slot, bin});
        commands[slot][bin].execute();
    }
    public void pressUNDO(){
        if (historys.isEmpty()) {
            System.out.println("historys is Empty");
        } else {
            int hs[] = historys.removeFirst();
            commands[hs[0]][hs[1]].undo(); // 改变了撤销的机制
        }
    }
    public static void main(String[] args) {
        FullPanel panel = new FullPanel();
		Light livingRoomLight = new Light("Living Room");
		Light kitchenLight = new Light("Kitchen");
		CeilingFan ceilingFan= new CeilingFan("Living Room");
		GarageDoor garageDoor = new GarageDoor("Garage");
		Stereo stereo = new Stereo("Living Room");
		LightONCommand livingRoomLightOn = new LightONCommand(livingRoomLight);
		LightOFFCommand livingRoomLightOff = new LightOFFCommand(livingRoomLight);
		LightONCommand kitchenLightOn = new LightONCommand(kitchenLight);
		LightOFFCommand kitchenLightOff = new LightOFFCommand(kitchenLight);
		CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
        CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
        CeilingFanLowCommand ceilingFanLow = new CeilingFanLowCommand(ceilingFan);
        CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);
		GarageDoorUpCommand garageDoorUp = new GarageDoorUpCommand(garageDoor);
		GarageDoorDownCommand garageDoorDown = new GarageDoorDownCommand(garageDoor);
		StereoONwithCDcommand stereoOnWithCD = new StereoONwithCDcommand(stereo);
        StereoOFFcommand  stereoOff = new StereoOFFcommand(stereo);
		panel.setCommand(1, livingRoomLightOff, livingRoomLightOn);
		panel.setCommand(2, kitchenLightOff, kitchenLightOn);
        panel.setCommand(3, ceilingFanMedium, ceilingFanHigh);
        panel.setCommand(4, ceilingFanOff, ceilingFanLow);
        panel.setCommand(5, garageDoorDown, garageDoorUp);
        panel.setCommand(6, stereoOff, stereoOnWithCD);
        System.out.println(panel);
        for (int i = 1; i < 7; i++) {
            panel.pressButton(i, 1); // press ON
            panel.pressButton(i, 0); // press OFF
        }
        System.out.println("*** UNDO ***");
        for (int i = 1; i < 13; i++) { panel.pressUNDO(); }
    }
}
```

##### 宏命令

上面的代码，卡槽7还没用，如果要做到：ON按钮——开灯、开音响、开电视机、开DVD，开热水器；而OFF把它们全部关闭。而且要实现UNDO，怎么办？

- 方式1，命令与设备耦合，CommandON的execute()包括所有设备的操作，缺点是只要设备改动，就必须改变代码。
- 方式2，传入很多单个命令，组成一个复合命令。

```java
public class MacroCommand implements Command {
    Command[] cmds; // 用数组存储一堆命令
    public MacroCommand(Command[] commands) {cmds = commands;} // 传入命令
    public void execute(){ // 依次执行每个命令
        for (Command c : cmds) { c.execute(); }
    }
}

    public static void main(String[] args) {  // 宏命令的使用
        FullPanel panel = new FullPanel();
		Light livingRoomLight = new Light("Living Room");
        TV tv = new TV("Living Room");
		Stereo stereo = new Stereo("Living Room");
         // 这些都是普通的单个命令
		LightONCommand livingRoomLightOn = new LightONCommand(livingRoomLight);
		LightOFFCommand livingRoomLightOff = new LightOFFCommand(livingRoomLight);
		StereoONwithCDcommand stereoOnWithCD = new StereoONwithCDcommand(stereo);
        StereoOFFcommand  stereoOff = new StereoOFFcommand(stereo);
        TvOffCommand tvOffCommand = new TvOffCommand(tv);
        TvONCommand tvONCommand = new TvONCommand(tv);
        // 创建 宏命令，首先创建一个命令数组，然后将数组传进 构造器
        Command[] comON = {livingRoomLightOn, stereoOnWithCD, tvONCommand};
        MacroCommand comOnMacro = new MacroCommand(comON);
        // 创建命令数组 + 传入构造器，一步到位
        MacroCommand comOffMacro = new MacroCommand(new Command[] {livingRoomLightOff, stereoOff, tvOffCommand});
        panel.setCommand(7, comOffMacro, comOnMacro);
        System.out.println(panel);
        panel.pressButton(7, 1); // press ON
        panel.pressButton(7, 0); // press OFF
    }
}
```

##### 命令模式的其他使用

- 日程安排Scheduler、线程池、动作队列

  运算块（一个对象，和一组动作）打包封装成命令对象，它可以传输、存储。在命令对象被创建很久之后，依然可以调用；可以在不同的线程中被调用。

  比如一个工作队列：一方面，有很多个工作安排器，往LinkedList 添加命令；另一方面，有多个线程，不停从队列中取出一个命令，然后调用execute()，之后就丢弃命令，再取下一个命令……

  在这里，工作安排器不管是哪儿来的任务，是什么任务，只要满足接口，打包成命令就可以；线程与命令对象完全解耦，这个线程，可能这时做财务运算，下一步却是播放音乐。

- 日志请求

  有些程序需要记录所有的操作，比如，很多调用大型数据结构的应用，没法在每次改变都发生时快速存储。如果使用日志将上次检查点CheckPoint之后的操作都记录下来，在故障后，能重新调用这些操作，以恢复之前的状态。这种事务Transaction处理，要么完成整个操作组，要么没有完成任何的操作。有两种方法达到：

  1. 对象的序列化Serialization（转为二进制）和持久化Persistence（存储到存储器，比如硬盘）。不建议
  2. 命令模式里，新增两个方法，执行命令时，用`store()`存储，加入宕机，批量地依次用`load()`加载

  的


### 适配器与外观模式

生活中的适配器：

- 仅仅改变外观，转换接头：港行的手机充电器，与国行不同，加个转换接头
- 改变内在实现，转换器：手机充电器，将高压交流电转为低压直流电

这两种适配器，目的都是将一个接口转为另一个接口，从而符合使用者的期望。面向对象设计中，也有类似的情况。有两种方式实现适配器：多重继承语法，类适配器；组合语法，对象适配器

设计任务：矢量图形最基本的元素是点。现有一个矢量编辑器，要统一操控「图形」和「文字」2种元素，但二者接口不一样。如果改写原来的代码，问题有

- 没源代码：未必能获取源代码
- 不能随便改：改动了某个部分，其他的依赖它的部分就无法工作了。

适配器模式：将一个类的接口，转换成客户期望的另一个接口。适配器可以让原本不兼容的接口可以相互合作；另外，适配器还可以将客户与接口的实现解耦，如果要改变接口，因为隔着适配器，客户也无需修改。

##### 类适配器

类适配器，也就是用类的继承语法，实现的适配器。因为java不能类的多重继承，所以类适配器，只能用于interface接口。

```java
class Point {
    float x, y;
    public Point(float x, float y){this.x = x; this.y = y;}
    public float[] getXY(){ return new float [] {x, y};}
}
interface Operator {} // 每种shape都会有个操纵器，用户拖拽时，操控图形
class TextOperator implements Operator{ public TextOperator(Shape s){} }
interface Shape { // 图形由边框限定，边框由左下角、右上角组成。
    abstract public Point[] boundingBox();
    abstract public Operator createOperator();
}
class TextView { // 文字则由 基点、宽度、高度限定
    private Point origin; private float width, height;
    public TextView(Point origin, float width, float height) {
        this.origin = origin; this.width = width; this.height = height;
    }
    public float[] getOrigin(){ return origin.getXY(); }
    public float[] getExtent(){ return new float[]{ width, height}; }
    public Boolean isEmpty(){ return origin==null;}
}
class TextShape extends TextView implements Shape { // 继承语法
    public TextShape(Point origin, float width, float height) {
        super(origin, width, height);
    }
    public Point[] boundingBox(){ 
        float bottom , left, width, height, fa[];
        fa = getOrigin(); left = fa[0]; bottom =fa[1];
        fa = getExtent(); width = fa[0]; height = fa[1];
        return new Point[] { new Point(left, bottom),
            new Point(left + width, bottom + height)} ;
    }
    public Operator createOperator(){ return new TextOperator(this);}
}
class Editor {
    public static void main(String[] args) {
        TextShape text = new TextShape(new Point(1, 1), 2, 3);
        Point[] parray = text.boundingBox();
        Point p1 = parray[0]; Point p2 = parray[1];
        System.out.println(p1.getXY()[0] + " "+ p1.getXY()[1] + "\n"
            + p2.getXY()[0] + " "+ p2.getXY()[1] + "\n" + text.createOperator());
    }
}
```

UML图

```plantuml
interface Target
Target : request()
Client .left.> Target
Adapter .up.|> Target
Adapter : 将继承的方法，转成所需的逻辑
Adapter : request() { this.specificRequest(); }
Adaptee : specificRequest()
Adapter -up-|> Adaptee
```

##### 对象适配器

只需要将`TextShape`改写为如下，就可。

```java
class TextShape implements Shape { // 转换器 要继承 目标接口
    TextView text; // 组合语法：将需要转换的对象，嵌入转换器
    public TextShape(Point origin, float width, float height) {
        text = new TextView(origin, width, height);
    }
    public Point[] boundingBox(){ // 转换原接口
        float bottom , left, width, height, fa[];
        fa = text.getOrigin(); left = fa[0]; bottom =fa[1];
        fa = text.getExtent(); width = fa[0]; height = fa[1];
        return new Point[] { new Point(left, bottom),
            new Point(left + width, bottom + height)} ;
    }
    public Boolean isEmpty(){ return text.isEmpty(); } // 直接转发请求
    public Operator createOperator(){ return new TextOperator(this);} // 新建接口
}
```

可以看到，在转换接口时，一般会有3类情况（类适配器、对象适配器都是）：

- 转发：只是把被转换的接口，改个名字而已，直接转发就可
- 转换：需要依靠被转换的接口，按照逻辑，拼接出目标接口
- 新建：被转换的接口没有此类相关功能，需要完全从新创建

另外，请看这是一个**双向适配器**：`isEmpty()`是留给旧接口依赖者使用的（类适配器、对象适配器都是）。这样一来，新旧代码都可以用这个类，作为统一的接口了。

UML图

```plantuml
interface Target
Target : request()
Client .left.> Target
Adapter .up.|> Target
Adapter : adaptee // 组合，转成所需的逻辑
Adapter : request() { adaptee.specificRequest(); }
Adaptee : specificRequest()
Adapter *-left-> Adaptee
```

对象适配器与类适配器的对比：

| 类适配器                                     | 对象适配器                                    |
| ---------------------------------------- | ---------------------------------------- |
| java 不能类多继承，最多只能有一个超类，其他只能是interface     | 组合语法非常灵活，内嵌的对象是一个类，就可以是它的子类。并且可以内嵌多个被适配对象，化身为外观模式。 |
| 继承语法：子类可以继承，比如`isEmpty()`；子类可以回调；子类可以覆盖`boundingBox()`。 | 委托给内嵌对象。委托前后，可以灵活定制。                     |

##### 从Enumeration到Iteration

早期 java 1.0/1.1 的容器Collection类型（Vector、Stack、Hashtable），都有一个elements()方法，也就是Enumeration列举。而后来 Sun大幅更新容器类型（Array、List、Map…），它们使用的是iterator()方法，也就是迭代器Iteration。

使用适配器，将以前的代码迁移到现在的Iteration（实际上，已经迁移过去了，下例中，第二个for循环就是）。

```java
import java.util.*;
class EnumerationToIterator implements Iterator {
    Enumeration Enum;
    public EnumerationToIterator(Enumeration e){Enum = e;}
    public boolean hasNext(){ return Enum.hasMoreElements(); }
    public Object next(){return Enum.nextElement(); }
    public void remove() { throw new UnsupportedOperationException("");}
    public static void main(String[] args) {
        Vector v = new Vector(Arrays.asList(0, 1, 2, 3, 4, 5));
        Enumeration e = v.elements();
        for(EnumerationToIterator t = new EnumerationToIterator(e); t.hasNext();){
            System.out.println("" + t.next());} // 使用转换器
        for(Iterator i = v.iterator(); i.hasNext();) {System.out.print(i.next());}
    }
}
```

Iterator接口实现了`remove()`，不覆盖也是可以的。

##### 外观模式Facade

设计任务：用Curtain投影布、Projector投影仪、Amplifier功放、DvdPlayer影碟播放器、TheaterLights影院灯光、PopcornPopper爆米花机这几个设备，组建一个家庭影院，播放电影。

```java
import java.io.*;
abstract class Device {
    String description;
    public Device(String description){ this.description = description; }
    public String toString() { return description; }
    public PrintStream puts(Object ... args) { // 
        return System.out.format((description + " %s %n"), args);}
    public void on() { puts("on"); }
	public void off() { puts("off"); }
}
class Curtain extends Device {
    public Curtain(String description) { super(description);}
    public void up() { puts("going up"); }
    public void down() { puts("going down"); }
}
class Projector extends Device {
	DvdPlayer dvdPlayer;
	public Projector(String description, DvdPlayer dvdPlayer) {
		super(description); this.dvdPlayer = dvdPlayer; }
	public void wideScreenMode() { puts("in widescreen mode");}
	public void tvMode() { puts("in tv mode");}
}
class Amplifier extends Device {
	Tuner tuner;    DvdPlayer dvd;  CdPlayer cd;
	public Amplifier(String description) { super(description);}
	public void setStereoSound() { puts("stereo mode on");}
	public void setSurroundSound() { puts("surround sound on");}
	public void setVolume(int level) { puts("setting volume to " + level);}
	public void setTuner(Tuner tuner) {
        this.tuner = tuner; puts("setting tuner: " + tuner); }
	public void setDvd(DvdPlayer dvd) {
        this.dvd = dvd; puts("setting DVD player: " + dvd); }
	public void setCd(CdPlayer cd) {
        this.cd = cd; puts("setting CD player to " + cd); }
}
class DvdPlayer extends Device {
	int currentTrack; Amplifier amplifier; String movie;
	public DvdPlayer(String description, Amplifier amplifier) {
		super(description); this.amplifier = amplifier;}
	public void eject() { movie = null; puts("eject"); }
	public void play(String movie) {
		this.movie = movie; currentTrack = 0; puts("playing: " + movie);
	}
	public void play(int track) {
		if (movie == null) {
			puts("can't play track " + track + " no dvd inserted");
		} else {
			currentTrack = track;
			puts("playing track " + track + " of " + movie);
		}
	}
	public void stop() { currentTrack = 0; puts("stopped " + movie);}
	public void pause() {puts("paused " + movie);}
	public void setTwoChannelAudio() {puts("set two channel audio");}
	public void setSurroundAudio() {puts("set surround audio");}
}
class TheaterLights extends Device {
	public TheaterLights(String description) {super(description);}
	public void dim(int level) {puts("dimming to " + level + "%");}
}
class PopcornPopper extends Device {
	public PopcornPopper(String description){super(description);}
	public void pop() { puts("popping popcorn!"); }
}
public class HomeTheater {
    public static void main(String[] args) {
		PopcornPopper popper = new PopcornPopper("PopcornPopper");
		TheaterLights light = new TheaterLights("TheaterLights");
		Curtain curtain = new Curtain("Curtain");
		Amplifier amp = new Amplifier("Amplifier");
		DvdPlayer dvd = new DvdPlayer("DvdPlayer", amp);
		Projector projector = new Projector("Projector", dvd);
		String movie = "Eyes wide open";
		popper.on(); popper.pop();
		light.dim(10);
		curtain.down();
		projector.on(); projector.wideScreenMode();
		amp.on(); amp.setDvd(dvd); amp.setSurroundSound(); amp.setVolume(5);
		dvd.on(); dvd.play(movie);
    }
}
```

`HomeTheater`的播放操作非常复杂。播放后还要关闭呢？如果这个家庭娱乐中心增加了新设备，步骤又要变得更复杂……

外观模式：提供了一个统一的接口，用来访问子系统中的一群接口。外观定义了一个高层接口，让子系统更容易使用。将一个或数个类的复杂逻辑和流程隐藏在背后，只显露出一个干净美好的外观。目的是为了简化接口。

如果使用外观模式，创建一个TheaterFacade新类，它提供几个简单的方法。外观类将内部的组件视为一个子系统，通过调用这个子系统实现自己的方法（也可能会增加某些自己的操作）。这样，使用者就不必调用关系复杂的子系统了，只需要调用外观类的简单方法“快捷方式”。同时，这也将调用者与纷繁复杂的子系统解耦，如果升级设备，只需要更改外观类，而无需更改调用者。

外观模式和普通的封装很不一样的是，外观类只是提供了一个简化接口，并没有完全封装、隔绝，而是将子系统完整的功能暴露出来，需要时仍然可以调用子系统内的组件。

```java
public class TheaterFacade {
	PopcornPopper popper; Amplifier amp;Curtain curtain;
	TheaterLights light; DvdPlayer dvd; Projector projector;
	public TheaterFacade(PopcornPopper popper, Curtain curtain, Amplifier amp,
		TheaterLights light, DvdPlayer dvd, Projector projector) { // 定制设备
		this.popper = popper; this.curtain = curtain; this.amp = amp;
		this.light = light; this.dvd = dvd; this.projector = projector;
	}
	public TheaterFacade(){ // 不定制设备，使用默认设备
		PopcornPopper popper = new PopcornPopper("PopcornPopper");
		TheaterLights light = new TheaterLights("TheaterLights");
		Curtain curtain = new Curtain("Curtain");
		Amplifier amp = new Amplifier("Amplifier");
		DvdPlayer dvd = new DvdPlayer("DvdPlayer", amp);
		Projector projector = new Projector("Projector", dvd);
	}
	public void watchMovie(String movie){
		System.out.println("Get ready to watch movie");
		popper.on(); popper.pop();
		light.dim(10);
		curtain.down();
		projector.on(); projector.wideScreenMode();
		amp.on(); amp.setDvd(dvd); amp.setSurroundSound(); amp.setVolume(5);
		dvd.on(); dvd.play(movie);
	}
	public void endMovie(){
		System.out.println("movie over");
		popper.off();
		light.on();
		curtain.up();
		projector.off();
		amp.off();
		dvd.stop(); dvd.eject(); dvd.off();
	}
}
public class HomeTheater { //对比上面，调用者HomeTheater变得非常简单
    public static void main(String[] args) {
		TheaterFacade facade = new TheaterFacade();
		facade.watchMovie("Eyes wide open");
		facade.endMovie();
    }
}
```

在这个案例里，一个子系统只有一个外观类，有2个方法。实际上可以有多个外观类，每个外观类可以有多个方法。

##### 装饰者模式、适配器模式、外观模式

装饰者模式、适配器模式、外观模式，都是组合语法，看起来很相似，但是设计意图相差甚远。

- 装饰者模式：不改变接口，但增加功能。将对象包装起来，赋予它们新的职责。这样，给代码加入新行为，进行功能扩展，却无需修改原有的代码。可以无限次包裹，但绝不会进行接口转换。
- 适配器模式：将一个接口转为另一个接口。将对象包装起来，让它们看起来不像自己，而像别的东西。将类的接口转换为想要的接口，从而使用不同的库和接口，却无需修改原有的代码。
- 外观模式：让接口更简单。将对象（甚至多个对象）包装起来，从而简化接口。

虽然大多数适配器只转化一个类，但也可以内嵌多个转化类，所以适配器模式和外观模式从语法上几乎是完全等价的。它们的区别在于设计意图：适配器是转换达到适配，外观是统一、简化顺便解耦。

##### 最少知识原则

尽量减少对象之间的交互。

实践时，可以遵守下面几个语法规则，只调用以下范围内的方法：

1. 该对象自己的方法（继承语法）
2. 该对象内嵌组件的方法（组合语法）
3. 被当做方法参数传递进来的对象
4. 这个方法创建的对象

```java
class Car{
	Engine engine;     Screen screen;
	public undateScreen(){} //更新汽车面板显示器
	public void start(Key key) {
		Doors door = new Doors();
		boolean authorized = key.turns(); // 3.传入的对象的方法
		if (authorized) {
			engine.start(); // 2.这个对象内嵌组件的方法
			undateScreen(); // 1.这个对象自己创建的方法
			door.lock();   // 4.这个方法创建的对象
		}
	}
	public void play_a(){ // 与 2 个类交互
		SongList list = screen.getSongList();
		list.play();
	}
	public void play(){ screen.playSong(); } //仅和 1 个类交互
}
```

在上面的代码里，`void start(Key key)` 作为例子展示了4个实践规则。`void play_a()`是反面例子，内嵌类的方法产生的对象的方法。作为修正， `void play()`让screen对象提供一个方法，直接调用就可。

所有的设计原则，都应该在有帮助的时候才遵守。所有的设计都是折中的产物，比如抽象层级与开发效率、代码空间和运行时间之间的权衡……原则只是提供方针，但是真实设计时，就必须考虑整个工程的所有因素。

恰当设计，会减少组件的依赖，降低维护成本。但过度设计，也会导致更多的层级，就不得不创建很多包装类处理各个组件的沟通，于是软件臃肿，逻辑复杂，开发困难，运行缓慢。



### 模板方法模式

设计任务：用java开发泡茶和冲咖啡的自动机

##### 简单实现

```java
class Coffee {
	public void prepareRecipe(){ // 冲咖啡四步走
		boilWater(); brewCoffeeGrinds();
		pourInCup(); addSugarAndMilk();}
	void boilWater(){ System.out.println("Boiling water"); } //烧开水
	void brewCoffeeGrinds(){ System.out.println("Dripping Coffee");} //过滤咖啡
	void pourInCup(){ System.out.println("Pouring into cup");} //倒进杯子
	void addSugarAndMilk(){System.out.println("Adding Sugar&Milk");} //加糖加奶
}
class Tea {
	public void prepareRecipe() { // 泡茶4步
		boilWater(); steepTeaBag();
		pourInCup(); addLemon();  }
	void boilWater(){ System.out.println("Boiling water"); } //烧开水
	void steepTeaBag(){ System.out.println("Steeping the tea");} // 投入茶包
    void pourInCup(){ System.out.println("Pouring into cup");} //倒进杯子
    void addLemon(){ System.out.println("Adding Lemon");} //加柠檬
}
public class SimpleBarista {
	public static void main(String[] args) {
		System.out.println("Making tea...");
		Tea tea = new Tea(); tea.prepareRecipe();
		System.out.println("Making coffee...");
		Coffee coffee = new Coffee(); coffee.prepareRecipe();
	}
}
```

##### 模板方法模式

但是Coffee和Tea两个类，代码重复率太高了，改善一下设计，增加一个抽象类，将共同的部分放进去：

```java
abstract class CaffeineBeverage{
	abstract void prepareRecipe(); // 抽象方法
	void boilWater(){ System.out.println("Boiling water"); } //烧开水
	void pourInCup(){ System.out.println("Pouring into cup");} //倒进杯子
}
class Coffee extends CaffeineBeverage{
	public void prepareRecipe(){boilWater();brewCoffeeGrinds();pourInCup();addSugarMilk();}
	void brewCoffeeGrinds(){ System.out.println("Dripping Coffee");} //过滤咖啡
	void addSugarMilk(){System.out.println("Adding Sugar&Milk");} //加糖加奶
}
class Tea extends CaffeineBeverage{
	public void prepareRecipe(){boilWater();steepTeaBag();pourInCup();addLemon();}
	void steepTeaBag(){ System.out.println("Steeping the tea");} // 投入茶包
    void addLemon(){ System.out.println("Adding Lemon");} //加柠檬
}
```

但抽象程度仍然不够。`brewCoffeeGrinds()steepTeaBag()`冲咖啡和泡茶，不都是将固形物溶解到水中吗？`addSugarMilk()addLemon()`加糖和奶，加柠檬，不都是加调料吗？将这两个方法泛化，从而进一步抽象：

```java
abstract class CaffeineBeverage{ // 抽象类
	public final void prepareRecipe(){ // final 不让子类覆盖。冲饮4步走
		boilWater();   soak();  // 烧开水，溶解
		pourInCup();   addCondiment(); // 倒进杯子，加调料
	}
	abstract void soak(); // 原语操作，必须是抽象方法，依赖子类实现
	abstract void addCondiment();
	final void boilWater(){System.out.println("Boiling water");}//自己实现，并且final禁止子类覆盖
	void pourInCup(){System.out.println("Pouring into cup");}//钩子hook，已经实现，但子类可以定制
	void hook(){} // 钩子hook，默认不做任何事的方法。子类自行决定要不要覆盖
}
class Coffee extends CaffeineBeverage{
	void soak(){ System.out.println("Dripping Coffee");}
	void addCondiment(){System.out.println("Adding Sugar&Milk");}
}
class Tea extends CaffeineBeverage{
	void soak(){ System.out.println("Steeping the tea");}
    void addCondiment(){ System.out.println("Adding Lemon");}
}
```

抽象类`CaffeineBeverage`的`prepareRecipe()`就是一个模板方法。它规定了算法（一个步骤框架），里面规划了一个个方法，比如`boilWater()`，这些步骤方法中，有些方法是超类实现的，有些方法则是子类实现的。模板方法，与步骤的具体实现是解耦的。

注意：算法内的步骤要合适。太细碎，子类继承时非常麻烦；太大，每个步骤就会过于臃肿。

与前两个代码对比，好处：代码复用最大化。超类提供抽象的算法，而子类提供具体的实现，更清晰易懂。超类控制和保护了步骤框架，子类没法乱来。如果要改变算法（或步骤），只需要改变超类，所以容易维护。如果要新增子类，只需要实现自己的方法，所以容易扩展。

模板方法模式：父类在一个方法中定义一个算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以在不改变算法结构的情况下，重新定义算法中的某些步骤。

模板方法的这种继承，父类规划子类实现的关系，特殊化后，就是抽象工厂方法。同时，封装算法的目的，也很像策略模式。

- 策略模式：定义一个算法族，使用**对象组合语法**把算法封装到一个个对象，并让这些算法可以互换。运行时，改用不同的策略对象，就可以改变算法。非常灵活，富有弹性。
- 模板方法模式：超类定义一个算法的大纲，而由子类定义某些步骤的实现。所以，个别步骤可以有不同的细节，但算法的结构依然维持不变。父类对算法有更多的控制权，而且没有冗余代码。这是最常用的模式，代码复用也最高效。高效复用，父类保持对算法的控制，又保留灵活性。

##### 模板与灵活性：abstract、final与hook

模板，有些是必须的，有些是固定的，有些是可有可无的。在抽象类的模板方法中，用abstract、final与hook，就可以创造很丰富的模板类型。hook既可以是空方法，也可以提供默认的实现。`abstract`抽象方法必须由子类实现，`final`超类已经实现并且锁定，hook则非常灵活。

```java
abstract class TemplateClass{ //必须抽象类
	public final void template(){ //必须final
		primitiveMethod(); //原语操作，抽象方法依赖子类，子类必须实现
		concreteMethod(); //固定操作，自己实现并且final，子类无法覆盖
		hook_a(); //钩子hook，已经实现。但子类仍然可以覆盖定制
		hook_b(); // 钩子hook，默认不做任何事的方法。子类自行决定要不要覆盖
	}
	abstract void primitiveMethod();
	final void concreteMethod(){System.out.println("");}
	void hook_a(){System.out.println("hook_a");}
	void hook_b(){} 
}
```

- hook不仅仅可以像上面那样，简单地让子类覆盖，还可以更灵活地使用，比如下例中，子类覆盖钩子后，居然影响了抽象类的模板方法流程，决定了下一个即将发生的操作`addCondiment()`：

  ```java
  abstract class CaffeineBeverage{
      public final void prepareRecipe(){
          boilWater(); soak(); pourInCup();
          if (customerWantCondiment()){addCondiment();} //顾客要求了，才加调料
      }
      abstract void soak();  // 原语操作
      abstract void addCondiment();
      final void boilWater(){System.out.println("Boiling water");} // 固定操作
      final void pourInCup(){System.out.println("Pouring into cup");}
      boolean customerWantCondiment(){ return true; } //hook缺省值是true
  }
  class Coffee extends CaffeineBeverage{
      void soak(){ System.out.println("Dripping Coffee");}
      void addCondiment(){System.out.println("Adding Sugar&Milk");}
      boolean customerWantCondiment(){
          System.out.println("Would you need milk and sugar");
          Scanner input = new Scanner(System.in);
          String answer = input.nextLine();
          if (answer.toLowerCase().equals("y")){ return true; } //根据回复返回布尔值
          return false;
      }
  }
  ```

##### 好莱坞法则，稳定依赖

```plantuml
CaffeineBeverage -() MajorBarista
CaffeineBeverage : public final void prepareRecipe()
CaffeineBeverage : {abstract}abstract void soak()
CaffeineBeverage : {abstract}abstract void addCondiment()
CaffeineBeverage : final void boilWater(){} 
CaffeineBeverage : final void pourInCup(){}
CaffeineBeverage : boolean customerWantCondiment(){} 
CaffeineBeverage <|-- Coffee
Coffee : void soak()
Coffee : void addCondiment()
Coffee : boolean customerWantCondiment(){} 
CaffeineBeverage <|-- Tea
Tea : void soak()
Tea : void addCondiment()
```

依赖方向由高到底，控制方向也由高到低：

- 使用者MajorBarista，只依赖抽象的CaffeineBeverage（因为调用的所有的接口都在CaffeineBeverage），而不依赖具体的Tea或Coffee，这可以减少整个系统的依赖（依赖倒置原则）
- CaffeineBeverage抽象类是高层组件，控制了冲泡算法，而且控制了什么时候调用子类。
- Tea/Coffee子类是低层组件，只是提供一些实现，但绝不直接调用抽象类。

依赖关系非常复杂，高层依赖低层、低层又依赖高层、高层依赖边侧组件、边侧组件依赖低层，这时候容易产生依赖腐败，非常难读懂和维护。创建框架时，主要就是使用继承语法，过多的继承外加复杂依赖，就特别容易导致依赖腐败。在**模板方法模式**里，利用好莱坞原则，高层低层相互依赖，共同实现算法，然而，只由高层控制何时、如何调用低层组件，这就是稳定依赖避免依赖腐败。好莱坞原则常用于框架和组件。

这并不是说，低层组件绝对不能调用高层组件的方法。低层组件常常会调用从超类继承的方法，但是要避免高层低层明显的环状依赖。

好莱坞原则与依赖倒置原则：依赖倒置是尽量避免使用具体类，而多使用抽象。好莱坞原则是创建框架或组件的技巧，创建一个弹性的架构，让低层组件能够参与运算流程，但又不会让高层组件和其他类过于依赖低层组件。二者的目标都是解耦，但是依赖倒置原则更加注重「避免依赖」。

##### JDK中的模板方法

- **Arrays排序**

  ```java
  import java.util.Arrays;
  class Person implements Comparable<Person> { //继承
      String name;       int age;
      public Person(String name, int age){this.name = name; this.age = age;}
      public String toString() { return name + " age: " + age;}
      public int compareTo(Person object) { // 传入另一个对象，与自己比较
          if (this.age < object.age) { return -1; } //自己更小，返回 -1
          else if (this.age == object.age) { return 0; } //相等返回0
          else { return 1; } //自己更大，返回 1
      }
  }
  public class AgeSort {
      public static void main(String[] args) {
          Person[] staff = {
              new Person("Daffy", 8), new Person("Dewey", 2),
              new Person("Howard", 7), new Person("Louie", 2),
              new Person("Donald", 10), new Person("Huey", 2) };
          System.out.println("Before sorting:");
          for (Person p: staff) { System.out.print(p + "\t"); }
          Arrays.sort(staff);
          System.out.println("\nAfter sorting:");
          for (Person p: staff) { System.out.print(p + "\t"); }
      }
  }
  ```

  这是怎样实现的呢？Arrays类有个sort静态方法，然后调用归并排序算法mergeSort（现在已经优化为TimSort）。这两者，就是模板方法。

  ```java
  // java 数组语法。从语法可知，每个数组都是不同类型。所以Arrays类只提供操作数组方法，不让继承
  Type[] array;  // 声明一个数组变量（栈空间）
  int[] a;
  array = new Type[N];   // 赋值方式1：为这个数组变量开辟堆空间，只指明数量，并不存入元素
  a = new int[9];
  array = new Type[] {a, b, c}; // 赋值方式2：使用初值表，开辟空间，同时存入元素
  a = new int[] {1, 2, 3};
  //java.util.Arrays.java 的sort伪代码。
  public class Arrays{ // Arrays类，每个接口都static，才能用于所有的数组
      public static void sort(Object[] a){
          Object[] work = (Object[])a.clone(); //现拷贝一份
          mergeSort(work, a, 0, a.length, 0); //归并排序算法。现已改为TimSort
      }
      private static void mergeSort(Object[] src, Object[] dest,
                                    int low, int high, int off) {
          for (int i = low; i < high; i++)
              for (int j = i; j > low &&  // 必须提供比较的方法
                      ((Comparable) dest[j - 1]).compareTo(dest[j]) > 0; j--)
                  swap(dest, j, j - 1);
          return;
      }
  }
  ```

  而在排序算法中，规定必须继承`interface  Comparable`，实现`compareTo()`接口。

  模板方法模式，父类在一个方法中定义一个算法的骨架，而将一些步骤延迟到子类中。而这个排序过程，虽然没有使用继承语法，但本质上依然是模板方法模式。而且，不需要继承就可以使用模板，这变得更有弹性、更有用。

  staff 借助 Arrays.sort实现了排序，这很像策略模式：借助别的对象，实现自己的功能。但是策略模式是组合的类实现了完整的算法，而不是像这样需要补充一个`compareTo()`

- **Swing的Jframe**

  ```java
  import java.awt.*;   import javax.swing.*;
  public class MyFrame extends JFrame {
  	private static final long serialVersionUID = 2L;
  	public MyFrame(String title) {
  		super(title); // 窗口的标题
  		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  		this.setSize(300,300); this.setVisible(true);
  	}
  	public void paint(Graphics graphics) { //这是个hook，JFrame的update()是模板方法
  		super.paint(graphics);
  		String msg = "print String in this window!!"; //窗口显示的消息
  		graphics.drawString(msg, 100, 100);
  	}
  	public static void main(String[] args) {
  		MyFrame myFrame = new MyFrame("Name of Window");
  	}
  }
  ```

- **Applet**——applet大量使用钩子。

  ```java
  import java.applet.Applet; import java.awt.Graphics;
  public class MyApplet extends Applet {
  	private static final long serialVersionUID = 2L;
      String message;
      public void init(){ //钩子，applet的初始化操作。
          message = "Hello World, I'm alive!";
          repaint(); // 这是Applet的方法，重绘这个applet
      }
      public void start(){ //钩子，applet正要被显示在网页时的动作
          message = "Now I'm starting up...";
          repaint();
      } 
      public void stop(){ // 钩子，如果用户调到别的网页，就要停止applet
          message = "Oh, now I'm being stopped...";
          repaint();
      }
      public void destroy(){ // 钩子，如果用户关闭浏览器，就注销applet
          message = "Goodbye, cruel world";
          repaint();
      }
      public void paint(Graphics g) { //钩子，和Jframe一样
          g.drawString(message, 5, 15);
      }
  ```

- **JDK其他的模板方法模式**：`java.io.InputStream`的`public int read(byte b[], int off, int len) throws IOException`是模板方法，子类必须实现`public abstract int read() throws IOException`


