[TOC]

## 数组Array

### 定义与常见操作

数组是一种复合数据类型，由一系列相同类型的元素 Element组成，元素的存储空间也是相邻的。类型表明每个元素的存储长度和操作，而`[n]`表明这个数组的长度。

```c
/* 声明数组 type var[LEN]; C99之前的标准，len必须是编译时确定的字面量 */
int count[4]; //定义了一个由4个int元素组成的集合，存储大小一致、且空间相邻，名字分别为a[0]...a[3]
int count[] = {3,2,1,0,}; //定义的同时进行初始化，就不必要指定长度
int count[4] = {3,2}; //指定长度同时初始化，按照顺序赋值。未指定就填0。结果 {3,2,0,0}
int count[4] = {[0]=1, [1]=3, 6,}; // 指定位置，未指定填0，结果{1,3,6,0}。适合稀疏矩阵，只限于C99

/* 定义语句 int count[N]; 里N是长度。count[N]访问时，N是下标index。下标起始计数是 0 
 * 大多数编程语言起始都是 0 zeroth。非常方便：a[0]是开头，而跳过 i 个单元就是 a[i]
 * count[N] 不仅可以表示存储单元中的值（元素的值），还可以表示存储单元本身（元素） */
printf("%d", count[0]); 
count[1] = count[0] * 2; /* 既可以做 左值， 也可以做 右值 */
int i = 10;
count[i] = count[i+1] /* N 可以是表达式，但是必须是整型 */
++count[2]; /* 虽然合法，但不建议。count[2] = count[2] + 1;  */
int a = count[100] /* 一定不能越界访问。越界可以编译，属于运行时错误。有时非常隐蔽：访问越界时不崩溃，在后面执行正确的语句，使用了count[100]所在的地址才崩溃 */

/* 数组与输入 */
scanf("%d", &count[0]);
/* 计算数组的长度，防止硬编码 */
printf("size: %lu\n", sizeof(count)); // 16。算出整个数组的内存占用
printf("len: %lu\n", sizeof(count)/sizeof(count[0])); // 4 这是数组长度

/* 注意：复制数组，下面这种方式，编译不会通过 */
int a[] = {1, 2, 3, 4,};
int b[] = a; // 数组变量，不能被赋值。
/* 正确方式：把一个数组的元素，复制到另一个数组 */
int a[] = {1, 2, 3, 4, };
int b[sizeof(a)/sizeof(a[0])];
for (int i; i < sizeof(a)/sizeof(a[0]); i++)
    b[i] = a[i];
```

数组越界，越界读取可能不出现问题，只有编译警告。但是，越界写入很容易segmengfault

```c
void fun(){
    int a[10] = {1};
    printf("%d\n", a[11]); // 越界读
    a[11] = 0; // 越界写入
    printf("%d\n", a[11]); // 0  Abort trap: 6 这句正确输出了，之后程序就崩溃了
}
int main() {fun(); return 0;}
```

在C99之前，数组的长度必须编译时确定。但是，C99 可以在运行时**动态确定**（更为常用的是使用指针。见指针动态内存章节）。比如，输入个数 N，然后输入 N 个数，统计平均值：

```c
void avg(){
    double sum = 0.0;
    int N = 0, tmp=0;
    printf("请输入数量："); scanf("%d", &N);
    if (N > 0) { // sizeof(input)/sizeof(input[0]) 可以防止硬编码
        int input[N]; // N 是运行时确定的
        printf("请输入数值，空格分隔：");
        for (N=0; N < sizeof(input)/sizeof(input[0]); N++){
            scanf("%d", &tmp);
            sum += input[N] = tmp;
        }
    }
    printf("平均值：%0.3f\n", sum / N);
}
```

数组遍历。一般用for，注意条件是`i<len`，否则`i<=len` 越界。用`sizeof(a)/sizeof(a[0])`可以防止硬编码。另外，要注意，遍历之后，`i==len`，此时使用`i`作为下标，正好越界。

```c
int i, count[4] = {3, 2, };
for (i=0; i < sizeof(count)/sizeof(count[0]); i++)
    printf("count[%d]=%d\n", i, count[i]);
count[i]; // 注意，遍历后，i==4 正好越界
```

数组作为参数。数组作为参数，必须指定 length

```c
int search(int key, int arr[], int len){ // 注意接收函数的声明
    for (int i=0; i < len; i++) // 这里无法用 sizeof 算出len
        if (arr[i] == key)
            return i;
    return -1;
}
int main() {
    int x, loc, a[] = {1,2,3,4,23,46,5,6,7,8,9,13};
    printf("请输入要查找的数："); scanf("%d", &x);
    loc = search(x, a, sizeof(a)/sizeof(a[0]));
    if (loc != -1) printf("%d 在位置%d上\n", x, loc);
    else           printf("%d 不存在\n", x);
    return 0;
}
```

后缀运算符：`a++; a--; func();point.x（结构体成员访问）; a[0]（数组元素访问）;`

单目运算符（前缀运算符）：`++a; --a; +a; -a; !(a>0)`

后缀运算符优先级最高，然后是单目运算符。

数组 与 结构体：

- 结构体和数组，都可以由基本类型、复合类型组成。比如，结构体可以组成数组，数组可以组成结构体。

  ```c
  struct {double x, y;} a[4]; /* 由4个 结构体 组成的 数组 */
  struct {double x, y; int count[4]; } s; /* 由2个double、一个 数组 组成的 结构体 */
  ```

- 结构体和数组都可以在定义的时候初始化

  ```c
  struct complex_struct z = {3.0, 4.0};
  int count[5] = {3, 2, 1, 0};
  ```

- 数组不能相互赋值，所以不能用数组初始化另一个数组，也不能作为函数的参数和返回值。结构体都可以。因为数组作为右值使用时，自动转换成数组首元素的指针：传递的是指针，而不是元素的值。

  ```c
  int a[5] = {1, 2, };
  int b[5] = a; /* 用一个数组初始化另一个数组，是错误的 */
  a = b; /* 数组相互赋值，是错误的 */
  func(a);  /* 传入数组作为参数，调用函数，是错误的 */
  ```

  

#### 例：统计随机数

计算机产生随机数需要复杂的手段。C标准库的是伪随机数PseudoRandom，是由线性同余和时钟作为种子产生的。但统计上，也接近均匀分布Uniform Distribution。

```c
/* C 语言用2个函数 void srand(unsigned int seed) 和 int rand(void) 生成随机数(伪随机)
 * srand()生成一个随机数种子（就是传入的参数），计算机根据seed生成随机数序列
 * 调用rand()就取得序列的一个值。随机数都在区间[0, RAND_MAX]内，RAND_MAX随平台而定，如 32767
 * 如果要取得 [X, Y] 区间的随机数，rand() % (Y-X+1) + X;
 * rand()个位数随机性不强。(int)(100.0*rand() / (RAND_MAX+1.0)); 区间[0, 99]，强化随机性 */
srand(time(NULL)); /* 传入Epoch(1970.1.1,Unix纪元)至今的秒数，所以每次运行，seed都不同 */
for (i=0; i<N; ++i) {  /* 循环N次，生成 N 个数 */
	srand(time(NULL)); /*不能在循环内srand()，循环一次小于1s，导致seed 和 随机数序列 不变 */
	a[i] = rand() % (Y-X+1) + X; /*从[0, RAND_MAX]区间，变为区间[X,Y] */
}
```

生成并且查看随机数

```c
#include <stdio.h> /* rand()  srand() */
#include <stdlib.h> 
#define N 20 /* 定义一个常量。很多地方写死20，Hard Coding非常难修改 */

int a[N];
void gen_random(int upper_bound){
    srand(time(NULL));
    for (int i=0; i<N; ++i)
        a[i] = rand() % upper_bound;
}
void show_random(){
    for (int i=0; i<N; ++i) {
        printf("%d  ", a[i]);
    }
    printf("\n");
}
int main(void){
    gen_random(10);
    show_random();
    return 0;
}
```

编译器的工作分为2步：

- 预处理Preprocess：执行`#`的预处理指令，比如头文件`#include <stdio.h>`在源文件中展开（复制进去），然后替换`#define N 20`之类的常量定义、宏定义。请注意，`define/include`不是保留字，可以作为标识符，`#include`才有语法意义。`#define`是在预处理的时候执行的，而枚举常量是在编译的时候执行的。

  命令是`gcc -E hello.c`或`cpp hello.c`

- 编译

- 随机数统计：将上面的代码，加一个函数，把main函数改掉

  ```c
  int count_rate(int value) { /* 增加这个函数 */
      int i, count = 0;
      for (i=0; i<N; ++i)
          if (a[i] == value) 
              ++count;
      return count;
  }
  int main(void){
      gen_random(10);
      int i;
      printf("value\trate\n");
      for (i=0; i<10; i++) {
          printf("  %d\t%d\n", i, count_rate(i));
      }
      return 0;
  }
  ```

- 给每个数出现的次数计数，并且打印出来

  ```c
  /* 如果要打印，首先应该将 0-9 所有的频率数据保存 */
  int rate_1 = count_rate(1);  /* 方案 1 使用一个个变量 */
  int rate_2 = count_rate(2); 
  int i, gram[10];             /* 方案 2 使用一个数组 */
  for (i=0; i<10; i++) { gram[i] = count_rate(i); } /* 每次循环，都计算i出现的频率。10*N次 */
  int i, gram[10] = {0,};      /* 方案 3 使用一个数组，弃用 count_rate() */
  for (i=0; i<N; i++) { gram[a[i]]++; } /* 每次循环，都统计相应的频率。 N次 */
  ```

- 代码：去掉了show_random()、count_rate(int value) 两个函数

  ```c
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #define N 50 /* 定义一个常量。 */
  int a[N];
  void gen_random(int upper_bound){
      int i;
      srand(time(NULL));
      for (i=0; i<N; ++i)
          a[i] = rand() % upper_bound;
  }
  int main(void){
      gen_random(10);
      int i, j, gram[10] = {0,};
      for (i=0; i<N; i++) {
          printf("%d ", a[i]);
          gram[a[i]]++;
      }
      printf("\n\n");
      for (i=0; i<11; ++i){
          for (j=0; j<10; ++j){
              if (i == 0){
                  printf("%d\t", j); /* 第0行，打印表头 */
              } else if ((i > 0) && (i <= gram[j])) { /* 之后每行每列，判断：要么打印*，要么\t */
                  printf("*\t");
              } else {
                  printf("\t");
              }
          }
          printf("\n");
      }
      return 0;
  }
  ```

#### 例：排列组合问题 P93

- 从N个字符中，取出M个作全排列。比如`a b c d`取出 `a b`，有`ab  ba`两个排列。

#### 例：统计数字[0,9]、空白符、字母出现的频率。

```c
#include <stdio.h>
int main(void)
{
    int c, i, nwhite, nother;
    nwhite = nother = 0;
    int ndigit[10];
    for (i = 0; i < 10; ++i) // 实际上 int ndigit[10] = {0, }; 就可以完成声明、初始化为0
        ndigit[i] = 0;
    while ((c = getchar()) != EOF) {
        if (c >= '0' && c <= '9')
            ++ndigit[c-'0']; /* 获得下标的方式非常讨巧 */
        else if (c == ' ' || c == '\n' || c == '\t') {
            ++nwhite;
        else
            ++nother;
    }
    for (i = 0; i < 10; ++i)
        printf("\t%d:\t%d\n", i, ndigit[i]);
    printf("\twhiteSpace: %d\nother: %d\n", nwhite, nother);
    return 0;
}
```

素数判断

```c
#include <math.h>
int isPrime(int x){ //素数返回 1，非素数返回 0
    int i;
    int cut[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
    if (x < 2) return 0; // -n, 0, 1 都不是素数
    for (i=0; i < sizeof(cut)/sizeof(cut[0]); i++){ // 捷径
        if (x == cut[i])   return 1;
        if (x%cut[i] == 0) return 0;
    }
    for (i=4; i <= (int)sqrt(x); i+=2)
        if (x % i == 0)
            return 0;
    return 1;
}
```

### 多维数组Multi-dimensional Array

C语言里，把概念模型的表格，一行行拼接，就组成物理存储。这种存储方式称为 Row-major。而有些语言是一列列拼接为物理存储，这称为 Column-major，比如Fortran

```c
int a[2][3] = {1, 2, 3, 4, 5 }; /* [row][column] 初始化赋值。 */
int a[2][3] = { {1, 2, 3},      /* 这种 结构对应 的初始化格式，更易读 */
                {4, 5   }  };   /* 最后一位没有赋值，初始化为0 */
/* a[3][2] 二维概念模型示意图（人的逻辑解释） */
      |        | Col  0 | Col  1 | Col  2 |
      | Row  0 |   1    |   2    |   3    |
      | Row  1 |   4    |   5    |   0    |
/* a[3][2] 物理存储示意图（在内存里怎么储存） */
|Index(addr)  | a[0][0] | a[0][1] | a[0][2] | a[1][0] | a[1][1] | a[1][2] |
|Object(value)|    1    |    2    |    3    |    4    |    5    |    0    |

int a[][3] = {1, 2, 3, 4, 5 }; // 行数可以省略，但列数不能省略。不完整的会自动补零
/* int a[][3] = {{1, 2, 3},      这样更容易读
                {4, 5,},};    */
// 二维数组遍历
int row, col;
for (row=0; row < 2; row++) 
    for (col=0; col < 3; col++)
        printf("行 %d, 列 %d, 值 %d\n", row, col, a[row][col]);
```

##### 例1

实现一个接口，根据输入的整数，打印出星期几。

```c
int main(void)
{
    void print_day(int day);
    print_day(2); /* 接口调用举例 */
    return 0;
}
```

用 switch 解决，将会非常繁琐

```c
void print_day(int day)
{
    switch (day) {
    case 1: printf("Monday\n");   break; case 2: printf("Tuesday\n");  break;
    case 3: printf("Wednesday\n");break; case 4: printf("Thursday\n"); break;
    case 5: printf("Friday\n");   break; case 6: printf("Saturday\n"); break;
    case 7: printf("Sunday\n");   break; default:printf("Illegal number\n");
    }
}
```

多维数组的解决方案

```c
void print_day(int day)
{
    char days[8][10] = { "", "Monday", "Tuesday", "Wednesday", "Thursday",
                        "Friday", "Saturday", "Sunday"}; /*用字符串初始化*/
    /* 三元表达式 组成的 语句 */
    (day<1 || day>7) ? printf("Invalid: %d\n", day) : printf("%s\n", days[day]); 
}
```

之所以更简洁了，是因为用数据代替了代码，用`days[day]`取代了`switch(day)`的选择结构，同时把`printf()`也从各个 case 提取出来。这是数据驱动编程（Data-Driven Programming）。

编程最重要的是选择正确的数据结构来组织信息，设计控制流程和算法尚在其次，只要数据结构选择的好，代码就会更容易理解和维护。

##### 例2

石头剪子布小游戏

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(void)
{
    char gesture[3][8] = {"Scissor", "Stone", "Cloth"};
    int man, computer, result, ret;
    srand(time(NULL));
    while (1) {
        computer = rand() % 3;
        printf("Input your gesture,\n(0-Scissor, 1-Stone, 2-Cloth):\n");
        ret = scanf("%d", &man); /*如果输入是数字，就会返回 1 */
        if (ret != 1 || man<0 || man >2) { /*输入必须是数字，而且只能是 0/1/2 */
            printf("Invalid Input");
            return 1;
        }
        printf("You: %s\tComputer:%s\n", gesture[man], gesture[computer]);
        result = (man - computer + 4) % 3 - 1; /* 4%3 - 1 = 0 加上4是为了防止出现负数 */
        if (result > 0) /* 石头剪子布实际上是一个 3 进制问题。只要在3进制上更大就赢 */
            printf("You win!\n"); 
        else if (result == 0)
            printf("Draw.\n");
        else
            printf("You lose.\n");
    }
    return 0;
}
```

- 例3：日期转换。一个函数将x年x月转为n天，另一个函数将n天转为x年x月。比如3月1日是1988闰年的60天，而普通年的60天才是3月1日。

    ```c
    #include <stdio.h>
    static char daytab[2][13] = { /* 一年的每个月有几天。char还可以存储小整数 */
        /* a[0]用0占位，这样才能 a[n]表示n月份 */
        {0, 31, 28, 31, 30, 31, 30, 31, 31,  30, 31, 30, 31},
        {0, 31, 29, 31, 30, 31, 30, 31, 31,  30, 31, 30, 31},
    }; /* daytab[x][y] x表示闰年与否，闰年x=1，非闰年x=0； y 表示月份 */
    int day_of_year(int year, int month, int day)
    {
        int i, leap;
        leap = year%4 == 0 && (year%100 != 0 || year%400 == 0);
        for (i = 1; i < month; i++)
            day += daytab[leap][i];
        return day;
    }
    void month_day(int year, int yearday, int *pmonth, int *pday){
        int i, leap;
        leap = year%4 == 0 && (year%100 != 0 || year%400 == 0);
        for (i = 1; yearday > daytab[leap][i]; i++)
            yearday -= daytab[leap][i];
        *pmonth = i,  *pday = yearday;
    }
    int main(void)
    {
        int month, day;
        month_day(1988, 60, &month, &day);
        printf("\tmonth: %d, day: %d\n", month, day);
        return 0;
    }
    ```

##### 将多维数组传给函数

如果在函数调用时，将一维数组作为参数，实际上传递的是一个指针，这个指针指向a[0]。

那么，如果在函数调用时，将二维数组作为参数，实际上传递的是一个指针，这个指针指向一列元素构成的一维数组（也称一个行向量，同时指向很多个元素）。那么，函数声明应该这么写：

```c
func(int daytab[2][13]) {...}
func(int daytab[][13]) {...} /* 也可以这么写。因为行数无关紧要，但列数是必须的 */
func(int (*daytab)[13]) {...} /* 还可以这么写。一个指向某个数组a[13]的指针 */
func(int *daytab[13]){...}/*不能这么写。因为运算符优先级，daytab是13个int指针的数组*/
```

## 指针

指针的用途：

- 给函数传入较大的数据（指针内存占用少，直接传值内存占用大），
- 给函数传入数组从而操作数组，
- 让函数能够返回多个结果，比如swap() 函数
- 动态申请内存，并且操作它

地址运算符、取址符`&`，它的作用是获得一个变量所对应的内存地址，只能用于内存中的对象（变量、数组元素），不能用于表达式/常量/register变量。

```c
int a;
scanf("%d", &a);
printf("%p\n", &a); // 指针的长度，取决于架构和编译器，不一定等于int
printf("0x%x\n", (int)&a);
// 表达式 &a 有意义是因为 a 是一个变量名。以下是错误的：
&(a+b);
&(a++);
&(a+1);
// 另外，用指针可以取到相邻变量的值，因为堆栈是自顶向下分配，所以 &i > &p
int main() {
    int i = 11, p; // 变量 i 和 p 声明语句相邻，因此它们地址空间也相邻。
    int *ptr_i = &i; // 将变量 i 的地址给 ptr_i
    *(ptr_i - 1) = i; // 地址 (ptr_i + 1) 就是 变量 p，因此，等于语句 p = i; 
    printf("%d\n", (&p - &i)/sizeof(int));
    printf("%d\n", p);
    return 0;
}
// 类似地，数组也很有趣
int a[10];
printf("%p\n", &a);    // 0x7ffeedb14480
printf("%p\n", a);     // 0x7ffeedb14480
printf("%p\n", &a[0]); // 0x7ffeedb14480
printf("%p\n", &a[1]); // 0x7ffeedb14484
printf("%ld\n", (&a[1] - &a[0])); // 1
```

变量是一个存储空间，存储各种类型的数据。而指针变量，存放的数据是存储空间的地址。无论`*nix/Win`，32位操作系统的指针大小是32位，64位同理。

每种数据类型，都用第一个byte的起始地址操纵。变量名，其实也是用一个地址来操作一段内存。所以 指针 也要有类型。与普通变量相比，指针变量的特殊之处，在于它存储的不是数据，而是一个内存地址。

```c
char c;  /* 变量名c用一个地址，操纵一个byte（char 是 1 byte） */
int a;   /* 变量名a用一个地址，操纵多个byte（int 一般是 4 byte） */
int *p;  /* 声明一个整型指针变量。也就是，表达式*p是一个int，p 是一个 int类型指针*/
int *p, j; // p是指针，j是int
double *atof(char *); /* 声明 double型指针 返回值函数。函数参数是 char 类型指针 */ 
void *x; // 不知道指向什么东西的指针。计算时，与char*相同。在底层比如操作系统常见

int a = 10; // a是普通变量，存储的是数据。p是指针变量，存储的是地址
int * p;  /* 这种写法*与p分开，不好。 *p 强调指针 */
p = &a;   // int * p = &a; 声明 和 初始化 一步到位 的写法
void *vp = p;  /* void 类型的指针 p，可以存放任何数据类型的地址（指针） */
int *m = (int *)vp;  /* 除了 void * 其余指针必须同类型才能赋值。不同类型的指针，必须要强制转换后，才能赋值  */
printf("addr: %p, num: %d, vp: %p, mp: %p\n", p, a, vp, m);/* 指针变量存放的地址数据，是正整数 */
```

间接寻址运算符、引用运算符`*`，用来访问/操控指针指向的对象（内存地址），如果声明`int *p`，那么`*p`等同于 一个int对象/变量，即可以作为左值，也可以作为右值

```c
int i = 11;
int *ptr_i = &i; // 等同于 int *ptr_i;  ptr_i = &i;
int k = *ptr_i; // 作为右值，取值（解释这块内存，存储了什么数据），然后赋值给变量 k
*ptr_i = 12; // 作为左值，用 12 给 指向的那块内存地址赋值。
printf("%d, %d\n", i, k); // 12, 11
```

间接寻址运算符`*`，地址运算符`&`，它们的功能正好相反。

```c
int x=1, y=2, z[10];
int *ip;
*&y; // 等同于 *(&y)，先得到地址，然后转为变量 >> 结果是一个 int变量，相当于 y 自己
&*ip; // 等同于 &(*ip) 先得到变量，然后取址 >> 结果是一个 int指针变量，相当于 ip

ip = &x;    /* ip 现在指向 x 所在的内存空间 */
y = *ip;    /* y 的值变为 1 （x的值）*/
*ip = 0;    /* x 的值变为 0  */
ip = &z[0]; /* ip 现在指向 z[0] */
```

其他示例

```c
int x=1, y=2, z[10];
int *ip;
/* 用法 1   与引用运算符结合 *p 相当于用一个普通变量那样使用这个整体 */
int *p, i;
p = &i; /* 以下的表达式，*p 与 i 完全可以互换，二者完全等价 */
*p = *p + 10;
*p /= 2;
*p++;(*p)++; /* 因为运算符*优先级比++更高，所以二者一样 */
int y = *p + 3;
/* 用法 2   指针变量，它也是一种变量，可以像普通变量那样使用指针变量 */
int *iq;
iq = p;  /* 同类型指针变量之间，才可以赋值 */
p = p + 1;
```

指针和地址，**最大的用处是函数传参**，特别是多个“返回值”的场景。将地址传入，用指针变量 间接操纵数据。注意观察：函数定义 和 调用，指针、地址怎么写 ？

```c
/* 交换，如果只是传入参数，因为每个函数的作用域内的变量都是单独副本，将无法交换。 */
// 错误写法
void swap(int x, int y) { 
    int tmp; tmp = x, x = y, y = tmp;
}
// 正确写法
void swap(int *px; int *py){  /* 参数 为 int指针 */
    int tmp; tmp = *px, *px = *py, *py = tmp;
}
void main(void){
    int a = 8, b = 3;
    swap(&a, &b);     /* 函数调用，传入变量地址 */
}
```

最常见的手法，是函数返回状态码，真实数据由地址-指针的形式传递。这样，检测状态码是否异常，再来读取数据块。

```c
int divide(int numerator, int denominator, int *result){
    if (denominator == 0)  // 除零错
        return 0; 
    *result = a / b;
    return 1;
}
int main() {
    int a=5, b=2, c;
    if (divide(a, b, &c)) // 调用者执行检测，以便排错
        printf("%d / %d = %d\n", a,b,c); // 如果执行正确，就读取数据
    return 0;
}
```

指针常见的错误：指针还没一个合法的内存地址，就用`*p`操作指针，这做法非常危险。因此：指针只要声明了，一定要初始化。

```c
int *p, k;
k = 11, *p = 12; // 声明了p，但没初始化，它还没有实际的地址，*p=12将会在随机地址存入12，引发程序崩溃
```

### 指针、数组、地址运算

```c
int a[10], *pa, x;
pa = &a[0]; /* 将 a[0] 的地址赋给 pa。a == &a[0]，所以也可以写成 pa = a; */
x = *pa;    /* 将 pa 引用的值，也就是 a[0] 的值，赋给 x */
pa = pa + 1; /* pa+1 的意思是指向下一个对象，也就是下一个同样尺寸的内存 */

/* 数组下标 和 指针运算，关系密切 */
pa += i;    /* pa = pa + i 既然pa就是a[0] 的地址，那么 pa+i 就是 a[i]的地址 */
x = *(pa + i); /* 如果 pa = &a[0]，那么 *(pa+i) 就是 a[i] 的值 */

/* 因此，a[n]数组名[下标] 的表达式，完全可以用 指针和偏移量 实现 */
pa = a; /* 数组名，本质是0元素的地址，a == &a[0] */
pa = pa + i; /*  pa+i == &a[i] == a+i  */
x = a[i];    /*  a[i] == *(a+i) == *(pa+i) == pa[i]  */

/* 但指针是变量，数组名是标识符而不是变量 */
pa = a; pa++;  /* 指针可以赋值、自增，但数组名不能。表达式 a = pa; a++; 都非法 */
x = pa[-1]; /* 指针可以乱指，越界也可以。但用数组名一定不能越界访问，a[-1]非法 */
```

数组名，是数组[0]元素的地址。下面的例子证明它是指针，编译器明确告知：sizeof on array function parameter will return size of 'char *' instead of 'char []'。而且，在调用者、被调用者里，算sizeof(array)结果不同，但是，`printf("%p\n", array);`的结果却是一样的——数组变量，本质就是指针，只是比较特殊，`a == &a == &a[0]`。数组的本质，是按照`size*N`分配一批内存。然而，数组名的特殊之处，是一个`const ElementType *ptr`的常量指针，数组创建后，这个数组名只能表示这块内存，无法再次赋值。

```c
int strlen(char s[]){   /* s是 char型 数组名 */
    int i;
    printf("sizeof(s):%d\n", sizeof(s)); // sizeof(s):8 得到指针的长度，unsigned long
    printf("s:%p\n", s); // s:0x7ffeefa464a6 编译器不警告，s的确是个指针
    for (i=0; s[i] != '\0'; i++);
    return i;
}
int main() {
    char a[] = "hello";
    //结果：size in main:6, strlen:5。上下的sizeof算出来，不一样
    printf("size in main:%d, strlen:%d\n",  
           sizeof(a), strlen(a));
    //a in main:0x7ffeefa464a6。a是数组名，却和上面的s一样。而且 a == &a == &a[0]
    printf("a in main:%p\n", a); 
    printf("a in main:%p\n", &a);
    printf("a in main:%p\n", &a[0]); 
    return 0;
}

/* 指针、变量、const */
int i; 
int * const p = &i;
*p = 26; // ok，等同于 i = 26; 赋值语句
p++;     // error，此时 p 是常量指针，无法自增

int i;
const int *p = &i; // 等同于 int const *p; 
*p = 26; // error!  (*p)是const，无法通过间接寻址修改。这种方式，可以保护函数传参
i = 26;  // OK  只能用 i = 26; 修改
p = &j;  // OK
int func(const int *p){...}; // 这种声明和定义，就保护了传入的参数

int * const p = array_A; // 这时候，p 相当于一个数组变量名
*p[0] = 26; // ok
p = array_B; // Error，原因和 数组变量 array_A, array_B 一样

const int a[] = {1, 2, 3, 4, 5}; // 这种数组，每个元素不可修改
a[0] = 13; // Error!
int sum(const int a[], int len){...} // 这种声明和定义，保护了原来的数组
size_t strlen(const char *s); // string.h 标准库
```

由此可见，从调用者来看，`s[]` 和 `*s` 完全是等价的接口

```c
/* 完全等价的接口 */
int sum(int *arr, int n){...}  // 声明 int sum(int *, int);
int sum(int arr[], int n){...} // 声明 int sum(int [], int);

/*函数内的实现可能不一样，数组名不能自增（但能整数加减运算），指针能够 ptr[2]下标取值 */
int strlen(char s[]){   /* s是 char型 数组名 */
    int i=0;
    for (i=0; s[i] != '\0'; i++); 
    // 可以写 for (; *(s+i) != '\0', i++);   数组名作为地址，能够与整数加减
    // 不能写 for (; *s != '\0'; s++)  i++;  数组名不能自增
    return i;
}
int strlen(char *s){  /*  s是 char型 指针 */
    int i = 0; /* 根据传入的指针，遍历直到 '\0' */
    for (; *s != '\0'; s++)  i++; 
    // 可以写 for (i=0; s[i] != '\0'; i++);  指针可以用ptr[N]取值
    return i;
}
/* 首先，s 是指针，可以自增运算。其次，s只是副本，怎么调用，都不会影响调用者的字符串 */
strlen("hello, world"); /* 字面量，常量 */
char array[] = "hello, world"; strlen(array); /* 数组名 */
char *strp = array; strlen(strp); /* 指针变量 */
strlen(&array[2]); strlen(array+2);  strlen(strp+2); /* 传入子字符串 */

/* 类似地，其他类型的数组，也可以这样 */
void func(int arr[]){...}
void func(int *arr){...}
int a[10];
func(&a[2]);  func(a+2);
```

另一个更加复杂的例子。用一个 char s[]作为一个内存块，可以从中提取和释放内存。

```c
#include <stdio.h>
#define ALLOCSIZE  10000 /* 限定存储空间上限 */
static char allocbuf[ALLOCSIZE]; /* alloc() afree() 所操作的存储空间 */
/* 定义一个指针，并且初始化。指针初始化，要么为 0/NULL，要么是 实际地址 */
static char *allocp = allocbuf; /* 下一个空闲空间的指针。初始值为 &allocbuf[0] */
/* 开辟n个char的空间，返回内存块的指针 */
char *alloc(int n)
{   /* 如果空间还足够，就分配空间，并且返回 这个内存块 起始点的指针。 */
    if (ALLOCSIZE - (allocp - allocbuf) >= n) { /* 剩余=总值-（现指针-初始位置）*/
        allocp += n;
        return allocp - n;
    } else
        return 0; /* 内存地址永远大于 0，所以可以用来表示异常状态 */
}
/* 释放 p 指向的内存区域 */
void afree(char *p)
{
    if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
        allocp = p; /* 只能是 栈 FILO 型存储，alloc() afree()必须轮流调用 */
}
```
指针、地址的运算类型：
1. 同类型的指针赋值运算

   ```c
   int i, *ip, *im;
   ip = &i;  // 将 int类型的地址，赋值给 int类型的指针
   im = ip;  // 将 int 类型的指针变量的值（也就是一个地址），赋值给同类型的指针变量
   float *ifl;  ifl = ip; // 非法，两个类型不同的指针不能赋值
   void *p_all; p_all = ip; // 合法，void 类型的指针是通用的
   ip + im; // 非法。两个指针，加减乘除余、位运算，都是非法的。
   ```

2. 指针 与 整数 的运算

   ```c
   /* 指针变量存放的地址数据，是整数。虽然编译器警告，还是可以打印 */
   printf("\t%d\n", ip);
   /* 指针可以何任意整数加减，表示地址往前、后移动多少个单位（单位由指针类型决定） */
   char *im, *ip; 
   int *r;
   ip = r; // 非法。r是int指针，ip是char指针，不能相互赋值(引发内存操作错误）
   ip = (char *)r; //合法。强制转换指针类型，赋值（极其容易引发内存操作错误）
   im = ip - 8; // 移动 8 个 ziseof(char)。常用于数组操作
   ip++;
   ip += 20;
   ip *= 7; // 非法，指针 与 整数 只能加减，不能乘除
   ip -= 0.6;  // 非法，指针 不能与 浮点数 运算
   char x = *ip++; // *ip 的值存入 x，之后 ip自增。在CPU是一条汇编语句，性能更强 *--p; *p--; *++p; *p++; 都有
   // 因此，遍历数组，还有种写法
   ip = array;
   while (*p != '\0') // 字符串以'\0'结尾
   	printf("%c\n", *p++);
   ip > im; // 两个指针可以做 > = < != 之类的比较运算。本质是比较内存地址
   
   /* 但 指针 和 整数 不能互相转换。但 0/NULL 很不一样 */
   int a = &i; // 非法。将地址赋值给 int 变量
   ip = 20;    // 非法。将 整数 赋值 给 指针
   ip > 9;     // 非法。将 指针 与 整数 进行比较运算
   ip = 0; // 合法，将0赋值给指针。一般将0写作NULL，有些编译器不允许使用0地址，只用NULL
   ip > 0; ip != 0;  ip == 0;  // 合法，将 指针 与 0 进行比较、相等运算
   if (p + 1 > NULL) printf("p + 1 > 0\n"); // 任何指针都可以与 0 进行 比较和相等运算
   ```

   在计算机，每个进程都有独立的虚拟空间，内存地址都是从0开始。编译器不允许你使用0地址。

3. 同一个数组内的元素指针地址运算

   ```c
   第一，减法得到 两个位置 之间的 元素个数
   int a[9]; 
   int *p = &a[3]; *m = &a[1];
   int i = p - m;
   第二，大小、相等运算，得到两个位置的 前后 信息。
   可以进行 p == m;  p != m; p > q; p < q; 之类的运算
   而且 a[1]在 a[3]前，地址更小，所以 m < p 成立
   另外，指针可以在界外 1 单元比较，比如 a - 1 < p; a + 10 > m;
   
   unsigned long int strlen(char *s)
   {
       char *p = s;
       while (*p != '\0') //  p-s+1 是 p与s之间的元素个数。p-s 除去了 '\0'
           p++;
       return (size_t)(p - s);
   }
   ```

### 指针与动态内存管理

C99可以实现动态数组。

```c
void avg(){ // 输入N，然后输入 N 个整数，求平均值
    double sum = 0.0;     int N = 0, tmp=0;
    printf("请输入数量："); scanf("%d", &N);
    if (N > 0) { // sizeof(input)/sizeof(input[0]) 可以防止硬编码
        int input[N]; // N 是运行时，动态确定的
        printf("请输入数值，空格分隔：");
        for (N=0; N < sizeof(input)/sizeof(input[0]); N++){
            scanf("%d", &tmp);
            sum += input[N] = tmp;
        }
    }
    printf("平均值：%0.3f\n", sum / N);
}
```

然而 C99之前，没有这种功能。而且，很多时候，不仅仅是动态申请数组，还有动态申请结构体内存等等。

```c
#include <stdlib.h> // void * malloc(size_t size);
void avg(){
    double sum = 0.0;
    int N = 0, tmp=0, i=0;
    // int *input; 这样也行，input 可以指向别的内存
    printf("请输入数量："); scanf("%d", &N); // 确定 N 的值
    if (N > 0) {
        int * const input = (int *)malloc(N * sizeof(int)); // 动态内存
        printf("请输入数值，空格分隔：");
        for (; i < N; i++){
            scanf("%d", &tmp);
            sum += input[i] = tmp;
            printf("tmp：%d\n", tmp);
        }
    }
    free(input); // 释放内存
    printf("平均值：%0.3f\n", sum/N);
}
```

如果内存耗尽，malloc 内存分配失败，就会**返回NULL/0**。下面的程序，可以探测系统可以给程序分配多少空间

```c
#include <stdlib.h>
#include <stdio.h>
int main(void){
    void *p;  int N = 0;
    while ((p = malloc(100*1024*1024)))
        N++;
    printf("共分配 %d00 MB空间", N);
    return 0;
}
```

注意，`free(p)`这个函数，只能用首地址去归还。系统会记住 malloc的地址，必须原封不动归还。常见的错误，是malloc 没有 free，长时间运行后内存溢出，新手是因为忘了，或者因为程序太复杂找不到 free 的合适时机。还有个问题，是 free() 后再 free()，这样也会崩溃。

```c
// 这种写法是错误的，因为这时候 p 的值已经不是这块内存的首地址了
int i, *p;
p = (int *) malloc(N * sizeof(int));
for (i=0; i < N; i++)
    printf("%d", *p++);
free(p);  // 异常退出
// 下面的也是错误的。
int i = 0;
int *p = &i;
free(p); // 异常退出
// 比较好的写法
int *p = NULL; // 一但定义一个指针要立即赋值，要么赋NULL，要么赋其他。不赋值就是随机值
p = (int *)malloc(N * sizeof(int));
do_somting();
free(p); // free(NULL)是没问题的

```





### 指针数组、指向指针的指针

**任务**：对一些文本行，进行排序。**问题**：字符串不是基本类型（比如int），长短不一，不易比较和交换；如果对字符串进行交换，内存管理会非常复杂，内存开销也很大。**解决方案**：1.规整。将文本首尾相连存入一个内存块`数组char buf[N]`，每个行都用第一个字符的指针进行访问。2. 间接访问。每个行的指针，存入一个指针数组`char *strlinep[N]`。3. 比较。依次访问数组，取出指针，strcmp()就可比较两个文本行。4. 交换。交换指针数组的指针，而不是文本行。

- 读取所有的文本行，存入数组`char buf[N]`，同时每个行的指针存入指针数组。
- 对文本行进行排序（比对、交换指针）
- 依次打印文本行

将问题按逻辑进行划分，然后再一步步建立相关的函数，而用主函数控制其他函数的运行。

```c
#include <stdio.h>
#include <string.h>
#define MAXLINES 5000
/* char *charp; 声明一个char指针。char *a[] 声明一个 char 指针数组 */
char *lineptr[MAXLINES]; /* 指向文本行的 指针数组 */
int readlines(char *lineptr[], int nlines);
void writelines(char *lineptr[], int nlines);
void qsort(char *lineptr[], int left, int right);
/* 对输入的文本行进行排序 */
int main(void)
{
    int nlines;
    if ((nlines = readlines(lineptr, MAXLINES)) >= 0) {
        qsort(lineptr, 0, nlines-1);
        writelines(lineptr, nlines);
        return 0;
    } else {
        printf("error: input str too long to sort!\n");
        return 1;
    }
}
#define MAXLEN 1000
int getline(char *, int);
char *alloc(int);
/* 读取输入行 */
int readlines(char *lineptr[], int maxlines){
    int len, nlines;
    char *p, line[MAXLEN];
    nlines = 0;
    while ((len = getline(line, MAXLEN)) > 0)
        if (nlines >= maxlines || (p = alloc(len)) == NULL)
            return -1; /* 异常： */
        else {
            line[len-1] = '\0';
            strcpy(p, line);
            lineptr[nlines++] = p;
        }
    return nlines;
}
/* 写出 行 */
void writelines(char *lineptr[], int nlines){
    int i;
    for (i = 0; i < nlines; i++)
        printf("%s\n", lineptr[i]);
}
#define ALLOCSIZE 10000
static char allocbuf[ALLOCSIZE];
static char  *allocp = allocbuf;
char *alloc(int n){
    if (ALLOCSIZE - (allocp - allocbuf) >= n) {
        allocp += n;
        return allocp - n;
    } else
        return 0;
}
/* getline() 每一行都保存到字符串变量 s 中，并且返回字符串长度 */
int getline(char s[], int lim){
    int c, i = 0;
    while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
        s[i++] = c;
    if (c == '\n')
        s[i++] = c, s[i] = '\0';
    return i;
}
/* 排序算法 */
void qsort(char *v[], int left, int right){
    int i, last;
    void swap(char *v[], int i, int j);
    if (left >= right)
        return;
    swap(v, left, (left + right)/2);
    last = left;
    for (i = left+1; i <= right; i++)
        if (strcmp(v[i], v[left]) < 0)
            swap(v, ++last, i);
    swap(v, left, last);
    qsort(v, left, last-1);
    qsort(v, last+1, right);
}
/* 交换 v[i] 和  v[j] */
void swap(char *v[], int i, int j){
    char *tmp;
    tmp = v[i], v[i] = v[j], v[j] = tmp;
}
```





### 指针数组的初始化

在下例中，name被声明为一个一维数组，数组元素是char 指针。name用一个字符串初值表初始化，首先将每个字符串转为 char指针，然后赋值给相应的`name[i]`。

```c
void print_day(int day)
{   /* 用常量列表初始化，不必指明数组长度。但将字符串转成 char 指针，编译器会警告 */
    static char *days[] = { "Illegal day", "Monday", "Tuesday", "Wednesday",
           "Thursday", "Friday", "Saturday", "Sunday"};
    printf("\t%s\n", (day<1 || day>7) ? days[0] : days[day]);
}
```
### 函数指针

任何运算，在计算机内部都是数学二进制运算。数据是二进制数据，指令也是二进制数据。所以，int指针存储一个int变量的首地址，那么，函数指针存储的也是函数首地址；int之类的指针变量必须有类型，函数指针也必须要有类型（返回值、参数类型）。

```c
/* 创建一个函数指针，最快捷的方式，是先写一个函数声明，然后用(*pf)替换函数名 */
int strlen(char *);
int (*pf)(char *); /* 因为运算符优先级，一定不能省略括号 */
int *func(char *); /* 如果省略了，func 是一个返回 int 指针的函数 */
```

用法举例：

```c
/* 创建 函数指针 */
void ToUpper(char *);
void ToLower(char *);
int round(double);
void (*pf)(char *); // 创建 函数指针
/* 函数指针赋值 */
pf = round; // 错误。函数类型不匹配
pf = ToLower(); // 错误。首先，这不是函数地址；其次，这个函数返回值void无法赋值
pf = ToUpper;  // 函数指针 赋值。函数名，本质就是函数的地址（指针）
/* 用 函数指针 调用函数 */
char mis[] = "How are you?";
(*pf)(mis);    // K&R风格指针调用函数。按指针的惯用方式，使用函数指针。ANSI 
pf = ToLower;
pf(mis);       // BSD风格指针调用函数。既然函数名就是指针，那二者等价。直接用指针调用函数
/* 函数指针 作为 其他函数的 参数 */
void show(void (*pf)(char *), char *s); //声明函数show，参数pf是函数指针
show(ToLower, mis); //直接使用函数名。函数名本质就是函数地址（指针）
show(pf, mis);      //使用函数指针
int atoi(char s[]);
printf("%d\n", atoi("123")); // 传入func()， 是用函数返回值作为参数
show(atoi); // 传入 func 函数名，是用函数指针作为参数
/* 函数指针 结合 typedef */
typedef void (*FP_CHARP)(char *); //声明了一种指针，并且将它定义为 FP_CHARP 类型
void show(FP_CHARP fp, char *); // 声明了函数show()，它的一个参数是 FP_CHARP 类型
FP_CHARP pfunc; // 用 FP_CHARP 类型，声明一个指针
FP_CHARP arpf[] = {ToUpper, ToLower, Transpose}; // 用 FP_CHARP 声明了一个数组，元素是一个个函数指针（函数名）
show(arpf[0], mis); // 数组下标，取出函数指针，传参给函数show()

/* 既然函数名就是函数地址（指针），函数名出现的情境有： */
int atoi(char s[]) {...}; // 函数定义
int atoi(char []);        // 函数声明
int x = atoi("324");      // 函数调用
int (*pf)(char []);
pf = atoi;  //作为指针（地址）赋值给 函数指针变量。声明同时初始化： int (*pf)(char []) = atoi;
stradd(atio, "12", "76"); // 作为指针（地址），传给函数作为参数
```





## 字符数组、指针、字符串

### 字符数组、字符串

字符串字面值可以看作一个数组，里面的内容是char，结尾是`\0`。这`\0`是八进制表示的ASCII的Null字符，它是字符串结束符号，然而不是字符串的一部分。只要是以Null字符结尾的一串字符都是字符串。无论是字面值`"Hello, World.\n" `，还是以`\0`结尾的字符数组，比如下面的例子。

```c
char line[] = {'H','e','l','l','o','\n'}; // 这不是C语言字符串
char line[] = {'H','e','l','l','o','\n','\0'}; // C语言字符串以'\0'结尾，char的'\0'就是整数值0，而'0'是整数值48
char line[] = "Hello, World.\n" // 字面量赋值时，编译器会自动加上\0 这个字符串本质是一个15个字符组成的数组，注意最后有个\0
+---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 |
| H | e | l | l | o | , |   | W | o | r | l  | d  | .  | \n | \0 |
+---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+
```

文件的字符串`a.txt`是没有`\0`的，这是C语言的内部表示方式。

```c
char c = "Hello, World.\n"[0]; /* 字符串可以当做数组，取元素作为右值 */
"Hello, World.\n"[0] = 'A';  /* 字符串却不可以作左值 */

char str[4] = "He"; /* 用字符串 初始化 字符数组，不足就用\0补全 {'H','e','\0','\0'} 补了一个\0*/
char str[4] = "Hello, World.\n"; /*字符串长度超过数组容量，被截断。编译器警告。没有Null不是字符串 */
char str[] = "Hello, World.\n"; /* 不指定数组容量，编译器自动确定长度为15 */
char str[14] = "Hello, World.\n"; /* 刚好截去了Null字符，编译器不警告。更危险 */
char *str = "Hello, World.\n"; // 字符串的指针。

printf("string: %s\n", str); /* %s最佳用法，格式化字符数组。如果没有Null字符，printf会越界访问*/

char buffer[100] = ""; // 数组长度100，存储的全是\0，字符串长度0
char buffer[] = "";    // 数组长度1，存储\0
```

编译器会将字符串常量放置在特殊的位置。

### char * 指针、字符串、函数

字符串（字符数组），在被函数使用的时候，一般接收的是首元素的指针，而以`'\0'`作为结束标志（`\0`的ASCII值是0，而`'0'`的ASCII 的值是48）。字符串以数组的方式在内存存储，而以数组或指针的方式访问（更多是以指针的方式）。

```c
printf("Hello"); /* 实际上传入的是 H 所在的地址数据（指针），函数遍历以'\0'结束 */
char *pmsg;
msg = "wake up"; /* 把一个字符串数组的指针赋给 msg变量。没有复制字符串，只关于指针 */
char *pmsg = "wake up";  /* 定义一个指针。它可以指向任何 char 型对象的地址 */
pmsg[0] = 'B'; // Bus error。
char amsg[] = "wake up"; /* 定义一个数组。它死守那些内存地址，只能改数据 */
char line[10] = "wake up"; // 如果指定长度，那么长度要足够。编译器会自动在空位填充 '\0'

// 下面的 字符串 字面量 写法都是正确的
char *str = "abc" "defg";
str = "abc"
      "defg"; // 这样换行比较好。编译器会自动拼接
str = "abc\
defg"; // 用 \ 换行。这样不太好
printf("%s\n",str);
strc = stra + strb; //错误！C语言不支持字符串拼接（JAVA Python很常见）
// 字符串完全可以像数组那样操作。特殊之处，是字符串字面量可以用来初始化char数组。
for (int i=0; s[i] != '\0'; i++) printf("%c\n", s[i]); // 字符串遍历
```

另外，要注意，`char *s`字符串，和`char arr[]`字符数组还有普通变量`int j`是不同的。代码中的字面量字符串，编译时候写死，运行的时候存储在程序的代码段，只读不可写。

`char *s = "ab";`实际上只存储了一个指针，这个指针指向字符串，而字符串存储在进程内存的代码段：

- 这种字符串是只读的，本质是 `const char *s = "ab";` 只是编译器不允许使用const写法。（注意`const char *p`是指针所在的对象不可修改，而`int * const p`是指针不可指向别的地址）。尝试修改字符串会导致进程崩溃（下面的str 和 msg 都不能修改字符串）。
- 如果字面量相同，编译器在代码段只有一份字符串（下面的str 和 msg 地址相同）。

而`char s[]="ab";`字符数组，编译器会将不可写的代码段内容，拷贝到`s[]`这个数组里。


```c
int main() {
    int i = 1234; 
    int j = 679;
    char arr[] = "Hello, World.\n";
    char *str = "Hello, World.\n";
    char *msg = "Hello, World.\n";
    /* char指针变量 str/msg 的地址相同，且与 i/j/arr相距很远。
    str:0x106436f80 msg:0x106436f80 它存储在程序的代码段 */
    printf("str:%p\tmsg:%p\n", str, msg);
    /* int i,j 和 char arr[] 地址相邻 
    i:0x7ffee97c9490   j:0x7ffee97c948c   arr:0x7ffee97c9499*/
    printf("i:%p\tj:%p\tarr:%p\n", &i, &j, arr);
    // str[0] = 'K'; 这会引发 bus error
    arr[0] = 'K'; // 这是可以的
    return 0;
}
```

#### 字符串、char *、char []辨析

字符串的本质，是一个 char 型的数组（一块存储char类型的连续内存空间），并且末尾是`'\0'`。在语法上，字符串有两种表达方式：`char *s`和`char s[]`。然而，`char *`不一定是数组，`char a; char *s=&a`是指向单个char变量的指针，而且也可能指向没有`\0`的char数组。同样的，`char s[]`也不一定是字符串。

那么，用`char s[]`还是`char *s`呢？

- 如果这个字符串就在本地（函数内），`char s[]`数组可以自动回收。
- 如果这个字符串不知道在哪里，就是指针`char *s`最好的应用场景，比如函数传参、动态内存分配

大致就是，如果要从字面量构建一个字符串，就用`char s[]`。如果要处理字符串，就用指针`char *s`。

#### 字符串输入输出

##### 字符串输入输出

基本例子

```c
int main() {
    char word[8];
    scanf("%s", word);
    printf("##%s##\n", word);
    return 0;
}
```

如果输入`hello world`，它只读取了`hello`。如果输入`abcdefgh`（正好是数组长度8），输出`abcdefgh##`，接着`Abort trap: 6`，说明读到了`abcdefgh`，但是数组越界。

下面的改进例子，输入`hello world`，word读取了`hello`，word2读取了`world`，中间的空格，没有读到。如果输入`abcdefgh abcdefgh`（两次都超过8字符），也会越界。

```c
int main() {
    char word[8];
    char word2[8];
    scanf("%s", word); scanf("%s", word2); // 读取 2 次
    printf("##%s##%s##\n", word, word2); // ##hello##world##
    return 0;
}
```

因此，`scanf()`的作用是读入一个单词（从字符，到空格、tab、回车为止）。而且，`scanf()`是**不安全**的，因为不确定输入的长度。正确的做法：

```c
int main() {
    char word[8];
    char word2[8];
    scanf("%7s", word); // 最多读取 N-1 个字符，要给\0留空间
    scanf("%7s", word2);
    printf("##%s##%s##\n", word, word2);
    return 0;
}
```

上面的代码，如果输入`hello world`，word读到的是`hello`，word2读到的是`world`，输出`##hello##world##`——第一次读取，hello长度5，没有到7，就按照空格/tab/换行分隔字符串。

如果输入`abcdefgh`，word读到的是`abcdefg`，word2读到的是`h`——长度8正好超过7，就按个数7分隔字符串，缓冲区还有字符剩余，下一次scanf获取了`h`。

但不可以使用没有初始化的`char *str;`接收scanf的输入。没有malloc并且赋予合法地址，很可能指向的是随机地址，有时候指向特殊代码段会崩溃，有时候又不崩溃——这种bug很难修改。

```c
int main() {
    char *str; // 本地变量不经过初始化，是随机值。非常危险
    scanf("%s", str); //str只是指针变量，未赋予有效地址，没有空间存储输入
    return 0;
}
```

##### 单个字符输入输出

int getchar(void); 从标准输入读取一个字符。为什么不是char而是int？读到EOF就会返回-1，所以需要返回int而不是char。而EOF，Windows 是Ctrl Z，Unix是 Ctrl D

```c
int main() {
    int c;
    while( (c = getchar()) != EOF)
        putchar(c);
    printf("EOF\n"); // Ctrl c只能结束。-1输出-1。Ctrl D才能输出EOF
    return 0;
}
```

这段程序，输入`1234`回车，就会输出`1234\n`

标准输入/输出——shell 缓冲区——你的程序。无论`scanf`还是`getchar()`，都是在操作shell 的缓冲区

### 标准库的string函数

标准库`string.h`常用函数。内部的实现，给了很多数组、指针怎么遍历字符串的做法。

 `size_t strlen(const char *s);`返回字符串长度（不包括结尾\0）。const可以避免修改传入的字符串。

```c
int main() {
    char str[] = "hello";
    printf("strlen=%lu\n", strlen(str)); // 5
    printf("size=%lu\n", sizeof(str)/sizeof(char)); //6
    return 0;
}
```

这个函数的实现，在【指针、数组、地址运算】章节有。

```c
size_t my_strlen(const char *s){
    int i = 0;
    for (; *s != '\0'; s++) i++; // while( s[i] != '\0') i++;
    return i;
}
```

函数`char *strcpy(char *s, const char *t);`将指针s 指向的字符串复制到指针 t 指向的位置。返回的是t指针。

```c
/* 错误做法 */
char *s1 = "abc";
char *s2 = s1; // 这只是将指针s1的地址，赋值给 s2，并没有新的、相等的字符串。

/* 用 数组 的方式实现 */
char *strcpy(const char s[], char t[]){
    int i = 0;
    while ((t[i] = s[i]) != '\0') i++;
    return t;
}
/* 用 指针 的方式实现 */
char *strcpy(const char *s, char *t){
    while ((*t = *s) != '\0') s++, t++;
    return t;
/*  while ((*t++ = *s++) != '\0'); 优化：自增符 比 引用符 高。放在一起
    while (*t++ = *s++); 优化：因为'\0'本质就是 0，可以省略 */
}
int main(void){
    // 注意这个 +1
    char *dst = (char *)malloc((strlen(src)+1) * sizeof(char));
    strcpy(dst, src);
}
```

函数`int strcmp(const char *s, const char *t);`，比较s 和  t 字符串，相同返回0，不同返回s[i] - t[i]

```c
/* 用 数组 实现 */
void strcmp(char s[], char t[]){
    int i;
    for (i=0; s[i] == t[i]; i++)
        if (s[i] == '\0')
            return 0;
    return s[i] - t[i];
}
/* 用 指针 的方式实现 */
void strcpy(char *s, char *t){
    for ( ; *s == *t; s++, t++)
        if (*s == '\0')
            return 0;
    return *s - *t;
}
```

函数`char *strcat(char *dst, const char *src)`将源字符串添加到目标字符串末尾，也就是字符串拼接。

```c
#include <stdio.h>
#include <stdlib.h>
char *strcat(char *dst, const char *src){
    char * tmp = dst;
    while (*dst++ != '\0');
    dst -= 1;
    while ((*dst++ = *src++) != '\0');
    return tmp;
}
char *strcpy(char *dst, const char *src){
    char * tmp = dst;
    while ((*dst++ = *src++) != '\0');
    return tmp;
}
size_t strlen(const char *src){
    size_t i = 0;
    for (; *src != '\0'; src++, i++);
    return i;
}
int main() { // 将 a b 字符串拼接为 c
    char *a = "hello";
    char *b = " world!";
    char *c = (char*)malloc(strlen(a)+strlen(b)+1);
    strcpy(c, a); strcat(c, b);
    printf("%s\n", c);
    free(c);
    return 0;
}
```

函数`strcpy/strcat/strncmp`，都需要调用者检查空间是否足够，不然会地址越界。函数`strncpy/strncat/strncmp`则有size限定，防止地址越界。

另外还有字符串搜索函数。`char *strchr(const char *s, int c);`从左往右搜索，找出字符`c`出现的位置，并且返回指针。`strrchr`是从右往左搜索。

```c
int main() {
    char *s = "hello";
    char *p = strchr(s, 'l'); // 找第一个 l
    printf("%s\n", p); // llo
    p = strchr(p + 1, 'l'); // 找第二个 l
    printf("%s\n", p); // lo 
    // 上面都是截尾的例子。怎么获取标志位之前的字符串？
    char string[] = "hello"; // 注意 char *s 不能这么做。
    p = strchr(string, 'l');
    char c = *p; *p = '\0'; // 将这个位置字符存入临时变量c，截断string
    char *tmp = (char*)malloc(strlen(s) + 1);
    strcpy(tmp, string);
    printf("%s\n", tmp);
    *p = c; free(tmp);  // 清理。string恢复原样、释放空间
    return 0;
}
```

还有`char *strstr(const char *big, const char *small);`是在一个字符串里寻找一个字符串。`char *strcasestr(const char *big, const char *little);`功能类似，但是会忽略大小写。

例子：不断输入一行文字，从中找出最长的一行，并且输出

```c
#include <stdio.h>
#define MAXLINE 1000 /* 输入行的长度上限 */
int main(void)
{
    int getline(char line[], int maxline);
    void copy(char to[], char from[]); /*从 from 数组复制到 to 数组 */
    char line[MAXLINE];
    char longest[MAXLINE];
    int len, max;
    max = 0;
    while ((len = getline(line, MAXLINE)) > 0) {
        if (len > max) {
            max = len;
            copy(longest, line); /* */
        }
    }
    if (max > 0) {
        printf("%s", longest);
    }
    return 0;
}
int getline(char s[], int lim){
    int c, i;
    for (i=0; i < lim-1 && (c=getchar()) != EOF && c!='\n'; i++)
        s[i] = c;
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}
void copy(char to[], char from[]){
    int i = 0;
    while ((to[i] = from[i]) != '\0')
        i++;
}
```



### 指针数组与多维数组

初学者很容易混淆 二维数组 和 指针数组。

```c
int a[2][3];
a = { {1, 2, 3}, {4, 5, 6} };
int *b[2] = { {1, 2, 3}, {4, 5, 6} };
```

`a[0][3]`和`b[0][3] `都是对 1 的合法引用。但是：

- a是一段真正的、`2*3=6`个int型存储空间。`a[row][col]`可以用 `3*row+col`计算得到这个元素的位置。
- b只是2个指针的数组，没有初始化之前，编译器只知道指针占用了2个指针的空间，却不知道int型占用了多少。而且，数组内的每个指针元素，引用的数组长度可以不同，可能指向3个元素的向量，也可能指向30个元素的向量，也可能哪儿也不指（NULL）。

指针数组最常见的用法：存放不同的字符串——字符串数组。

#### 字符串数组

  ```c
char **a; // a 是一个指针，指向另一个指针，那个指针指向一个字符或字符串
char *name[] = {"Jackson", "Adam", "Frank", "None"};
  /* pointer_array --> str(char []) 数组中的指针指向字符串，示意图 */
  | name[0] |   -->   | "Jackson\0" |  //name[N]是一个指针char * a
  | name[1] |   -->   | "Adam\0" |     //指针指向的数组不等长
  | name[2] |   -->   | "Frank\0" |
  | name[3] |   -->   | "None\0" |

char aname[][8] = {"Jackson", "Adam", "Frank", "None"};
  /* 二维数组  在内存中存储，示意图：每行都是等长的 */
  |Jackson0|Adam0000|Frank000|None0000|
  ```

指针数组最常见的用法：存放不同的字符串，比如main函数的参数。



### 程序在命令行执行的参数

Linux 里，运行程序还可以传入参数。

##### 例1：echo 程序

```Shell
$ echo hello, world
```

Linux 最常见的 echo 程序，它的主函数声明：

```c
int main(int argc, char **argv) {...}
int main(int argc, char *argv[]) {...} /* 为了易读，实际上是指针数组 */
```

- 第一个参数，int argc 用作参数计数。`echo hello, world`有3个参数（包括echo程序名）

- 第二个参数，是一个指针数组，每个指针元素都是一个向量，指向一个字符串。`argv[0]`不一定是程序名，也可能是符号链接名之类的

    ```shell
    /*既然 argv 是数组，那么数组名 argv就是指向 argv[0] 的指针，是 指针的指针 */
                argv           char *p
    argv  -->   | 0 |   -->   |echo\0|   /* 第一个argv[0]是固定的，本程序名 */
                | 1 |   -->   |hello,\0|  /* argv[1]-argv[argc-1]是可选的参数 */
                | 2 |   -->   |world\0|
                | 3 |   NULL(0,空指针)    /*最后那个 argv[argc] 是固定的，空指针 */
    ```


倘若不考虑shell环境展开，那么echo可以这样实现：

```c
/* 将 argv 看作一个数组 */
int main(int argc, char *argv[]){
    int i;
    for(i = 1; i < argc; i++)
        printf("%s%s", argv[i], (i < argc-1) ? " " : "\n");
    return 0;
}
/* 将 argv 看作一个指针 */
int main(int argc, char **argv){
    while (--argc > 0)
        printf("%s%s", *(++argv), (argc > 1) ? " " : "\n");
        /* 打印还可以这么写  printf((argc > 1 ? "%s " : "%s\n"), *(++argv)); */
    return 0;
}
```

##### 例2：仿效grep







## 全局变量与静态变量

##### 全局变量

全局变量：定义在函数外的变量。本地变量，也就是`{}`内的变量，生存期和作用域都在括号内，然而全局变量不是。

下面的例子，可以看到，全局变量`G`，初始值、再次赋值，在所有的函数都有影响，都有意义。不管函数是否运行完成、是否执行到`{}`内部，只要程序在运行，全局变量一直都存续。

```c
#include <stdio.h>
int G = 12; // 
int f(void){
    printf("In %s 1: G = %d\n", __func__, G);
    G += 2;
    printf("In %s 2: G = %d\n", __func__, G);
    return G;
}
int main(void){
    printf("In %s 1: G = %d\n", __func__, G);
    printf("f() return: %d\n", f());
    printf("In %s 2: G = %d\n", __func__, G);
    return 0;
}
```

全局变量初始化：

- 如果没有手动初始化，编译器会自动初始化0值（如果是指针，初始化为NULL值）。比如`int G=12;`改为`int G;`，编译器就初始化为`G=0`
- 如果手动初始化，需要编译时就确定值，比如字面量、`#define`值等等。

全局变量的初始化，发生在 `main()`函数之前，也就是启动之前。是编译器进行初始化的。

```c
int x = G;
const int G = 12; // 全局变量也能 const
int main(void){
    printf("In %s 1: G = %d\n", __func__, x);
    return 0;
}
```

另外，如果本地变量与全局变量同名，本地变量会覆盖全局变量。

```c
int G = 12; // 全局变量
int f(void){
    int G = 888888; // 这个本地变量，覆盖了上面的全局变量
    printf("In %s 1: G = %d\n", __func__, G);
    G += 2;
    printf("In %s 2: G = %d\n", __func__, G);
    return G;
}
int main(void){
    printf("In %s 1: G = %d\n", __func__, G);
    printf("f() return: %d\n", f());
    printf("In %s 2: G = %d\n", __func__, G);
    return 0;
}
```

警告⚠️：虽然全局变量可以在函数之间传递参数和结果，但这容易引发很多问题（写汇编的人很喜欢这么做，因为汇编没有本地变量）。这会让工程的模块关系复杂，难以追溯bug、难以修改。另外，使用全局变量和静态本地变量的函数，是**线程不安全**的。

##### 静态变量

静态本地变量：生存期与全局变量相同，作用域与本地变量相同（全局的生存期，局部的作用域）。

- 函数执行完毕，程序已经运行到括号外，静态本地变量仍然会存续，并且存储相关的值
- 静态本地变量只初始化一次，也就是第一次进入函数的时候。

下面的例子，说明静态变量的初始化语句`static int S = 3;`只执行了一次。调用 3 次 `f()`，变量`S`的值是累加的，说明每次的操作都有效

```c
int S = 12;
int f(void){
    static int S = 3; // 静态本地变量。删掉 static 关键字，结果完全不同
    printf("In %s 1: s = %d\n", __func__, S);
    S += 2;
    printf("In %s 2: S = %d\n", __func__, S);
    return S;
}
int main(void){
    printf("f() return: %d\n", f());
    printf("f() return: %d\n", f());
    printf("f() return: %d\n", f());
    return 0;
}
```

本质上，静态本地变量是特殊的全局变量，它们位于相同的内存区域。

```c
int G = 12;
int f(void){
    static int S = 3;
    int L = 9;
    // G:0x10c2ff018  S:0x10c2ff01c L:0x7ffee39014cc
    printf("G:%p\tS:%p\tL:%p\n", &G, &S, &L);
    return S;
}
int main(void){
    f();
    return 0;
}
```

##### 变量生存期与指针

如果一个函数返回指针：

- 返回本地变量的指针（地址），是危险的。本地变量在函数调用时已经死亡，地址没有意义。
- 返回全局变量或者静态变量的指针（地址），是安全的。

下面就是返回本地变量指针，出现的惨剧。

```c
int * fun(void){
    int i = 12;
    return &i; // 返回本地变量的指针
}
void kf(void){
    int k = 24;
    printf("k=%d\n", k);
}
int main(void){
    int *p = fun();        // 这个指针，其实指向一块无意义的地址
    printf("*p=%d\n", *p); // 12
    kf();                  // 24 这块内存被分配给了另一个函数的变量 k
    printf("*p=%d\n", *p); // 24
    return 0;
}
```

返回在函数内 malloc 的内存是安全带，但是容易造成问题。比较合适的方法，是返回传入的指针。







## 抽象数据类型Abstract Data Type

### 尺寸可变的数组

应该做到：

- 可以伸缩
- 获取目前的大小
- 数据的增删改查

还是看看 Redis 的实现，裁剪后建一个小库

### 链表



