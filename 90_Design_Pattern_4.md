[TOC]


### 迭代器与组合模式

每种编程语言都有不同的数据结构工具，每一种都有自己的优缺点、使用时机。

迭代器模式：为java不同的数据结构，建立统一的遍历方式，而不暴露内部实现。

组合模式：创建超大的数据结构。

#### 迭代器模式

设计任务：一个酒店，煎饼屋负责早餐，餐厅负责午餐。他们的菜单内容是一样的`MenuItem`，但菜单使用了不同的数据结构（见下面代码）。现在，要为他们制作统一的自动点餐机。需要实现功能：

- `printMenu()`打印所有的菜单项
- `printBreakfastMenu()`只打印早餐
- `printLunchMenu()`只打印午餐
- `printVegetarianMenu()`只打印素食菜单
- `isItemVegetarian(String name)`查询这个菜点是不是素食

##### 简单实现：

```java
import java.util.ArrayList;
class MenuItem {
	String name, description; boolean vegetarian; double price;
	public MenuItem(String name, String description,
	                boolean vegetarian, double price) {
		this.name = name; this.description = description;
		this.vegetarian = vegetarian; this.price = price; }
	public String getName() { return name; }
	public String getDescription() { return description; }
	public double getPrice() { return price; }
	public boolean isVegetarian() { return vegetarian; }
	public String toString() {return (name+"\t$"+price+"\t"+description);}
}
class PancakeHouseMenu {
	ArrayList<MenuItem> menuItems;
	public PancakeHouseMenu() {
		menuItems = new ArrayList<MenuItem>();
		addItem("K&B's Pancake Breakfast", "with eggs, toast", true, 2.99);
		addItem("Regular Pancake Breakfast", "with eggs, sausage", false,2.99);
		addItem("Blueberry Pancakes","with blueberries", true,3.49);
		addItem("Waffles", "with blueberries or strawberries",true,3.59);}
	public void addItem(String name, String description,
		boolean vegetarian, double price){
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menuItem);}
	public ArrayList<MenuItem> getMenuItems(){ return menuItems;}
	public String toString(){ return "Pancake House Menu";}
}
class DinerMenu{
	static final int MAX_ITEMS = 6;
	int numberOfItems = 0; MenuItem[] menuItems;
	public DinerMenu() {
		menuItems = new MenuItem[MAX_ITEMS];
		addItem("Vegetarian BLT",
			"(Fakin')Bacon with lettuce&tomato on wheat", true, 2.99);
		addItem("BLT", "Bacon with lettuce & tomato on wheat", false, 2.99);
		addItem("Soup of the day","with a side of potato salad", false, 3.29);
		addItem("Hotdog","with saurkraut, relish, onions, cheese",false, 3.05);
		addItem("Steamed Veggies over Brown Rice", "", true, 3.99);
		addItem("Pasta", "Spaghetti with Marinara Sauce, bread",true, 3.89);}
	public void addItem(String name, String description,
	                     boolean vegetarian, double price) {
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		if (numberOfItems >= MAX_ITEMS) {
			System.err.println("Sorry, menu is full!  Can't add item to menu");
		} else {
			menuItems[numberOfItems] = menuItem;
			numberOfItems = numberOfItems + 1;
		}
	}
	public MenuItem[] getMenuItems(){ return menuItems; }
	public String toString(){ return "Diner Menu";}
}
class OrderMachine {
	PancakeHouseMenu pancakeHouseMenu; DinerMenu dinerMenu;
	public OrderMachine() {
		pancakeHouseMenu = new PancakeHouseMenu();
		dinerMenu = new DinerMenu(); }
	public void printMenu() {
		System.out.println("MENU\n----\n- BREAKFAST -");
		ArrayList<MenuItem> breakfastItems = pancakeHouseMenu.getMenuItems();
		for (int i = 0; i < breakfastItems.size(); i++) {
			MenuItem item = (MenuItem) breakfastItems.get(i);
			System.out.println(item);
		}
		System.out.println("\n- LUNCH -");
		MenuItem[] lunchItems = dinerMenu.getMenuItems();
		for (int i = 0; i < lunchItems.length; i++) {
			System.out.println(lunchItems[i]);
		}
	}
}
public class SimpleHotel { // 调用示例
    public static void main(String[] args) {
		(new OrderMachine()).printMenu(); }
}
```

OrderMachine点餐机只实现了`printMenu()`，但其他的方法，也大致相似。因为有数组、ArrayList两种数据结构，所以每个方法内部必须用两个for循环去遍历。问题有：

- 这是针对PancakeHouseMenu和DinerMenu的具体实现编码，而不是针对接口和抽象，造成过度耦合。
- OrderMachine需要知道PancakeHouseMenu的内部实现才能编码，这违反了封装。
- 后果：数据结构稍微有些改动，就必须改变。比如，新增晚餐菜单，内部由Set实现，就必须加上一个for循环。比如，PancakeHouseMenu从ArrayList改为HashMap，就必须改写遍历代码。

##### 迭代器模式封装遍历

在这个案例里，变化的因子是：因为采用不同的数据结构（Collection）类型，造成的数据访问不同。所以要创造一个通用的遍历方式：

迭代器模式：提供一种方法，顺序访问（遍历）聚合对象中数据结构内的各个元素，但是又不暴露其内部的实现。

有一个统一的方法访问不同数据结构中的每一个对象，你就可以只用一种方式遍历了（多态遍历）。

```java
import java.util.ArrayList;
interface Iterator{ // 定义统一的迭代器
	boolean hasNext(); // 是否还有下一项
	Object next(); // 返回下一项
}
class MenuItem { } // 这个类不变
class PancakeHouseMenu { // 其余方法不变，只替换getMenuItems()方法
	// public ArrayList<MenuItem> getMenuItems(){ return menuItems;}
	public Iterator createIterator(){ //不返回具体数据结构，而是提供迭代器
         return new PancakeHouseIterator(menuItems);}
}
class PancakeHouseIterator implements Iterator{ //针对特定数据结构，实现迭代器
	int pointer = 0; ArrayList<MenuItem> items;
	public PancakeHouseIterator(ArrayList<MenuItem> menuItems){ items = menuItems;}
	public boolean hasNext(){
		return (pointer >= items.size() || items.get(pointer) == null)
			? false : true; }
	public Object next(){ return items.get(pointer++); }
}
class DinerMenu{ // 其余方法不变，只替换getMenuItems()方法
	public Iterator createIterator(){ return new DinerIterator(menuItems);}
	public MenuItem[] getMenuItems(){ return menuItems; }
}
class DinerIterator implements Iterator{ //针对特定数据结构，实现迭代器
	int pointer = 0; MenuItem[] items;
	public DinerIterator(MenuItem[] menuItems){ items = menuItems;}
	public boolean hasNext(){
		return (pointer >= items.length || items[pointer] == null)
			? false : true; }
	public Object next(){ return items[pointer++]; }
}
class OrderMachine {
	PancakeHouseMenu pancakeHouseMenu; DinerMenu dinerMenu;
	public OrderMachine() {
		pancakeHouseMenu = new PancakeHouseMenu();
		dinerMenu = new DinerMenu(); }
	public void printMenu() { // 迭代器，统一的遍历方式
		System.out.println("---- MENU ----\nBREAKFAST");
		Iterator breakfastIter = pancakeHouseMenu.createIterator();
		prt(breakfastIter);
		System.out.println("\nLUNCH");
		prt(dinerMenu.createIterator());
	}
	public void prt(Iterator it){ // 为了避免冗余，抽取函数
		while(it.hasNext()){System.out.println("- " + it.next()); }}
}
public class IterHotel {
    public static void main(String[] args) {
		(new OrderMachine()).printMenu(); }
}
```

这样，就将菜单内部的数据结构封装起来了。无需知道内部实现，只要一个迭代器接口，就可以遍历。但是仍然有改善的空间：

- 使用 JDK内置的`interface Iterator`。特别强调的是它的`remove()`方法，如果不需要使用，抛出一个`UnsupportedOperationException`异常就可，而不真正实现它。另外，这种对数据的改动，一般多线程不安全，所以多线程最好不要调用`remove()`。
- OrderMachine仍然与具体类PancakeHouseMenu之类的挂钩，应当为此抽象出一个接口。

```java
// JDKIterHotel.java 全部代码
import java.util.ArrayList;
import java.util.Iterator; // 引入java的 interface Iterator<E>
class MenuItem { // 这个类不变
	String name, description; boolean vegetarian; double price;
	public MenuItem(String name, String description,
	                boolean vegetarian, double price) {
		this.name = name; this.description = description;
		this.vegetarian = vegetarian; this.price = price; }
	public String getName() { return name; }
	public String getDescription() { return description; }
	public double getPrice() { return price; }
	public boolean isVegetarian() { return vegetarian; }
	public String toString() {return (name+"\t$"+price+"\t"+description);}
}
interface  Menu { public Iterator createIterator(); }
class PancakeHouseMenu implements Menu {
	ArrayList<MenuItem> menuItems;
	public PancakeHouseMenu() {
		menuItems = new ArrayList<MenuItem>();
		addItem("K&B's Pancake Breakfast", "with eggs, toast", true, 2.99);
		addItem("Regular Pancake Breakfast", "with eggs, sausage", false,2.99);
		addItem("Blueberry Pancakes","with blueberries", true,3.49);
		addItem("Waffles", "with blueberries or strawberries",true,3.59);}
	public void addItem(String name, String description,
		boolean vegetarian, double price){
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menuItem);}
     //ArrayList已有迭代器的接口，直接使用就可。无需PancakeHouseIterator类
	public Iterator createIterator(){ return menuItems.iterator();}
	public String toString(){ return "Pancake House Menu";}
}
class DinerMenu implements Menu {
	static final int MAX_ITEMS = 6;
	int numberOfItems = 0; MenuItem[] menuItems;
	public DinerMenu() {
		menuItems = new MenuItem[MAX_ITEMS];
		addItem("Vegetarian BLT",
			"(Fakin')Bacon with lettuce&tomato on wheat", true, 2.99);
		addItem("BLT", "Bacon with lettuce & tomato on wheat", false, 2.99);
		addItem("Soup of the day","with a side of potato salad", false, 3.29);
		addItem("Hotdog","with saurkraut, relish, onions, cheese",false, 3.05);
		addItem("Steamed Veggies over Brown Rice", "", true, 3.99);
		addItem("Pasta", "Spaghetti with Marinara Sauce, bread",true, 3.89);}
	public void addItem(String name, String description,
	                     boolean vegetarian, double price) {
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		if (numberOfItems >= MAX_ITEMS) {
			System.err.println("Sorry, menu is full!  Can't add item to menu");
		} else {
			menuItems[numberOfItems] = menuItem;
			numberOfItems = numberOfItems + 1;
		}
	}
  //数组没有迭代器的接口。必须使用DinerIterator类
	public Iterator createIterator(){ return new DinerIterator(menuItems);}
	public String toString(){ return "Diner Menu";}
}
class DinerIterator implements Iterator {// 必须写DinerIterator类
	int pointer = 0; MenuItem[] items;
	public DinerIterator(MenuItem[] menuItems){ items = menuItems;}
	public boolean hasNext(){
		return (pointer>=items.length || items[pointer]==null) ? false:true;}
	public Object next(){ return items[pointer++]; }
	// public void remove() { 如果不需实现，直接throw异常就可
	// 	throw new UnsupportedOperationException("Not support remove"); }
	public void remove(){
		for (int i = pointer; i < items.length-1; i++) {
			items[i] = items[i+1];}
		items[items.length - 1] = null;
	}
}
class OrderMachine {
	Menu pancakeHouseMenu; Menu dinerMenu; //这里使用接口，而不是具体的类
	public OrderMachine(Menu pancakeHouseMenu, Menu dinerMenu) {
		this.pancakeHouseMenu = pancakeHouseMenu;
		this.dinerMenu = dinerMenu; }
	public void printMenu() {
		System.out.println("---- MENU ----\nBREAKFAST");
		prt(pancakeHouseMenu.createIterator());
		Iterator dinerIterator = dinerMenu.createIterator();
		System.out.println("\nLUNCH");
		prt(dinerIterator);
	}
	public void prt(Iterator it){
		while(it.hasNext()){System.out.println("- " + it.next()); }}
}
public class JDKIterHotel {
    public static void main(String[] args) {
		PancakeHouseMenu pancakeMenu = new PancakeHouseMenu();
		DinerMenu dinerMenu = new DinerMenu();
		OrderMachine machine = new OrderMachine(pancakeMenu, dinerMenu);
		machine.printMenu();}
}
```

UML图

```plantuml
interface Menu
Menu : createIterator(){//返回一个迭代器}
Menu *-right-> Iterator : "组合"
Menu <|-- DinerMenu
DinerMenu : createIterator()
DinerMenu : menuItems//某种数据结构
Menu <|-- PancakeHouseMenu
PancakeHouseMenu : createIterator()
PancakeHouseMenu : menuItems//某种数据结构
DinerMenu *--> MenuItem
PancakeHouseMenu *--> MenuItem
MenuItem : //数据结构内的对象
interface Iterator
Iterator : boolean hasNext()
Iterator : Object next(){}
Iterator : void remove(){//可选。多线程不安全}
Iterator <|-- DinerIterator
Iterator <|-- PancakeHouseIterator
note bottom of PancakeHouseIterator
有些数据结构可以自动产生迭代器。就不需实现
end note
OrderMachine : printMenu()
OrderMachine -down-> Menu
OrderMachine -down-> Iterator
main ()- OrderMachine
```

按照这种方式，使用者OrderMachine只需要调用`Menu : createIterator()`获取迭代器，然后调用`Iterator : next()` 之类的方法进行迭代。这样，就无需知道具体的数据结构了，同时，OrderMachine只依赖两个`interface `：Menu和Iterator，而不依赖它们的子类——针对接口编程而不是针对具体实现编程。

很多数据结构都可以自动产生 iterator，所以对于大部分数据结构来说，无需手动实现Iterator接口。只是数组需要实现。

旧版的java中，遍历器有可能是`first() next() isDone() currentItem()`这几个方法。但都差不多。

迭代器内部，迭代时既可以指针一步步向前，也可以一步步向后。而且，迭代，意味着没有次序，只是全部过一遍而已；有没有次序、有没有重复，这是具体的数据结构决定的。

##### 单一责任原则

在上面的迭代器模式案例中，遍历对象的责任由迭代器类承担，比如`DinerIterator`，而持有对象的责任由聚合对象承担，比如`DinerMenu`。其实它们完全可以归拢到一起，为什么不呢？

- 简化了各自的接口和实现
- 责任更清晰。聚合对象只关注对象和数据结构的管理；迭代器只关注遍历。否则，如果数据结构改变，这个类要改变；遍历方式改变，这个类也要改变。

**单一责任**原则：让一个类应该只有一个引起变化的原因，区分责任，让每个类尽量保持单一的责任。类的每个责任，都有变更的潜在可能。如果一个类承担很多的责任，那么每个责任的细节有了变动，这个类就必须变动。

这听起来很简单，却非常难做到：我们的大脑习惯归纳和整合，尽管知道它们属于不同的责任，但我们还是习惯于将它们归拢到一块。

内聚性Cohesion：一个组件紧密地达到单一目的或责任的程度。当一个组件被设计成只支持一组相关的功能时，就是高内聚的，反之，支持多组不相干的功能，就是低内聚。内聚是比「单一责任」更普遍的概念。一般来说，遵守单一责任的类，容易高内聚，而且容易维护。

##### 迭代器模式应对变更

如果餐厅需要实现一个需求：每周一周三周五周日，与周二周四周六不同。可以新增一个特殊的迭代器，然后换用这个迭代器就可以：

```java
import java.util.Calendar;
class AlternativeDinnerIterator implements Iterator {
	int pointer; MenuItem[] items;
	public DinerIterator(MenuItem[] menuItems){
		items = menuItems;
		pointer =  Calendar.getInstance().get(calendar.DAY_OF_WEEK) % 2; }
	public boolean hasNext(){
		return (pointer>=items.length || items[pointer]==null) ? false:true;}
	public Object next(){
		MenuItem menuItem = items[pointer];
		pointer += 2;
		return menuItem; }
}
class DinerMenu implements Menu { // 然后仅需要改变这个类的一个方法
  public Iterator createIterator(){ return new AlternativeDinnerIterator(menuItems);}
}
```

如果有个咖啡厅供应小食充当午饭，增加这个菜谱，它使用的是HashMap，而餐厅变为晚饭。那么这个酒店就要变为

```java
import java.util.*;
class MenuItem {
	String name, description; boolean vegetarian; double price;
	public MenuItem(String name, String description,boolean vegetarian, double price) {
		this.name = name; this.description = description;
		this.vegetarian = vegetarian; this.price = price; }
	public String getName() { return name; }
	public String getDescription() { return description; }
	public double getPrice() { return price; }
	public boolean isVegetarian() { return vegetarian; }
	public String toString() {return (name+"\t$"+price+"\t"+description);}
}
interface  Menu { public Iterator createIterator(); }
class PancakeHouseMenu implements Menu {
	ArrayList<MenuItem> menuItems;
	public PancakeHouseMenu() {
		menuItems = new ArrayList<MenuItem>();
		addItem("K&B's Pancake Breakfast", "with eggs, toast", true, 2.99);
		addItem("Regular Pancake Breakfast", "with eggs, sausage", false,2.99);
		addItem("Blueberry Pancakes","with blueberries", true,3.49);
		addItem("Waffles", "with blueberries or strawberries",true,3.59);}
	public void addItem(String name, String description,
		boolean vegetarian, double price){
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menuItem);}
	public Iterator createIterator(){ return menuItems.iterator();}
	public String toString(){ return "Pancake House Menu";}
}
class CafeMenu implements Menu {
	HashMap<String, MenuItem> menuItems = new HashMap<String, MenuItem>();
	public CafeMenu() {
		addItem("Veggie Burger and Air Fries","lettuce, tomato", true, 3.99);
		addItem("Soup of the day","with a side salad",false, 3.69);
		addItem("Burrito","with pinto beans, salsa, guacamole",true, 4.29);}
	public void addItem(String name, String description,
	                     boolean vegetarian, double price){
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.put(menuItem.getName(), menuItem);
	}
	public Iterator<MenuItem> createIterator(){return menuItems.values().iterator();}
}
class DinerMenu implements Menu {
	static final int MAX_ITEMS = 6;
	int numberOfItems = 0; MenuItem[] menuItems;
	public DinerMenu() {
		menuItems = new MenuItem[MAX_ITEMS];
		addItem("Vegetarian BLT",
			"(Fakin')Bacon with lettuce&tomato on wheat", true, 2.99);
		addItem("BLT", "Bacon with lettuce & tomato on wheat", false, 2.99);
		addItem("Soup of the day","with a side of potato salad", false, 3.29);
		addItem("Hotdog","with saurkraut, relish, onions, cheese",false, 3.05);
		addItem("Steamed Veggies over Brown Rice", "", true, 3.99);
		addItem("Pasta", "Spaghetti with Marinara Sauce, bread",true, 3.89);}
	public void addItem(String name, String description,
	                     boolean vegetarian, double price) {
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		if (numberOfItems >= MAX_ITEMS) {
			System.err.println("Sorry, menu is full!  Can't add item to menu");
		} else {
			menuItems[numberOfItems] = menuItem;
			numberOfItems = numberOfItems + 1;
		}
	}
	public Iterator createIterator(){ return new DinerIterator(menuItems);}
	public MenuItem[] getMenuItems(){ return menuItems; }
	public String toString(){ return "Diner Menu";}
}
class DinerIterator implements Iterator {
	int pointer = 0; MenuItem[] items;
	public DinerIterator(MenuItem[] menuItems){ items = menuItems;}
	public boolean hasNext(){
		return (pointer>=items.length || items[pointer]==null) ? false:true;}
	public Object next(){ return items[pointer++]; }
	public void remove(){
		for (int i = pointer; i < items.length-1; i++) {
			items[i] = items[i+1];}
		items[items.length - 1] = null;
	}
}
class OrderMachine {
	Menu pancakeHouseMenu; Menu dinerMenu; Menu cafeMenu; //增加一项，同时修改构造器
	public OrderMachine(Menu pancakeHouseMenu,Menu cafeMenu,Menu dinerMenu){
		this.pancakeHouseMenu = pancakeHouseMenu;
		this.cafeMenu = cafeMenu;
		this.dinerMenu = dinerMenu;}
	public void printMenu() { // 增加一项。餐厅变为晚餐提供者，所以改变打印顺序
		System.out.println("---- MENU ----\nBREAKFAST");
		prt(pancakeHouseMenu.createIterator());
		System.out.println("\nLUNCH");
		prt(cafeMenu.createIterator());
		System.out.println("\nDINNER");
		prt(dinerMenu.createIterator());}
	public void prt(Iterator it){
		while(it.hasNext()){System.out.println("- " + it.next()); }}
}
public class JDKallIterHotel {
    public static void main(String[] args) {
		PancakeHouseMenu pancakeMenu = new PancakeHouseMenu();
		CafeMenu cafeMenu = new CafeMenu();
		DinerMenu dinerMenu = new DinerMenu();
		OrderMachine machine = new OrderMachine(pancakeMenu, cafeMenu, dinerMenu);
		machine.printMenu();}
}
```

- `class CafeMenu implements Menu`实现了`createIterator()`接口。同时要注意，HashMap是key-value数据结构，是`menuItems.values().iterator()`提供遍历器的。
- 同时稍微修改OrderMachine类

由此可知迭代器模式的好处：

- 用简单的方式遍历
- 无需知道内部实现
- 使用迭代器接口，与实现解耦。这样，
  - 就可以遍历不同的数据结构（多态遍历）
  - 同时具有很强的扩展性（新增一种数据结构、改变现有数据结构，都不会被影响）

几乎所有的java 数据结构，都提供了`java.util.Iterator`接口的实现，自动创建迭代器。如果没有提供（比如数组），可以手动实现一个。

##### 集合类

java1.5（或者java5）的数据类型如下：

- 基本类型：boolean、char 16、byte 8、short 16、int 32、long 64、float 32、double 64、void（前面是类型名，数字是大小，char 是 16 bit）。像C/C++一样存储在栈里、直接操作内存空间，而不是存储在堆里、使用引用操作内存空间。

- 包装器类型。每一种基本类型，都有相关的包装器类型。只有包装器类型，才能放进容器中。

  ```mermaid
  graph BT
  Number --> Object; Boolean --> Object; Character --> Object; Void --> Object;
  Byte --> Number; Short --> Number; Integer --> Number; Long --> Number; Float --> Number; Double  --> Number
  ```

- 数组：基本类型数组`int[] a = new int[5];`里面的元素就是内存空间，并且初始化为0。对象数组`Object[] b = new Object[5];`里面的元素是对象的引用，并且初始化为null

- 工具类：`java.util.Arrays;java.util.Collections;`都是工具类，Arrays是数组的工具，而Collections是容器的工具（Collection是所有容器类的一个接口）。`java.util.Comparator;java.lang.Comparable`是提供比较的接口，这样Arrays才能进行排序Collections

- 容器类库

  ```plantuml
  @startuml
  interface Map
  Map .right.> Collection : Produces
  abstract class AbstractMap
  interface SortedMap
  Map <|.. AbstractMap
  Map <|-- SortedMap
  AbstractMap <|-- HashMap
  AbstractMap <|-- WeakHashMap
  AbstractMap <|-- HashTable
  AbstractMap <|-- IdentityHashMap
  HashMap <|-- LinkedHashMap
  interface NavigableMap
  SortedMap   <|-- NavigableMap
  AbstractMap <|-- TreeMap
  NavigableMap<|.. TreeMap

  interface Iterable
  interface Collection
  Iterable <|-- Collection
  interface List
  abstract class AbstractCollection
  interface Set
  interface Queue
  Collection <|-- Set
  Collection <|.. AbstractCollection
  Collection <|-- Queue
  Collection <|-- List

  abstract class AbstractSet
  Set <|.. AbstractSet
  AbstractCollection <|-- AbstractSet
  AbstractSet <|-- HashSet
  HashSet     <|-- LinkedHashSet
  interface SortedSet
  interface NavigableSet
  Set      <|-- SortedSet
  SortedSet<|-- NavigableSet
  AbstractSet <|-- TreeSet
  NavigableSet<|.. TreeSet

  interface Deque
  Queue <|-- Deque
  abstract class AbstractQueue
  AbstractCollection <|-- AbstractQueue
  Deque              <|.. AbstractQueue
  AbstractQueue <|-- PriorityQueue
  AbstractCollection <|-- ArrayDeque
  Deque              <|.. ArrayDeque

  abstract class AbstractList
  AbstractCollection <|-- AbstractList
  List               <|.. AbstractList
  AbstractList <|--  Vector
  Vector <|-- Stack
  AbstractList <|--  ArrayList
  AbstractList <|--  LinkedList
  Deque  <|.. LinkedList

  interface Iterator
  interface ListIterator
  Iterator <|-- ListIterator
  Iterable .right.> Iterator : Produces
  ListIterator <.. List : Produces
  @enduml
  ```

  - 历史遗留的数据结构：Vector、Stack、HashTable
  - 通常用来替换它们：ArrayList、LinkedList、HashMap。另外，HashSet也很常用。
  - 集合细讲：http://blog.csdn.net/Guofengpu/article/details/52092333
  - 线程安全：http://blog.csdn.net/u014482758/article/details/50669483

interface Collection有很多的方法。有以下几大类：

- 新增、删除元素
- 查询。`size()`元素个数。
- 产生迭代器`iterator()`
- 转换：`toArray()`转为数组

interface Map：

- 新增、删除、查询
- 转换：`m.values()`产生一个Collection，`m.values().iterator()`产生一个迭代器。`m.keySet()`产生一个Set

##### For-each语法

java1.5（或者java5）产生了一种新的语法，可以不使用迭代器模式了。

```java
public static void main(String[] args) {
	// 基本类型构成的数组
	int[] intArray = new int[]{1, 2, 3, 4, 5};
	for (int item : intArray) { System.out.println(item); }
	// 对象 构成的数组
	Integer[] array = new Integer[]{1, 2, 3, 4, 5};
	for (Integer item : array) { System.out.println(item); }
	// 对象 构成的 Collection
	List<Integer> list = new ArrayList<Integer>(Arrays.asList(array));
	Collections.addAll(list, 6, 7, 8, 9, 10); // 另一种一次填充很多数据的方法
	for (Integer item : list) { System.out.println(item); }
	// 对象 构成的 Map
	Map<String, Integer> map = new HashMap<String, Integer>() {{
		put("1", 1); put("2", 2); put("3", 3); }};
	for (String key : map.keySet()) {
		System.out.println("key: " + key + "value: " + map.get(key));
	}
}
```



#### 组合模式

虽然迭代器模式很不错，但是依然有些不足：

- 每个方法本质上仍然是一个数据结构一个遍历循环，而不是一个循环遍历所有的数据结构。如果想构建一个树形结构，现有的混乱的数据结构是无法实现的。
- 据说餐厅需要提供甜点，那么就需要一个子菜单。餐厅用的是数组，没法嵌入子菜单（`MenuItem[]  items;`这个数组不是二维数组，里面的元素都是MenuItem对象，不可能有一个元素是另一个数组）。

树形数据结构的基本概念：有根树，自由树。根节点，非终结节点，叶节点。父节点、子节点、兄弟节点。

```mermaid
graph TD
root-->node; root-->leaf_1; node-->leaf_2; node-->leaf_3;
```

组合模式：将对象组合成树形结构来表现「整体/部分」层次结构。组合模式能让使用者以一致的方式处理个别对象以及对象的组合。所谓对象的组合，就是非终结节点（分支节点），它既可能包含叶节点，也可能包含其他分支。所谓个别对象，就是叶节点。

```mermaid
graph TD
All-->PancakeHouse; PancakeHouse-->1; PancakeHouse-->2; PancakeHouse-->3; PancakeHouse-->4;
All-->DinnerMenu; DinnerMenu-->a; DinnerMenu-->b; DinnerMenu-->c;
DinnerMenu-->Dessert; Dessert-->糕; Dessert-->饼; Dessert-->酥;
All-->CafeMenu; CafeMenu-->A; CafeMenu-->B; CafeMenu-->COFFEE;
COFFEE-->Ca; COFFEE-->Ba; COFFEE-->Bi;
```

用创建树形的数据结构，就能包含组合（分支节点）和个别对象（叶节点），并且可以使用递归，用一种方式遍历所有的节点，而**忽略组合和个别对象的区别**。

```java
//  文件 MenuTest.java
import java.util.*;
abstract class MenuComponent {
	void add(MenuComponent component){throw new UnsupportedOperationException();}
	void remove(MenuComponent component){throw new UnsupportedOperationException();}
	MenuComponent getChild(int i){throw new UnsupportedOperationException();}
	String getName(){throw new UnsupportedOperationException();}
	String getDescript(){throw new UnsupportedOperationException();}
	double getPrice(){throw new UnsupportedOperationException();}
	boolean isVeg(){throw new UnsupportedOperationException();}
	void print(){throw new UnsupportedOperationException();}
}
class MenuItem extends MenuComponent {
	String name; String descript; boolean veg; double price;
	public MenuItem(String name, String descript,
	                boolean veg, double price){
		this.name = name; this.descript = descript;
		this.veg = veg; this.price = price; }
	public String getName() { return name; }
	public String getDescript() { return descript; }
	public double getPrice() { return price; }
	public boolean isVeg() { return veg; }
	public void print() {
        System.out.printf("\t%s%s, %s\n\t-- %s\n", getName(),
            (isVeg()) ? "" : "(v)",   getPrice(), getDescript());
	}
}
class Menu extends MenuComponent {
	ArrayList<MenuComponent> components; String name; String descript;
	public Menu(String name, String descript) {
		this.name = name; this.descript = descript;
        components = new ArrayList<MenuComponent>(); }
	public void add(MenuComponent component){
        this.components.add(component);}
	public void remove(MenuComponent component){
        this.components.remove(component);}
	public MenuComponent getChild(int i) {
        return (MenuComponent)components.get(i);}
    public String getName() { return name; }
    public String getDescript() { return descript; }
	public void print() {
        System.out.printf("%n %s, %s %n", getName(), getDescript());
        System.out.println("---------------------");
		Iterator<MenuComponent> iter = components.iterator();//利用List产生迭代器
		while (iter.hasNext()){ //迭代+递归
              ((MenuComponent)iter.next()).print();}
	}
}
class OrderMachine {
	MenuComponent allMenus;
	public OrderMachine(MenuComponent all_menu){allMenus = all_menu;}
	public void printMenu() {allMenus.print();}
}
public class MenuTest {
	public static void main(String args[]) { // 首先构造数据
        MenuComponent allMenus = new Menu("ALL MENUS", "All menus combined");
        MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE", "Breakfast");
        allMenus.add(pancakeHouseMenu);
		pancakeHouseMenu.add(new MenuItem( "K&B's Pancake Breakfast",
			"Pancakes with scrambled eggs, and toast",
			true, 2.99));
		pancakeHouseMenu.add(new MenuItem( "Regular Pancake Breakfast",
			"Pancakes with fried eggs, sausage",
			false, 2.99));
		pancakeHouseMenu.add(new MenuItem( "Blueberry Pancakes",
			"Pancakes made with fresh blueberries, and blueberry syrup",
			true, 3.49));
		pancakeHouseMenu.add(new MenuItem( "Waffles",
			"Waffles, with your choice of blueberries or strawberries",
            true, 3.59));

        MenuComponent dinerMenu = new Menu("DINER MENU", "Dinner");
        allMenus.add(dinerMenu);
		dinerMenu.add(new MenuItem( "Vegetarian BLT",
			"(Fakin') Bacon with lettuce & tomato on whole wheat",
			true, 2.99));
		dinerMenu.add(new MenuItem( "BLT",
			"Bacon with lettuce & tomato on whole wheat",
			false, 2.99));
		dinerMenu.add(new MenuItem( "Soup of the day",
			"A bowl of the soup of the day, with a side of potato salad",
			false, 3.29));
		dinerMenu.add(new MenuItem( "Hotdog",
			"A hot dog, with saurkraut, relish, onions, topped with cheese",
			false, 3.05));
		dinerMenu.add(new MenuItem( "Steamed Veggies and Brown Rice",
			"Steamed vegetables over brown rice",
			true, 3.99));
		dinerMenu.add(new MenuItem( "Pasta",
			"Spaghetti with Marinara Sauce, and a slice of sourdough bread",
            true, 3.89));

        MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert");
		dinerMenu.add(dessertMenu);
		dessertMenu.add(new MenuItem("Apple Pie",
			"Apple pie with a flakey crust, topped with vanilla icecream",
			true, 1.59));
		dessertMenu.add(new MenuItem( "Cheesecake",
			"Creamy New York cheesecake, with a chocolate graham crust",
			true, 1.99));
		dessertMenu.add(new MenuItem( "Sorbet",
			"A scoop of raspberry and a scoop of lime",
            true, 1.89));

        MenuComponent cafeMenu = new Menu("CAFE MENU", "Lunch");
        allMenus.add(cafeMenu);
		cafeMenu.add(new MenuItem( "Veggie Burger and Air Fries",
			"Veggie burger on a whole wheat bun, lettuce, tomato, and fries",
			true, 3.99));
		cafeMenu.add(new MenuItem( "Soup of the day",
			"A cup of the soup of the day, with a side salad",
			false, 3.69));
		cafeMenu.add(new MenuItem( "Burrito",
			"A large burrito, with whole pinto beans, salsa, guacamole",
            true, 4.29));

        MenuComponent coffeeMenu = new Menu("COFFEE MENU", "afternoon coffee");
		cafeMenu.add(coffeeMenu);
		coffeeMenu.add(new MenuItem("Coffee Cake",
			"Crumbly cake topped with cinnamon and walnuts",
			true, 1.59));
		coffeeMenu.add(new MenuItem("Bagel",
			"Flavors include sesame, poppyseed, cinnamon raisin, pumpkin",
			false, 0.69));
		coffeeMenu.add(new MenuItem("Biscotti",
			"Three almond or hazelnut biscotti cookies",
            true, 0.89));

		OrderMachine machine = new OrderMachine(allMenus);
        machine.printMenu(); // 递归打印
	}
}
```

在C语言中，树状结构是利用自引用结构体实现的：

```c
struct tnode { 
    char *word;  int count;  // 结构内的数据
    struct tnode *left, *right; // 左右节点（这是二叉树）
};
```

而C语言中的结构体，几乎就是OO语言中的class。这个UML大致如下，可以看出，MenuComponent实现了自引用。

```plantuml
abstract class MenuComponent {
	void print(){}
	String getName(){}
	String getDescript(){}
	double getPrice(){ 叶节点 }
	boolean isVeg(){ 叶节点 }
	void add(MenuComponent component){ 分支 }
	void remove(MenuComponent component){ 分支 }
	MenuComponent getChild(int i){ 分支 }
}
OrderMachine *.right.> MenuComponent
MenuComponent <|-- MenuItem
MenuComponent <|-- Menu
MenuItem : String getName(){}
MenuItem : String getDescript(){}
MenuItem : void print(){}
MenuItem : double getPrice(){ 特有 }
MenuItem : boolean isVeg(){ 特有 }
Menu : {field}ArrayList<MenuComponent> 自引用父类
Menu : String getName(){}
Menu : String getDescript(){}
Menu : void print(){}
Menu : void add(MenuComponent component){ 特有 }
Menu : void remove(MenuComponent component){ 特有 }
Menu : MenuComponent getChild(int i){ 特有 }
Menu *..> MenuComponent
main *--> MenuComponent
main *--> OrderMachine
```

MenuComponent是叶节点和分支节点的抽象父类（而不是`interface`），包含它们二者所有的方法，并且默认抛出异常。子类需要什么方法，就需要自己覆盖，否则必然异常。MenuItem 和 Menu类 各自覆盖自己需要的方法，从同一个父类继承而来，就分化称为叶节点和分支节点。

这个设计其实并不符合单一责任原则的。不仅仅是抽象类MenuComponent包罗了两种子类的功能，就以子类Menu来说，它包括了自身基本功能外，还要管理自己的子节点。这是折衷——牺牲`单一设计`原则，换取`透明性Transparency`：让抽象类MenuComponent囊括叶节点、分支节点两种子类的功能，客户就可以不管它是什么节点，用通用的方式处理。这种方式也有隐患：如果使用者尝试一个逻辑不通、没有意义的操作，比如在叶节点上添加元素，就会引发异常。

- 另一种更强调安全性的方式，是把分支节点、叶节点区别对待。比如使用`if (ob instanceof Menu){...}`的方式处理。但这样使用者就没法使用通用的方式处理整个数据结构了。

在组合模式中，充满了这种安全性、透明性之间的折衷。（个人觉得，加一个`isLeafNode()`接口，是最好的折衷）。

需要管理对象的集合，而这些对象彼此之间有「整体/部分」的关系，并且想用一致的方式调用时，就需要组合模式。

「整体/部分」的关系：比如用户界面，顶层组件Frame/Panel包含其他的组件（菜单、文字面板、滚动条、按钮）。GUI包含了若干部分，但显示的时候，又是一个整体。

用一致的方式调用：给分支节点、叶节点建立一个统一的接口，调用的时候，就使用这些通用的方法。为了保持透明性，组合内所有的对象都必须实现相同的接口，否则调用者就必须判断对象的类型——这就不再是组合模式了。这是组合模式最大的优点——不需要判断分支节点还是叶节点，使用通用的方式操作整个数据结构。这种方式的缺点：有些对象具备一些没有意义的方法。规避方式：dummy不执行、返回false/null、抛异常。

一般来说，父节点持有子节点的指针，并且管理增删子节点。但是，也可以设计为：子节点也持有父节点的指针，这样，子节点也可以主动解除关系。

如果兄弟节点之间具有次序，那么增删管理、遍历就需要更仔细地考虑了。

如果数据结构非常复杂，或者遍历的代价太高，那么有必要实现组合节点的缓存。

##### 组合迭代器

新的需求：打印所有的素食菜单。

```java
import java.util.*;
class CompositeIter implements Iterator { //实现接口，构建迭代器类
    Stack stack = new Stack(); // LIFO堆栈。一般用LinkedList作LIFO/FIFO
    public CompositeIter(Iterator iter) {stack.push(iter);}//将顶层数据结构的迭代器放进堆栈
    public Object next(){
        if (hasNext()) {
            Iterator iter = (Iterator)stack.peek(); //查看栈顶的迭代器
            MenuComponent component = (MenuComponent)iter.next(); //迭代一个元素
            if (component instanceof Menu) { // 如果这个元素是分支节点，那就存入栈顶
                stack.push(component.createIterator());}
            return component; //不管这个节点是什么节点，都要返回
        } else {return null;}} //如果迭代器无法生成下一个元素，返回null
    public boolean hasNext(){
        if (stack.empty()) {return false; //如果堆栈已空，肯定是false
        } else { //如果堆栈不空，判断步骤如下： 
            Iterator iter = (Iterator)stack.peek();//先查看栈顶的迭代器
            if (!iter.hasNext()){//如果这个迭代器没有下一个元素
                stack.pop();  return hasNext();//从堆栈移除这个迭代器，递归调用本方法
            } else { return true;}}}//如果这个迭代器有下一个元素，返回true
    public void remove(){throw new UnsupportedOperationException();}//不支持删除
}
class NullIter implements Iterator { //空迭代器类
    public Object next(){ return null;}
    public boolean hasNext(){return false;}
    public void remove(){throw new UnsupportedOperationException();}
}
abstract class MenuComponent { //改动：超类增加一个接口
    Iterator createIterator(){throw new UnsupportedOperationException();}
}
class MenuItem extends MenuComponent { //仅有一个改动。返回空迭代器（而不是null）
    Iterator createIterator(){return new NullIter();} //如果返回null，调用者就需要判断是不是null
}
class Menu extends MenuComponent { //仅有一个改动
    Iterator createIterator(){ // 返回一个树结构迭代器
        return new CompositeIter(components.iterator());}
}
class OrderMachine {
    MenuComponent allMenus;
    public OrderMachine(MenuComponent all_menu){allMenus = all_menu;}
    public void printMenu() {allMenus.print();}
    public void printVegeMenu(){ //改动：如何调用迭代器
        System.out.println("\nVegetarian Menu\n---------------");
        Iterator iter = allMenus.createIterator();
        while(iter.hasNext()) {
            MenuComponent component = (MenuComponent)iter.next();
            try{ if(component.isVeg()){component.print();}
                }catch(UnsupportedOperationException e){} //do nothing
        }
    }
}
public class MenuIterTest {
    public static void main(String args[]) {      
        machine.printVegeMenu(); // machine.printMenu();唯一的改动
    }
}
```



### 状态模式

策略模式 和 状态模式 非常像：

- 策略模式：切换不同的算法，从而改变对象的行为。
- 状态模式：改变对象的状态，从而改变对象的行为。

每一个状态：代表以不同的配置、以某种方式行动。切换状态：必须做些事情，执行一些方法。



设计任务：自动售糖果的机器，状态和操作下图


```mermaid
graph LR;
    S(start:NoCoin)-->|insert|A[haveCoin]
    A[haveCoin] -->|eject| S
    A -->|turnTunck| C{sold...rollOut}
    C-->|.gum>0.|S
    C-->|.gum=0.|E(end:soldOut)
    style C fill:#bffffb
    style E fill:#bbbbff
    style S fill:#ffbbbb
```

##### 条件分支状态

步骤： 在对象内部创建一个变量表示状态，在方法内用条件判断，处理不同的状态。

1. 找出所有的状态。本例中，有4个状态。
2. 用一个成员变量表示状态，而且定义不同状态的值（相当于 #define OUT 0）
3. 将系统中可能发生的动作整合起来。它们怎么改变了状态？那些是外部接口？哪些是内部接口？
4. 每个动作都创建对应的方法。方法内部，用判断语句决定操作流。

```java
import java.io.*;
enum STATE {NO_QUARTER, HAS_QUARTER, SOLD, SOLD_OUT} //枚举4个状态。也可以用final static常量
class Machine {  // 状态机 类
    STATE state = STATE.SOLD_OUT; //这个变量跟踪状态。拆箱的状态SOLD_OUT
    int count = 0; //这个变量跟踪糖果数。拆箱的糖果数0
    public Machine(int num){ //初始化，启动糖果机：糖果数、状态
        count = num; if (count>0){state = STATE.NO_QUARTER;}}//填入糖果，就开始等待投币
    public PrintStream puts(Object ... args){// 简化打印
        return System.out.format(("" + " %s %n"), args);}
    public void insertQuarter() { // 投入25分硬币
        // if语句使用枚举  if (state == STATE.NO_QUARTER)
        switch (state) {
        case HAS_QUARTER: //已投币，不能再投
            puts("Has QUARTER, so you cannot insert another."); break;
        case NO_QUARTER: //未投币，收钱并且切换状态
            state = STATE.HAS_QUARTER;
            puts("you inserted a QUARTER."); break;
        case SOLD_OUT: //售罄。不能投币
            puts("SOLD OUT, you cannot insert."); break;
        case SOLD: //正在售糖出糖。不能投币。
            puts("please wait for gumball."); break;
        } }
    public void ejectQuater() { // 顾客尝试退钱
        switch (state) {
            case HAS_QUARTER: //已投币，退钱并且切换状态
                state = STATE.NO_QUARTER;
                puts("QUARTER return."); break;
            case NO_QUARTER: //未投币，不能退钱
                puts("you have not inserted."); break;
            case SOLD_OUT:   //售罄。
                puts("SOLD OUT, you cannot eject."); break;
            case SOLD:  //正在售糖出糖。
                puts("please wait for gumball."); break;
            } }
    public void turnCrank(){ //顾客转动手柄，买糖
        switch (state) {
            case HAS_QUARTER: //已投币，切换状态，出糖
                puts("Crank truned, please wait for gumball.");
                state = STATE.SOLD;  dispense(); break;
            case NO_QUARTER: //未投币，不能出糖
                puts("you have not inserted."); break;
            case SOLD_OUT:  //售罄。
                puts("SOLD OUT, you cannot turnCrank."); break;
            case SOLD: //正在售糖出糖，但顾客转手柄想多拿几个糖！
                puts("Turning times doesn't get another gumball"); break;
            }
    }
    private void dispense(){ //糖果机内部调用此方法，发放糖果。注意private
        switch (state) {
            case HAS_QUARTER: puts("error."); break;
            case NO_QUARTER: puts("error."); break;
            case SOLD_OUT: puts("SOLD OUT."); break;
            case SOLD:
                puts("A gumball is rolling out...");
                count -= 1;
                if (count > 0) { state = STATE.NO_QUARTER; //如果还有糖，就继续等人投币
                } else { //否则就售罄。
                    puts("Oops, out of gumballs"); state = STATE.SOLD_OUT; }
                break;
            }
    }
}
public class Conditionals {
    public static void main(String[] args) {
        Machine m = new Machine(3);
        System.out.println(m + "-------insert, turnCrank");
        m.insertQuarter(); m.turnCrank();
        System.out.println(m + "-------insert, eject, turnCrank");
        m.insertQuarter(); m.ejectQuater(); m.turnCrank();
        System.out.println(m + "-------insert, turnCrank, eject");
        m.insertQuarter(); m.turnCrank(); m.ejectQuater();
        System.out.println(m + "-------insert, insert, turnCrank");
        m.insertQuarter(); m.insertQuarter(); m.turnCrank();  }
}
```

这种实现的缺点：

- 冗余。所有与状态有关的函数，内部都差不多是“状态判断，执行语句”，逻辑几乎是一样的。冗余意味着维护容易出错，比如要增加一个状态，那么所有的功能都要改变，万一有个函数改错了……
- 高度耦合。每一个函数，对象的状态 与 操作语句混合在一起，没有将行为抽象为一个有限的状态机，导致可读性太差。
- 不能扩大规模。大型的状态机，一页页的复杂缠绕，同时又单调乏味的分支逻辑，维护绝对是噩梦。
- 未能遵循开放-关闭原则。

新的需求：有10%的概率中奖，一倍的钱得到2倍糖果。

如果说要增加一个赢家状态，那么上面每一个方法内部的条件语句都要更改。这样就暴露了上面的那些缺点。

##### 抽象状态机

糖果机用一系列的状态对象代表自己目前的状态，并且将动作委托给内部的状态对象。每个状态能够执行的行为都放在各自的类中，每个状态都实现它自己的动作。

- 首先定义State接口/抽象类，规划每个状态都有哪些方法
- 有多少个状态，就实现多少个状态类。它们都实现了State的接口
- 将实体的动作，委托给当前的状态。

```java
import java.util.Random; import java.io.*;
class GumballMachine { // 糖果机实体
    State soldOutState; // 可能的各种状态
    State noQuarterState;
    State hasQuarterState;
    State soldState;
    State winnerState;
    State state = soldOutState; //需要封装，用get()获取、用set()设置
    int count = 0;
    public GumballMachine(int gumNUM) {
        soldOutState = new SoldOutState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        soldState = new SoldState(this);
        winnerState = new WinnerState(this);
        this.count = gumNUM;
        if (gumNUM > 0) { state = noQuarterState; }
    }
    void setState(State state) { this.state = state; }
    void releaseBall() {
        System.out.println("A gumball is rolling out...");
        if (count > 0) { count -= 1; }
    }
    public void insertQuarter() { state.insertQuarter(); }
    public void ejectQuater() { state.ejectQuater(); }
    public void turnCrank() { state.turnCrank();state.dispense();}
}
abstract class State { //抽象状态接口：原代码用接口实现，但用抽象类更好
    GumballMachine gumballMachine;
    abstract public void insertQuarter();
    abstract public void ejectQuater();
    abstract public void turnCrank();
    abstract public void dispense();
    PrintStream puts(Object ...args){return System.out.format((""+"%s%n"),args);}
}
class NoQuarterState extends State { //具体的状态实现
    public NoQuarterState(GumballMachine g) { this.gumballMachine = g;}
    public void insertQuarter() {
        puts("you inserted a QUARTER.");
        gumballMachine.setState(gumballMachine.hasQuarterState);
    }
    public void ejectQuater() { puts("you have not inserted.");}
    public void turnCrank() { puts("you have not inserted.");}
    public void dispense() { puts("error.");}
}
class SoldOutState extends State {
    public SoldOutState(GumballMachine g) { this.gumballMachine = g;}
    public void insertQuarter() { puts("SOLD OUT, you cannot insert.");}
    public void ejectQuater() { puts("SOLD OUT, you cannot eject.");}
    public void turnCrank() { puts("SOLD OUT, you cannot turnCrank.");}
    public void dispense() { puts("SOLD OUT, you cannot dispense.");}
}
class HasQuarterState extends State {
    Random randomWinner = new Random(System.currentTimeMillis());
    public HasQuarterState(GumballMachine g) { this.gumballMachine = g;}
    public void insertQuarter(){puts("Has QUARTER, so you cannot insert another.");}
    public void ejectQuater() {
        puts("QUARTER return.");
        gumballMachine.setState(gumballMachine.noQuarterState);}
    public void turnCrank() {
        puts("Crank truned, please wait for gumball.");
        int winner = randomWinner.nextInt(10);
        if ((winner == 0) && (gumballMachine.count >= 2)) {
            gumballMachine.setState(gumballMachine.winnerState); // 剩余糖果必须大于 2
        } else {
            gumballMachine.setState(gumballMachine.soldState);
        }
    }
    public void dispense() {puts("error.");}
}
class SoldState extends State {
    public SoldState(GumballMachine g) { this.gumballMachine = g;}
    public void insertQuarter() { puts("please wait for gumball.");}
    public void ejectQuater() { puts("please wait for gumball.");}
    public void turnCrank() { puts("Turning times doesn't get another gumball");}
    public void dispense() {
        gumballMachine.releaseBall();
        if (gumballMachine.count > 0) {
            gumballMachine.setState(gumballMachine.noQuarterState);
        } else {
            puts("Oops, out of gumballs");
            gumballMachine.setState(gumballMachine.soldOutState);
        }
    }
}
class WinnerState extends SoldState {
    public WinnerState(GumballMachine g) {
        super(g); this.gumballMachine = g; }
    public void dispense() {
        puts("You're a WINNER! You get two with a QUARTER.");
        super.dispense(); super.dispense(); }
}
public class StateMachine {
    public static void main(String[] args) {
        GumballMachine m = new GumballMachine(5);
        System.out.println(m + "-------insert, turnCrank");
        m.insertQuarter(); m.turnCrank();
        System.out.println(m + "-------insert, turnCrank");
        m.insertQuarter(); m.turnCrank();
        System.out.println(m + "-------insert, turnCrank");
        m.insertQuarter(); m.turnCrank(); }
}
```

我们常说“今天的你，已经不是昨天的你了”，但你只有一个。上面这种实现，相当于：一个机器有多个状态，那么，多个状态相当于它的分身。外部只能操作这个机器，而这个机器究竟会怎样反应，有怎样的运作，由内部当前是哪个分身决定。

状态模式：允许一个对象基于内部状态而拥有不同的行为（在内部状态改变时，改变自己的行为）。

将状态封装为独立的类，并且将动作委托到代表当前状态的对象，这样行为就能随着内部状态的改变而改变了。

既然一个对象能够完全改变自己的行为，看起来就像对象改变了自己的类，让人觉得这是不同的class的实例。但这只是假象。

UML图：

```plantuml
abstract class State
State : handle()
Machine *-right-> State
Machine : request()
Machine : State innerState
Machine : State state_a
Machine : State state_b
user  ()- Machine
State <|-- ConcreteState_A
State <|-- ConcreteState_B
ConcreteState_A : handle()
ConcreteState_B : handle()
```



状态机Machine内部组合了不同的状态对象，调用者给Machine发送请求，就会被委托到当前的状态（调用者从不接触状态类）。

State接口定义了一个状态对象的接口，任何具体的状态类都用继承自同一个接口，那么状态之间就可以互相替换。具体的状态类，则处理一个个请求，状态对象切换，行为也跟着切换。

使用状态机模式，看起来比条件分支状态多了很多的类（原来一个Machine，后来多了State及其子类），是不是维护更麻烦了呢？

- 用户只能看到Machine的接口，State及其子类却是隐藏的，所以并不“纷繁复杂”
- 将变更局限在State类中，所以总体维护更简单了
- 将大量的分支语句，变为State类，更易读，维护更简单。

状态切换的逻辑和动作，究竟应该放在Machine还是State子类？这其实是在做另一个决策：当变更时，是Machine变更，还是State及其子类变更。

- 当状态转换非常固定的时候（其实这时候不必要使用状态机），就适合放在Machine里
- 状态转换更动态的时候（比如这个糖果机），就放在State子类中。这时，各个State产生了依赖，当然使用get和set减低了依赖，但是仍然存在依赖。

如果Machine有多个实例，并且实例之间要分享状态，那么每个状态都必须是static变量。

状态模式和策略模式的UML大致是相同的，因为都使用语法。不同之处在于：

- 最主要还是意图不同：状态模式，封装基于状态的行为，并将行为委托到状态当前；它“改变行为”是必然。策略模式将可以互换的行为封装起来，然后使用委托的方法，决定使用哪一个行为，它“改变行为”则由调用者决定。
- 状态模式内部拥有一群对象，随时委托给任一个对象，而策略模式内部只拥有一个对象，并且让用户指定使用哪个类的对象；
- 状态模式是条件分支语句的替代方案，而策略模式是继承语法的更有弹性的方案。

##### 状态机优化

P417



### 代理模式

代理模式：为另一个对象提供一个替身或占位符以访问这个对象，这样，就用代理控制和管理了这个对象的访问。

- 远程代理管理客户和远程对象之间的交互
- 虚拟代理控制访问实例化开销大的对象
- 保护代理基于调用者控制对象方法的访问
- 另外的变体：缓存代理、同步代理、防火墙代理、写入时复制代理

代理模式会增加类的数目。

##### 引入

设计需求：让前一章的糖果机能够展示机器位置、糖果存量、机器当前状态

```java
class GumballMonitor { //新增的需求对应的类
	GumballMachine machine;
	public GumballMonitor(GumballMachine machine){this.machine = machine;}
	public void report() {
		System.out.println("Gumball Machine: " + machine.getLocation());
		System.out.println("Current inventory: " + machine.getCount() + " gumballs");
		System.out.println("Current state: " + machine.getState());
	}
}
public class Test{
	public static void main(String[] args){//包含命令行解析。但用默认值覆盖了
	    args = new String[]{"machine", "5"}; //默认值。相当于运行命令行java Test machine 5
      	int count = 0;
        if (args.length < 2) { //首先判断命令行个数
            System.out.println("java Test <String:name> <int:gum>"); System.exit(1);}
        try { count = Integer.parseInt(args[1]); //然后尝试解析
		} catch (Exception e) {e.printStackTrace();System.exit(1);}
		int count = Integer.parseInt(args[1]);
		GumballMachine gumballMachine = new GumballMachine(args[0], count);
		GumballMonitor monitor = new GumballMonitor(gumballMachine);
		System.out.println(gumballMachine);
		gumballMachine.insertQuarter(); gumballMachine.turnCrank();
		gumballMachine.insertQuarter(); gumballMachine.turnCrank();
		monitor.report(); //新增的需求
	}
}
```

变更需求：如果要远程看到这个糖果机的各种参数呢？那就需要远程代理了。



##### RMI 远程代理

Server端  RemoteServer.java 

```java
import java.rmi.*; //Remote接口，Naming类，RemoteException异常
import java.rmi.server.UnicastRemoteObject;
interface MyRemote extends Remote { // 1.扩展这个远程接口
    //方法的 参数和返回值，必须是一级类型（基本类型、数组、集合）或可序列化的类型
    // 否则，无法序列化，也就是无法通过网络传输
    public String sayHello() throws RemoteException;
}
// 2. 实现Server类。实现接口，扩展UnicastRemoteObject
public class RemoteServer extends UnicastRemoteObject implements MyRemote{
    public RemoteServer() throws RemoteException {} // 3. 构造器必须无参数、抛异常
    public String sayHello(){ return "remote server: Hello"; } //实现接口
    public static void main(String[] args) {//4.启动时，必须注册
        try{
            MyRemote server = new RemoteServer();
            //rmi://localhost:9000/RemoteHello
            Naming.bind("RemoteHello", server);//注册：名字、对象
        } catch (Exception e){ e.printStackTrace(); }
    }
}
```

终端运行Server：

```shell
$ javac RemoteServer.java  # 编译成 *.class
$ rmic RemoteServer # RMIC会自动根据这个类，生成Stub和Skeleton
$ rmiregistry  # 启动rmiregistry，才能执行 Naming.bind("name", obj);
$ java RemoteServer # 启动 Server
```

ProxyUser端：ProxyUser.java  运行就能得到远程的回应了。

```java
import java.rmi.Naming;
class Proxy {
    public void sayHello(){
        try {
            MyRemote remoteService = (MyRemote)Naming.lookup("rmi://127.0.0.1/RemoteHello");
            System.out.println(remoteService.sayHello());
        } catch (Exception e){ e.printStackTrace(); }}
}
public class ProxyUser {
    public static void main(String[] args) {
        Proxy proxy = new Proxy();
        proxy.sayHello();
        proxy.sayHello(); }
}
```

比较常见的错误，都是没有遵循这样的流程。另外，`rmi://localhost:9000/RemoteHello`  ip和端口配置也会出现异常。

##### 实现远程糖果机













代理模式的语法结构与装饰者模式很像，但目的不同。

- 装饰者：给对象加上行为
- 代理：控制访问




[TOC]



### 复合模式Compound Pattern

#### 模式合作游戏

- 首先建立一堆呱呱叫的鸭子

  ```java
  // QuackMain.java
  interface Quackable { public void quack(); }
  class MallardDuck implements Quackable {
  	public void quack() { System.out.println("Quack");}
  }
  class RedheadDuck implements Quackable {
  	public void quack() { System.out.println("Quack");}
  }
  class DuckCall implements Quackable {
  	public void quack() { System.out.println("Kwak");}
  }
  class RubberDuck implements Quackable {
  	public void quack() { System.out.println("Squeak");}
  }
  public class QuackMain {
      public static void main(String[] args) {
          Quackable[] quackables = new Quackable[]{
              new MallardDuck(), new RedheadDuck(),
              new DuckCall(), new RubberDuck() };
          System.out.println("Duck quacking");
          for(Quackable bird:quackables){bird.quack();}
      }
  }
  ```

- 一只鹅出现了，适配器模式

  ```java

  ```

  ​

- 鸟类学家要统计鸟叫次数，装饰者模式

- 鸟类创建规范化，抽象工厂模式

- 鸟类管理规范化，组合模式

- 鸟类学家与鸟类关系，观察者模式





#### MVC模式解析

MVC：

View 视图：交互控件，显示和编辑。常常需要直接从模型取得数据，来更新显示。同时将控件操作传递给控制器。

Controller 控制器：逻辑，模型与视图的状态改变、数据同步。取得用户输入，并且解读输入对于模型的意思。

Model 模型：程序所有的数据、状态和程序逻辑。M与VC是彻底分离的，只是提供操纵、检索数据的接口，并且在数据更新时通知VC。

- View向 Model询问状态。交互界面查询文件系统，得到文件树
- 用户与View交互。用户在交互界面（视图）按下按钮，视图就告诉控制器「按钮D被按下了」。
- Controller 要求Model 或者View 改变状态。控制器将P解读为delete，然后在模型里找到相关的文件进行删除，同时让视图进入删除后界面。
- Model 改变了，就会通知View和Controller。删除文件后，交互界面看不到这个文件了，并且不可能操作已删除的文件。

MVC的难点，就在于它是由多个模式组合的复合模式。

```mermaid
graph LR
USER --> View; View --> USER;
View  --> Controller; Controller -->View;
Controller --> Model; Model --> Controller;
View --> Model; Model --> View;
```

View内部：组合模式。GUI包括窗口、按钮、文本等等。控制器只需要告诉GUI顶层组件更新，组合模式下，这组对象就会自动更新其余部分。

View与Controller：策略模式。Controller 是 视图的算法，视图可以使用不同的控制器。视图只做GUI，任何行为和逻辑都委托给控制器。由控制器与模型交互，从而让视图与模型完全解耦。

Model与View（或者Controller）：观察者模式。View和Controller都可能是观察者，这让Model与它们彻底解耦，一个Model可以使用不同的视图，甚至多个视图。View作观察者，比如股票下跌的走势；Controller也可能作观察者，比如按下删除后这个文件图标会变化，这是Model通知Controller，然后Controller控制View实现的。

其实不分层，也可以实现一个工程，但为什么要分层？松耦合，高度复用，边界清晰

- 单一责任，边界清晰：如果View既要控制显示，又要处理与Model的数据逻辑，那么就有2个责任，代码缠绕在一起，任何变更都要改动整个工程
- 松耦合，高度复用：Controller把逻辑从View 中分离，Model和View就解耦了。同一个视图就可以复用处理其他的模型；同一个模型，也可以用其他的视图展示。更有弹性，容易扩展。

##### 案例：节拍器

视图：界面有节拍输入框、设置节拍按钮；加速按钮、减速按钮；开始、停止、退出。

控制器：实现视图的内部逻辑。

模型：管理节拍速率；实现了节拍停止、启动逻辑；实现节拍声音。



### 模式总结

设计：在某种情境下，针对某类问题的某种解决方案。模式：是为了解决一个经常重复发生的问题。

- 情境Context，就是应用某个模式的情况。这是会不断出现的情况（而不是非常罕见的）
- 问题：你想在某个情境下达到的目标，但也可以是某情境下的约束。
- 解决方案：一个通用的设计，用来解决约束，达到目标。

如果你发现自己处于某个情境下，面对着所欲达到的目标被一群约束影响着，然而你能够应用某个设计，解决这些问题并且达到目标，将你领向某个解决方案。




