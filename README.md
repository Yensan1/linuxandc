# LinuxAndC

Linux Shell命令和编程、C语言学习。



Linux

- 主要针对 RedHat 和 Ubuntu 的常用命令。

- 主要参看书籍 ：the linux command line

  ![](./tlcl.jpg)





C 语言

- Linux，仅仅使用 bash 和 vim。

- Windows & Mac IDE 是 VS code （Linux-gcc&gdb，Win-MinGW，Mac-Clang&gbd），配置请参考 `.vscode`的内容。插件：C/C++，Include Autocomplete，vscode-icons。 Windows的MinGW是下载CodeBlock安装的，MinGW安装器太麻烦。

- 主要参考书籍 ： 

  如果你是编程新手，建议使用《 Linux C编程一站式学习》和《 C primer plus 》

  如果你从业多年，只是认真再次复习以下，建议使用《C程序设计语言the C programming Language》和 《C标准库*The Standard C Library*》

    - [《 Linux C编程一站式学习》](http://docs.linuxtone.org/ebooks/C&CPP/c/index.html)
    - [C/C++ Primer**](https://github.com/andycai/cprimer) ，
    - 《C程序设计语言the C programming Language》![](./tcpl.jpg)
    - C标准库(*The Standard C Library*)

  ​



参考链接
  - https://zhuanlan.zhihu.com/p/24633092
  - https://www.zhihu.com/question/23893390
  - https://www.zhihu.com/question/32255673
  - https://zhuanlan.zhihu.com/p/19694823?columnSlug=linjr
  - https://www.zhihu.com/question/38836382?sort=created
  - http://composingprograms.com

