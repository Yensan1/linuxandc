

## C 语言设计模式

#### 单一职责

单一职责：只专注于做一件事和仅有一个引起它变化的原因。接口、实现、函数级别的单一职责很容易，但很可能会忽略返回值、参数。接口永远是给别人使用的，一定要把使用者当成傻瓜，才能设计出好的接口。往往就是某些不符合单一职责原则的设计，导致很难发现的BUG。比如标准库函数`realloc()`就是返回值不单一：

```c
/* void *realloc(void *mem_address, unsigned newsize); // 给一个指针，赋予新的内存空间。如果重新分配成功则返回指向被分配内存的指针，否则返回NULL */
pBuf = (byte*)realloc( pBuf, size);
if( pbBuf != NULL ) {
       // do something
} //Bug：如果内存分配失败，pBuf被赋值NULL，原空间就成了泄露的内存
```

如果改善`realloc()`的设计，让返回值「单一职责」，就会带来更简单、无隐患的调用方式：

```c
boolean realloc(void **ppmen_addr, unsigned size) {
  boolean flag = true;
  if (succeed())
      **ppmen_addr = new_addr; // 分配成功，指针就指向新内存。返回true
  else
      boolean flag = false;// 分配失败，指针指向原内存。返回false
  return flag;
}
/* 更简单的、无隐患的 调用方式 */
if (realloc(&pBuf, size)) {// do something
} else { // failed. do something special
}
```

#### 接口隔离

接口隔离：客户端不应该依赖它不需用的接口（只提供外部接口，隐藏内部接口）。

在C语言中，一般将头文件作为模块的接口。但在实际的项目中，往往头文件不符合接口隔离原则。

- 例1：头文件包含模块内部接口（内部类型定义、内部接口声明）和外部接口（外部接口声明）

  ```c
  /* moudle.h */
  struct s_point { int a, b; }; // 内部接口
  typedef struct s_point Point; // 内部接口
  void func(void); // 外部接口

  /* moudle.c */
  #include "moudle.h"
  void func(void) {  // 接口的实现
      Point p = {0, 0};
      // do something
  }
  ```

  改善：将内部接口放在`moudle.inc`文件，而外部接口放在`moudle.h`文件

  ```c
  /* moudle.h */
  void func(void);

  /* moudle.inc */
  struct s_point { int a, b; };
  typedef struct s_point Point;
  ```

- 例2：千万别用「万能头文件」——一个头文件包含项目所有的接口

  ```c
  /* global.h */
  #inlcude "moudle1.h"
  #inlcude "moudle2.h"
  ....
  #inlcude "moudlen.h"
  ```

内外隔离、头文件层次分明的好处：

- 缩减编译时间。大项目、头文件混乱，头文件处理会耗费大量的编译时间
- 头文件层次分明，能让调用者清楚模块的依赖关系，并且减少误操作。比如一个数据获取模块有两个子模块：网络获取、本地缓存获取。后台使用时，调用网络；前台则调用本地缓存。如果两个子模块的头文件混合，维护者可能不知道这样的模块划分，前台使用网络就会出现性能问题；后台使用本地缓存则是bug。

#### 依赖于抽象，不依赖于具体

依赖倒置：依赖于抽象，而不是依赖于具体；让调用者对抽象进行编程，而不是对实现进行编程，从而降低调用者与实现模块间的耦合。

例如，对应不同的终端，显示不同的内容

```c
/* 调用者的代码 */
if (type == terminal_1) { // 判断究竟使用哪种终端。if switch都可
  T1_display();
} else if (type == terminal_2) {
  T2_display();
}
```

```mermaid
graph TD;
A(user)-->B;
subgraph Terminal
B(display)
end;
B-->C; B-->D;
subgraph terminal_2
D(T2_display)
end;
subgraph terminal_1
C(T1_display)
end;

c(user)-->b; c-->a;
subgraph terminal_2
b(T2_display)
end;
subgraph terminal_1
a(T1_display)
end;
```

这样做，调用者依赖了实现，业务依赖了底层，完全耦合。如果新增一种终端，就必须改动调用的代码，增加一个判断分支。如果系统非常复杂，那么工程中就会遍布这种判断，开发维护都很难。

改进：提供一个稳定的接口。在面向对象的语言比如 JAVA 中，接口很容易实现。其实C也可以实现。

1. 底层

   ```c
   #include "Terminal.h"
   void T1_display() {
       printf("Terminal1_display \r\n");
   }
   Terminal init_Terminal() {
       Terminal t;
       t.display = T1_display; // 在这里可以增加判断语句
       return t;
   }
   ```

2. 接口

   ```c
   /*Terminal.h*/
   typedef struct Terminal_s Terminal;
   Terminal init_Terminal();
   struct Terminal_s
   {
       void (*display)();
   };
   ```

3. 调用者

   ```c
   Terminal t;
   t = init_Terminal();
   t.display();
   ```


#### 实现「面向对象」

不建议用 C 实现”面向对象“语法（抽象、封装、继承、多态）。而是建议使用面向对象的思想：将数据和操作绑定，用指针变换操作方式。

- 用定义实现类。
- 将类的属性（数据和方法）组成一个`struct`。如果是`private`的属性，就使用不透明指针。将这个`struct`和相关的操作方法的声明，放在同一个头文件。
- 每一个函数的第一参数，都是`int func(MyStruct *mystruct_t);`这种形式。而这个指针，可以这么声明`typedef struct Mystruct *mystruct_t;`。
- 函数的实现，放在相关的`*.c`文件里。公开的函数，就把声明放在`*.h`文件里。而私有的函数，只在`*.c`里定义，而且定义为`static`避免命名冲突。而且命名的时候，尽量`mystruct_func()`比较好区分。另外，内存管理严格注意，可能需要构造函数 `mystruct_t *create()` 以及析构函数 `void destroy(mystruct_t *target)` 。

[博文](http://blog.csdn.net/besidemyself/article/category/959727)



- 抽象：将数据与操作，组织为一个类

  ```java
  // java 代码
  class Square { // 类
      int side; // 成员变量
      public Square(int side) { // 成员函数
          this.side = side;
      }
      public void draw() {
          System.out.println("Square len: " + this.side);
      }
  }
  public class Main {
      public static void main(String[] args){
          Square s = new Square(12);
          s.draw();
      }
  }
  ```

  用C，则用结构体和指针实现

  ```c
  typedef struct square_s { // tag _square 不可以省略，为了继承
      int side; // 用结构成员，表示 类的成员变量
      void (*draw) (void *); // 用结构成员(指针)，表示 类的成员方法
  } Square;
  static void draw(void *sq_obj) {    // 需要手动传入自身的地址
      Square *sp = (Square *) sq_obj;
      printf("Square len is %d\n", sp->side);
  }
  int main(void) {
      Square s = {12, draw}; // 初始化
      s.draw(&s);  // 函数调用
      return 0;
  }
  ```

- 封装

  ```java
  // java 代码
  class Square { // 类
      private int side; // 用 private 等访问关键字，就可封装
      ...
  }
  ```



## *"Patterns in C"* series

*"Patterns in C"* series by [Adam Petersen](http://adampetersen.se):

- [State](http://adampetersen.se/Patterns%20in%20C%202,%20STATE.pdf)
- [Strategy](http://www.adampetersen.se/Patterns%20in%20C%203,%20STRATEGY.pdf)
- [Observer](http://www.adampetersen.se/Patterns%20in%20C%204,%20OBSERVER.pdf)
- [Reactor](http://www.adampetersen.se/Patterns%20in%20C%205,%20REACTOR.pdf)

软件开发越来越复杂，所以产生了：

1. 组件/模块化开发，更容易设计、实现、编译、维护。
   - 将模块切分，然后命名，就像 JAVA class。别仿造OO语言的语法（特别是继承），这会非常难以维护，组合还是可以使用的。
   - 将接口和实现分开。只把必须的接口暴露给其他模块（C语言没有命名空间，这样也可以降低标识符冲突）。
   - 隐藏全局变量，而用`read() write()`之类的函数去读写。
   - 维护方面，给代码写注释和文档，并且工程里写上测试代码。
2. 设计模式。（方便沟通，并且可以将软件设计的更好）

设计模式，是软件工程界借用的建筑学词汇，它有利于正确、精确地表达和交流。所以，「设计模式」更像是一种沟通工具，而不是软件工程的解决方案。所谓模式，是如何在不同的上下文中、解决重复性出现的问题的简单而优雅的公式。

「设计模式」分为3类：

1. *惯用法Idiom*。语言层面，某种特定语言的经验用法，无法迁移到其他语言。

   - 常用的优秀表达式。表现力强，而且健壮。

     ```c
     void strcpy(char *s, char *t)
     {
     	while (*s++ = *t++);
     }
     ```

   - First-Class ADT。*ADT，数据抽象Abstract Data Type。数据类型可以分为三级：*

     | First-Class     一级数据类型 | 可作为函数参数        | 可作为返回值        | 可赋值给变量        |
     | ---------------------- | -------------- | ------------- | ------------- |
     | Second-Class 二级数据类型    | 可作为函数参数        | ***不可***作为返回值 | ***不可***赋值给变量 |
     | Third-Class     三级数据类型 | ***不可***作为函数参数 | ***不可***作为返回值 | ***不可***赋值给变量 |

2. *设计Design*。组件、模块的设计方法。

   - 状态机模式。
   - 策略模式。封装了算法簇，对扩展开放，对修改关闭。
   - 观察者模式。松耦合实现了通知机制。

3. *架构Architecture*。软件整体架构法则。最基本的法则，确定有多少个子系统、各组件功能、组件相互关系。

   - Reactor反应堆架构：减弱了事件驱动程序的职责


#### First-Class ADT

下面的代码导致”一个客户只能有42个订单“。当然，给`MAX_ORDERS`赋个更大的值可以改善，却不能根治。

```c
#define MAX_ORDERS 42
/* Internal representation of a customer. */
typedef struct {
  const char* name;
  Address address;
  size_t noOfOrders;
  Order orders[MAX_ORDERS];
} Customer;
void initCustomer(Customer* man, const char* name, const Address* addr);
void placeOrder(Customer* man, const Order* od);
/* A lot of other related functions and struct... */
```

采用链表可以解决，但如果变换数据结构，那么相关的函数每个地方都得跟着修改，别说是可能引入新的bug，就是所有client重新编译，都是大把时间，怎么办？程序设计的理想状况是：一个组件对扩展开放，对修改关闭，封装内部实现，只向上层client提供唯一的接口，这样只需要修改这个模块。使用First-Class ADT：

- 松耦合：把接口和实现分离
- 强封装：隐藏细节
- 更可控：构造函数和析构函数
- 增加抽象：所有的数据操作，都包裹了一个抽象层
- 动态内存：动态分配内存

ADT/Abstract Data Type，是数据和操作数据的集合。First-Class，当我们需要考虑很多独特场景时，ADT是首要考虑的。

C语言语法：当size无需考虑的时候，允许声明不完整类型的对象。比如下例，这个无需`int`之类指定size的声明语句，Customer没有结构体、不是完整的类型，然而还是可以声明指向这类对象的指针（当然指针是完整类型）。只有编译器遇到下面的标识符，相同的tag、完整的结构体声明，才会认为结构体Customer是完整的。

```c
typedef struct Customer* CustomerPtr;//这时候Customer并不完整
struct Customer {
  const char* name;
  Address address;
  size_t noOfOrders;
  Order orders[42];
};//这时候Customer才完整
```

用这种方式，指针作为访问ADT的句柄，就可以强制调用者使用提供的接口，因为没有办法获取内部信息。只要接口不增不减不变，无论内部怎么变化，调用者都无需改变——松耦合、强封装。当调用者无法干涉内部机制的时候，对象的初始化、销毁都可以让ADT自己管理——更可控、动态内存。

- 对外提供的接口`Customer.h`

  ```c
  typedef struct Customer* CustomerPtr; // 句柄
  CustomerPtr createCustomer(const char* name, const Address* addr); // 构造函数
  void destroyCustomer(CustomerPtr man); // 析构函数
  ```

- 内部的实现`Customer.c`

  ```c
  #include "Customer.h"
  #include "Order.h"  // 外部提供的一种数据类型。
  #include <stdlib.h>
  struct Customer {
    const char* name;
    Address address;
    size_t noOfOrders;
    Order orders[42]; // 使用者无法访问，所以可以任意更改实现方案
  };
  CustomerPtr createCustomer(const char* name, const Address* addr) {
    CustomerPtr customer = malloc(sizeof *customer);
    if(customer) {
      /* Initialize each field in the customer... */
    }
    return customer;
  }
  void destroyCustomer(CustomerPtr man) {
    free(man); /*clean internal data if necessary. */
  }
  ```

  上面的代码用malloc获取内存空间。有些嵌入式编程不能这么做。如果说数量有个上限，那么也很容易用数组实现：

  ```c
  #define MAX_CUSTOMERS 42
  static struct Customer objectPool[MAX_CUSTOMERS];
  static size_t numberOfObjects = 0;
  CustomerPtr createCustomer(const char* name, const Address* addr) {
    CustomerPtr customer = NULL;
    if(numberOfObjects < MAX_CUSTOMERS) {
      customer = &objectPool[numberOfObjects++];
      /* Initialize the object... */
    }
    return customer;
  }
  void destroyCustomer(CustomerPtr man) {
    /* 这里需要很强的内存管理算法*/
  }
  ```

- 其他问题：

  拷贝：如果只是拷贝指针（就像Python），这会提高运行性能，但是要注意，如果实体已经销毁，指针是非法的。如果想深层拷贝实体，那就必须提供接口、实现接口。——增加抽象。

- 应用案例：

  C语言标准库的 FILE 结构体：虽然FILE是完整类型，但原则是一样的，FILE内部各平台的实现可能不同，不可移植，但接口相同。

  Sockets：Uinx 和 Win的实现肯定不同，但是提供了相同的接口（`*.h`文件），所以客户端用相同的代码，跨平台调用。

  算法-C语言实现 Robert Sedgewick 使用了很多ADT。



#### 状态机State Machine

编程中（特别是过程式语言），尽量将状态和副作用最小化，这些都很容易出问题。状态很难进行逻辑推理和跟踪：

- 多个因素共同影响：输入、当前的状态，影响下一个状态
- 状态变量大多是隐藏的：化身为不同的变量名，隐藏在设计中

状态机就是为了解决这类问题而生的：

- 将共同的状态算法（相关的函数）归为一组
- 显式、规则地表达状态

##### 最简单的例子：数字秒表

- 用分支语句、First-Class ADT 来实现，单个文件

  ```c
  #include <stdio.h>
  #include <time.h>
  #include <stdlib.h>
  #ifdef _WIN32   /* windows ************ */
  #include <windows.h>
  #define sleep(n)  Sleep((n))  /* _Sleep函数 milliseconds 毫秒， 1/1000 s  */
  #else   /* Unix && Linux ************ */
  #include <unistd.h> /* usleep函数 microseconds 微秒， 1/1000 000 s  */
  #define sleep(n)  usleep(1000 * (n)) /* 改造为_Sleep函数 1/1000 s  */
  #endif
  typedef struct DigitalStopWatch* DigitalStopWatchPtr;
  typedef enum { stopped, started } State;
  typedef void (*WatchDisplay)(DigitalStopWatchPtr instance);
  struct DigitalStopWatch {
      State state;
      int starttime, finishtime;
      WatchDisplay display;
  };
  void startWatch(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:  /* Already started -> do nothing. */
          break;
      case stopped:
          instance->state = started;
          instance->starttime = clock();
          break;
      default:
          fprintf(stderr, "Illegal state");
          exit(0); // error("Illegal state");     break;
      }
  }
  void stopWatch(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:
          instance->state = stopped;
          instance->finishtime = clock();
          break;
      case stopped: /* Already stopped -> do nothing. */
          break;
      default:
          fprintf(stderr, "Illegal state");
          exit(0); // error("Illegal state");     break;
      }
  }

  void display(DigitalStopWatchPtr instance) {
      switch(instance->state) {
      case started:
          printf("\t%d\n", (clock() - instance->starttime));
          break;
      case stopped: /* Already stopped -> do nothing. */
          printf("\t%d\n", (instance->finishtime - instance->starttime));
          break;
      default:
          fprintf(stderr, "Illegal state");   exit(0);
      }
  }
  int main(void) {
      struct DigitalStopWatch watch = {stopped, 0, 0, display};
      startWatch(&watch);
      watch.display(&watch);
      sleep(3000);
      watch.display(&watch);
      sleep(7000);
      stopWatch(&watch);
      watch.display(&watch);
      return 0;
  }
  ```

  这种实现的缺点：

  - 冗余。所有与状态有关的函数，内部都差不多是“状态判断，执行语句”，逻辑几乎是一样的。冗余意味着维护容易出错，比如要增加一个状态，那么所有的功能都要改变，万一有个函数改错了……
  - 高度耦合。每一个函数，对象的状态 与 操作语句混合在一起，没有将行为抽象为一个有限的状态机，导致可读性太差。
  - 不能扩大规模。大型的状态机，一页页的复杂缠绕，同时又单调乏味的分支逻辑，维护绝对是噩梦。

- 用转换表 transition tables 来实现

  易读，没有冗余，扩展规模非常方便。缺点是：除了切换状态之外，很难添加别的函数（display()就没法用这种方式重构），也不易读。

  ```c
  /* 新增代码 */
  #define NUM_OF_STATES 2
  #define NUM_OF_EVENTS 2
  typedef enum { stopped, started } State;
  typedef enum { stopEvent, startEvent } Event;
  static State TransitionTable[NUM_OF_STATES][NUM_OF_EVENTS] = {
      { stopped, started },
      { stopped, started }
  };

  /* 重构的代码 */
  void startWatch(DigitalStopWatchPtr instance) {  // 避开了 分支语句
      const State currentState = instance->state;
      instance->state = TransitionTable[currentState][startEvent];
  }
  void stopWatch(DigitalStopWatchPtr instance) {
      const State currentState = instance->state;
      instance->state = TransitionTable[currentState][stopEvent];
  }
  ```

- State Machine Pattern

  转换表，本质是关注状态的转换，所以这种设计不太贴合。

  State Machine Pattern，本质是针对’‘特定模式的方法“进行建模。

  ​

  ​

  ​

  ​

  1

- 1




## UML

#### plantUML

VScode 安装 `plantuml/jeb`。如果需要预览，2种方法：

- 使用server预览（以markdown为例）：

  写在markdown里，VScode 安装`markdown-all-in-one/yzh`

  ```plantuml
  Duck <|-right- RedheadDuck
  ```

  `Ctrl Shift V`预览Markdown，联网就可预览。如果需要本地预览，则必须设立本地[plantUML Server](http://plantuml.com/server)：

  - 下载 [plantuml.war](http://sourceforge.net/projects/plantuml/files/plantuml.war/download) 和 tomcat。将`*.war`放在apps目录，比如`..\Tomcat 8.5\webapps\`，重启tomcat，自动会解压，生成`Tomcat 8.5\webapps\plantuml`。浏览器打开`http://localhost:8080/plantuml`就可使用
  - 在VScode里面，设置settings.json，`"plantuml.server": "http://localhost:8080/plantuml","plantuml.render": "PlantUMLServer",`就可以预览markdown里面的UML

- 使用 `plantuml.jar`和`graphviz` 预览（以`*.puml`文件为例）

  - 一般来说，文件名后缀`puml`，文件内容以`@startuml`起头，以`@enduml`结尾。
  - VScode预览：安装JDK，下载[plantuml.jar](http://sourceforge.net/projects/plantuml/files/plantuml.jar/download) ，默认要放在`jdk/jre/ext`，下载 [Graphviz](http://www.graphviz.org)，默认放在`C:/Program Files/graphviz/bin/dot.exe`。右击选择预览，或者Ctrl D
  - 输出图片文件：`"cd $workspaceRoot &&java -DGRAPHVIZ_DOT=\"C:/Program Files/graphviz/bin/dot.exe\"  -jar F:/Note/plantuml.jar $fullFileName"`（请注意，jar包、graphviz路径都在命令行设置了，如果没有设置，请参考上一条的默认路径）

#### UML 六大关系

继承和实现是is-a关系（有时统称继承），聚合和组合是has-a关系（有时统称组合）。

上下关系：继承和实现

平行关系：依赖 --->--- 关联 --->--- 聚合 --->--- 组合

- 继承Extension：`Super <|-- Sub`，子类继承父类。

  java代码略

  plantUML：

  ```plantuml
  class Super {
  }
  class Sub extends Super {
  }
  ```

- 实现Realization：`InterFaceA <|.. B`，子类实现抽象类、接口

  java代码略

  plantUML：

  ```plantuml
  interface InterFaceA {
  	public void quack()
  }
  class B implements InterFaceA {
      public void quack()
  }
  ```

- 依赖Dependency：`A <.. B`，类A偶然、临时地用到了类B，B类的变化可能会影响A类。比如，类B是类A方法的一个参数。

  Java代码：

  ```java
  A a = new A();
  B b = new B();
  a.method_m(b);
  ```


  plantUML：

  ```plantuml
  A <.left. B
  ```


- 关联Association：`A <-- B`，两个类（或者类与接口）之间长期稳定的强依赖关系。比如，被关联的类B，是类A的类属性。

  ```java
  class A {
    static B b;
  }
  ```


  plantUML：

  ```plantuml
    class A {
      static B   b;
    }
    A <-left- B
  ```

- 聚合Aggregation：`Family o--> Child`，一个类是另一个类的组成部分，但是也可以分离。比如孩子与家庭。代码中，一般类B是类A的成员属性

  ```java
  class Family {
    Child alice = new Girl();
  }
  class Family {}
  class Girls extends Family {}
  ```
  plantUML：

  ```plantuml
    class Family {
      Child alice
      getName()
    }
    Family o-left-> Child
    Child <-- Girl
  ```

- 组合Composition：强聚合。比如，脑器官与人体。有可能是内部类，但聚合与组合不是从语法区分，而是从逻辑区分的。

  ```java
  class Body {
    Brain b = new Brain();
  }
  class Brain {}
  ```
  plantUML：

  ```plantuml
    class Body {
      Brain b;
    }
    class Brain
    Body *-right-> Brain
  ```





