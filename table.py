#!/usr/local/bin/venv2/bin/python python
# -*- coding: utf-8 -*-
# Mac: ~/.bash_profile  OR ~/.bashrc
from prettytable import PrettyTable; from sys import version_info
if version_info[0] == 2:
    from __builtin__ import raw_input as input # from __future__ import print_function
string = input("please ipunt table content, end with Enter\n")
split_note = input("please input split note( split string into table cells), default is space")
split_note = split_note if split_note else None
s_list = string.split(split_note)
col_num = len(s_list)
if not s_list:
    print("input error. Bye.\n"); exit(1)
table = PrettyTable(range(1, col_num + 1))
table.add_row(s_list)
while 1:
    temp_dict = dict((k, '-*-') for k in enumerate(s_list))  # dict() & generator
    string = input("please ipunt table content, end with Enter\n").rstrip()
    # print(string, type(string))
    if string:
        s_list = string.split(split_note)
        for index, value in enumerate(s_list):
            temp_dict[index] = value
        table.add_row([temp_dict[k] for k in range(col_num)])
    else:
        answer = input("input finished? (yes OR Enter)\n")
        if answer == "yes": break
try:
    print(table)
except ValueError as e:
    print("DecodeError: %s.\n\nIf using ssh in Win to connect Linux, please set ssh client encode utf-8." % e)
