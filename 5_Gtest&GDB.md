## GDB







## 引入 googletest——C/C++单元测试框架

[TOC]



#### 搭建环境

从 [github](https://github.com/google/googletest/releases) 下载最新的releases（建议1.7.0），千万不能下载master，解压。

##### Win+MinGW+VScode：

Cmake构建：下载[cmake](https://cmake.org/download/)的`Windows win32-x86 ZIP`版，解压，把bin和share 目录拷贝到MinGW目录下，合并目录。打开`cmake-gui.exe`，（路径随实际情况而定）

1. source code 填 F:/Downloads/googletest-release-1.7.0
2. where to build 填 F:/gbuild （可能要新建文件夹）
3. 点`Configure`，选编译平台为`MinGW`（也可以`codeBlocks-MinGW`），编译器选`use default`，点`Finish`，等待配置
4. 框中会出现选项，仅勾选`gtest_disable_pthreads`
5. 点`Configure`，再点`Generate`

MinGW编译：

- 如果编译平台当初选`codeBlocks-MinGW`，用CodeBlocks打开F:/gbuild/gtest.cbp，点Build>Build
- 如果编译平台当初选`MinGW`，cmd.exe 进入F:/gbuild/，运行`mingw32-make`命令
- 编译结果没有error，编译文件夹F:/gbuild 里面就会有`libgtest.a`和`libgtest_main.a`

配置到MinGW环境里：

- 头文件：将文件夹`F:\Downloads\googletest-release-1.7.0\include\gtest`拷贝到MinGW头文件库`..\MinGW\include`，操作结果`C:\CodeBlocks\MinGW\include\gtest`
- lib文件：将文件`libgtest.a`和`libgtest_main.a`拷贝到`..\MinGW\lib\gcc\mingw32\4.9.2`，操作结果`C:\CodeBlocks\MinGW\lib\gcc\mingw32\4.9.2\libgtest.a`

##### MacOS+clang+VScode：

Cmake构建：

下载[cmake](https://cmake.org/download/)的`cmake-3.9.4-Darwin-x86_64.dmg`版，点击安装。再在终端运行安装`sudo "/Applications/CMake.app/Contents/bin/cmake-gui" --install`。打开`cmake-gui.exe`，（路径随实际情况而定）

命令行操作：

- `cd ~/Downloads && mkdir gbuild && cd gbuild`
- `cmake /Users/aChing/Downloads/googletest-release-1.8.0/googletest`
- `make && make install`


UI界面的操作类似，选择Xcode编译器，然后编译安装。

但是，安装后`lib_path:/usr/local/lib/`并不是clang默认的lib路径，所以编译时候要指定libPATH，并且主动链接Gtest ，比如编译源文件 `gtest_sample.cc`

```c
clang++ -L /usr/local/lib/ -lgtest sample_all_in.cc -g -Wall -o sample_all_in.o
```


#### 第一个测试

```c
/* sample_all_in.cc */
#include <gtest/gtest.h> /* must include gtest.h */
int foo(int a, int b) {return a + b;} /* tested object */
TEST(FooTest, testName) /* tests combine to TEST */
{
    EXPECT_EQ(3, foo(1, 2)); /* a test: give it pass */
    EXPECT_EQ(7, foo(3, 5)); /* a test: give it fail */
}
int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
```

编译这个文件`g++ sample_all_in.cc -Wall -lgtest -o tst_all_in.out`，一定要`-lgtest`包含gtest库，否则无法编译。然后运行` tst_all_in.out`，就会看到测试报告。



如果分开编译，则

```c
/* file sample1.cc */
int foo(int a, int b) {return a + b;}

/* file sample1.h */
#ifndef GTEST_SAMPLES_SAMPLE1_H_
#define GTEST_SAMPLES_SAMPLE1_H_
int foo(int a, int b);
#endif  // GTEST_SAMPLES_SAMPLE1_H_

/* file sample1_unittest.cc */
#include "sample1.h"
#include "gtest/gtest.h"
TEST(FooTest, testName) /* test case */
{
    EXPECT_EQ(3, foo(1, 2)); /* give it pass */
    EXPECT_EQ(7, foo(3, 5)); /* give it fail */
}

/* file gtest_main.cc */
#include <stdio.h>
#include "gtest/gtest.h"
GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
```

编译命令`g++ sample1.cc sample1_unittest.cc gtest_main.cc -Wall -lgtest -o main.out`，也就是要把`被测物sample1.cc`、`测试用例sample1_unittest.cc`、`main入口gtest_main.cc`  源文件都编译到一个文件，然后运行这个文件。

可以缩写为`g++ sample1* gtest_main.cc -Wall -lgtest -o main.out`













