#include <gtest/gtest.h>
#include <stdio.h>

int foo(int a, int b) {return a + b;} /* tested object */

TEST(FooTest, testName) /* tests combine to TEST */
{
    EXPECT_EQ(3, foo(1, 2)); /* a test: give it pass */
    EXPECT_EQ(7, foo(3, 5)); /* a test: give it fail */
}
int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}



