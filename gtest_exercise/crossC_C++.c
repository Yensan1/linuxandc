/* common code, such as include & define */

#include <stdlib.h>  /* header files that have C/C++ Guard, such as stdlib */

#ifdef __cplusplus  /* C++ OR  C */
extern "C" {
#endif

/* most code of C, such as function */
#include "lib.h"  /* header files that do not have C/C++ Guard, such as self lib */

void func(void)
{
    printf("func");
}

#ifndef __cplusplus  /* C main() for release */

int main(void)
{
    func();
    return 0;
}

#else
}     /* C++ Gtest */
#include "gtest/gtest.h"
TEST(getword, throwEOF) /* test case */
{
    func();
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif     /* C++ OR  C */
