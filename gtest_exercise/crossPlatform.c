#ifndef _CROSPLATFM_H
#define _CROSPLATFM_H

#ifdef __cplusplus
extern "C" {
#endif     /* C++ OR  C */


#if defined(_WIN32)  /* Windows code in below */

/* Windows code */

#elif defined(__APPLE__)    /* exclude iOS */
    #include <TargetConditionals.h>
    #if TARGET_OS_IPHONE
    #   error "Just for MacOS ONLY, not for iOS platform"
    #elif TARGET_OS_MAC
    #define MAC_OS 1
    #else
    #   error "Unknown Apple platform"
    #endif /* Mac */
#endif
#if defined(__linux__) || defined(MAC_OS)   /* MacOS & Linux code in below */

/* MacOS code */

#endif


#ifdef __cplusplus
}
#endif  /* C++ OR  C */

#endif /* _CROSPLATFM_H */
