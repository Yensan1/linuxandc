[TOC]





# 函数 与 程序结构


### 函数定义与声明

声明Declaration 是说明变量或标识符的性质，但并不分配存储单元。定义Define是声明的基础上，分配存储单元。函数定义不可以嵌套，但函数声明可以嵌套。同一个工程内，不能有同名函数（不存在面向对象的方法重载）。

函数定义与声明相结合，就能检测出更多的错误。参数声明得当，程序可以自动地进行适当的强制类型转换。

函数定义 和 函数声明 语法：

```c
return_type func_name(type arg1, type arg2...)
{
    declarations; /* 各类 标识符 声明 */
    statements;   /* 运算操作语句 */
}
/* 除了函数名，各部分都可以省略 */
dummy() {} /* 这种函数定义一般用于占位，以后填上代码。实际含义 int dummy(void){} */
return_type func_name(type arg1, type arg2...); /* 函数原型声明，就是去掉函数体的 函数定义。 */
```

C语言函数，不允许嵌套定义。

```c
void funA(){ // 这种写法不被允许
    void funB(){printf("B");}
    funB();
}
```

函数之间的通信方式：参数与返回值、外部变量。

##### 函数定义

- 例子

  ```c
  #include <math.h> /* gcc 编译时需要 -lm */
  #include <stdio.h> 
  /* K&R old style C 写成 main(){ } 不建议这样写。*/
  /* main也可以带参数的 int main(int argc, char *argv[]){ } */ 
  int main(void) /* 函数定义语法：返回值类型 函数名(参数列表) 函数体 */
  { 
      double  pi = 3.1416;
      printf("sin(pi/2) = %f\nlog1=%f\n", sin(pi/2), log(1.0));
      return 0; /* 返回值：main函数返回给系统。正常结束返回0，异常结束非零。$ echo $?可以查看*/
  }
  /* log(1.0) 函数调用function call，log是函数function，1.0是参数Argument
   * 函数调用也是表达式，由 操作数 log 和 函数调用运算符() 构成。
   * 操作数 log 是 函数名Function designator，类型是 函数类型function type
   * 函数调用表达式的值，称为返回值Return value。有些函数没有返回值，但有Side Effect副作用（改变存储、输入输出之类）比如 printf() */
  ```


- 另一个例子

  ```c
  #include <stdio.h>
  void newline(void)
  {
      printf("a line\n");
  }
  void threeline(void)
  {
      newline();  newline();  newline();
  }
  int main(void)
  {
      printf("start Line.\n");
      threeline(); /* main---threeline---newline 函数可以多层次调用 */
      printf("end line.\n");
      return 0;
  }
  ```

  既然函数newline没有返回值，那么表达式`newline();`不就没有值了吗？这违反了「任何表达式都有值和类型」。这就是 void 关键字的意义：从语法上规定没有返回值的函数调用表达式的结果是void类型的，有一个void值（这就有值也有类型）；同时语义上规定void类型的表达式不能参与运算，`newline() + 1;`不能通过编译。语法一致，语义也没毛病。在某些语言的语法里，有返回值的是函数，没有返回值的是过程 Procedure，比如 Pascal。


可以看到，函数的意义：

- 复用代码：可以被重复调用，而不必每次都写同样一段代码。这样，就可以用函数作为砖块进一步构建更大的计算任务。
- 工程管理：将大的计算任务分解为小计算任务，黑盒抽象隐藏细节，从而降低管理和维护的难度。函数是一种抽象手段，将一系列复杂的操作封装为一个简单的函数名，使用函数的时候无需考虑具体的实现，只需要知道函数的接口和功能。并且调用很灵活，函数A可以调用B，函数B可以调用C……。

C语言的函数，考虑高效性和易用性，一般不会很大。一个程序由很多小函数构成，而不是几个大函数构成。

##### 函数声明

函数也必须“先声明后使用”。函数不能嵌套定义，但可以在函数内部进行函数原型声明。

一般来说，函数定义、函数调用是最常见的代码顺序，比如上例。但是如果要反过来呢？或者被调用的函数在另外的源文件中呢？根据“先声明后使用”的语法原则，就必须加上 函数原型声明：函数声明、函数调用、函数定义，三者必须完备。

如果不声明函数原型，那就是“隐式声明”，编译器默认隐式声明的函数返回值类型都是int，也等于`int newline(void)`，但编译器继续编译，得到下面真正的函数定义`void newline(void){...}`知道了返回值是`void`，就会告警。

以后做头文件的时候，函数对外接口，统一集合成 `abc.h`文件。

- 例：

  ```c
  /* 使用在前，定义在后，用声明函数原型解决语法问题 */
  #include <stdio.h>
  void newline(void); /* 函数声明Declare：函数原型Prototype */
  // void newline();  K&R old style，省略void参数。不建议
  int main(void) {
      // void newline(void);  也可以放在这里
      printf("First Line.\n");
      newline(); /* 调用函数。声明了原型，就知道是哪一个函数，就可以调用 */
      printf("Second line.\n");
      return 0;
  }
  void newline(void){ /* 函数定义Define：声明函数原型，同时定义函数体 */
      printf("\n");
  }
  ```

- 例：C语言没有幂运算`**`，写个函数实现它。声明，是为了让编译器知道该如何检查编译这段代码。声明的关键：函数名、返回值类型、参数类型（参数名不重要）。

  ```c
  #include <stdio.h>
  int power(int m, int n); /* 函数声明（声明函数原型），返回值和参数类型必须和函数定义一样，但参数名可以不一样，也可以省略 */
  int main(void)
  {
      int i; /* 使用的声明 power(int m, int n); */
      for (i=0; i<10; ++i)
          printf("\t%d\t%d\t%d\n", i, power(2, i), power(-3, i));
      return 0;
  }
  int power(int base, int n) /* 函数定义 */
  {
      int i=1, p=1;
      for (; i<=n; ++i)
          p = p * base;
      return p;
  }
  ```

注意一点：`int fun(void);`是确认无参数，`int fun();`是参数不确定。

```c
void fun(); // 这样能够编译通过，但无法成功运行
int main() {
    int a = 5, b = 123456;
    fun(a, b); // 传入的是 int
    return 0;
}
void fun(double a, double b){ // 需要的是 double
    printf("a: %f, b:%f\n", a, b);
}
```



### return语句：

- 没有return 语句的函数，函数执行到函数体末尾（右边的花括号）就结束，控制权交回调用者。

- 有 return 没有返回值的函数`void fun(int a);`，return语句的作用是结束调用、将控制权交回，特别是遇到错误时候提前终止函数。这种函数，常用于“副作用”，比如输出。

- 有返回值的函数`return expression;` ，return 语句的作用是提供返回值（调用者可以忽略返回值），同时结束调用将控制权交回调用的地方。C语言只能返回一个值。

  - 返回值可以是任何合法的表达式。返回值类型：可以返回基本类型、结构、联合、指针。表达式可以加括号，比如 `return (1+i);` ，但一般是省略括号

  - 如果一个地方返回有返回值（比如`return a+3;`），另一个没有返回值（只是`return`），这样并不非法。但这种写法不好，不如返回一个标志数。比如：

    ```c
    int strcheck(char s[]) /* 如果字符串有'\n'，就返回索引。否则返回-1*/
    {
        int i;
        for (i=0; s[i] != '\0'; i++)
            if (s[i] == '\n')
                return i;
        return -1;  /* 只有 return 不好，最好返回一个标志数，比如 -1 */
    }
    ```


- 如果返回值不是int 型，则调用时必须进行显式声明（返回值必须标明type或void，参数必须标明type或void）。

  ```c
  #include <stdio.h>
  #include <math.h>  /* 引入 cos() 函数。 */
  int main(void)
  {
      double a, cos(double); /* 同时声明了变量 a， 函数 cos。实际上头文件已经声明，此仅为示例。 */
      a = cos(2.9); /* 否则，就是隐式声明，返回值默认int，不检查参数类型 */
      printf("\t%f\n", a);return 0;
  }
  ```

- 如果返回值type 与运算结果不同，则最好主动转换

  ```c
  #include <stdio.h>
  int main(void)
  {
      int func(void);
      printf("\t%d\n", func());
      return 0;
  }
  int func(void)
  {  /* 这只是示例，所以就一句 return ，现实情况是 复杂运算得到结果。 */
      return (int)2.6; /*虽然会进行自动转换，但不保证精度，编译器会警告 */
  }
  ```

`main`函数必须有返回值，返回给操作系统，0表示程序执行正常完毕，非0说明异常或错误结束。

```c
#include <math.h>
#include <stdio.h>
void print_lagarithm(double x)
{
    if (x <= 0.0) { /* double x 所以 x <= 0.0 而不是 0 */
        printf("Positive numbers ONLY");
        return; /* return 作为提前返回 */
    }
    printf("the log of x is %f", log(x));
}
```

返回值，传递的是**数值**，而不是变量，因为这块内存空间马上就被释放。如果要实现传变量，只能使用 指针。

- 例：返回布尔值的函数

  这种函数非常有用，通常作为控制表达式（Python不准这么写）。函数名通常带有`is`或`if`

  ```c
  int is_even(int x)
  {
      return (x%2==0) ? 1 : 0; /* 可以写成 return !(x % 2); */
  }
  if (is_even(3))
      /* do something */
  else
      /* do something */
  ```

编写有 return 语句的函数，一定要小心检查，有些代码路径在任何时候都执行不到，这成为 Dead Code。这时候没有语法错误，编译器不报错（`-Wall`会告警），这比报错更糟糕：有dead code就说明一定有bug，因为你写这段代码就是设定为在某些情况下会执行的，对应着某些业务和场景。所以一定有逻辑和语义错误。



编写函数时，可以采取增量式开发的策略：

- 编写某个函数时，仅有 return 语句，看看调用结果
- 之后一步步细分，充实这个函数
- printf虽然原始，却是最快捷的调试方法

编写函数时，适当使用临时变量是有意义的，更易读，也更好调试：

```c
double distance(double x1, double y1, double x2, double y2)
{ /*求两点距离。可直接返回表达式  return sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));  */
    double dx = x2 - x1;
    double dy = y2 - y1;
    double dsquared = dx * dx + dy * dy;
    double result = sqrt(dsquared);
    return result; /*虽然步骤和变量更多，但调试更简单，容易知道哪儿出错了 */
}
```



### 参数

函数定义中，需要指明参数的个数和每个参数的类型（参数列表）。本质是定义函数体内的自动变量，所以按照变量的命名规则。

```c
int hour, minute; /* 变量定义可以连续列出 */
void print_time(int hour, minute){...} /* 错误：参数定义不可以连续列出 */
void print_time(int hour, int minute){...} /* 因为参数定义中，只能用,号，没有变量定义的;号*/
```

- 形参Parameter  和  实参Argument。实参，可以是：字面量、变量、函数的返回值、有结果的表达式

  ```c
  #include <stdio.h>
  void print_time(int hour, int minute) /* 形参相当于定义变量 */
  {
      printf("%d:%d\n", hour, minute);
  }
  int main(void)
  {
      int h = 23, m = 59; /* main函数的h 与 print_time的hour 是不同的变量（存储区域），虽然值一样 */
      /*传入实参，相当于给形参初始化赋值 */
      print_time(h, m);   // 实参是 变量名
      print_time(11, 32); // 实参是 字面量
      print_time(max(1, 12), 32); // 实参是 函数调用返回值
      print_time(5+6, 3+10); // 实参是 表达式
      return 0;
  }
  ```
  
  形参与实参，**类型不匹配时**，编译器会 warning，但不会 error，所以，很容易出bug，必须注意这个（JAVA/C++ 必须严格匹配）
  
  ```c
  void show(int n){printf("show %d", n);}
  int main() {
      double f = 2.4;
      show(f); // 编译器会帮助你转成 int：这会导致精度损失
      return 0;
  }
  ```
  
  函数也叫接口Interface，调用接口就必须遵循接口规定，传入合法的实参。实参必须和形参的个数、类型对应。有一类函数的实参个数是不确定的，称为可变参数Variable Argument，比如
  
  ```sh
  $ man 3 printf # 查看 stdio.h 的 printf 函数
  int printf(const char *format, ...);  
  ```
  
  C语言的传参过程，是**直接传入值，而不是变量**，这种方式叫做 call by value（Python中传递的是对象的引用，也就是指针）。实参被放在临时变量中，而不是存放在原来的变量中。
  
  被调用的函数不能直接修改调用者的变量，修改的是被调用函数本地的副本。如果要被调用的函数直接变量，就使用数组或指针。
  
  ```c
  /* power()没有改变 a 和 p。p是赋值运算符=修改的 */
  int  a, p;
  p = power(a, a); 
  /* swap() 是没有意义的，因为传参，只是传入值 */
  void swap(int a, int b){
      int tmp;
      tmp = a, a = b, b = a;  // 虽然 a b 变量名相同，但作用域不同
  }
  int main() {
      int a = 5, b = 7; // 两个函数内部是完全不同的地址。交换没有意义
      swap(a, b);
      printf("a:%d, b:%d\n", a, b);
      return 0;
  }
  /* swap() 正确写法：使用指针和地址 */
  void swap(int *a, int *b){
      int tmp;
      tmp = *a, *a = *b, *b = tmp;
  }
  int main() {
      int a = 5, b = 7;
      swap(&a, &b);
  }
  ```




### 外部变量、内部变量、作用域

C 语言中，最常用的概念不是全局 Global、局部 Local 变量 Variable（对象），而是内部、外部变量（对象）。

变量可以只在函数内部、函数外部，在一个源文件内、整个程序不同的源文件内有效。

C语言程序可以看成是一系列外部对象构成的，也就是「函数+外部变量」。一个源文件，除了预处理指令，其他的都是一些「外部external」对象的定义语句：

- 定义在所有函数之外的变量，是「external外部」变量。定义在函数外的变量，可以被很多函数使用。
- 函数定义不能嵌套，一个函数对于其他函数来说，也是外面，因此函数是外部对象。
  - 函数里定义的参数和变量，都是「internal内部」变量。这是最常见的变量。

外部链接：只要是名字相同，使用的都是同一个外部对象（函数或者变量）。所以C语言没有java的函数重载之类的说法。

#### 内部变量

函数中定义的变量称为内部变量、本地变量。作用域、生存期，都是由花括号界定的的。形参是函数中隐式声明的变量，所以也是。

- 一个函数的变量，是私有变量，不能被另一个函数访问、使用。函数 func_a 调用 func_b，即使都有变量 int hour，并且数值一样，它们仍然是不同的变量（存储区域）。
- 是「自动 auto」的，每个局部变量都是函数调用时建立（分配存储空间），函数执行完毕时消失（释放存储空间），所以也称为自动变量。也正因为每次调用建立，所以每次调用函数，局部变量都表示不同的存储空间，不能保存前一次调用时的赋值，每次进入的时候都要显式赋值，如果没有赋值，那就会是随机值。

#### 外部变量

外部变量定义在任何函数之外，一般是源文件前部。全局变量在程序开始运行时分配存储空间，结束时释放存储空间，任何函数都可以访问全局变量。因为所有的函数都可以访问，所以就可以成为函数之间的数据交换方式，用来替代参数和返回值。

- 只能定义一次，定义后，编译程序将会为它分配存储单元（自动变量是函数调用时，才分配存储单元）。只能在定义处进行初始化赋值，只能用常量进行初始化赋值。

- 可以声明很多次。用`extern` 显式声明，也可以通过上下文隐式声明

  ```c
  /* 如果外部变量的定义，在函数使用它之前，就无需 extern。*/
  int max = 0; /* 定义一个全局变量 */
  int main(void){ max = 1;} /* 使用全局变量 */
  
  /* 如果外部变量 定义在使用之后，就一定要 extern。建议写明 extern */
  int main(void){ extern int max = 1;} /* 使用全局变量 */
  int max = 0;  /* 定义全局变量 */
  ```

注意：

- 既然任何函数都可访问，如果不仔细设计，那么全局变量的读写顺序、代码顺序、代码执行过程就会非常复杂。如果某个全局变量操作  引入bug，会非常难找。因此，虽然全局变量使用很方便，但不要轻易使用，传参和返回值  就能解决很多问题。
- 结构耦合、数据依赖：用全局变量来传递数据，会使函数之间耦合性太强，失去通用性。
- 多次改变外部变量的值，要非常小心，应该以一种一致的方式进行，有一套统一的规则，规定哪儿读（取值）、哪儿写（赋值）。

适用的情景：

- 两个函数需要共享大量的变量，用外部变量替代长长的参数列表
- 两个函数必须共享数据，但是两个函数不互相调用对方。




#### 作用域  与 标识符覆盖

标示符的作用域，指的是程序中可以使用该名字的部分。

##### 内部对象作用域

- 函数、程序块 内部变量的作用域，仅在函数、程序块内部。


##### 外部对象（函数和全局变量）的作用域

- 默认作用域从声明语句开始，一直到源文件的末尾。

- 如果在定义前使用外部变量，或者外部变量不在同一个源文件，这是在作用域之外访问外部对象，就必须使用`extern`关键字

  ```c
  int main(void)
  {
    void push(double f);  /*必须声明函数原型 */
    extern sp = 1; /*必须声明extern */
  }
  int sp;
  
  void show(double f){printf("%g",f);}
  void push(double f){
    show(f); /*无需声明函数就可使用（不建议）*/
    sp = 0;/*无需extern就可使用（不建议）*/
  }
  ```


##### 变量名覆盖

访问变量名时，按照「就近原则」，所以，程序块、内部变量、外部变量、其他源文件的变量可能会依次覆盖。

```c
#include <stdio.h>
char *s = "external var\n";
void show(void)
{
    char *s = "auto var in show()\n";
    printf(s); /* 本地（函数内）找到。找不到，才去 源文件文件 */
    {   /* 注意，程序块  也是一个命名空间 */
        char *s = "auto var of block in show()\n";
        printf(s); /* 程序块内找到。找不到，才去 块 外面找。 */
    }
}
int main(void)
{
    printf(s); /* 本地（函数内）找不到，去 源文件（函数外面）找 */
    show();
    return 0;
}
```



### 关键字 static、register 与初始化

##### static关键字

- 用于外部对象，比如函数、外部变量，这个对象的作用域仅从声明处到这个源文件末尾，其他源文件无法访问。用于隐藏外部对象，防止标识符冲突。

  ```c
  static char allocbuf[ALLOCSIZE]; /* static 外部变量，只在模块内使用 */
  static int getline(char *, int); /* static 函数，只在模块内使用 */
  char *alloc(int n)
  {
     ;
  }
  ```

- 用于内部变量，与自动变量不同的是，调用结束后不会消失，会一直存在，直到程序结束。这是只能在某个特定函数中使用、但一直占据存储空间的变量。

  ```c
  int static_inter_var(void)
  {
      static int i = 0; /* 用在内部变量，就变成 特用、不消失 */
      return i++;
  }
  int main(void)
  {
      printf("\t%d\n", static_inter_var()); /* 结果为 0 */
      printf("\t%d\n", static_inter_var()); /* 结果为 1 */
      return 0;
  }
  ```

##### register 关键字

只能用于函数内的自动变量、形参（本质也是自动变量）。作用是：告诉编译器，将变量放在寄存器中，这样程序会更小更快。但是编译器未必会这样做，所以过量的register没有害处。只是，使用后，不能访问地址，所以没法使用指针。

```c
int func(register long n) /* 可以用在 形式参数 */
{
    register int i; /* 可以用在  自动变量 */
}
```

##### 变量的初始化

- 外部变量与静态变量，只能用字面量表达式初始化（自动变量、寄存器变量可以用任何表达式）。因为程序运行开始就需要初始化外部变量、静态变量，所以在编译时就要确定数值嵌入到可执行文件中，所以只能用字面量及其表达式。外部变量只能在定义时初始化。

  ```c
  double pi = 3.14 + 0.0016 /* 合法：常量（字面量）及其表达式 */
  double pi = acos(-1.0); /* 不合法 */
  int minute = 360; int hour = minute / 60;  /* 不合法 */
  /* 外部变量、static变量 在定义的时候初始化，是最好的 */
  int hour = 12;
  int array[MAX];
  ```

- 变量一定要初始化。外部变量和静态变量如果不初始化，那么默认为0；但自动变量、寄存器变量如果不初始化，初始值是随机的（由内存此时的电位确定）。

  ```c
  /* 自动变量、register变量，先声明，后初始化赋值，是最好的。显眼，容易看到 */
  int num;
  num = 10;
  ```

  例子：如果没有那个干扰内存的 printf 语句，运行结果是 4200718（随机值）、79（赋值后的“残留”）；加上这句，1997017442（随机值）、1997114581（printf 后的残留）。所以，初始值真是随机的。

  ```c
  #include <stdio.h>

  void show_local(void)
  {
      int i;
      printf("%d\n", i); /* 未初始化就使用 */
      i = 79;
  }

  int main(void)
  {
      show_local();
      /* printf("Hello\n"); */      /* 干扰内存的语句 */
      show_local();
      return 0;
  }
  ```

- 数组的初始化

  ```c
  /* 用 初始化表达式 */
  int week[] = {1, 2, 3, 4, 5, 6, 7}; /* 省略了长度 week[7] */
  int week[7] = {1, 2, 3, 4, 5, 6, 7}; /* 也可以注明长度，并不建议 */
  int week[2] = {1, 2, 3, 4, 5, 6, 7}; /* 错误。长度和初始化表达式不对应。所以不建议注明长度 */
  int a[3] = { , , 2}; /* 错误。不能跳过前面的元素，直接赋值后面的元素 */
  int b[3] = a = {1, 2, 3}; /* 错误。一个初始化表达式不能同时赋值给 多个 列表 */ 
  /* char 数组的初始化，可以用 字符串 代替 字符初值表 */
  char hi[4] = "How";
  char hi[4} = {'H', 'o', 'w', '\0'};   /* char 初值表。很少用 */
  ```

  




### 递归 Recursive 函数

递归的数学例子：阶乘Factorial `n! = n * (n - 1)`，但是有一个最基本的条件 `0! = 1`

直接或者间接调用自己的函数称为递归函数。间接调用：A—B—A...

任何函数都可以递归调用。

```c
/* 打印整数 */
void intPrint(int n)
{
    if (n < 0){
        putchar('-');
        n = -n;
    }
    if (n / 10)
        intPrint(n / 10);
    putchar(n % 10 + '0');
}

/* 快速排序。以递增顺序 对 v[left]...v[right] 进行排序 */
void qsort(int v[], int left, int right)
{
    int i, last;
    void swap(int v[], int i, int j);
    if (left >= right)
        return;
    swap(v, left, (left + right) / 2);
    last = left;
    for (i = left + 1, i <= right, i++)
        if (v[i] < v[left])
            swap(v, ++last, i);
    swap(v, left, last);
    qsort(v, left, last - 1);
    qsort(v, last + 1, right);
}
void swap(int v[], int i, int j)
{
    int tmp;
    tmp = v[i], v[i] = v[j], v[j] = tmp;
}
```

递归会增加内存使用量，因为需要一层层的调用栈，而且递归也没有执行速度的优势。所以使用递归并不是为了性能，而是为了紧凑、易读，尤其适合树状结构。










### 预处理器

##### #include 包含头文件

将头文件的内容，复制粘贴到当前文件里。

```c
#include <stdio.h>  /*在库中查找，路径由系统环境变量INCLUDE或编译器指定 */
#include "str.h"  /* 在源文件所在的目录查找文件 */
```

把所有声明捆绑在一起，抱在所有的源文件都有相同的定义和声明，才不至于导致低级的错误。一般来说，头文件包含：#define语句、extern声明、共用的函数原型、库文件。比如`calc.h`文件：

```c
#include <stdio.h>
#define NUMBER '0'
int getop(char s[]); 
double pop(void);
void push(double f);
int getch(void);
void ungetch(int c);
```

##### #define 宏替换

- 宏最简单的应用：文本替换

  ```c
  #include <stdio.h>
  #define YES "ye" \
  "s!!!"    /* 用任意文本 替换 标识符。一般一行，可以\分行。作用域：从定义点，到源文件末尾 */
  #define  forever  for(;;)  /* 替换文本可以是任意的 */
  main(void)
  {
      printf(YES);   /* 能替换 */
      printf("YES, I AM");  /* 不能替换字符串 */
      printf(YES"MAN");  /* 能替换 */
      printf(YESMAN);  /* 不能替换 相似 但不同 的标识符 */
  }
  ```

- 带参数的宏扩展：函数式的宏

  ```c
  #define forever for(;;) /* 定义一个死循环 */

  /* 一定要注意括号的应用 */
  #define max(A, B)  ((A) > (B) ? (A) : (B)) 
  /* 
  x = max(p+q, r+s); 替换为
  x = ((p+q) > (r+s) ? (p+q) : (r+s));
  m = (i++, j++) 因为 max() 宏的AB要计算2次，所以存在副作用（自增、输入输出）的表达式就会出错
  */
  #define square(x)   x * x
  /* 
  square(z+1) 替换为 z+1 * z+1 所以是错的。定义宏的时候注意括号怎么写。
  正确定义是   #define square(x)  (x)*(x)
  */

  #define swap(t, x, y)  ({t tmp; tmp = (x), (x) = (y), (y) = tmp;})
  /* 
  int a, b; 
  swap(int ,a, b); 替换为一个程序块结构 ({...});
  ({int tmp; tmp = (a), (a) = (b), (b) = tmp;});
  */

  /* 宏定义函数的参数，与字符串的处理 */
  #define dprint(expr)  printf(#expr " = %g\n",  expr)
  /* 
  dprint(x/y);   替换为（特殊字符比如 " \ 会自动转义）
  printf("x/y" " = %g\n",  x/y); 字符串会自动进行合并，结果是
  printf("x/y = %g\n",  x/y); 
  */

  /* 宏定义的参数，如何连接 */
  #define paste(front, back)  front##back
  /* 
  paste(name, 1) 替换为 name1
  */
  ```

- 取消宏定义

  ```c
  /* 如果以前定义过这个宏，就取消它的定义（一般接着 新定义代码） */
  #undef  __mingw_stdio_redirect__ /* mingw stdio.h 源码 */
  #define __mingw_stdio_redirect__(F) __cdecl __MINGW_NOTHROW __mingw_##F
  ```

##### 条件包含（条件编译）

根据编译环境，选择性地包含不同的代码。编译环境会提供某些整型常量，以此为判断。

```c
#if
#elif
#else
#ednif

/* 头文件 如何避免重复 include 自身？ mingw stdio.h 为例 */
#if !defined(_STDIO_H)
# define _STDIO_H
     /* 整个头文件其余部分代码就写在这里了 */
#endif

/* 对于 define与否，有专门的语法 */
#ifdef _STDIO_H  /* if defined(_STDIO_H) */
#ifndef _STDIO_H /* if !defined(_STDIO_H) */

/* 条件编译还可以嵌套。以 mingw stdio.h 源码 为例 */
#if __USE_MINGW_ANSI_STDIO /* 如果编译选择 -std=c99 */
/* 嵌套后，为了易读，使用了缩进 */
# undef __mingw_stdio_redirect__
# ifdef __cplusplus /* 如果是 C++ 代码 */
#  define __mingw_stdio_redirect__  inline __cdecl __MINGW_NOTHROW
# else              /* 如果不是 C++ */
#  define __mingw_stdio_redirect__  static __inline__ __cdecl __MINGW_NOTHROW
# endif /* __cplusplus */
#else /* __USE_MINGW_ANSI_STDIO */
	/* 这里有大段代码 */
#endif /* __USE_MINGW_ANSI_STDIO */
```

- 常用宏定义：判断操作系统平台，编写跨平台代码

  ```c
  /* 操作系统平台 */
  #ifdef _WIN32 /* win64 是 win32 的子集 */
     //define something for Windows (32-bit and 64-bit, this part is common)
     #ifdef _WIN64
        //define something for Windows (64-bit only)
     #else
        //define something for Windows (32-bit only)
     #endif
  #elif __APPLE__
      #include <TargetConditionals.h>
      #if TARGET_IPHONE_SIMULATOR  /* TARGET_IPHONE_SIMULATOR 是 TARGET_OS_IPHONE 的子集，TARGET_OS_IPHONE 是 TARGET_OS_MAC的子集。*/
           // iOS Simulator
      #elif TARGET_OS_IPHONE
          // iOS device
      #elif TARGET_OS_MAC
          // Other kinds of Mac OS
      #else
      #   error "Unknown Apple platform"
      #endif
  #elif __ANDROID__
      // android
  #elif __linux__
      // linux
  #elif __unix__ // all unices not caught above
      // Unix
  #elif defined(_POSIX_VERSION)
      // POSIX
  #else
  #   error "Unknown compiler"
  #endif

  /* 编译器 */
  #ifdef _MSC_VER /* visual studio */
  #elif defined(__GNUC__) /* GCC/G++ */
  #elif defined(__SUNPRO_C) || defined(__SUNPRO_CC) /* SunCC */
  #endif
  ```

- 常用宏定义：判断C 与 C++，编写通用模块

  ```c
  #ifdef __cplusplus
  extern "C" {
  #endif
    /* 这里写大量的 C 代码 */
  #ifdef __cplusplus
  }
  #endif
  ```

  

  







### 程序结构

一个程序可以保存在一个或者多个源文件中，各个文件可以单独编译，并且可以和库中已编译的函数一起加载。

一个程序，可以看成是变量定义和函数定义的集合。

不同的函数，可能有调用和依赖关系，但在源文件中出现的次序可以是任意的，甚至可以出现在多个单独编译的不同源文件中（但是，一个函数不可以切割为不同的文件）。

跨文件使用函数和变量：

- 只需关注外部对象：全局变量和函数（局部变量和静态局部变量的作用域仅限于函数内）。 
- 函数和变量，都有定义、声明、引用三要素。定义是创造这个变量或者函数，声明是向编译器交代它的原型，引用是使用这个变量或函数。 在一个程序里面，一个函数/变量只能定义一次，引用可以有无数次，声明可以有无数次。因为函数/变量的定义实际上是创造了这个函数/变量，所以只能有一次。多次创造同名的变量会造成变量名重复，冲突；多次创造同名的函数也会造成函数名重名冲突。

用头文件解决函数声明问题

- 项目有很多个源文件，每个源文件中都有很多个函数，并且需要在各个文件中相互穿插引用函数。如果每次都将函数声明写一遍，将会非常累。 

- `#include <stdio.h>`包含库文件，`#include "some.h"`包含自己编辑的项目头文件。 

- 防止重复包含头文件：

  ```c
  #ifndef HDR
  #define HDR
   /* 将 hdr.h 文件内容粘贴到这里 */
  #endif
  ```

- 最好不要在头文件中定义变量。因为头文件被多个源文件包含，就会出现重复定义问题。全局变量的定义就应该放在某个源文件中，然后在别的源文件中使用前进行extern声明。

[1](https://blog.codingnow.com/2010/01/modularization_in_c_1.html)

[2](http://blog.csdn.net/zhzht19861011/article/details/5974945)

[3](http://blog.csdn.net/chunhuayun/article/details/48677619)

[4](http://blog.csdn.net/weixin_38239856/article/details/76358636)

[5](http://blog.csdn.net/candcplusplus/article/details/53326368)

[6](http://blog.csdn.net/candcplusplus/article/details/7317472)

[7](http://blog.csdn.net/rston/article/details/50867546)





##### 程序结构案例：逆波兰表示法的计算器

```c
///////////////////   calc.h
#include <stdio.h>
#define NUMBER '0' /* 标志找到一个数 */

int getop(char s[]); /* 各个函数的声明 */
double pop(void);
void push(double f);
int getch(void);
void ungetch(int c);
///////////////////   calc_stack.h
#include "calc.h"
#define MAXVAL 100
int sp = 0;  /* 下一个空闲栈的位置 */
double val[MAXVAL];  /* 保存值的栈（一个数组） */
/* 把数值一个个压入到栈中 */
void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f; /* sp++ 返回原值 */
    else
        printf("error: stack full, can't push %g in.\n", f);
}
/* 弹出并且返回栈顶的值 */
double pop(void)
{
    if (sp > 0)
        return val[--sp]; /* --sp 返回运算后的值 */
    else
        printf("error: stack is empty.\n");
        return 0.0;
}
///////////////////   calc_getop.h
#include <ctype.h>
#include "calc.h"
#define BUFSIZE 100
char buf[BUFSIZE];
int bufp = 0;
/* getch() ungetch() 纯粹为了IO缓冲，没有任何实际意义 */
int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}

/* 获取下一个运算符或者操作数 */
int getop(char s[])
{
    int i, c;
    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c;
    i = 0;
    if (isdigit(c))
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);
    return NUMBER;
}
/* 单元测试代码 */
// int main(void)
// {
//     char s[100];
//     char c;
//     while ((c = getop(s)) != EOF) {
//         printf("\t%c\n", c);
//     }
//     return 0;
// }

///////////////////   calc_main.c

#include <stdlib.h> /* 为了使用 atof() 函数 */
#include "calc.h"
#define MAXOP 100 /* 操作数、运算符 字符串最大长度 */
/* 实现 逆波兰计算器 */
int main(void)
{
    int type;
    double op2;
    char s[MAXOP];
    while ((type = getop(s)) != EOF) {
        switch (type) {
        case NUMBER:
            push(atof(s));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '-':
            op2 = pop(); /* 减法除法，操作数有顺序。push(pop() - pop()) 不能保证哪个pop()先运行。 */
            push(pop() - op2);
            break;
        case '*':
            push(pop() * pop());
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2);
            else
                printf("error: divided by zero");
            break;
        case '\n':
            printf("\t%.8g\n", pop());
            break;
        default:
            printf("error: unknown command!");
            break;
        }
    }
    return 0;
}
```





















