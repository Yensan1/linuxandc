from abc import ABCMeta, abstractmethod
ABC = ABCMeta(u'ABC', (object,), {})
class Subject(ABC):
    @abstractmethod
    def register_observer(self, observer_obj):pass
    @abstractmethod
    def remove_observer(self, observer_obj):pass
    @abstractmethod
    def notify_observers(self, observer_obj):pass

class WeatherData(Subject):
    def __init__(self):
        self.__observers = []
        self.temperature = None
        self.humidity = None
        self.pressure = None
    def register_observer(self, observer_obj):
        self.__observers.append(observer_obj)
    def remove_observer(self, observer_obj):
        # TODO 也许不是这个接口
        self.__observers.remove(observer_obj)
    def notify_observers(self):
        for item in self.__observers:
            item.update(self.temperature, self.humidity, self.pressure)
    def setMeasurements(self, temperature, humidity, pressure):
    # def setMeasurements(temperature: float, humidity: float, pressure: float):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.notify_observers()


class Observer(ABC):
    @abstractmethod
    def update(self, temperature, humidity, pressure): pass
    # def update(self, temperature: float, humidity: float, pressure: float):

class DisplayElement(ABC):
    @abstractmethod
    def display(self): pass
    # def update():

class CurrentConditionDisplay(Observer, DisplayElement):
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.weatherData.register_observer(self)
        self.temperature = None
        self.humidity = None
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.display()
    def display(self):
        print("Current Condition: ", self.temperature, "F, ", self.humidity, "% humidity")
    def trunDown(self):
        self.weatherData.remove_observer(self)
if __name__ == '__main__':
    weatherData = WeatherData()
    currentDisplay = CurrentConditionDisplay(weatherData)
    weatherData.setMeasurements(80.0, 65.0, 30.4)
    currentDisplay.trunDown()
    weatherData.setMeasurements(5.0, 4.0, 3.4)




