// package observer_pattern;
public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay currentDisplay = new CurrentConditionDisplay(weatherData);
        weatherData.setMeasurements(80.0f, 65.0f, 30.4f);
        currentDisplay.trunDown();
        weatherData.setMeasurements(5.0f, 4.0f, 3.4f);
    }
}
