#ifndef _CROSPLATFM_H
#define _CROSPLATFM_H

#ifdef __cplusplus
extern "C" {
#endif     /* C++ */


#if defined(_WIN32)  /* Windows code in below */

/* Windows code */

#elif defined(__APPLE__)    /* exclude iOS */
    #include <TargetConditionals.h>
    #if TARGET_OS_IPHONE
    #   error "Just for MacOS ONLY, not for iOS platform"
    #elif TARGET_OS_MAC
    #define MAC_OS 1
    #else
    #   error "Unknown Apple platform"
    #endif /* Mac */
#endif
#if defined(__linux__) || defined(MAC_OS)   /* MacOS & Linux code in below */

#include <sys/ioctl.h>
#include <termios.h>
#include <ncurses.h>

int kbhit();
int kbhit()
{
    struct termios term;
    tcgetattr(0, &term);

    struct termios term2 = term;
    term2.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &term2);

    int byteswaiting;
    ioctl(0, FIONREAD, &byteswaiting);

    tcsetattr(0, TCSANOW, &term);

    return (byteswaiting > 0) ? 1 : 0;
}


#endif


#ifdef __cplusplus
}
#endif  /* C++ */

#endif /* _CROSPLATFM_H */
