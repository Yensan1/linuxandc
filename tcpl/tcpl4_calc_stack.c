#include "tcpl4_calc.h"
#define MAXVAL 100
int sp = 0;  /* 下一个空闲栈的位置 */
double val[MAXVAL];  /* 保存值的栈（一个数组） */
/* 把数值一个个压入到栈中 */
void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f; /* sp++ 返回原值 */
    else
        printf("error: stack full, can't push %g in.\n", f);
}
/* 弹出并且返回栈顶的值 */
double pop(void)
{
    if (sp > 0)
        return val[--sp]; /* --sp 返回运算后的值 */
    else
        printf("error: stack is empty.\n");
        return 0.0;
}
