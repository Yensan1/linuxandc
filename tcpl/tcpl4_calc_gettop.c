#include <ctype.h>
#include "tcpl4_calc.h"
#define BUFSIZE 100
char buf[BUFSIZE];
int bufp = 0;
/* getch() ungetch() 纯粹为了IO缓冲，没有任何实际意义 */
int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}

/* 获取下一个运算符或者操作数 */
int getop(char s[])
{
    int i, c;
    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c;
    i = 0;
    if (isdigit(c))
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);
    return NUMBER;
}
/* 单元测试代码 */
// int main(void)
// {
//     char s[100];
//     char c;
//     while ((c = getop(s)) != EOF) {
//         printf("\t%c\n", c);
//     }
//     return 0;
// }
