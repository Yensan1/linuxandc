#ifdef __cplusplus
#include <gtest/gtest.h>
extern "C" {
#endif     /* C++ */

#include <stdio.h>
#define XMAX 100
#define YMAX 100
struct point {int x, y;};
struct rect { struct point pt1, pt2;};

struct point makepoint(int x, int y);

int main(void)
{
    struct rect screen;
    struct point middle;
    screen.pt1 = makepoint(0, 0);
    screen.pt2 = makepoint(XMAX, YMAX);
    struct pta = {1, 3};
    struct ptb = pta; /* 结构体可以相互赋值。用一个结构体，初始化赋值 另一个结构体 */
    middle = makepoint((screen.pt1.x + screen.pt2.x) / 2,
                       (screen.pt1.y + screen.pt2.Y) / 2,);
}

/* 1. 传入成员，返回结构体。适用于结构体变量的动态初始化（非常量初始化） */
/* 结构体无法算术、逻辑运算，但内部的基本类型可以。用内部基本类型的运算，实现结构体运算  */
struct point makepoint(lenu x, lenu y)
{
    struct point tmp;
    tmp.x = x,  tmp.y = y;
    return tmp;

}


/* 传入整个结构 */
struct point addpoint(struct point p1, struct point p2)
{
    p1.x += p2.x,   p1.y += p2.y;
    return p1;
}


/* 2. 传入结构体，返回结构体 */
struct point middle_point(struct point pt1, struct point pt2)
{
    struct point tmp;
    tmp.x = (pt1.x + pt2.x) / 2;
    tmp.y = (pt1.y + pt2.y) / 2;
    return tmp;
}
/*   改为：传入结构体指针，返回结构体 */
struct point middle_point_p(struct point *pt1p, struct point *pt2p)
{
    struct point tmp;
    tmp.x = (pt1p->x + pt2p->x) / 2;
    tmp.y = (pt1p->y + pt2p->y) / 2;
    return tmp;
}

/*  传入结构体，判断一个点是否在矩形内，返回 0/1 */
int p_in_rect(struct point p, struct rect r)
{  /* 判断点是不是在矩形内。默认矩形 点1坐标最小，点2 坐标最大 */
    return p.x > r.pt1.x && p.x < r.pt2.x && p.y > r.pt1.y && p.y < r.pt2.y;
}

/*  改为：传入结构体指针 */
int pp_in_rectp(struct point *p, struct rect *r)
{
    return (p->x > r->pt1.x && p->y > r->pt1.y &&
            p->x < r->pt2.x && p->y < r->pt2.y    );
}

#define min(a, b)  ((a) < (b) ? (a) : (b))
#define max(a, b)  ((a) > (b) ? (a) : (b))
struct rect canon_rect(struct rect r)
{  /* 将矩形格式化为标准矩形： 点1坐标最小，点2 坐标最大 */
    struct rect tmp;
    tmp.pt1.x = min(r.pt1.x, r.pt2.x);
    tmp.pt1.y = min(r.pt1.y, r.pt2.y);
    tmp.pt2.x = max(r.pt1.x, r.pt2.x);
    tmp.pt2.y = max(r.pt1.y, r.pt2.y);
    return tmp;
}

/* 重写canon_rect()：传入指针，而不是整个结构体 */
void canon_rect_p(struct rect *rp)
{
    struct point tmp1, tmp2;
    tmp1.x = min(rp->pt1.x, rp->pt2.x);
    tmp1.y = min(rp->pt1.y, rp->pt2.y);
    tmp2.x = max(rp->pt1.x, rp->pt2.x);
    tmp2.y = max(rp->pt1.y, rp->pt2.y);
    rp->pt1 = tmp1;
    rp->pt2 = tmp2;
}




#ifdef __cplusplus  /* C++  with Gtest   */
}


TEST(FooTest, pointInScreen) /* tests combine to TEST */
{
    struct point pt1 = makepoint(0, 10);
    struct point pt2 = makepoint(10, 0);
    struct rect  screen = {pt1, pt2};
    struct point a = makepoint(3, 4);
    EXPECT_FALSE(p_in_rect(a, screen));
    EXPECT_FALSE(pp_in_rectp(&a, &screen));
}
TEST(FooTest, pointInCanonRect) /* tests combine to TEST */
{
    struct point pt1 = makepoint(0, 10);
    struct point pt2 = makepoint(10, 0);
    struct rect  screen = {pt1, pt2};
    struct point a = makepoint(3, 4);
    // screen = canon_rect(screen);
    canon_rect_p(&screen);
    EXPECT_TRUE(p_in_rect(a, screen));
    EXPECT_TRUE(pp_in_rectp(&a, &screen));


}
TEST(StructTest, middlepoint) /* tests combine to TEST */
{
    struct point pt1 = makepoint(0, 10);
    struct point pt2 = makepoint(10, 0);
    struct point md1 = middle_point_p(&pt1, &pt2);
    EXPECT_EQ(md1.x, 5);EXPECT_EQ(md1.y, 5);
    struct point pt3 = makepoint(0,0);
    struct point pt4 = makepoint(4,4);
    struct point md2 = middle_point_p(&pt3, &pt4);
    EXPECT_EQ(md2.x, 2);EXPECT_EQ(md2.y, 2);
}



int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif  /* C++ */
