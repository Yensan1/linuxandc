///////////////////   calc.h
#include <stdio.h>
#define NUMBER '0' /* 标志找到一个数 */

int getop(char s[]); /* 各个函数的声明 */
double pop(void);
void push(double f);
int getch(void);
void ungetch(int c);
