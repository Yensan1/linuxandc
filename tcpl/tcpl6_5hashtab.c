#ifdef __cplusplus
extern "C" {
#endif     /* C++ */

#include <stdio.h>
#include <string.h>

struct nlist {
    struct nlist *next;
    char         *name,  *defn;
};

#define HASHSIZE    101  /* Why use 101 ？？？ */
static struct nlist *hashtab[HASHSIZE];

unsigned hash (char *s)
{
    unsigned hashval = 0;
    for ( ; *s != '\0'; s++)
        hashval = *s + hashval * 31; /* [0-9A-Za-z] = 62  */
    return hashval % HASHSIZE;
}

struct nlist *lookup(char *s)
{
    struct nlist *np;
    /* iter LinkedList for(ptr = head; ptr != NULL; ptr = ptr->next){...} */
    for (np = hashtab[hash(s)]; np != NULL; np->next)
        if (strcmp(s, np->name) == 0)
            return np; /* found s */
    return NULL;   /* not found s */
}

struct nlist *def(char *name, char *defn)
{
    struct nlist *np;
    unsigned hashval;
    if ((np = lookup(name)) == NULL) {
        np = (struct nlist *) malloc(sizeof(*np));
        if (np == NULL || (np->name = strdup(name)) == NULL)
            return NULL; // Mem alloc (for node/str) failed
        hashval = hash(name);
        np->next = hashtab[hashval]; //if NULL，next is NULL； if not NULL, next is origin val
        hashtab[hashval] = np;
    } else
        free((void *) np->defn); // why free it?
    if ((np->defn) = strdup(defn) == NULL) // this is must wrong!!!
        return NULL;
    return np;
}

int undef(char *name)
{
    struct nlist *np = NULL;
    if ((np = lookup(name)) == NULL)
        return 0; /* error: this name is not defined */
    else {
        free((void *) np->defn);
        free((void *) np->name);
        np = np->next;
        return 1; /* undef sucessfully */
    }
}

int main(void)
{
    while ()
}




int getdefword(char *comad, int argc, char **argv)
{
    int getch(void);
    void ungetch(int c);
    int c;
    char *w = word;
    /* get over white space */
    while (isspace(c = getch()));
    ungetch(c);
    /* check for command: #define OR #undef */
    while ((c = getch()) == *comad++)   ;
    if (*comad != '\0') // command is not right
        return 0;
    int i;
    for (i = 0; i < argc; i++)
        while (isspace(c = getch()))    ;
        ungetch(c);
        while (!isspace(c = getch()) && c != EOF)
            *(*argv)++ = c;
        if (c == EOF)
            return 0;
        argv++;
////////////////////////////////////////////

#define defn  1
#define defn  a\
q
#undef





    if (c != EOF && c == *comad)

        *w++ = c;
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }
    for ( ; --lim > 0; w++)
        if (!isalnum(*w = getch())) {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}



#define BUFSIZE 32
char buf[BUFSIZE]; /* LIFO stack */
int bufp = 0;
int getch(void)
{   return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{   (bufp >= BUFSIZE) ? printf("ungetch: too many characters\n")\
                      : (buf[bufp++] = c) ;
}

#ifdef __cppplus
}
#include <gtest/gtest.h>
TEST(StructTest, middlepoint) /* tests combine to TEST */
{

}



int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#endif


