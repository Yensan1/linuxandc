#include <stdio.h>
/* 华氏温标摄氏温标的转换 */

int main(void)
{

    /* while 循环的版本 */
    // float fahr, celsius; /* Fahrenheit Celsius/centigrade temperature scale */
    // int lower, upper, step;
    // lower = 0;
    // upper = 300;
    // step  = 20;
    // fahr = lower;
    // while (fahr <= upper) {
    //     celsius = 5.0 * (fahr-32) / 9.0;
    //     printf("%.0f%7.1f\n", fahr, celsius);
    //     fahr = fahr + step;
    // }


    /* for 循环的版本 */
    int fahr;
    for (fahr = 0; fahr <= 300; fahr = fahr + 20) {
        printf("%3d%7.1f\n", fahr, (5.0 * (fahr-32) / 9));
    }
    return 0;
}

