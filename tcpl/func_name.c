#include <stdio.h>
struct complex_xct { double x, y; };
struct complex_xct add_complex(struct complex_xct z1, struct complex_xct z2)
{
    z1.x = z1.x + z2.x;
    z1.y = z1.y + z2.y;
    return z1;
}
void print_complex(struct complex_xct z)
{
    if (z.y < 0) { printf("%.1f%.1fi\n", z.x, z.y);
    } else { printf("%.1f+%.1fi\n", z.x, z.y); }
}
int main(void){
    struct complex_xct z1 = {3.0, 4.0};
    struct complex_xct z2 = {1.5, -6.3};
    struct complex_xct z3 = z1; /* 赋值运算 */
    /* z3 = z1 + z2; 这种表达式是不行的 */
    z1 = add_complex(z1, z1);
    print_complex(z1);
    print_complex(z2);
    print_complex(z3);
    return 0;
}









