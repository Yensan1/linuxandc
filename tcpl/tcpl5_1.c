#include <stdio.h>
/*
void swap(int x, int y)
{
    int tmp;
    tmp = x, x = y, y = tmp;
}
void main(void)
{
    int a = 8, b = 3;
    swap(a, b);
}
*/
void swap(int *px; int *py)
{
    int tmp;
    tmp = *px, *px = *py, *py = tmp;
}
void main(void)
{
    int a = 8, b = 3;
    swap(&a, &b);
}
