#include <stdio.h>
#include <string.h>
#define STR "I am a string"

int main(void)
{
    void reverse(char s[]);
    char s[] = STR;
    printf("before:\t%s\n", s);
    reverse(s);
    printf("after:\t%s\n", s);
}

/* 将 字符串 翻转顺序 */
void reverse(char s[])
{
    int tmp, i, j;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        tmp = s[i];
        s[i] = s[j];
        s[j] = tmp;
    }
}

