#include <stdio.h>
#include <ctype.h>
int getch(void);
void ungetch(int c);

int getint(int *pn)
{
    int c, sign;
    while (isspace(c = getch()))    ;
    if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
        ungetch(c);
        return 0;
    }
    sign = (c == '-') ? -1 : 1;
    if (c == '+' || c == '-')
        c = getch();
    for (*pn = 0; isdigit(c))
}
/*
void swap(int x, int y)
{
    int tmp;
    tmp = x, x = y, y = tmp;
}
void main(void)
{
    int a = 8, b = 3;
    swap(a, b);
}
*/
void swap(int *px; int *py)
{
    int tmp;
    tmp = *px, *px = *py, *py = tmp;
}
void main(void)
{
    int a = 8, b = 3;
    swap(&a, &b);
}
