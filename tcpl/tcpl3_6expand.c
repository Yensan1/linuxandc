#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define STR "-a-b0-9string"

int main(void)
{
    void expand(char t[], char s[]);
    void expand_answer(char t[], char s[]);
    char s[] = STR;
    char t[strlen(s) * 36];
    printf("%d", sizeof(t));
    printf("before:\t%s\n", s);
    expand_answer(s, t);
    printf("after:\t%s\n", t);
}

/* 将 速记符号扩展，自己写的，有bug */
void expand(char tgt[], char s[])
{
    int i, j, k;
    for (i=j=0; s[i] != '\0'; i++, j++){
        if (isalpha(s[i]) && s[i+1] == '-' && isalpha (s[i+2]))  {
            tgt[j++] = s[i];
            for (k=s[i]+1; (k-s[i]) < (s[i+2] - s[i]); k++, j++)
                tgt[j] = k;
            i += 2;
        }
            tgt[j] = s[i];
    }
    tgt[j] = '\0';

}

/* 答案，没有 bug */
void expand_answer(char s1[], char s2[])
{
    char c;
    int i, j;
    i = j = 0;
    while ((c = s1[i++]) != '\0') /* fentch a char */
        if (s1[i] == '-' && s1[i+1] >= c) {
            i++;
            while (c < s1[i])
                s2[j++] = c++; /* expand the shorthand */
        } else {
            s2[j++] = c; /* copy the character */
        }
    s2[j++] = '\0';
}



