#include <stdio.h>

#define MAXHIST 15 /* max length of histogram */
#define MAXWORD 11 /* max length of a word */
#define IN      1  /* inside a word  */
#define OUT     0  /* outside a word */




#ifdef __cplusplus
#include <gtest/gtest.h>

TEST(FooTest, testName) /* tests combine to TEST */
{
    EXPECT_EQ(3, foo(1, 2)); /* a test: give it pass */
    EXPECT_EQ(7, foo(3, 5)); /* a test: give it fail */
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

extern "C" {
#else

int main(void)
{
    int c, i, j, nc, state, maxvalue, overflow;
    int wl[MAXWORD];

    state = OUT;
    nc = 0;
    overflow = 0;
    for (i = 0; i < MAXWORD; i++)
        wl[i] = 0;
    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\n' || c == '\t') {
            state = OUT;
            if (nc > 0) {
                if (nc < MAXWORD)
                    ++wl[nc];
                else
                    overflow++;
            }
            nc = 0;
        } else if (state == OUT) {
            state = IN;
            nc = 1;
        } else
            nc++;
    }
    maxvalue = 0;
    for (i = 1; i < MAXWORD; i++)
        if (wl[i] > maxvalue)
            maxvalue = wl[i];
    for (i = MAXHIST; i > 0; --i) {
        for (j = 1; j < MAXWORD; ++j)
            if ((len = wl[i] * MAXHIST / maxvalue) >= 1)
                printf(" * ");
            else
                printf("   ");
            putchar('\n');
    }
    for (i = 1; i < MAXWORD; i++)
        printf("%4d", i);
    putchar('\n');
    for (i = 1; i < MAXWORD; i++)
        printf("%4d", wl[i]);
    putchar('\n');
    if (overflow > 0)
    printf("There are %d words >= %d\n", overflow, MAXWORD);
}

#endif






