#include <stdio.h>

#define ALLOCSIZE  10000 /* 定义最大的尺寸 */
static char allocbuf[ALLOCSIZE]; /* alloc afree 操作的存储空间 */
/* 定义一个指针，并且初始化。指针初始化，要么为 0/NULL，要么是 实际地址 */
static char *allocp = allocbuf; /* 下一个空闲空间的指针。初始值为 &allocbuf[0] */
/* 开辟n个char的空间，返回内存块的指针 */
char *alloc(int n)
{   /* 如果空间还足够，就分配空间，并且返回 这个内存块 起始点的指针。 */
    if (ALLOCSIZE - (allocp - allocbuf) >= n) {  /* 剩余 = 总值 - (现指针 - 初始位置) */
        allocp += n;
        return allocp - n;
    } else
        return 0; /* 内存地址永远大于 0，所以可以用来表示异常状态 */
}
/* 释放 p 指向的内存区域 */
void afree(char *p)
{
    if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
        allocp = p; /* 只能是 栈 FILO 型存储，alloc() afree()必须轮流调用 */
}

int main(void)
{
    printf("\t%lu\n", strlen("haofgsdnj"));


    return 0;
}

