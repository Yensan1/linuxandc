#include <stdio.h>
#include <ctype.h>
#include <string.h>

struct key { char *word;    int count;};
struct key keytab[] = {
        "auto", 0,     "break", 0,      "case", 0,     "char", 0,
        "const", 0,    "continue", 0,   "default", 0,  "do", 0,
        "double", 0,   "else", 0,       "enum", 0,     "extern", 0,
        "float", 0,    "for", 0,        "goto", 0,     "if", 0,
        "int", 0,      "long", 0,       "register", 0, "return", 0,
        "short", 0,    "signed", 0,     "sizeof", 0,   "static", 0,
        "struct", 0,   "switch", 0,     "typedef", 0,  "union", 0,
        "unsigned", 0, "void", 0,       "volatile", 0, "while", 0
};

#define NKEYS (sizeof keytab / sizeof keytab[0])
#define MAXWORD     100

int getword(char *, int);
struct key *binsearch(char *, struct key *, int);

#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#else   /* C main() */

int main(void)
{
    char word[MAXWORD];
    struct key *p = NULL;

    while (getword(word, MAXWORD) != EOF)
        if (isalpha(word[0]))
            if ((p = binsearch(word, keytab, NKEYS)) != NULL)
                p->count++;
    for (p = keytab; p < keytab + NKEYS; p++)
        if (p->count > 0)
            printf("\t%s: %4d\n", p->word, p->count);
    return 0;
}

#endif     /* C++ */

/* 在 tab[] 中有n个项，从中找 word字符串 */
struct key *binsearch(char *, struct key *, int)
{
    int cond;
    struct key *low, *high, *mid;
    low = &tab[0];  high = &tab[n];
    while (low < high) {
        mid = low + (high - low) / 2;
        if ((cond = strcmp(word, mid->word)) < 0)
            high = mid - 1;
        else if (cond > 0)
            low  = mid + 1;
        else
            return mid;
    }
    return NULL;
}

int getword(char *word, int lim)
{

    int getch(void);
    void ungetch(int c);
    int c;
    char *w = word;

    while (isspace(c = getch()));
    if (c != EOF)
        *w++ = c;
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }
    for ( ; --lim > 0; w++)
        if (!isalnum(*w = getch())) {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}


#define BUFSIZE 100
char buf[BUFSIZE]; /* LIFO stack */
int bufp = 0;

int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE) /* 如果 */
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}


#ifdef __cplusplus
}

#include "gtest/gtest.h"

TEST(getword, throwEOF) /* test case */
{
    int getword(char *, int);
    char word[MAXWORD];
    int re;
    while ((re = getword(word, MAXWORD)) != EOF)
        printf("\t%s  %d\n",word, re);
    getchar();
}


int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif     /* C++ */
