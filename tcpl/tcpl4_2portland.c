#include <stdio.h>
#include <stdlib.h> /* 为了使用 atof() 函数 */

#define MAXOP 100 /* 操作数、运算符 字符串最大长度 */
#define NUMBER '0' /* 标志找到一个数 */


int getop(char s[]); /* 各个函数的声明 */
double pop(void);
void push(double f);

/* 实现 逆波兰计算器 */
int main(void)
{
    int type;
    double op2;
    char s[MAXOP];
    while ((type = getop(s)) != EOF) {
        switch (type) {
        case NUMBER:
            push(atof(s));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '-':
            op2 = pop(); /* 减法除法，操作数有顺序。push(pop() - pop()) 不能保证哪个pop()先运行。 */
            push(pop() - op2);
            break;
        case '*':
            push(pop() * pop());
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2);
            else
                printf("error: divided by zero");
            break;
        case '\n':
            printf("\t%.8g\n", pop());
            break;
        default:
            printf("error: unknown command!");
            break;
        }
    }
    return 0;
}





#define MAXVAL 100

int sp = 0;  /* 下一个空闲栈的位置 */
double val[MAXVAL];  /* 保存值的栈（一个数组） */

/* 把数值一个个压入到栈中 */
void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f; /* sp++ 返回原值 */
    else
        printf("error: stack full, can't push %g in.\n", f);
}

/* 弹出并且返回栈顶的值 */
double pop(void)
{
    if (sp > 0)
        return val[--sp]; /* --sp 返回运算后的值 */
    else
        printf("error: stack is empty.\n");
        return 0.0;
}



#include <ctype.h>

int getch(void);
void ungetch(int);

/* 获取下一个运算符或者操作数 */
int getop(char s[])
{
    int i, c;

    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c;
    i = 0;
    if (isdigit(c))
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.')
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);
    return NUMBER;
}



#define BUFSIZE 100

char buf[BUFSIZE];
int bufp = 0;

int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}













