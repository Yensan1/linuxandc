#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    char gesture[3][10] = {"Scissor", "Stone", "Cloth"};
    int man, computer, result, ret;
    srand(time(NULL));
    while (1) {
        computer = rand() % 3;
        printf("Input your gesture,\n(0-Scissor, 1-Stone, 2-Cloth):\n");
        ret = scanf("%d", &man); /* 如果输入不是数字，就会返回 1 */
        if (ret != 1 || man < 0 || man >2) { /* 输入必须是数字，而且只能是 0, 1, 2 */
            printf("Invalid Input");
            return 1;
        }
        printf("You: %s\tComputer:%s\n", gesture[man], gesture[computer]);
        result = (man - computer + 4) % 3 - 1; /* 4%3 - 1 = 0 加上4是为了防止出现负数 */
        if (result > 0) {
            printf("You win!\n"); /* 石头剪子布实际上是一个 3 进制问题。只要在3进制上更大就赢 */
        } else if (result == 0) {
            printf("Draw.\n");
        } else {
            printf("You lose.\n");
        }
    }
    return 0;
}
