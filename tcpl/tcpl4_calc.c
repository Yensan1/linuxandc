
#include <stdlib.h> /* 为了使用 atof() 函数 */
#include "tcpl4_calc.h"
#define MAXOP 100 /* 操作数、运算符 字符串最大长度 */
/* 实现 逆波兰计算器 */
int main(void)
{
    int type;
    double op2;
    char s[MAXOP];
    while ((type = getop(s)) != EOF) {
        switch (type) {
        case NUMBER:
            push(atof(s));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '-':
            op2 = pop(); /* 减法除法，操作数有顺序。push(pop() - pop()) 不能保证哪个pop()先运行。 */
            push(pop() - op2);
            break;
        case '*':
            push(pop() * pop());
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2);
            else
                printf("error: divided by zero");
            break;
        case '\n':
            printf("\t%.8g\n", pop());
            break;
        default:
            printf("error: unknown command!");
            break;
        }
    }
    return 0;
}
