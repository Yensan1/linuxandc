#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define MAXWORD     100
struct tnode { char *word;  int count;  struct tnode *left, *right; };
int getword(char *, int);
struct tnode *addtree(struct tnode *, char *);
void traverse(struct tnode *p, int flag);
#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#else   /* C main() */

int main(void)
{
    char word[MAXWORD];
    struct tnode *root = NULL;
    while (getword(word, MAXWORD) != EOF)
        if (isalpha(word[0]))
            root = addtree(root, word);
    traverse(root, 1);
    traverse(root, 0);
    getchar();
    return 0;
}

#endif     /* C++ */

void traverse(struct tnode *p, int flag)
{   if (p != NULL) {
        traverse(p->left, flag);
        switch (flag) {
        case 0: free((void *)p->word); free((void *)p);     break;
        case 1: printf("\t%s\t%4d\n", p->word, p->count);   break;
        default: printf("Illegal flag");                    return;
        }
        traverse(p->right, flag);
    }
}

struct tnode *talloc(void);
char *str_in(char *);

struct tnode *addtree(struct tnode *p, char *w)
{
    int cond;
    if (p == NULL) {
        p = talloc();
        p->word = str_in(w);
        p->count = 1;
        p->left = p->right = NULL;
    } else if ((cond = strcmp(w, p->word)) == 0)
        p->count ++;
    else if (cond < 0)
        p->left  = addtree(p->left,  w);
    else
        p->right = addtree(p->right, w);
    return p;
}

#include <stdlib.h>
struct tnode *talloc(void)
{   return (struct tnode *) malloc(sizeof(struct tnode));
}

/* strdup() is C lib fnnc, rename to str_in*/
char *str_in(char *s)
{   char *p = (char *) malloc(strlen(s) + 1);
    if (p != NULL)
        strcpy(p, s);
    return p;
}

int getword(char *word, int lim)
{   int getch(void);
    void ungetch(int c);
    int c;
    char *w = word;
    while (isspace(c = getch()))    ;
    if (c != EOF)
        *w++ = c;
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }
    for ( ; --lim > 0; w++)
        if (!isalnum(*w = getch())) {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}

#define BUFSIZE 100
char buf[BUFSIZE]; /* LIFO stack */
int bufp = 0;
int getch(void)
{   return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{   (bufp >= BUFSIZE) ? printf("ungetch: too many characters\n")\
                      : (buf[bufp++] = c) ;
}


#ifdef __cplusplus
}

#include "gtest/gtest.h"

TEST(getword, throwEOF) /* test case */
{
    int getword(char *, int);
    char word[MAXWORD];
    int re;
    while ((re = getword(word, MAXWORD)) != EOF)
        printf("\t%s  %d\n",word, re);
    getchar();
}


int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif     /* C++ */
