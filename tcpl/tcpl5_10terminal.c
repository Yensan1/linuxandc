#include <stdio.h>
#include <string.h>

//官方代码，会定义程序名称
// #define PROGRAM_NAME "findstr"


#define MAXLINE 1000

int getline(char *line, int max);

int find_main_a(int argc, char **argv)
{
    char line[MAXLINE];
    int found;
    if (argc != 2)
        printf("Usage: findstr pattern");
    else
        while (getline(line, MAXLINE) > 0)
            if (strstr(line, argv[1]) != NULL) {
                printf("%s", line);
                found++;
            }
    return found;
}

int find_main_b(int argc, char **argv)
{
    char line[MAXLINE];
    long lineno = 0;
    int c, except, number, found;
    except = number = found = 0;
    while (--argc > 0 && (*++argv)[0] == '-' )
        while (c = *++argv[0])
            switch (c) {
            case 'x': except = 1;   break;
            case 'n': number = 1;   break;
            default:
                printf("findstr： illegal option %c\n", c);
                argc = 0;
                found = -1;
                break;
            }
    if (argc != 1)
        printf("Usage: findstr pattern");
    else
        while (getline(line, MAXLINE) > 0)
            lineno++;
            if ((strstr(line, *argv) != NULL) != except) {
                if (number)
                    printf("%ld:", lineno);
                printf("%s", line);
                found++;
            }
    return found;
}





int getline(char s[], int lim)
{
    int c, i;
    i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
        s[i++] = c;
    if (c == '\n')
        s[i++] = c;
    s[i] = '\0';
    return i;
}




