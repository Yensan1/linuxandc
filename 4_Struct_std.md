

[TOC]



## 枚举enumeration

枚举 enum 的作用是取别名，本质是将 int 字面量 转为单词。比const int 更易读、防止硬编码。比宏定义更好，因为枚举有类型（int类型）。不建议将枚举类型当作特殊的类型。

```c
#include <stdio.h>
enum COLOR {RED, YELLOW, GREEN}; // 声明。注意标点符号 {0, 1, 2}

void fun(enum COLOR c){ // 函数声明、使用 enum
    printf("%d", c);
}

int main(void) {
    enum COLOR c = RED; // 变量声明
    scanf("%d", &c);    // 变量使用 和 int 一样
    fun(c);
    return 0;
}
```

如果在枚举的末端放一个哨兵，还可以自动计数

```c
enum COLOR {RED, YELLOW, GREEN, COLOR_N}; // COLOR_N 是计数器和哨兵
int main(void) {
    enum COLOR c = -1;
    char *color_names[COLOR_N] = {"red", "yellow", "green"};
    char *color_name = NULL; // 不需要malloc，只需要指针指向进程代码段的某地址
    printf("请输入颜色:\t0:red\t1:yellow\t2:green\n"); scanf("%d",&c);
    if (c >=0 && c < COLOR_N){
        color_name = color_names[c];
        printf("你选的颜色是：%s\n", color_name);
    } else
        printf("输入错误");
    return 0;
}
```

枚举的值，默认从0到N。但是可以手动指定

```c
enum COLOR {RED=1, YELLOW, GREEN=5, COLOR_N}; // COLOR_N 是计数器和哨兵
int main(void) {
    // RED=1 YELLOW=2  GREEN=5  COLOR_N=6 这时候计数器和哨兵就没用了
    printf("RED=%d\tYELLOW=%d\tGREEN=%d\tCOLOR_N=%d\n", RED, YELLOW, GREEN, COLOR_N);
    return 0;
}
```

以前的一些编译器，枚举变量不可以赋int值，比如 `enum COLOR c = -1;`但后来大部分编译器都可以这么做。

## 结构体structure

最基本的、不可再分的数据类型称为基本类型Primitive type，例如 int、double；根据语法规则由基本类型组合而成的类型称为复合类型Compound type，比如字符串由char组成。

组合类型有时候可以当做一个整体来使用，有时候又可以分解为基本类型使用。这种两面性，为数据抽象Data Abstraction 奠定了基础。

#### 结构体的语法：

首先要定义一个结构体。“定义”，也就是用基本数据类型， 组合出一个新的类型。它只是描述了一个模板，并没有分配存储空间。

```c
int main(void) {
    struct date { // 结构标记 structure tag也就是 date 是可选的
        int year; // 结构的成员。可以缩写为 int year, month, day; 
        int month;
        int day;
    }; // 定义结构体类型。这是一个语句，而不是语句块，末尾一定要有 ;号
    // 缩写后，这样声明 struct date {int year, month, day; };
    struct date today; // 声明变量，这才分配存储空间。和 int i; 本质一样
    today.year=2022, today.month=7, today.day=31; // 初始化（赋值）
    // struct date today = {2022, 7, 31}; 变量声明和初始化一步到位
    printf("Today is %i-%i-%i\n", 
           today.year, today.month, today.day); // 结构成员访问
    return 0;
}
```

如果结构体只用一次，可以不取名（structure tag）。下面的例子，用缩写语法声明了一个结构体，省略了结构标识（没有给这个数据类型取名）。

```c
int main(void) {
    // 变量 someday 用初值表赋值
    struct {int year, month, day; } today, yesterday, someday = {1918, 7, 30};
    // 变量 today 没有在声明的时候赋值，只能只能用结构成员赋值。
    printf("Enter totay's date (yyyy mm dd):\t");
    scanf("%i %i %i",  // 运算符优先级&(today.year)
    &today.year, &today.month, &today.day);
    // today.year = 2022, today.month = today.day = 7;
    // yesterday = {2022, 7, 30}; 声明变量yesterday后，用初值表赋值，是错误的！
    printf("Today is %i-%i-%i\n", someday.year, someday.month, someday.day);
    return 0;
}
```

结构标记、结构成员、与结构无关的变量，三者可以重名，并不干扰。但会影响易读性

```c
int main(void) {
    struct x { //
        int x; //
        int y;
    };
    int x;     // 
}
```

就像变量，如果是函数内部声明，这个结构体只能在函数内部使用。大多数情况下，都是在外部声明的：

```c
#include <stdio.h>
struct Point{double x, y;};
struct Rectangle{ /* 结构体嵌套：对角线两个点，确定矩形 */
        struct Point p1;
        struct Point p2;
};
double area(struct Rectangle r){
    double area = (r.p1.x - r.p2.x) * (r.p1.y - r.p2.y);
    return area > 0 ? area :  -area;
}
int main(void) {
    struct Point p1 = {1.0, 2.0};//struct Point p1={.x=1, };这样也行。p1.y没指定就默认0
    struct Point p2 = {2+3.0, 4.0}; //不仅可以常量初始化，还可以用表达式、函数
    struct Rectangle rec = {p1, p2};
    // struct Rectangle rec = {{1.0, 2.0},{3.0, 4.0}};  一步到位
    printf("Area of Rectangle:%.2f\n", area(rec));
    return 0;
}
```

如果是外部变量、static变量，应该用常量成员初值表进行初始化。自动结构体变量则可以灵活地初始化。

#### 结构体的使用

- 可以算术运算的，`+ - * / %`，属于算术类型Arithmetic type，整型和浮点型。
- 可以表示0或非零，参与逻辑运算的，`&& || !`，属于标量类型Scalar type，包括算术类型和指针类型。

结构体不能算术运算、逻辑运算，但可以作为一个整体进行复制、赋值运算。既然可以进行赋值运算，就可以当做函数的参数和返回值（这二者的本质就是赋值）。可以用`&`取址。可以访问其成员。

需要注意的是，作为参数、返回值时，是传入值的，就像一个int——传入的是结构的克隆体，而不是指针；返回的时候，`struct new = parse(struct_old);`也只是一个结构体返回值，然后赋值操作。如果结构体很大，那么，时间和空间的消耗都很大，所以更好的方式，是指针传递。

```c
struct point {int x, y;};
struct rect { struct point pt1, pt2;};
/* 1. 传入成员，返回结构体。适用于结构体变量的动态初始化（非常量初始化） */
/* 结构体无法算术、逻辑运算，但内部的基本类型可以。用内部基本类型的运算，实现结构体运算  */
struct point makepoint(int x, int y){
    struct point tmp;
    tmp.x = x,  tmp.y = y;
    return tmp;
}

/* 2. 传入结构体，返回结构体 */
struct point middle_point(struct point pt1, struct point pt2){
    struct point tmp;
    tmp.x = (pt1.x + pt2.x) / 2;
    tmp.y = (pt1.y + pt2.y) / 2;
    return tmp;
}

/*  3. 传入结构体指针：点 是否在 长方形 内 */
int pp_in_rectp(struct point *p, struct rect *r){
    return (p->x > r->pt1.x && p->y > r->pt1.y 
         && p->x < r->pt2.x && p->y < r->pt2.y );
}

int main(void){
    struct rect screen;
    struct point middle;
    screen.pt1 = makepoint(0, 0);
    screen.pt2 = makepoint(XMAX, YMAX);
    struct pta = {1, 3};
    struct ptb = pta; /* 结构体可以相互赋值、初始化赋值 */
    struct middle = makepoint((screen.pt1.x + screen.pt2.x) / 2,
                       (screen.pt1.y + screen.pt2.Y) / 2,);
    pp_in_rectp(&a, &screen); /* 结构体指针接口调用 */
```



##### 结构体与指针

```c
struct point {int x, y;}; /* 创建了一个数据类型，相当于int，但没开辟内存空间 */
struct rect { struct point pt1, pt2;};
struct point *pp; /* 声明了一个 struct point 类型的指针 */
pp = makepoint(2, 3); /* (*pp) 就是一个 struct point 型变量 */
(*pp).x = 0; (*pp).y = 0; // (*pp).x 是结构体成员。括号()不可省略，因为运算符优先级
pp->x = 5; pp->y = 5; /* 结构 指针取成员 的简写方式 */
struct rect r; // 声明了一个 结构体rect 变量
struct *rp = &r; /* 取址，赋值给指针 */
r.pt1.x = 0;  /* 嵌套结构体。这 4 种 等效 写法 */
rp->pt1.x = 0; // 不能 rp->pt1->x 因为 (rp->pt1)不是指针，而是结构体
(r.pt1).x = 0; 
(rp->pt1).x = 0;

/*优先级：所有运算符中，结构运算 r.pt1 rp->pt1 函数调用 func() 数组下标 a[1] 最高*/
/* 以 前缀自增 运算符为例，说明其他运算符 操纵指针、操纵成员的区别 */
struct point p1;
pp = &p1;
++pp->x;   ++(pp->x);   ++(p1.x);   ++p1.x; /*这四种写法等价，都是操纵成员*/
/* 倘若要操纵 结构体 指针，则必须加括号 */
(++pp)->x;   /* 指针加 1，再操纵结构成员 x */
(pp++)->x;   /* 操纵结构成员 x ，然后 指针加 1，*/
```

例子

```c
#include <stdio.h>
struct point {int x, y;};
struct point * init_point(struct point *p){ // 传入指针，返回指针
    printf("请输入坐标：x y\t"); scanf("%i %i", &p->x, &p->y);
    return p;
}
void ptr_show(const struct point *p){  // 传入指针
    printf("point.x %d, point.y %d\n", p->x, p->y);
}
void struct_show(const struct point p){ // 传入结构体
    printf("point.x %d, point.y %d\n", p.x, p.y);
}
struct point create_point(int x, int y){ // 返回结构体
    return (struct point) {x, y};
}
int main(void){
    struct point p1; 
    // 操作指针接口
    init_point(&p1);
    ptr_show(&p1);
    ptr_show(init_point(&p1));
    // 操作 结构体接口
    struct_show(p1);
    // 复杂方式 *init_point(&p1) 得到一个结构体，再传递给struct_show()
    struct_show(*init_point(&p1));
    // 复杂方式 *init_point(&p1) 不仅可以作为右值，还可以作为左值！
    *init_point(&p1) = (struct point){1, 2};
    struct_show(p1);
    // 返回结构体的函数，作用域不同，调用者/被调用者操作的不是同一块内存
    struct point p2 = create_point(2, 3);
    struct_show(p2);
}
```

#### 结构体、数组、指针

```c
struct key {  
    char *word;
    int count;
};              /* 先声明一个结构 */
struct key keytab[32];  /* 然后用这种结构，定义一个数组 */
struct key keytab[] = {  /* 当然，可以定义 同时 初值表进行 初始化赋值 */
        "auto", 0,     "break", 0,      "case", 0,     "char", 0,
        "const", 0,    "continue", 0,   "default", 0,  "do", 0
}; /* 没有空值可以这么写。如果有空值，必须嵌套{}严格对应结构 {{"auto", 0,}}  */ 

/* 编译时运算符sizeof 可以算出任意对象的长度。返回size_t 类型的值，也就是unsigned int  */
#define NKEYS (sizeof keytab / sizeof keytab[0]) /* sizeof只能用于define，不能用于 #if  */
/* 一个结构的长度未必是各成员的总长，因为要求对齐可能出现空穴hole。所以不能瞎猜，要用sizeof
比如 struct a{char x, int i;};  a长度是8，char是1，int是4 */
struct a { char a;    int count;};
printf("\t %d %d %d\n",   sizeof(struct a), sizeof(int), sizeof(char) );
for (int i=0; i < NKEYS; i++){printf("%s", keytab[i].word);} // 遍历

struct key *p; /* 声明一个指向结构体的指针 */
struct key *binsearch(char *word, struct key *tab, int n); /* 声明一个函数。参数有结构体指针，返回结构体指针 */
```

例：统计输入中，输入文本，统计C 语言关键字出现了多少次。

```c
#include <stdio.h>
#include <ctype.h>
#include <string.h>
struct key { char *word;	int count; };
struct key keytab[] = {
        "auto", 0,     "break", 0,      "case", 0,     "char", 0,
        "const", 0,    "continue", 0,   "default", 0,  "do", 0,
        "double", 0,   "else", 0,       "enum", 0,     "extern", 0,
        "float", 0,    "for", 0,        "goto", 0,     "if", 0,
        "int", 0,      "long", 0,       "register", 0, "return", 0,
        "short", 0,    "signed", 0,     "sizeof", 0,   "static", 0,
        "struct", 0,   "switch", 0,     "typedef", 0,  "union", 0,
        "unsigned", 0, "void", 0,       "volatile", 0, "while", 0
};
#define NKEYS (sizeof keytab / sizeof keytab[0])
#define MAXWORD     100
int getword(char *, int);
int binsearch(char *, struct key *, int);
int main(void)
{
    int n;
    char word[MAXWORD];
    while (getword(word, MAXWORD) != EOF)
        if (isalpha(word[0]) && (n = binsearch(word, keytab, NKEYS)) >= 0)
            keytab[n].count++;
    for (n=0; n < NKEYS; n++)
        if (keytab[n].count > 0)
            printf("\t%s: %4d\n", keytab[n].word, keytab[n].count);
    return 0;
}
/* 在 tab[] 中有n个项，从中找 word字符串 */
int binsearch(char * word, struct key tab[], int n)
{
    int cond, low, high, mid;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if ((cond = strcmp(word, tab[mid].word)) < 0)
            high = mid - 1;
        else if (cond > 0)
            low  = mid + 1;
        else
            return mid;
    }
    return -1;
}
int getword(char *word, int lim)
{
    int getch(void);	void ungetch(int c);
    int c;
    char *w = word;
    while (isspace(c = getch()));
    if (c != EOF)
        *w++ = c;
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }
    for ( ; --lim > 0; w++)
        if (!isalnum(*w = getch())) {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}
#define BUFSIZE 100
char buf[BUFSIZE]; /* LIFO stack */
int bufp = 0;
int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}
void ungetch(int c)
{
    (bufp >= BUFSIZE) ? printf("ungetch: too many characters\n")\
                      : (buf[bufp++] = c) ;
}
```

- 倘若用结构体的指针语法来写这个程序，就改写 `binserach()`函数，让它接收结构体指针、返回指针，同时调用者`main()`也必须随着修改。

  ```c
  int main(void)
  {
      char word[MAXWORD];
      struct key *p = NULL;
      while (getword(word, MAXWORD) != EOF)
          if (isalpha(word[0]))
              if ((p = binsearch(word, keytab, NKEYS)) != NULL)
                  p->count++;
      for (p = keytab; p < keytab + NKEYS; p++)
          if (p->count > 0)
              printf("\t%s: %4d\n", p->word, p->count);
      return 0;
  }
  struct key *
  binsearch(char *word, struct key *tab, int n) /* 返回值很复杂，为了易读，折行突出 函数名 */
  {
      int cond;
      struct key *low, *high, *mid;
      low = &tab[0];  high = &tab[n]; /* a[-1] a[n] 都越界，但a[-1]绝对非法，而&a[n]指针算术不违法，x = a[n] 这样取值是非法的 */
      while (low < high) {
          mid = low + (high - low) / 2; /*下标运算 (low + high)/2 改为地址运算 */
          if ((cond = strcmp(word, mid->word)) < 0)
              high = mid - 1;
          else if (cond > 0)
              low  = mid + 1;
          else
              return mid;
      }
      return NULL;
  }
  ```

#### 自引用结构、二叉树

统计输入中，所有单词出现频率。如果每输入一个单词，都要查找、判断是否存在和排序插入、统计频率，这种解决方案会很困难。即使用了指针数组，线性查找仍然很耗时。使用二维解决方案，二叉树。二叉树：任何节点最多拥有两个子树，也可能只有一个子树或者一个也没有。

```c
struct tnode {   /* 结构：以此类型，定义 二叉树的节点 */
    char *word;  /* 指向单词的指针 */
    int count;   /* 单词出现的频率 */
    struct tnode *left;   /* 左右节点的指针。自引用：同种结构之间相互引用 */
    struct tnode *right;  /* 对象引用自身是非法的。但这个是引用同种类型、其他对象。 */
};
/* 另一类自引用：不同结构之间相互引用 */
struct mnode { int i;  struct rnode *rp; }; /* mnode 与 rnode 两类结构实现 自引用 */
struct rnode { char c; struct mnode *mp; }; 
```

所有的操作都要保证，任何节点，以自己为界，左树所有节点全是更小的单词，右树全是更大的单词（字典序）。

- 查找与插入：从根节点开始，比较新单词与此节点的单词。相等，那就是此节点；小于就去左树查找；大于就去右树查找。如果下面没有子树，说明全树没有这个单词，而且这个新单词就应该放在当前位置。这是一个递归过程。打印也类似，是递归的。

  ```c
  #include <stdio.h>
  #include <ctype.h>
  #include <string.h>
  #define MAXWORD     100
  struct tnode { char *word;  int count;  struct tnode *left, *right; };
  int getword(char *, int);  /* getword()  getch()  ungetch() 省略了，前面有 */
  struct tnode *addtree(struct tnode *, char *);
  void traverse(struct tnode *p, int flag);
  
  int main(void)
  {   char word[MAXWORD];
      struct tnode *root = NULL;
      while (getword(word, MAXWORD) != EOF)
          if (isalpha(word[0])) /*如果单词以字母开头，就建立节点，或者增加计数器*/
              root = addtree(root, word); /* 第一个单词建立根节点。后面的单词建立节点或计数 */
      traverse(root, 1);  /* 从树根开始，打印每个节点 */
      traverse(root, 0);  /* 从树根开始，清理每个节点的内存空间 */
      return 0;
  }
  /* 从p遍历整个树，由flag指定操作。 */
  void traverse(struct tnode *p, int flag)
  {   if (p != NULL) {
          traverse(p->left, flag);  /* 对左侧递归 */
          switch (flag) { /* 按 flag，对当前节点执行指定操作 */
          case 0: free((void *)p->word); free((void *)p);     break;
          case 1: printf("\t%s\t%4d\n", p->word, p->count);   break;
          default: printf("Illegal flag");                    return;
          }
          traverse(p->right, flag);  /* 对右侧递归 */
      }
  }
  /* 第一个单词直接建立根节点。后面的单词就往左右侧递归，最后要么增加计数、要么新增节点 */
  struct tnode *addtree(struct tnode *p, char *w)
  {   struct tnode *talloc(void);
      char *str_in(char *);
      int cond;
      if (p == NULL) {  /* 遇到空指针说明要建立新节点 */
          p = talloc();
          p->word = str_in(w);
          p->count = 1;
          p->left = p->right = NULL;
      } else if ((cond = strcmp(w, p->word)) == 0) /* 相等，就等价计数 */
          p->count ++;
      else if (cond < 0) /* 不等于，就要增加节点 */
          p->left  = addtree(p->left,  w); /* 如果新增了节点，就保存在父节点 */
      else
          p->right = addtree(p->right, w);
      return p;
  }
  #include <stdlib.h> /* malloc()能够对齐，返回一个void * 指针 */
  struct tnode *talloc(void)
  {   return (struct tnode *) malloc(sizeof(struct tnode));
  }
  /* strdup() is C lib fnnc, rename to str_in*/
  char *str_in(char *s)
  {   char *p = (char *) malloc(strlen(s) + 1);
      if (p != NULL)
          strcpy(p, s);
      return p;
  }
  ```

  



## union联合

联合可以在不同时刻保存不同类型和长度的对象的变量，编译器负责存储空间的长度和对齐。这样，一个变量（存储区域）可以保存管理多种数据类型中的任何一种数据，却不要为硬件费心。

联合的定义、使用，和结构非常像。先定模板，再用模板定义变量、数组、指针。

```c
/* union 使用示例 */
union hold { /* 定义一个union，取名为hold。没有开辟内存空间 */
    int ival; /* 初始化赋值，只能赋给 第一种变量，也就是 ival */
    float fval;
    char *sval;
}; 
union hold fit = {10}; /* 定义变量（开辟内存）。外部变量，定义时必须初始化赋值 */
int main(void)
{
    extern union hold fit; /* 怎么使用 外部union变量  */
    // size:8 i:4 f:4 char*:8  由此可见，尺寸和对齐适合所有成员
    printf("size:%lu\ti:%lu\tf:%lu\tchar*:%lu\n", 
        sizeof(fit), sizeof(fit.ival), sizeof(fit.fval), sizeof(fit.sval));
    fit.fval = 3.14;
    printf("%f\n", fit.fval); /* 提取的，是最近一次存入的值 */
    printf("%d\n", fit.ival); // 如果强行提取，数据是错误的、无意义的值
    return 0;
}

/* 创建union、定义变量、初始化一步到位。但最好别省略 tag，没法用 extern 声明 */
union hold {
    int ival;
    float fval;
    char *sval;
} fit = {88};

/* 访问成员和使用指针 */
fit.ival = 1; /* 访问成员 */
union hold *hp = &fit;
hp->ival = 1; /* 使用指针 */

/* union 和 struct 可以互相嵌套 */
typedef struct tab { /* union嵌入 struct，并且以此 typedef 一个新类型 */
    char *name;
    int flags;
    int utype;
    union {int ival; float fval; char *sval;} u;
} Table; 
Table systab[NSYM]; /* 以复合结构，声明一个数组 */
```

为什么union 的语法与 struct 那么像？因为 unoin 本身就是一种特殊的 struct。都可以作为一个整体进行赋值、赋值、取址、指针运算、访问成员。只是：

- union只有一段内存空间，尺寸和对齐要适合所有成员的类型。struct每个成员有一个单独空间。
- union 只能用第一个成员进行初始化（c99可以指定），并且只能存一个成员，取最近存入的成员。

## 位运算与bit-field 位段

## 位运算与位段

很多时候要进行二进制编程。为了节省空间，可能将数据保存在bit里。对接某些硬件，必须操纵bit。

一般来说，有两种方法：

使用define符号常量，或者enum枚举常量，然后进行二进制的位运算。

```c
#define KEYWORD     01
#define EXTERNAL    02
#define STATIC      04
/* 使用 enum 也一样 */
enum { KEYWORD = 01, EXTERNAL = 02, STATIC = 04 };

flags |= EXTERNAL | STATIC; /* bit操作：将flags的 EXTERNAL STATIC 位，设为 1 */
flags &= ~(EXTERNAL | STATIC); /*将flags的 EXTERNAL STATIC 位，设为 0 */
if ((flags & (EXTERNAL | STATIC)) == 0){...} /*如果 EXTERNAL STATIC 都为 0，布尔值为 1 */
```

使用struct bit-field：直接定义访问一个字word中的位字段bit-filed的能力，而不需要按位运算。也就是说，int之类普通数据，用相应尺寸的空间，整体地存储、操纵数据。而bit-filed，是用一种尺寸的空间（一个字），以bit为单位存储、操纵数据。这种尺寸，一般是 unsigned int，而不是 WORD（Windows里，typedef unsigned short WORD ）。bit-field没有取址、指针运算。

```c
struct { /* 一个bit-field，是很多bit的集合，尺寸一般是unsigned int。 */
    unsigned int is_keyword : 1; /* 一个字段  :  宽度 1bit */
    unsigned int is_extern  : 1; /* 为了方便移植，要显式声明 signed/unsigned */
    unsigned int is_static  : 1; 
} flags; /* 定义一个变量。它占据 32bit的空间，只包含 1+1+1 = 3bit 的数据 */

flags.is_extern = flags.is_static = 1; /* bit操作：将EXTERNAL STATIC 位，设为 1 */
flags.is_extern = flags.is_static = 0; /* 将 EXTERNAL STATIC 位，设为 0 */
if (flags.is_extern == 0 && flags.is_static== 0){...} /*如果 EXTERNAL STATIC 都为 0，布尔值为 1 */

/* 其他注意事项 */
struct {
    unsigned int bitf : 34; /* 错误，unsigned int 4byte 32bit，这超过尺寸了 */
} flags;

struct { /* 强制使用下个字段，所以这个变量占用 2个字宽。2 unsigned int 64bit */
    unsigned int field1 : 1;
    unsigned int        : 2; /* 无名字段。用来占位、填充，制造空隙 */
    unsigned int field2 : 1;
    unsigned int        : 0; /* 宽度为0。强制往下一个字边界对齐 */
    unsigned int field3 : 1; /* 前面4个都在第一个字，它被强制存储在下一个字里 */
} flags; 
```



### 位运算

位运算符（要将 & | 和逻辑运算分开）

```c
&与   |或   ~取反  ^异或   <<左移   >>右移
```

按位与运算：当两个bit都为1，结果为1；其余都为0

```c
按位与运算示例：
   0101 1010  // 0x5A
&  1000 1100  // 0x8C
   0000 1000  // 0x08 只有两bit都是1，结果才是1
应用1. 屏蔽某些bit，比如想要抹掉 char 最后bit。  x & 0xFE
   1010 0101  // 要抹掉这个char点最后bit
&  1111 1110  // 0xFE 只需要在屏蔽位段设为0，其余设为1
   1010 0100  // 抹掉了最后一个bit
应用2. 作为mask，取某些bit    x & 0xFF
   ---- ---- ---- 0xAA // 一个int，4byte，32bit
&  0x00 0x00 0x00 0xFF // 将最后1byte(8bit)设为1，也就是0xFF
   0x00 0x00 0x00 0xAA // 除了最后1byte，其余全置0，也就是取到了最后byte
```

按位或运算：两个bit，当 a 或者 b 是1，结果是1

```c
按位与运算示例：
   1010 1010  // 0xAA
&  1101 0100  // 0xD4
   1111 1110  // 0xFE 只要出现一个1，结果就是1
应用1. 将某些位段 置1。比如将 char 的后4bit置1:  x | 0x0F
   1010 1010 // 0xAA 需要将后4bit 置为1
|  0000 1111 // 0x0F 用这个数  
   1010 1111 // 0XAF 取得结果
应用2. 拼接将两个变量的不同位段  x | y
   0x00FF | 0xFF00 = 0xFFFF
```

按位取反运算：一个bit，当 a 是0，结果是1；当a是1，结果是0

```c
按位与运算示例：
~ 1010 1010 // 0xAA
  0101 0101 // 0x55 每一bit进行0/1翻转，得到结果
// 求补码、按位取反，运算是不一样的
  1 0000 0000 // 补码超高位1，其余的为0
-   1010 1010 // 0xAA
    0101 0110 // 0x56 补码结果
// 代码得到 补码 和 取反
unsigned char c = 0xAA;
printf("原数 c: %hhX\n", c);        // 0xAA
printf("取反~c: %hhx\n", (char)~c); // 0x55
printf("补码-c: %hhx\n", (char)-c); // 0x56
```

按位异或运算：两个bit，当 a==b，`a^b=0`，当a≠b，`a^b=1`。由此，有2个推论：

- 如果两个变量`x==y`，那么，`x^y = 0`
- 对一个变量`x`用同一个值`y`异或两次，那么没有变化`x^y^y == x`

左移运算：表达式`i<<j`是`i`中所有的bit向左移动 `j`个bit，右边填0。由此可知，`x <<= n`这个表达式，等价于 `x *= 2^n`，2的N次方。当然，如果移动位数超过sizeof产生溢出，这个等式就不再成立了。

```c
    1010 0101  // 0xA5
<<  2
 10 1001 0100  // 超额的抹掉，得到 1001 0100也就是 0x94
// 编程
unsigned char c = 0xA5;
printf("原数 c: %hhX\n", c);
printf("c<<2: %hhx\n", c<<2); // 移位后超限，编译器告警
printf("原数 c: %d\n", c);   // 165
printf("c<<2: %d\n", c<<2); // 660，也就是 165*(2**2)
```



右移运算：表达式`i>>j`是`i`中所有的bit向右移动 `j`个bit，对于`unsigned`类型，左边填0；对于`signed`类型，左边保持原来的最高位（保持正负号）。类似地，`x >>= n`这个表达式，等价于 `x /= 2^n`，2的N次方。

```c
char a = 0x80;
unsigned char b = 0x80;
printf("a: %d, 0x%hhX\n", a, a); // -128  0x80
printf("a>>1: %d, 0x%hhX\n", a>>1, a>>1); // -64 0xC0
printf("b: %u, 0x%hhX\n", b, b); // 128  0x80
printf("b>>1: %d, 0x%hhX\n", b>>1, b>>1); // 64 0x40
// signed char a 的移位
   1000 0000 >> 1  = 1100 0000 // 往右移，然后最高位再填原值。0xC0
// unsigned char b 的移位
   1000 0000 >> 1  = 0100 0000 // 0x40
// 当然了，它们右移 1 bit 都会超限然后成为 0x00
printf("a<<1: 0x%hhX\n", a<<1);
printf("b<<1: 0x%hhX\n", b<<1);
```

左移不需要关注 singned/unsigned 和最高符号位问题，右移需要。

另外，移位的**位数不准使用负数**，比如`x << -2`或`x >> -1`，这是没有定义的行为。

##### 位运算例子

输出一个整数的二进制编码

```c
#include <stdio.h>
int main(void){
    char number;
    printf("input a int\n");scanf("%d", &number);
    unsigned char mask = 1u<<(8*sizeof(unsigned char) - 1);
    for (; mask; mask >>= 1)
        printf("%d", number & mask ? 1 : 0);
    printf("\n");
    return 0;
}
```

操作硬件。比如MCU 的 专用寄存器/特殊功能寄存器(*SFR*,Special Function Register)，比如操作第3、第4位。很容易地，用`|`可以轻易将目标bit置1，用`var &= ~flag;`可以轻易置0

```c
const unsigned int SBS = 1u << 2; // 第 3 bit。000...0100
const unsigned int PE = 1u << 3;  // 第 4 bit。000...1000
LCR |= SBS | PE;   // SBS|PE= 000...1100，LCR|(SBS|PE)就令2、3bit为1
LCR &= ~SBS;       // 将第3bit清0
LCR & = ~(SBS|PE); // 将第3、4 bit 清0
```

### 位段

用位运算操作 bit 仍然比较繁琐，因此，C语言提供了专门操作bit的结构体：位段。

```c
#include <stdio.h>
struct U0{ // 共 32 bit，也就是4byte 一个 int 大小
    unsigned int leading : 3; // : N 表示占据多少个bit
    unsigned int flag_A : 1;
    unsigned int flag_B : 1;
    int trailing : 27;
};
void showBin(unsigned int number){
    unsigned int mask = 1u<<(8*sizeof(int) - 1);
    for (; mask; mask >>= 1)
        printf("%d", number & mask ? 1 : 0);
    printf("\n");
}
int main(void){
    struct U0 u;
    u.leading = 2;
    u.flag_A = 0;
    u.flag_B = 1;
    u.trailing = 0; // 如果没有赋值，就会是随机值
    printf("sizeof(u)=%lu\n", sizeof(u)); // sizeof(u)=4
    showBin(*(int *)&u); // 强制转换类型
    return 0;
}
```

如果超过 1 个int 的长度，那么它就会用 2 个int 来表达

```c
struct U1{ // 共 37 bit
    unsigned int leading : 3; 
    unsigned int flag_A : 1;
    unsigned int flag_B : 1;
    int trailing : 32;
};
int main(void){
    printf("sizeof(U1)=%lu\n", sizeof(struct U1)); // 8byte
    return 0;
}
```

位段的特点：

- 可以直接使用成员名访问，比位运算更简单
- 编译器会安排bit 的排列，不具有可移植性（就像大小端）
- 当所需的bit总数超过一个 int，会采用多个int的尺寸













## 复杂声明 与 typedef

```c
int * func(); /* 声明了一个函数func，它返回一个 int 指针 */
int (*pfunc)(); /* 声明了一个函数指针 pfunc，函数 (*pfunc)()返回值 int */
```

如何解读、创建复杂的声明？多做 2 类练习：

- 将 C 语言声明转为文字描述（解读）
- 将 文字描述 转为 C语言声明（写作）

如何创建比较复杂的声明？最好是用 typedef 一步步合成。

##### 类型定义 typedef

typedef 没有创建新的数据类型，只是给已有类型取了一个新名字。它也没有创建新的语法和语义，就相当于`#define`那样替换文本而已。但define是预处理器解释，而typedef 是由编译器解释的，所以功能更强大。

```c
/* 1. define 能用于值 和 类型，而 typedef 只能用于 类型 */
#define BUFSIZE 512 // 标量值，取别名
#define Length int
/* 2. define 与 typedef 重合的地方：简单类型。主要是为了在不同平台方便移植  */
typedef int Length; /* 像定义一个变量那样建立新的数据类型名。一般首字母大写 */
Length len, maxlen; /* 用这个类型声明变量 */
Length *lengths[];  /* 声明数组 */
/* 3. typedef 能将 复杂类型 灵活定义为简单类型，define不能  */
typedef char *String; /* 定义 char * 为 String */
String name, addr; /* 相当于 char *name; char *addr;—— name addr 都是指针 */
#define String  char * /* 如果用 define，只是简单替换 */
String name, addr; /* 替换后char * name, addr—— name是指针，addr是char */
/* 4. 简化复杂定义 */
#include <stdio.h>
typedef char* Strings[3]; // Strings 是字符串指针的数组（存储3指针）的类型
Strings s = {"1 hello", "2 hello", "3 hello", };
```

结构体这种复杂类型，更为常用。但 Linux 内核，为了代码的透明性，拒绝使用typedef 定义 结构体和指针https://www.kernel.org/doc/Documentation/process/coding-style.rst。

```c
/* 普通方式 */
struct date{ int year, month, day;};
typedef struct date Date;
/* 合写 */
typedef struct date {int year,month,day;} Date;
/* 合写，省略struct Tag 'date' */
typedef struct {int year,month,day;} Date;
/* 用 Date 定义指针 */
typedef Date *Ptr_Date;

/* 一次到位 */
typedef struct {int year,month,day;} Date, *Ptr_Date;
#include <stdio.h>
int main(void){
    Date d = {2022, 7, 8};
    Ptr_Date p = &d;
    printf("%d-%.2d-%.2d\n", p->year, p->month, p->day);
    return 0;
}
```

使用 typedef 后，表达更为简洁，也可能更易读

```c
typedef char *String; /* char * 为 String */
String p, lineptr[MAXLINES], alloc(int);  /*用这种类型，声明 变量、数组、返回值的函数 */
int strcmp(String, String);
p = (String) malloc(sizeof(*String) * 100);
/* 一个更复杂的例子 */
typedef struct tnode *Treeptr; /*将struct tnode * 定义为 Treeptr 类型*/
typedef struct tnode {
    char *word;
    int count;
    Treeptr left, right;
} Treenode;  /* 将结构定义为 Treenode 类型*/

struct tnode *talloc(void)
{
    return (struct tnode *) malloc(sizeof(struct tnode));
}
Treeptr talloc(void) /* 改写后，更简洁、易读 */ 
{
    return (Treeptr) malloc(sizeof(Treenode));
}

/* 另一个更复杂的例子：  PFI是一个函数指针。这个函数返回int，有两个char * 参数 */
typedef int (*PFI)(char *, char *);
PFI strcmp, numcmp;
typedef int arr5[5];
typedef arr5 * p_arr5;
typedef p_arr5 arrp10[10];
arr5  togs; /*相当于 int togs[5]，togs是含5个int元素的数组 */
p_arr5 p1; /*相当于 int (*p1)[5]，p1是指针，指向长度为5的int数组  */
arrp10 ap; /*相当于int (*ap[10])[5]，ap是数组，内含10个指针，每个指针指向长度为5的int数组*/
```

可以提高移植性

```c
#define __SIZE_TYPE__ long unsigned int
typedef __SIZE_TYPE__ 	size_t; /* 移植到不同的宿主机，选择不同的short/int/long类型，只需要改变 typedef或define */
```

##### 解读声明

如何解读复杂的声明？千万不要按照日常文字阅读顺序从左至右解读，而应该从核心标识符一步步发散。

1. 找到核心，也就是作为标识符的那个单词（注意排除 typedef 类型名的干扰）

2. 按照运算符优先级，一步步从这个核心发散解析：声明语法只会出现3类运算符，数组`[]`和函数`()`，指针`*`以及改变运算符优先级的结合符`()`。数组函数符号`()[]`优先级比指针`*`高，结合符`()`可以改变优先级；函数数组`()[]`都是从左到右与标识符结合。

   ```c
   int goods[12][50]; /* []与标识符从左往右结合。所以是12个元素的数组，元素是50个int */
   int *oof[3][4]; /* []优先级比*高。一个3元素的数组，oof每个元素是4元素的数组，数组内是int指针 */
   int (*uuf)[3][4]; /* ()使uuf先与*结合，这是一个指针，指向 3x4 int 数组 */
   char *fump(int);    /* 返回char 指针的函数 */
   char (*fump)(int);  /* 指向函数的指针，返回char类型 */
   char (*fump[3])(int);  /* 由3个指针构成的数组，每个指针指向 char func(int)这种函数 */
   Header *morecore(unsigned);  /* Header是typedef定义的类型，标识符是 morecore。() 比 * 优先级高，morecore是一个函数。返回的是 Header 类型的指针。参数是 unsigned 类型 */
   ```

## 预处理指令

用`gcc a.c --save-temps`命令，可以得到一系列的过程文件。

- `a.c`是源码
- `a.i`，预处理后的临时文件
- `a.s`，汇编语言的代码文件
- `a.o`，汇编后，得到目标平台的二进制文件。
- `a.out`，链接其他库，得到目标平台的可执行文件。

所有`#`开头的预处理指令。

```c
#include <stdio.h> // 预处理指令 preprocessor directive
#include "abc.h"
#include "a/CMAttachment.h"

#define PI 3.14159 
```

gcc的预处理命令是`gcc -E hello.c`。

##### 宏定义

定义一个字面量，防止硬编码。仅仅是文本替换。它不是一个C语句（用于各种编程语言），**不需要分号**。

如果一个宏定义语句超过 1 行，行末用`\`换行。

如果一个宏定义，含有其他的宏（嵌套定义的宏），也会被替换。

```c
#define PI 3.14159  // 注意没有分号 ;
#define 2PI (2*PI)  // 宏 中又有 宏
#define XXX 2*PI 3  // 虽然带空格，也算。但这样容易出错
int main(void){
    printf("%d\n", XXX * 3); // 编译会出错
}
#define show printf("hello"); \
		     printf("world");


#ifndef _LINKEDLIST_  // 条件编译
#define _LINKEDLIST_  // 没有值的宏，经常用于条件编译
typedef struct _linkedlist_node {
    int value;
    struct _linkedlist_node *next;
} LinkedList_Node;
#endif
```

编译器预定义的宏

```c
__LINE__ // 源代码行号。__FILE__ 是文件名
__TIME__ // 编译的时间。__DATE__ 日期
__STDC__ // 
```

带参数的宏——伪函数。但是这种宏特别容易出错；关键点在于：`(x)`和整个宏要加括号`(exep)`

```c
#include <stdio.h>
#define cube(x) ((x)*(x)*(x)) // 括号包裹参数，括号包裹整个表达式
int main(void){
    int i = 1;
    printf("%d\n", cube(i+1)); // 就像一个函数那样使用
    return 0;
}
// 一定要将参数包裹起来 (x)  也要将整个表达式包裹起来 (exep) 
#define DOUBLE(x) ((x) * 2) // 正确的定义：翻倍运算，乘以 2
#define DOUBLE(x) (x * 2) // 错误
DOUBLE(i+1); // 替换后 i+1*2 因为运算符优先级，最后出现问题
#define DOUBLE(x) (x)*2   // 错误
18/DOUBLE(3); //原意是 18/(3*2) 实际替换后是 18/(3)*2 

// 常见的带参数的宏
#define min(a,b) ( (a) > (b) ? (b) : (a) )
#define max(a,b) ( (a) > (b) ? (a) : (b) )
```

宏定义，会导致源代码增大，从而编译出来的程序更大，但是在执行的过程中减少了很多函数调用的开销，所以性能更好。外国的程序更喜欢用宏定义。部分宏，会被 inline 函数取代。

还有两个`##`的宏。



## 头文件与模块分割

项目组织

编译：编译单个源代码。成为`file.o`文件。

构建：链接成为一个完整的可执行文件。

```c
#include <stdio.h> // 只是把这个头文件复制到这里。
#include "dir/a.h" // 引入自己写的头文件。<stdio.h>是库文件
```

编译器知道自己的标准库位置。如果要特意指定，也可以通过命令行指定。

语句`#include <stdio.h>`只是在预处理的时候，将头文件复制进来，这样，编译器就知道这个函数的原型，保证编译的时候，传参是正确的类型。`printf()`函数的代码、在`.lib`（Win）或`.a`（Unix）文件中。

一个`max.h`文件，是定义和使用函数的双方都应该遵守的约定。编译命令：`$gcc main.c max.c`

```c
/* max.h 文件 */
int max(int a, int b);

/* main.c 文件 */
#include <stdio.h>
#include "max.h"
int main(void){
    int a = 5, b = 6;
    printf("%d\n", max(a, b));
    return 0;
}

/* max.c 文件 */
#include "max.h"
static int G_IN_C = 0; // 这个全局变量，只能在 max.c 使用
int max(int a, int b){
    return a > b ? a : b;
}
static void fun(){;} // 这个函数，只能在 mac.c 使用
```

如果要在不同的`file.c`使用全局变量，则需要声明。

```c
/* max.h 文件 */
extern int GALL; // 头文件声明

/* main.c 文件 */
int GALL = 12; // 文件内部定义

/* max.c 文件 */
printf("%d", GALL); // 其他文件使用
```

另外，因为`#include "file.c"`可能会出现在不同的文件里，这些文件再相互引用，那么各种声明就会重复——最后导致编译出错。所以，头文件经常见到这种条件编译语句，保证一个编译单元中只`include`一次：

```c
/* max.h */
#ifndef _MAX_H_
#define _MAX_H_
..... // 正式的头文件声明语句

#endif
```



## 输入输出与文件

#### 标准输入输出

标准库提供的输入/输出模型非常简单：无论文本从何处输入、何处输出，输入输出都按照文本流的方式处理。文本流是由多行字符构成的字符序列（也就是字符数组char [ ]，以`\0` 结束），每行字符是由0或多个字符构成（文本行以`\n` 结束）。标准库处理了底层，使得所有的输入输出都遵循这个模型，比如Unix/Linux 换行是 LF，Win是CRLF，而C语言都是`\n`

##### 例：文件（文本流）复制

「文件」指的是 Unix 里面的文本流。Unix文本流可以来自输入、文件、管道。

```c
#include <stdio.h>
int main(void){
    int c; /* 因为 EOF 用 char 会溢出，所以只能用 int */
    c = getchar(); /* getchar()读取一个字符 */
    while (c != EOF) { /*如果不是 EOF/end of file就继续。EOF的定义在 stdio.h */
        putchar(c); /* putchar()输出一个字符 */
        c = getchar(); /* 读取下一个字符 */
    } /* Win 用 Ctrl Z 发送 EOF，Unix 用 Ctrl D*/
    printf("EOF is: %d", c); /* EOF == -1 */
    return 0;
}
```

可以写的更紧凑

```c
c = getchar(); while (c != EOF) {...} /* 将这两句合并。python不可这样合并 */
while ((c = getchar()) != EOF) {...} /* 可以合并为这样 */
c = getchar() != EOF /*不可以这样。根据运算符优先级，是 c = (getchar()!=EOF) */
```

##### 例：统计字符数

```c
#include <stdio.h>
int main(void){
    long nc = 0;
    while (getchar() != EOF) /* 如果不是 EOF ，就统计 */
        ++nc;
    /* for (nc=0; getchar() != EOF; ++nc) // 改写为for循环，里面是 空语句
     *     ;
     */
    printf("%ld", nc);
    return 0;
}
```

##### 例：行计数

```c
int i, j, k;
i = j = k = 0; /*赋值顺序：从右到左 k = 0; j = k; i = j;*/
```

##### printf() 函数的格式化

转换说明Conversion Specification

占位符 Placeholder

```c
int fahr;
for (fahr = 0; fahr <= 300; fahr = fahr + 20) {
  printf("%d\t%d\n", fahr, (5 * (fahr-32) / 9));// 制表符，左对齐
}
0       -17
20      -6
40      4
60      15
printf("%3d%7d\n", fahr, (5 * (fahr-32) / 9));// 指定宽度，右对齐
  0     -17
 20      -6
 40       4
 60      15
int fahr;
for (fahr = 0; fahr <= 300; fahr = fahr + 20) {
  printf("%3d\t%.1f\n", fahr, (5.0 * (fahr-32) / 9));// %.0d 不指定宽度，但是指定小数点后多少位
}
printf("%.0d%7d\n", fahr, celsius); // %.0d 不指定宽度，但是指定小数点后多少位
0  -17.8
20   -6.7
40    4.4
60   15.6
%0 表示八进制数，%x表示十六进制，%s表示字符串，%%表示百分号自身
```

更详细的语法：

```c
printf("%[flag][width][.perc][hIL]type", var);
```

flag：`-`表示左对齐。`+`表示明确打印正负号（负数本来就有负号，正数强制加上正号）。`\space`留下一个空格表示正数留空。0表示0填充。

width/.perc：`%9.2f`这个浮点数，整个输出9个字宽，小数点精度2位。这两个参数，可以变成这样

```c
printf("%9.2f", 3.3333);
printf("9.*", 2, 3.3333); // 2 表示小数点精度 2 位
printf("*.2", 9, 3.3333); // 9 表示整个字宽 9 位
```

hIL：类型修饰符。`hh`char，`h`short，`l`long，`ll`long long，`L`long double

```c
printf("%hhd\n", 12345); // 解析为整数，但是按照一个char打印
```

type：`i/d`int，`u`unsigned int，`o`八进制，`x/X`十六进制，`f/F`float，`e/E`指数，`g/G`float，`a/A`十六进制浮点数，`c`char，`s`字符串，`p`指针，`n`读入或者写出的字数。

```c
int num;
printf("%d++%n\n", 12345, &num);
printf("%d\n", num); // 7。因为从字符串开始，到%n共输出7个字
```

##### 标准输入scanf

```c
scanf("%[flag]type", &var);
```

flag：`*`跳过。`9`读取的最大字符数。`hh`char，`h`short，`l`long，`ll`long long，`L`long double

```c
int num;
scanf("%*d%d", &num); //输入 123 345 它只读到了 345，用%*跳过了123
printf("%d\n", num); //
```

type：`d`十进制整数，`i`可以是十进制八进制十六进制的整数，`u`unsigned int，`o`八进制，`x`十六进制，`a/e/f/g`float，`c`char，`s`单词（不读入空格），`p`指针，`n`读入或者写出的字数。



另外，printf/scanf 是有返回值的，返回的是读入、输出的字符数。在长期运行的严格程序，调用printf/scanf 后应该判断返回值，从而了解运行中的输入输出是否存在问题。

```c
int num, a, b;
a = scanf("%i", &num);
b = printf("%d\n", num);
printf("%d\t%d\n", a, b); // 看看返回值
```

#### 文件

在Linux shell 和 Windows cmd，都能重定向输入输出。

```shell
$ echo < In.txt > Out.txt # 
```

##### 文本文件

```c
int main(void){
    FILE * fp = fopen("a.html", "r");
    if (!fp) {// 文件不存在
        printf("无法打开文件\n");
        return 0;
    }
    char line[1024]; 
    fscanf(fp, "%s", line); // 和 scanf 很像
    printf("%s\n", line);
    fclose(fp); // 使用完毕一定要关闭文件
    return 0;
}
```

文本文件打开的模式有：

- `r`，只读。`r+`，读写，offset从0开始，所以写入是从文件头部开始写入的。
- `w`，只写。`w+`，读写。两个都是，如果不存在就新建，如果存在就清空。
- `a`，追加。如果不存在就新建，如果存在，就从文件尾部开始写。
- `wx`或`ax`，只新建，如果存在这个文件则打开不成功。

上面都是文本文件的模式，像 Linux 的more  tail  cat  vi 都是以这种方式读取。

但很多文件并不是文本文件，而是二进制文件。二进制文件需要特殊程序进行读写，比如 jpeg mp3 mp4 之类的多媒体文件。文本文件是特殊的二进制文件，输入输出需要格式化，而这格式化的过程非常统一。

Unix 喜欢用文本文件存储程序的数据和配置，因为交互式的 shell 的缘故，这样有利于人类阅读，Linux的文本工具也很丰富。Windows 则喜欢用二进制文件存储程序的数据和配置（比如注册表），这样效率更高，而Windows也有丰富的界面展示和操作这些数据。

一个程序用到的文件：

- 配置项：Unix用文本，Windows用注册表
- 数据：数据量大的时候，使用数据库，比如SQLite、Mysql
- 多媒体文件：只能是二进制的。

一般来说，程序都是使用第三方库读写文件，很少直接操作二进制文件。

##### 二进制文件

二进制读写函数 fread/fwrite 文件声明非常相似。

返回值：读写的字节数
参数：ptr内存指针，size读写元素的大小，nitems读写几个元素，stream文件指针

```c
size_t fread(void * restrict ptr, size_t size, size_t nitems, FILE * restrict stream); 
size_t fwrite(void * restrict ptr, size_t size, size_t nitems, FILE * restrict stream);
```

二进制文件读写示例

```c
#include <stdio.h>
const int STR_LEN = 20;
typedef struct {char name[STR_LEN]; int gender, age;} Student;
int main(void){
    int number = 0;
    printf("输入学生数量\n"); scanf("%d", &number);
    Student students[number];
    getlist(students, number);
    if (save(students, number)) printf("保存成功\n");
    else printf("保存失败\n");
    return 0;

}
void getlist(Student s[], int n){
    char format[STR_LEN];
    sprintf(format, "%%%ds", STR_LEN-1); // "%19s"
    int i = 0;
    for (; i < n; i++){
        printf("第%d个学生:\n", i+1);
        printf("\t姓名:");
        scanf(format, s[i].name);
        printf("\t性别(0男 1女 2其他):");
        scanf("%d", s[i].gender);
        printf("\t年龄:");
        scanf("%d", s[i].name);
    }
}
int save(Student s[], int n){
    int result = -1;
    FILE * fp = fopen("student.data", "w");
    if (fp){
        result = fwrite(s, sizeof(Student), n, fp);
        fclose(fp); // 使用完毕一定要关闭文件
    }
    return result == n;
}
```

读取

```c
#include <stdio.h>
const int STR_LEN = 20;
typedef struct {char name[STR_LEN]; int gender, age;} Student;

int main(void){
    FILE * fp = fopen("student.data", "r");
    if (fp){
        fseek(fp, 0L, SEEK_END); // 将偏移量移到文件尾。ftell()得到文件位置。两者得到文件大小
        int number = ftell(fp) / sizeof(Student);
        int index = 0;
        printf("有%d个学生数据，你要看第几个:", number);
        scanf("%d", &index);
        read(fp, index-1);
        fclose(fp); // 使用完毕一定要关闭文件
    return 0;
}
void read(FILE *fp, int index){
    fseek(fp, index*sizeof(Student), SEEK_SET); //将偏移量重置，就能读出第几个学生
    Student s;
    if (fread(&s, sizeof(Student), 1, fp) == 1){
        printf("第%d位学生", index+1);
        printf("\t姓名:%s\t性别:", s.name);
        switch (s.gender){
            case 0: printf("男"); break;
            case 1: printf("女"); break;
            case 2: printf("其他"); break;
        }
        printf("\t年龄:%d\n", s.age);
    }
}
```

如上所示，它就是把内存的bit直接写入文件中。因此，二进制数据，很多不具有可移植性，比如，int 32 的机器写入的数据文件，在 int 64  的机器无法读出。解决方案是使用 typedef 定义固定长度的类型。



























# settings  备份

```json
    // java 设置
    // 操作系统：系统变量  新建 JAVA_HOME       C:/Program Files/Java/jdk1.8.0_131
    // 操作系统：系统变量，新增一条 PATH        ;C:/Program Files/Java/jdk1.8.0_131/bin;
    // 操作系统：系统变量  新建 CLASSPATH     .;C:/Program Files/Java/jdk1.8.0_131/lib
    // 安装插件： redhat.java（仅需设置Javahome）    donjayamanne.javadebugger
    //"java.home": "C:/Program Files/Java/jdk1.8.0_131",

    // python 设置
    "window.zoomLevel": 0,
    //整个mac 的venv：/usr/local/bin/venv2/bin/python
    // "python.pythonPath": "${workspaceRoot}/.venv2/Scripts/python.exe", // "/Users/aChing/Documents/LinuxAndC/.venv2/bin/python",
    "python.venvPath": "${workspaceRoot}/.venv2",
    "python.envFile": "${workspaceRoot}/.venv2/Scripts/python.exe"//"${workspaceRoot}/.venv2/bin/python",
```





# 