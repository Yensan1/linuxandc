C_Primer_Plus

编译过程：

- 预处理。
- 编译器处理成目标代码（机器语言指令，二进制），比如`file.o`或`file.obj`
- 链接器将 目标代码、库代码、启动代码 合并为一个可执行文件。库一般是预编译的，无需编译器再次处理，直接链接就好了。

Unix 的 CC 是最早的编译器。

而后，有GCC、LLVM/Clang。

Windows 里面有 Cygwin、MinGW、Borland、VS C++ 。

