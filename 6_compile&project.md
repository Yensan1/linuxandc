# 编译工具与工程结构

## 编译器

GCC/GNU Compiler Collection， 是 Linux 下的编译工具集，包含 gcc、g++ 等编译器，还有例如 ar、nm 等工具。不仅能编译 C/C++ 语言，还能编译 Objective-C、Pascal、Fortran、Java、Ada 等。可以编译跨平台的目标。








## make

##### 基本语法

```makefile
target:dependency # 依赖项可以省略
    command # 前面是tab键，不能用空格。
```

hello world 

```makefile
# ./Makefile
a:               # make 命令默认执行第一个目标
	@echo "hello"  
b:
	@echo "world"
```

执行`$ make`就会输出 hello，`$make b`就会输出 world。如果写成这样：

```makefile
b:
	@echo "world"
a:
	@echo "hello"
```

执行`$ make`就会输出 world

加上依赖项：

```makefile
tar : a b
	@echo "!"
a :
	@echo "hello"
b :
	@echo "world"
```

执行`$ make`就会依次执行 a、b、tar。输出`hello\nworld\n!\n`

一个单文件的



##### 变量

用户自定义变量、预定义变量、自动变量、环境变量







## cmake