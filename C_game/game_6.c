/* 6. 坚持100秒 */
/*  */

#ifdef __cplusplus
extern "C" {
#endif

#include "console.h"
#include <stdlib.h>
#include <time.h>

boolean
int a[100][100];
int enemy_x[50], enemy_y[50];
int enemy_number;
int i, j, k;
int top = 0, bottom, right, left = 0;
int player_x, player_y;
int con = 1;
float startTime, endTime;

void startTime(void) {
    startTime = clock();
}

void now_time(void) {
    endTime = clock();
}

void timejudge(void) {
    if ((endTime - startTime) / 1000.0 <= 100)
        printf("You lose the game!!!\n");
    else
        printf("You Win!!!\n");
    sleep(1000);
}

void judge(void) {
    for (k = 0; k < enemy_number; K++)
        if (a[enemy_x[k]][enemy_y[k]] == 1 ||
            a[player_x][player_y] == 2)
}

void startup(void) {
    srand(time(NULL));
    printf("请输入地图的长宽：\n");
    scanf("%d%d", &bottom, &right);
    for (i = top; i < bottom; i++) {
        for (j = left; j < right; j++) {
            if (i == 0 || i == bottom - 1)
                a[i][j] = 3;
            else {
                if (j == 0 || j == right - 1)
                    a[i][j] = 4;
            }
        }
    }
    player_x = rand() % (bottom - 1) + 1;
    player_y = rand() % (right - 1) + 1;
}

void uppdate_No_input(void) {
    for (i = top + 1; i < bottom - 1; i++)
        for (j = left + 1; j < right - 1; j++)
            a[i][j] = 0;
    a[player_x][player_y] = 1;
}

int uppdate_with_input(void) {
    int input;
    if (kbhit()) {
        input = getch();
        // printf("\tinput: %d\n", input);
        switch (input) {
            case 'a': player_y -= 2; break;
            case 'd': player_y += 2; break;
            case 'w': player_x -= 2; break;
            case 's': player_x += 2; break;

            case CTRL_EOF: return 0;
        }
    }
    return 1;
}

void show(void) {
    gotoxy(0, 0);
    for (i = 0; i < bottom; i++)  {
        for (j = 0; j < right; j++) {
            switch (a[i][j]) {
                case 0: putchar(' '); break;
                case 1: putchar('@'); break;
                case 3: putchar('-'); break;
                case 4: putchar('|'); break;
            }
        }
        putchar('\n');
    }
}

#ifndef __cplusplus // C main
int main(void) {
    startup();
    clrscr();
    _setcursortype(_NOCURSOR);
    int keep = 1;
    while(keep) {
        show();
        uppdate_No_input();
        keep = uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
    return 0;
}

#else  // Gtest Main
}
#include "gtest/gtest.h"

TEST(game, normal) /* test case */
{
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    int keep = 1;
    while(keep) {
        show();
        uppdate_No_input();
        keep = uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
