#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdio.h>
#include <limits.h>
#define cprintf printf

enum COLORS {
  BLACK = 0,        BLUE = 1,         GREEN = 2,
  CYAN = 3,         RED = 4,          MAGENTA = 5,
  BROWN = 6,        LIGHTGRAY = 7,    DARKGRAY = 8,
  LIGHTBLUE = 9,    LIGHTGREEN = 10,  LIGHTCYAN = 11,
  LIGHTRED = 12,    LIGHTMAGENTA = 13,YELLOW = 14,
  WHITE = 15,       BLINK = 128
};

enum CURSORTYPE {
  _NOCURSOR,//     (turns off the cursor
  _SOLIDCURSOR,//  (solid block cursor
  _NORMALCURSOR // (normal underscore cursor
};

void clrscr(void);
void gotoxy(int x, int y);
void _setcursortype(int cur_t);
void textbackground(int newcolor);
void textcolor(int newcolor);

#ifdef _WIN32   /* windows ************ */
#include <windows.h>
#include <conio.h> //from window
/* _Sleep函数 milliseconds 毫秒， 1/1000 s  */
#define sleep(n)  Sleep((n))
#undef CTRL_EOF
#define CTRL_EOF 26
#define getch _getch
#define putch _putch
#define kbhit _kbhit
#define cputs _cputs
#define ungetch _ungetch
#define getche _getche
#define putch _putch
#define cputs _cputs

#else   /* Unix && Linux ************ */
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
/* usleep函数 microseconds 微秒， 1/1000 000 s  */
#define sleep(n)  usleep(1000 * (n)) /* 改造为_Sleep函数 1/1000 s  */
char * cgets(char * _Buffer);
int cputs(const char *str);
int getch(void);
int getche(void);
/* Checks for currently available keystrokes.
kbhit checks to see if a keystroke is currently available.
Any available keystrokes can be retrieved with getch or getche.*/
int kbhit(void);
int putch(int c);
int ungetch(int ch);
#endif

#endif // CONSOLE_H
