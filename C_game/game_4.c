/* flappy bird */

/*利用空战游戏、反弹球、flappy bird的实现思路，实现很多常见的小游戏。
如果要求空战游戏中2台敌机同时出现、3个反弹球同时反弹、flappy bird画面中同时出现4个柱子，如何实现？*/

#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#endif   /* C main() */

#include <stdlib.h>
#include <time.h>
#include "console.h"

int high, width;
int bird_x, bird_y;
int bar1_x, bar1_yDown, bar1_yTop; // 障碍物的相关坐标
int score;

void startup(void) {
    srand(time(NULL));
    high = 20;
    width = 50;
    bird_x = 3;
    bird_y = high / 2;
    bar1_x = width / 2;
    bar1_yDown = high / 3;
    bar1_yTop = high /2;
    score = 0;

}

void show(void) {
    gotoxy(0, 0);
    int i, j;
    for (i = 0; i < high; i++) {
        for (j = 0; j < width; j++) {
            if ((i == bird_y) && (j == bird_x))
                printf("@");
            else if ((j == bar1_x) &&
                    ((i < bar1_yDown) || (i > bar1_yTop)))
                printf("#");
            else
                printf(" ");
        }
        printf("\n");
    }
    printf("\tscore: %d\n", score);
}

void uppdate_No_input(void) {
    bird_y ++;
    bar1_x --;
    if (bird_x == bar1_x) {
        if ((bird_y >= bar1_yDown) && bird_y <= bar1_yTop)
            score ++;
        else {
            printf("鸟儿撞死了。。。\n");
            sleep(1000);
            exit(0);
        }
    }
    if (bar1_x <= 0) {
        bar1_x = width;
        int tmp = rand() % (int)(high * 0.8);
        bar1_yDown = tmp - high / 10;
        bar1_yTop = tmp + high / 10;
    }
    sleep(150);
}

int uppdate_with_input(void) {
    int input;
    if (kbhit())  {
        input = getch();
        switch (input) {
            case ' ': bird_y -= 2; break;
            case CTRL_EOF: return 0;
        }
    }
    return 1;
}


#ifndef __cplusplus // C main
int main(void) {
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    int keep = 1;
    while(keep) {
        show();
        uppdate_No_input();
        keep = uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
    return 0;
}

#else  // Gtest Main
}
#include "gtest/gtest.h"
TEST(game, normal) /* test case */
{
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    int keep = 1;
    while(keep) {
        show();
        uppdate_No_input();
        keep = uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
