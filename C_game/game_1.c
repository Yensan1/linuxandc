/* 弹跳小球游戏 */

/* 屏幕的坐标原点在左上角。
    + . . . .  X
    .
    .
    .
    Y

*/

#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#endif   /* C main() */

#include "console.h"

void print_pause(void);
void draw_horizon(int x);
void ball_static(int x, int y);
void ball_fall(int x, int y);
void ball_fall_velocity(int x, int height);
void ball_collide(int right, int bottom);

/* 左右弹跳，碰到边界就 回弹 */
void ball_collide(int right, int bottom)
{
    int TOP = 0, LEFT = 0;
    int x = 1, y = 0;
    int velocity_x = 1, velocity_y = 1;
    int i;
    while (1) {
        x += velocity_x,  y += velocity_y;
        clrscr();
        draw_horizon(right);
        ball_static(x, y);
        for (i=0; i < bottom-y; i++)
            printf("\n");
        draw_horizon(right);
        sleep(0.01);
        if (x == TOP  || x == right)
            velocity_x *= -1;
        if (y == LEFT || y == bottom)
            velocity_y *= -1;
    }
}

/* 输出一个 上下弹跳的球   x height 是右边界、下边界*/
void ball_fall_velocity(int x, int height)
{
    int velocity, y, j;
    velocity = 1;
    y = 0;
    while (1) {
        y = y + velocity;
        clrscr();
        ball_static(x, y);
        for (j=0; j < height-y; j++)
            printf("\n");
        if (y == height || y == 0)
            velocity *= -1;
    }
}

/* 输出一个 下落的球。 x y 是横坐标 下边界 */
void ball_fall(int x, int y)
{
    int i, j;
    for (i=0; i<=y; i++) {
        clrscr();
        ball_static(x, i);
        for (j=0; j < y-i; j++)
            printf("\n");
        // sleep();
    }
}

/* 输出一个 静止的球。 x y 是横坐标 纵坐标*/
void ball_static(int x, int y)
{
    int i;
    for (i=0; i < y; i++)
        printf("\n");
    for (i=0; i < x; i++)
        printf(" ");
    printf("O\n");
}

void draw_horizon(int x)
{
    for (; x > 0; x--)
        printf("-");
    printf("\n");
}

/* 输出一行字，并且暂停 */
void print_pause(void)
{
    printf("Press any key to start\n");
    getchar();
    clrscr();
}



#ifdef __cplusplus
}

#include "gtest/gtest.h"
TEST(BallStatic, normal) /* test case */
{
    ball_static(40, 25);
    print_pause();
}

TEST(BallFall, normal) /* test case */
{
    ball_fall(40, 25);
    print_pause();
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
