#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#endif   /* C main() */

#include <stdlib.h>

#ifdef __cplusplus  /* C++ Gtest */
}
#include "console.h"
#include "gtest/gtest.h"

TEST(getchputch, Normalize) /* test case */
{
    char ch = 0;
    printf("Input a string, Ctrl+Z(Win) OR Ctrl+D(*nix) to Exit:");
    while ((ch = getch()) != 26)
        putchar(ch);
        printf("%c\n", 26);
}

TEST(clrscr, Normalize) {
    int i, j;
    clrscr();
    for (i=0; i<9; i++)
    {
        for (j=0; j<80; j++)
            cprintf("C");
        cprintf("\r\n");
        textcolor(i+1);
        textbackground(i);
    }
    textcolor(15);
    textbackground(0);
}



TEST(kbhit, Normalize)
{
    cprintf("Press any key to continue:");
    while (!kbhit()) /* do nothing */ ;
    cprintf("\r\nA key was pressed...\r\n");
}

TEST(getch, Normalize)
{
    clrscr();
    gotoxy(35, 12);
    cprintf("Hello world");
    getch();
}

TEST(getche, Normalize)
{
    char ch;
    printf("Input a character:");
    ch = getche();
    printf("\nYou input a '%c'\n", ch);
}

TEST(extended, Normalize)
{
    int c;
    int extended = 0;
    c = getch();
    if (!c)
        extended = getch();
    if (extended)
        printf("The character is extended\n");
    else
        printf("The character isn't extended\n");
}

TEST(cprintf, Normalize)
{
    int i;
    clrscr();
    for (i = 0; i < 20; i++)
        cprintf("%d\r\n", i);
    cprintf("\r\nPress any key to clear screen");
    getch();
    clrscr();
    cprintf("The screen has been cleared!");
    getch();
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif   /* C main() */
