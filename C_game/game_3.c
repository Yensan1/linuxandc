/* 改进游戏，
比如显示复杂的飞机图形、多台敌机、飞机被撞后重新出现、更好玩的子弹、
飞机血量、升级、登录界面、游戏暂停等等...... */

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include "console.h"

int high,     width;    //  游戏画面尺寸
int plane_x,  plane_y;  // 飞机位置
int bullet_x, bullet_y; // 子弹位置
int tgt_x,    tgt_y;    // 敌机位置
int score;  //




void startup(void) {    // 数据初始化
    srand(time(NULL));
    high = 20;
    width = 30;
    plane_x = width / 2;
    plane_y = high /2;
    bullet_x = plane_x;
    bullet_y = -2;
    tgt_y = 0;
    tgt_x = rand() % width;
    score = 0;
}

void show(void) {   // 显示画面
    // clrscr();
    gotoxy(0, 0);
    int i, j, k;
    for (i = 0; i < high; i++) {
        for (j = 0; j < width; j++) {
            if ((i == plane_y) && (j == plane_x)) {
                printf("-*|*-        \n");
                for (k=0; k < j; k++)
                    printf(" ");
                printf("  +");
            }
            else if ((i == bullet_y) && (j == bullet_x))
                printf("  .");
            else if ((i == tgt_y) && (j == tgt_x))
                printf("@");
            else
                printf(" ");
        }
        printf("\n");
    }
    printf("\tscore:%d\n",score);
}

void uppdate_No_input(void) {   // 与用户输入无关的更新
    static int speed = 0;
    if (bullet_y > -1)
        bullet_y --;
    if ((bullet_y == tgt_y) && (bullet_x == tgt_x - 2)) { // 子弹击中敌机
        score++;
        tgt_y = -1;
        tgt_x = rand() % width;
        bullet_y = -2;
    }
    if (tgt_y > high) { // 敌机跑出显示屏幕
        tgt_y = -1;
        tgt_x = rand() % width;
        if (score > 0)
            score --;
    }
    if (speed < 5)
        speed++;
    if (speed == 5) {
        tgt_y ++;
        speed = 0;
    }

}

int uppdate_with_input(void) {
    static int input;
    if (kbhit()) {
        input = getch();
        switch (input) {
            case 'a':   plane_x--;  break;
            case 'd':   plane_x++;  break;
            case 'w':   plane_y--;  break;
            case 's':   plane_y++;  break;
            case ' ':
                bullet_y = plane_y - 1;  // 发射子弹的初始位置在飞机的正上方
                bullet_x = plane_x;
                break;
            case CTRL_EOF:  return 0; // Windows ctrl Z 退出
        }
    }
    return 1;
}


void game(void) {
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    int keep = 1;
    while(keep) {
        show();
        uppdate_No_input();
        keep = uppdate_with_input();
    }
    clrscr();
    _setcursortype(_NORMALCURSOR);
}



#ifdef __cplusplus
}
#include "gtest/gtest.h"

TEST(game, normal) /* test case */
{
    game();
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
