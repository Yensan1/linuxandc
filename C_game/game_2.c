/* 如何实习移动的敌机，我们的飞机发射子弹击中他？如何显示得分？
敌机撞击我们后，如何判读并提示游戏失败？ */

#ifdef __cplusplus  /* C++ Gtest */
extern "C" {
#endif   /* C main() */

#include <stdlib.h>
#include <time.h>
#include "console.h"

int gen_random(int upper_limit);
void pause_for_KEY(void);
void print_pause(void);
void draw_horizon(int x);
void point_print(int x, int y);
void point_control_alpha(void);
void point_control(void);
void plane_print(int x, int y, int *isFire);
void plane_control(void);

/* 控制飞机。wsad 上下左右，space 发射 */
void plane_control(void)
{
    _setcursortype(_NOCURSOR);
    int gen_random(int upper_bound);
    char input;
    int x = 5, y = 10;
    int isFire = 0;
    int tgt_x = 10;
    int i = 0, isKilled = 0;
    int score = 0;
    while (1) {
        clrscr();
        printf("\tScore:\t%d\n", score); /* 打印计分 */
        for (i=0; i<tgt_x; i++) /* 打印敌人 */
            printf(" ");
        printf("@\n");
        plane_print(x, y, &isFire); /* 打印飞机 和 子弹 */
        if (kbhit()){  /* 获取键盘输入，然后更改数据 */
            input = getch();
            switch (input) {
                case 'a':   x--;        break;
                case 'd':   x++;        break;
                case 'w':   y--;        break;
                case 's':   y++;        break;
                case ' ':   isFire = 1; break;
            }
        }
        if (!isKilled && isFire && tgt_x == x+2 ) /* 如果有敌人、开火了，并且击中了 */
            isKilled = 1;
        if (isKilled) { /* 如果没有了敌人、那就计分，并且再树立一个敌人 */
            tgt_x = gen_random(50);
            score++;
            isKilled = 0;
        }
    }
    _setcursortype(_NORMALCURSOR);
}

/* 输出一个 飞机。 x y 是横坐标 纵坐标  *isFire==0 表示开火*/
void plane_print(int x, int y, int *isFire)
{
    /* 坐标系  x从左到右  y从上到下

    * * * X 坐标系
    *
    * Y 坐标系

    飞机的坐标原点在左上角（选用的是第一个飞机）
            -*|*-
              +
                    +
                --+=+=+--
                  - | -
                    |
              .
            ——+——
             _|_
                      +
                    --+--
                     . .
    */
    int i, j;
    if (*isFire) {
        for (j=0; j < y; j++){
            for (i=0; i < x; i++)
                printf(" ");
            printf("  .\n");
        }
        *isFire = 0;
    } else
        for (j=0; j < y; j++)
            printf("\n");
    for (i=0; i < x; i++)
        printf(" ");
    printf("-*|*-\n");
    for (i=0; i < x; i++)
        printf(" ");
    printf("  +\n\n");
}

/* 控制一个点上下左右移动， 不用 scanf和getchar，用 conio.h  getch() 不出现暂停。*/
void point_control(void)
{
    char input;
    int x = 5, y = 10;
    while (1) {
        clrscr();
        // plane_print(x, y);
        point_print(x ,y);
        if (kbhit()){
            input = getch();
            switch (input) {
                case 'a':   x--;    break;
                case 'd':   x++;    break;
                case 'w':   y--;    break;
                case 's':   y++;    break;
            }
            printf("\tgetch(): d:%d, c:%c\n", input, input);
            sleep(0.2);
            if (input == EOF)
                break;
        }
    }
}

/* 控制一个点上下左右移动， 但是会出现暂停 */
void point_control_alpha(void)
{
    char input;
    int x = 5, y = 10;
    while (1) {
        clrscr();
        point_print(x ,y);
        scanf("%c", &input);
        if (input == 'a')
            x--;
        else if (input == 'd')
            x++;
        else if (input == 'w')
            y--;
        else if (input == 's')
            y++;
    }
}

/* 输出一个 点。 x y 是横坐标 纵坐标*/
void point_print(int x, int y)
{
    int i;
    for (i=0; i < y; i++)
        printf("\n");
    for (i=0; i < x; i++)
        printf(" ");
    printf("*\n");
}

void draw_horizon(int x)
{
    for (; x > 0; x--)
        printf("-");
    printf("\n");
}

/* 输出一行字，并且暂停 */
void print_pause(void)
{
    void pause_for_KEY(void);
    printf("Press any key...\n");
    pause_for_KEY();
    clrscr();
}

/* 暂停 */
void pause_for_KEY(void)
{
    for (;;)
        if (getchar())  break;
}

int gen_random(int upper_limit)
{
    srand(time(NULL));
    return rand() % upper_limit;
}

#ifdef __cplusplus
}
#include "gtest/gtest.h"
// TEST(pointPrint, normal) /* test case */
// {
//     point_print(40, 25);
//     print_pause();
// }

// TEST(pointControl, normal) /* test case */
// {
//     point_control();
//     print_pause();
// }

TEST(planePrint, normal) /* test case */
{
    int fire = 1;
    plane_print(40, 25, &fire);
    print_pause();
}

TEST(planeControl, normal) /* test case */
{
    plane_control();
    print_pause();
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
