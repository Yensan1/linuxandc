/* 5 数组之生命游戏 */
/* 大家可以尝试修改规则，比如初始化不同的数据，或者3，2的时候都增加；增加另一个物种，食肉动物、食草动物，互相抑制；增加不同地形，比如某块区域有水源，生命更容易生存；交互投食，按+加速、按-减速；模拟生态进化、还有模拟大气污染、谣言传播等等。 */

#ifdef __cplusplus
extern "C" {
#endif

#include "console.h"
#include <stdlib.h>
#include <time.h>

#define High  25
#define Width 50

int cells[High][Width] = {0, }; // 所有位置细胞生1或死0


void startup(void) {
    srand(time(NULL));
    int i, j;
    for (i = 0; i < High; i++) {
        for (j = 0; j < Width; j++) {
            cells [i][j] = 1;
        }
    }
}

void show(void) {
    gotoxy(0, 0);
    int i, j;
    for (i = 0; i < High; i++) {
        for (j = 0; j < Width; j++) {
            if (cells[i][j] == 1)
                printf("*");
            else
                printf(" ");
        }
        printf("\n");
    }
    sleep(50);
}

void uppdate_No_input(void) {
    int newCells[High][Width];
    int NeibourNum;
    int i, j;
    for (i = 1; i < High - 1; i++) {
        for (j = 1; j < Width - 1; j++) {
            NeibourNum = cells[i-1][j-1] + cells[i-1][j] + cells[i-1][j+1]
                + cells[i][j-1] + cells[i][j+1]
                + cells[i+1][j-1] + cells[i+1][j] + cells[i+1][j+1];
            if (NeibourNum == 3)
                newCells[i][j] = 1;
            else if (NeibourNum == 2)
                newCells[i][j] = cells[i][j];
            else
                newCells[i][j] = 0;
        }
    }
    for (i = 1; i <= High - 1; i++)
        for (j = 1; j <= Width - 1; j++)
            cells[i][j] = newCells[i][j];
}


int uppdate_with_input(void) {

}





#ifndef __cplusplus // C main
int main(void) {
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    // int keep = 1;
    while(1) {
        show();
        uppdate_No_input();
        uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
    return 0;
}



#else  // Gtest Main
}
#include "gtest/gtest.h"

TEST(game, normal) /* test case */
{
    clrscr();
    _setcursortype(_NOCURSOR);
    startup();
    int keep = 1;
    while(1) {
        show();
        uppdate_No_input();
        uppdate_with_input();
    }
    printf("Game Over!\n");
    _setcursortype(_NORMALCURSOR);
}

int main(int argc, char* argv[]) /* test runner */
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#endif    /* C++ OR  C */
