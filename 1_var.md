[TOC]



1. 变量与常量、运算符、控制流、
2. 函数
3. 数组、指针、结构
4. 基本输入、输出



## 回到 hello world

```c
/* #include 声明 此文件使用的库函数
 * # 井号Pound sign/number sign/hash sign 
 * 尖括号Angle bracket 里面的 stdio.h 是一个文件名 
 */
#include <stdio.h>
/* comment 段注释
 * main 入口 每个程序都必须的Boilerplate
 * main: generate simple output
 */
int main(void){ /* {}花括号Curly Brace 分出语句组*/
    printf(/*comment2*/ "Hello World!\n"); /* 缩进Indent 由 空格blank 和 Tab制表符 构成*/
    return 0; /* 任何语句都以;号Semicolon 结尾 */
} // 两个//斜线slash也可以组成注释，C99的标准。
```

- 预处理指令`#include`
  - C标准主要由 2 部分组成：C语法，C标准库。C标准库定义了一组标准头文件，每个头文件包含一些函数、变量、类型声明和宏定义。要在一个平台上支持 C 语言，要实现C编译器（语法），还要实现C标准库。有些单片机C语言没有完整的C标准库。Linux 上最广泛的库是 glibc，包括C标准库和系统接口。glibc 是Linux 平台的C程序的运行基础。

注释：

- 段注释：`/*comment*/`，首尾的`/*`和`*/`是界定符Delimiter，中间行的`*`是Asterisk没语法意义，仅仅是为了对齐，这是C Coding Style
- 行注释：`//`两个斜线Slash，不能跨行，也不能插在代码中间。在C99中标准化。

`"Hello, World.\n"`

- `"`是界定符，里面的是字符串
- `\n`是转义序列，Escape Sequence

空行、空格、tab都不是Token，无关语法，仅仅是为了读起来更好。但是为了可读性和维护，必须注意代码风格。

main 是一个特殊的函数名，每个 C 程序必须包含 main 函数，而且都从 main 函数开始执行。

main 函数会调用其他函数，这些函数可能是自己编写的，也可能来自于函数库。

C语言程序，就是由 函数 和 变量 组成的。函数中的语句，执行操作；变量则用于存储计算过程中使用的值。



理解组合Composition 才是理解语法规则的关键，根据规则任意组合，才可以用简单的元素组建很大的事物。



## 数据类型、运算符、表达式

### 字面量的表示方法

字面量也叫直接量Literal

整型常量：13、0、-13；

浮点型常量：13.33、-24.4；

字符常量：‘a’、‘M’

字符串常量：”I love imooc!”

- 整数：一般整数都默认是int，超过限额就默认是 long。强调类型，`l/L`表示long，`u/U`表示unsigned。

  ```c
  long a = 1000L; /* 最好是大写 L 易读 */
  unsigned long a = 1000UL; /* 最好是大写 U ，和 L 搭配 */
  /* 整数可以用 oct hex 表示 */
  int b = 31, c = 037, d = 0x1f, e = 0x1F; /* 31，分别用十进制、八进制、十六进制表示 */
  85         /* 十进制 */
  0213       /* 八进制 。078 非法，8 不是八进制的数字*/
  0x4b       /* 十六进制 */
  30         /* 整数 */
  30u        /* 无符号整数。而032UU非法，不能重复后缀 */
  30l        /* 长整数 */
  30ul       /* 无符号长整数 */
  ```

- char 字符。

  ```c
  char a = '4', b = '\n'; /*字符、转义字符。单引号。计算机内部，实际是一个整数 */
  char c = '\012', d = '\x0a'; /* 可以用整数值对应的八进制、十六进制 表达 */
  char b = 'a', c = '\141', d = '\x61'; /* 可以用整数值对应的八进制、十六进制 表达 */
  ```

  转义序列 Escape Sequence，2个作用：用普通字符表示特殊字符（无法输入或者不可见），比如`\n`；特殊字符转为普通字符，比如`\\`

  | 转义字符 | 意义                            | ASCII码值（十进制） | 转义字符 | 意义                   | ASCII码值（十进制） |
  | ---- | ----------------------------- | ------------ | ---- | -------------------- | ------------ |
  | \a   | 响铃(BEL)                       | 007          | \b   | 退格 Backspace         | 008          |
  | \f   | 换页 Form Feed                  | 012          | \n   | 换行 Line Feed         | 010          |
  | \r   | 回车 Carriage Return            | 013          | \t   | 水平制表 Horizon Tab     | 009          |
  | \v   | 垂直制表 Vertical Tab             | 011          | \\\  | 反斜线字符''\'  Backslash | 092          |
  | \'   | 单引号 Single Quote / Apostrophe | 039          | \"   | 双引号 Double Quote     | 034          |
  | \?   | 问号 Question Mark              | 063          | \0   | 空字符(NULL)            | 000          |

  Windows 和 很多网络应用层协议（比如HTTP）是用\r\n，而 Unix/Linux 是用 \n

  转义序列是在编译时候处理的，而转换说明是在运行时调用 printf 函数处理的。

- 浮点数：小数点、指数、或者小数点带指数

  ```c
  double a = 123.4, b = 1e-2, c = 1.2e-3;
  float d = 1.3f, e = 2.7F; /* f/F 表示 float */
  long double y = 12512353.23562345L; /* l/L 表示 long double，最好是大写 L */
  ```


- 字符串

  ```c
  char a = 'x'; char b[] = "x"; /*字符 和 字符串 不同。字符串是包含字符的、以\0结尾的数组*/
  char c[] = "you say: \"Hello, "
             "World!\"\n"; /* 字符串分行，编译器可以自动连接。可以包含转义字符*/
  char empty = ""; /* 空字符串 */
  ```

- 常量表达式：仅仅包含常量的表达式，在编译时求值，而不在运行时求值。

  ```c
  #define MAX 100
  char line[MAX + 1]; /* 常量表达式 */
  ```

### 变量 Variable

变量是编程语言中最重要的概念之一。变量是计算机存储器中的一块命名的空间，可以存储一个值 value ，值可以随时变化。变量的类型，决定了存储空间的大小。计算机的基本存储单位是 比特bit，但变量的最小存储单位是 字节Byte。

声明 Declaration 包括 变量声明、函数声明、类型声明。

如果一个声明，同时会要求编译器为它分配存储空间，就是定义 Definition。

变量和常量是程序的两种基本数据对象。声明语句说明变量的名字和类型，也可以指定初始值。变量的类型决定该对象可取值的范围，以及可以执行的操作。

- 变量声明：说明了变量的属性：数据类型 和 变量名。

  ```c
  '5' "5"  5  5.0 /* 这是4种不同的字面量：字符，字符串，整数，小数 */ 
  type var_name; /* 语法：类型 变量名 */
  /* 变量声明 Declaration，变量名关系到存储地址，变量类型关系到存储空间的大小 */
  char first_letter;
  int  hour, minute;
  /* 变量赋值 Assignment 。 没有声明的变量不可赋值 */
  first_letter = 'a';
  hour = 11;
  minute = 59;
  /* 声明赋值一步到位：初始化Initializer ，特殊的声明 */
  char first_letter = 'a';
  unsigned count;  /* unsigned int 无负号 */
  char c, line[100]; /* 多个变量同时声明。分开容易修改，但合在一起更紧凑 */
  int i, j = 0; /* 语法可行，但不推荐。不易读，容易误解 */
  int i=0, j=0; // 同时定义多个变量，并且初始化。
  const double e = 2.718281828 /* const 限定符，此变量只能赋值一次，不能被修改 */
  ```

- 变量初始化

  自动变量（函数、语句块的局部变量，且没有static限定），可以用任何表达式初始化，每次进入函数/语句块都被初始化一次。未进行初始化，值为随机值（当时内存的电位而定，垃圾数据）。

  非自动变量（外部变量、静态变量），能且只能在程序开始之前  初始化  一次，所以必须用常量表达式初始化。未进行初始化，默认为0。

  const 修饰后，只能初始化一次，没法修改。

另外，C99能在任何定义变量。而 ANSI C，只能在函数的开头部分定义变量：

```c
int makeChange(){
    float change = 0, price = 0;
    int money = 0;
    // 定义变量后，才能输入输出、进行运算。
    printf("请输入物品价格(元):\n");scanf("%f", &price);
    printf("请输入收到的大钞(元):\n");scanf("%d", &money);
    change = money - price;
    printf("找零：%0.2f 元\n", change);
}
```



### 常量 Constant

各种数据都可以组成常量。

##### 符号常量

编译的预处理阶段：`#include`包含其他源文件/函数库、`#define`宏替换、条件编译。

简单的宏定义`#define`，就是**文本替换**replace。所以，可以将一个符号名（符号常量）定义为一个特定的字符串。

```c
#include <stdio.h>
#define LOWER   0  /* 注意末尾没有 ; 分号  常量一般全大写*/
#define UPPER   300
#define STEP    20
int main(void)
{
    int fahr;
    for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP) {
        printf("\t%3d %7.1f\n", fahr, (5.0/9.0)*(fahr-32));
    }
    return 0;
}
```

##### 枚举常量

```c
enum boolean {NO, YES}; /* 如果不指定，默认第一个枚举值为0，下一个为1，以此类推 */
enum months {Mon = 1, Tue, Wed, Thu, Fri, Sat, Sun} /* 指定第一个枚举值为1，下一个为2，以此类推 */
enum escapes { BELL = '\a', BACKSPACE = '\b', TAB = '\t',
               NEWLINE = '\n', VTAB = '\v', RETURN = '\r'} /* 全部设定枚举值 */
```

##### const ：将变量「固化」

```c
const double PI = 3.1415926; // 只能赋值一次。无法修改（赋值第二次）
```

### 数据类型和长度

##### 二进制与数的表示

|          | 十 Dec   | 二 Bin     | 八 Oct            | 十六 Hex          |
| -------- | -------- | ---------- | ----------------- | ----------------- |
| 基本数字 | 10个:0~9 | 2个:0,1    | 8个:0~7           | 16个:`0~9~A~F`    |
| 进位规则 | 十进一   | 二进一     | 八进一            | 十六进一          |
| 适用范围 | 日常生活 | 计算机内部 | 3 bit 缩为 1个Oct | 4 bit 缩为 1个Hex |

二进制转十进制，按权展开：`111001`=2^5^+2^4^+2^3^+2^0^=32+16+8+1=57

十进制转二进制：不停地除以2，得到余数序列，翻转过来，就是二进制数。57/2=28…1；28/2=14…0；14/2=7…0；7/2=3…1；3/2=1…1；1/2=0…1，余数序列为 100111，翻转一下 111001。当然，二进制数一般以 8 bit / 1 byte 操作

```c
// 用8bit二进制数表示57，00111001。二进制换八进制：每3个二进制缩为一个Oct，071。二进制换十六进制：每4个二进制缩为一个hex，0309。
// 位置：第0位，是最低位Least Significant bit/LSB，7位是最高位Most Significant bit/MSB
| 0 | 0 | 1 | 1 | 1 | 0 | 0 | 1 |  // 存储的值
| 7   6   5   4   3   2   1   0 |  // bit 的编号
// 最高字节在前（Big-Endian）大端序，符合人的阅读习惯。上面就是。最低字节在前（Small-Endian），小端序。PowerPC/IBM/SUN是大端序，x86/DEC是小端序，ARM大小端均可。通信协议都是大端序。
#include <stdio.h>
int isBigEndian(){ /*测试端序*/
    int a = 0x1234;
    char b = * (char *) &a;
    if (b == 0x12) return 1;
    return 0;
}
int main(){
    if (isBigEndian())  printf("BigEndian\n");
    else                printf("LittleEndian\n");
    return 0;
}
```

**整数的表示（以 1 byte 的char 为例）**

unsigned char 最简单，所有的位都是数字，按权位相加。

```c
| 1 | 0 | 1 | 1 | 1 | 0 | 0 | 1 |  // 185
```

 但 signed char 怎么表示负数？有3种办法，这里讲2种。现在所有的计算机都用反码

```c
// 1.符号幅值Sign and Magnitude表达法。最高位作为符号位，1表示负0表示正；其余bit不变；范围[-127, 127]。但是 0 有两种表达，而且运算很难
| 1 | 0 | 1 | 1 | 1 | 0 | 0 | 1 |  // (-1) * 57 = 57
// 2.反码2's Complement表达法（现代计算机）。单单最高位作为负数；其余bit不变；范围  [-128, 127]
| 1 | 0 | 1 | 1 | 1 | 0 | 0 | 1 |  // 最高位：2**7=128，其他：57。结果：-128+57=-71
```

char 作为字母时：ASCII [0,127]只用后7位，最高位弃之不用，所以，究竟内部怎么表示一个字母，各个编译器自己决定。一般是unsigned，这样可以扩展更多字符，比如MS-DOS CP437 和 Western/Latin-1(ISO/8859-1)

```c
| 0 | 0 | 1 | 1 | 1 | 0 | 0 | 1 | // ASCII char '9'
```

**浮点数的表示：**

- 十进制小数转为二进制，有些数没法精确转换，无论float/double/long double，比如2.2。所以，浮点数运算必然是不精确的，不论用什么精度。

  ```c
  3.21e4 // 科学计数法：3.21是尾数Mantissa/Signiificand，4是指数Exponent。
  // 如果要把 8.25 变为科学记数法，尾数0<a<1, 8.25 =8.25*1= 17.0 * E0，尾数的小数点往左移动1位，指数+1，就变成 0.825E1（这里用E表示10为底）。
  // 类似地，如果把 8.25 用二进制表示 1000.01 = (1000.01) * e0（这里用e表示2为底），把尾数的小数点往左移动3位，指数+3，就变为  (1.0001) * e3。
  
  // 实数 Real ≈ 正负号sign*尾数Mantissa*阶位exponent
  8.25 = (+1) * 1.0001 * e3
  
  |0|0000 0000|0000 0000 0000 0000 0000 000| // float有32bit，sign 1bit，exponent 8bit，Mantissa 23bit
  // 既然二进制尾数 1.xxx 首位都是1，那就省略小数点前面的1（所以float的精度是24bit，也就是小数点后6位）。尾数就成了 0001 。如果右侧不够，用0填充 |0|...|0001 0000 0000 0000 0000 000|
  // 8 bit 能表达[-127, 128]，中间的指数用移位存储（而不是补码）。0000 0000表示 -127，1111 1111 表示 128。 此例的3 --> 3+127=130 --> 01111111 + 11 = 1000 0010 
  // 最终： |0|1000 0010|0001 0000 0000 0000 0000 000|
  
  |0|0000...0000|00...00|  // double有64 bit，sign 1bit，exponent 11 bit，Mantissa 52bit 。以类似的原理存储和运算
  ```



##### 数据类型

C语言有类型的概念，但对类型的安全检查不如 JAVA/C++ 那么严格。

数据类型：基本类型（字符、各种长度的整型/浮点型），还可通过指针、数组、结构、联合派生出各种数据类型。

仅有 2 大 基本类型：

- 整型：int（长度不定。加上限定符，可以衍生出很多种，short/int/long）、char（8 bit/1 byte）、
- 浮点型：float（4 byte，至少有6 bit有效数字）、double（8 byte）

其他类型：

- 构造类型：枚举类型enum，数组类型array，结构类型struct，共用体类型union
- 指针类型
- 空类型 void

C99 增加了bool、long long、long double 类型。

一般来说，char 是 1 byte，short 是 2 byte，long long 是8 byte。int 和 long 的长度是不固定的，一般是一个“字长”（也就是寄存器和内存总线的基础操作单位），int是和寄存器和内存总线最适合的长度。

short long 限定符：

- 组成了 4 种整数类型代表不同的大小：short int ≤ int ≤ long int ≤ long long int（可以缩写），但C标准只规定 bit 最小值 16, 16, 32, 64，所以实际上它们通常有重叠。现在的普通个人计算机一般是 16, 16/32, 32, 64。ILP32 也就是最常见的X86——是 16, 32, 32, 64, 指针32。LP64 个人64位计算机——是 16, 32, 64, 64, 指针64。
- 普通情况，**优先使用简单常见的 int**，int是机器中整数的最自然长度（所以各平台可能不同）。
- 考虑大小和速度：如果为了更小的空间（数组占用内存太大、特殊变量要考虑寄存器），可以使用 short 甚至是 signed char / unsigned char ，否则不要使用。如果要表示更大的数据，优先考虑使用 unsigned int，而不是直接使用 long，因为会降低运算速度。
- 考虑移植性：因为 int 类型的长度各平台不定，所以如果一定需要32位，就一定要用long，需要64位就用long long

类似地，存储空间 float ≤ double ≤ long double 也可能重叠。一般来说，**double 最常用**，要节约就使用 float，但很少使用 long double。

signed/unsigned 限定符：所谓sign指的是负号，所以signed就是有负号的，unsigned就是没有负号的。

- 用于 short/int/long：所有整数（short/int/long）默认都是signed，取值范围 [-Min, Max] ，所以标明`signed` 没有实际作用，只为了强调有符号而已；如果标明`unsiged` 类型，没有负数，取值范围 [0, Max] 。结论：unsigned只用于非负值，还有位运算。大部分时候，使用 short/int/long 就好。
- 用于 char ：char 表示字符时，signed/unsigned随编译器自定，因为无论怎样，都能放得下ASCII编码。但如果是用来表示整数，就必须加上signed/unsigned，因为每种编译器的char类型实现不同，声明`signed char a;`，那么 `a `取值范围 [-128, 127]；声明`unsigned char a;`，那么 `a `取值范围 [0, 255]。结论：用 char 表示整数，必须指定正负。

char 类型：

- char 就是 int，但取值范围更小，0-127。有些字符是不可见字符 Non-printable Character 和 空白字符 Whitespace。
- 每个字符在计算机内部用一个整数表示，称为字符编码Character Encoding，目前最常用的是 ASCII/American Standard Code for  Information Interchange 。



### 类型转换

首先，禁止使用无意义的表达式。其次，超过限额的表达式可能会导致意外结果。然后，很多表达式会自动转换。最后，可以进行强制转换。

```c
int a = 10; float b = 1.4;
char c = array[1.0]; /* 1.禁止使用无意义的表达式（比如浮点数用作下标），不存在这种转换问题 */
char d = 798; short cut = 32.76; /* 2.超出限额的赋值，会损失信息，可能导致意外结果。编译器会警告，但表达式并不非法 */
float sum = a + b; /* 3.自动把 a 转为 float 再运算 */
a = (int)(3.1 + 4.3); // 强制转换
```

##### 隐式转换 Implicit Conversion/Coercion

1. 整数升级 Integer Promotion。调用函数时，**如果参数类型未知**，会进行默认转换 Default Argument Promotion。char short 会转为 int（K&R时，float会转为double）

   ```c
   // 1. Old style C，函数声明没有写明类型。默认为 参数和返回值都是int
   func_a(a, b){  // 应该写为 int func_a(int a, int b)
       return a+b;
   }
   func_a('m', 'n'); // 调用时，char会转为int
   // 2. 参数不确定的函数，也会进行提升（这是计算机硬件架构决定的）
   int printf (const char *__format, ...) // printf()的函数声明有... ，参数不确定
   char ch = 'a';  printf("%c", ch); // char会提升为int，再传给printf
   ```

   算术运算中，如果使用了比int更短的类型（char、short、bit-filed）都会转为int型，如果不够容纳，会转为unsigned int。这是因为int是操作系统的自然长度，这样才能进行运算。

   ```c
   unsigned char c1 = 255, c2 = 3;
   int n = c1 + c2; // 其实都是转为int，再相加的
   ```

2. 算术运算，自动转换 Arithmetic Conversion

   如果是 float 和 double ，将**不会自动转换**，这是考虑 存储空间 和 运算速度。高精度计算，比如`<math.h>`的函数，才用 double，一般都用float。

   ```c
   +, -, *, /, %, >, >=, <, <=, ==, !=  // 这些运算，都要自动转换 
   
   // char short 首先要进行 Integer Promotion
   signed char a = 20;
   a + 10; // 首先要进行 Integer Promotion
   
   // 两边都是 signed/unsigned，自动转为更大的 long
   int b = 10
   long c = 3;
   b + c; // b+c 的运算，两边都是 signed，转为更大的long
   
   // 两边 signed/unsigned 不一样时，转换规则非常复杂。千万别这样，建议显式强制转换。
   // unsigned 更宽，就转为 unsigned。
   unsigned long e = 4;
   b + e; // 长度：unsigned long > int，所以都转为 unsigned
   c + e; // 表示范围更宽：unsigned long > long，所以都转为 unsigned
   
   // 另外的例子
   long int a = -1;
   unsigned int b = 1;
   unsigned long int c = 1;
   if (a > b) /* 关系运算不正确。算术运算正确。 */
       printf("a: %d > b: %d; a+b: %d; a*b: %d\n", a, b, a+b, a*b);
   if (a > c) /* 关系运算不正确。算术运算正确。 */
       printf("a: %d > c: %d; a+c: %d; a*c: %d\n", a, c, a+c, a*c);
   
   /* 注意，两个char/short/int/long 除法只能得到整数 */
   int a = 1, b = 3; c = a/b;
   printf("%f\n", c); // 0.000000 实际上就得到了 int 0
   
   /* 相除的两个数，必须要有float/double，而且存入的变量c也是浮点数，才能得到小数 */
   int   a = 1;  // 相除的两个数 ab 中，b是浮点数
   double b = 3, c = a / b;  // 存储结果的 c 也是浮点数
   printf("%lf\n", c);   // 0.333333
   ```

3. 赋值运算，自动转换。函数传参、return 是一种隐形赋值，也会自动转换

   ```c
   int i; char c = 'x';
   i = c; /* 这种赋值，隐含的数据转换不会丢失信息 */
   c = i; /* 这种赋值，隐含的数据转换很可能会丢失信息 */
   int i = 3; float f = 4.5; double d = 3.122423454777777786;
   i = f; /* float转为整数，会截尾 */
   f = d; /* double 转为 float，究竟是四舍五入，还是截尾，随编译器而定 */
   
   int func(int a, int b) {
      return (a + b) / 2.5; // return，会自动转换
   }
   func(3.1, 8.7); // 调用函数，传入参数，会自动转换
   ```

##### 显式强制转换 Explicit Conversion/Type Cast

不遵循四舍五入法则。强制转换有危险，使用需谨慎，特别是从更大类型转为更小的类型。

```c
i = (int) a; /* a 可以是字面量，也可以是任意表达式 。不会改变 a 的值*/
i = (int)(3.7 + 7.8); /* 表达式要加括号 */
i = (int) 3.7 + 7.8; // 如果没有括号，就变成 3 + 7.8
/* 没有声明函数原型，传参前转换，避免编译器告警 */
int num = 10;
x = sqrt((double) num); 
/* 如果声明了函数原型，就会进行自动转换，不必要强制转换（不建议） */
double sqrt(double);
root = sqrt(2); 
```

##### 字符的转换（字符编码与int的映射）

- 将 char 强制转换为 int：char表示字符的时候，没有signed/unsigned的说法（都是正整数），不同的编译器可能实现不同，因此 `(int)c`可能得到一个负数（ANSI只是要求标准打印字符集一定非负）。因此，用 char 存储非字符数据，必须要指定 signed/unsigned。

- 将字符串`"12894"`转为整数`12894`，如上所述，加上 `'0'`的ASCII 码不是整数 0，所以不能用 `(int)c`这种方式

  ```c
  #include <stdio.h>
  int atoi(char s[]) /* 将 字符串转为数字，比如 "153" 转为 153 */
  {
      int i = n = 0;
      for (; s[i] >= '0' && s[i] <= '9'; ++i)
          n = 10 * n + (s[i] - '0');
      return n;
  }
  int main(void) /* 用法 */
  {
      char str[] = "1897333";
      printf("\t%d\n", atoi(str));
      return 0;
  }
  ```

- `<ctype.h>`有大小写转换函数，也有判断是不是大小写、字母数字等函数。但要注意：

  ```c
  d = (c >= '0' && c <= '9'); /* 判断 c 是不是数字，结果只会是 0 或 1 */
  d = isdigit(c); /* 用函数判断 c 是不是数字，结果可能是 0 或 其他非零值 */
  ```






### 运算符

运算符指定将要进行的操作。

##### 算术运算符

没有`**`幂运算

```c
/* 单目运算 +和-  数值不变+，取相反数-，常见如下*/
int a=1, b=3, c = a*-b;  // 先取b的相反数，然后与a相乘，c == -3

+, -,  *, /, %      /* 运算符 + - 优先级比 * / % 更低 */
float c = 3 % 4.0;  /* 错！ % 不可以用于浮点数 */
int b = 5 / -3;     /* 有负数的 / 运算，截尾方向取决于编译器的实现 */
int e = 5 % -3;     /* 有负数的 % 运算，结果取决于编译器的实现 */
```

如果某个运算符所有的操作数（常量或者表达式）都是整型，就执行整型运算。

```c
int a, b=10;
a = 3 / 2; /* 整数除法将会 舍位，也就是 截尾，舍弃小数部分 */
b = b / 2;
float a, b = 10.0; /* 虽然 b = 10 也可以，但 10.0 强调浮点，更易读 */
float a = 3 / 4;   /* 注意，3/4整数除法，截尾 */
a = 3.0 / 2;   /* 如果有浮点数，就转换为浮点数，然后运算，不截尾 */
b = b / 3;     /* 如果有浮点数，就转换为浮点数，然后运算，不截尾 */
```

% 运算：

- C99规定，如果 a 和 b 是整形，并且 b ≠0，那么`(a/b)*b + a%b == a`
- 因此，%结果的正负号总是与被除数相同。`5%2=1 5%-2=1 -5%2=-1`

##### 布尔运算：关系运算符 与 逻辑运算符

比较运算，只有int 0/1 两种结果。

```c
>, >=, <, <=, ==, !=; /* 关系运算符   */
!, &&, ||             /* 逻辑运算符，非，与，或  */

if (i < lim -1)  等价于 if (i < (lim-1))  /* 关系运算符 优先级 都比 算术运算符 低 */
if (i<lim-1  &&  (c=getchar()) != '\n'  &&  c != EOF) /* && 和 || 都存在短路运算*/
if ((c=getchar()) != EOF) /* 因为赋值运算符 优先级 更低，所以需要加括号 */
if (!valid) 等价于 if (valid == 0)
```

##### 自增自减运算符

++i 前缀自增运算符Prefix Increment Operator，--i 前缀自减运算符 Prefix Decrement Operator

i++ 后缀自增运算符Postfix Increment Operator，i-- 后缀自减运算符 Postfix Decrement Operator

如果把自增运算看成一个函数，调用自增运算，++i 和 i++ 都导致同样的 side effect `i = i + 1`，但是却有不同的返回值（自增自减是前还是后，就表示副作用+1发生在赋值的前还是后）。自减也是如此。

```c
int i, j, m, n;
i = j = 1;
m = i++; /* 后缀运算，先返回值，再运算。i: 2; m: 1 */
n = ++j; /* 前缀运算，先运算，再返回值。j: 2; n: 2 */
a[i]++;  /* 这种表达式合法 */
(x+y)++; /* 这种表达式是没有意义的。 */
/* 不要写出复杂的组合，非常难读 */
++i++;
i++++;
a = b += c++-d+--e/-f; 
```

过多使用 自增运算符  会影响可读性。一般只用在循环体内。

##### 二进制运算

只有整型才有二进制运算，浮点型没有。一般用于`unsigned`整型上，`signed`很少用。按位异或运算Bitwise XOR：两者不同为真，否则为假。

```c
// 请注意，& | << 不要和 &&  || < 搞混 。用数学算术来说明原理（& | ^ ~ 运算前，short/char都要提升为int，没有4bit的位运算。所以，仅为演示原理）
  AND     |   或 OR   |  异或 XOR  |  取反 NOT |  左移  |  右移  |
  0011    |    0011   |     0011   |          |  0011  |  0011  |
& 0101    |  | 0101   |   ^ 0101   |  ~ 0011  | <<  1  | >>  1  |
-------   |  -------- |   -------- |  ------- | ------ | ------ |
  0001    |    0111   |     0110   |    1100  |  0110  |  0001  |

unsigned char c = 0xfc; //常量0xfc是int，转为u_char赋给变量c，进行 ~c 运算时，先把 c 提升为int，然后进行运算，得到结果。（如果仅为char 取反，是 0x03，错误）
unsigned int i = ~ c; //  ffff ff03
```

常用  & ~ | 掩码Mask运算。以 0x0000 ff00 为例

```c
unsigned MASK = 0x0000ff00, a = 0x12345678;
/* 只留 8-15 位(清空了其他bit) */
unsigned b = a & MASK; printf("%x\n", b);// 结果 0x 0000 5600
/* 取出 8-15 位 */
b = (a & MASK) >> 8; // 结果 0x 0000 0056
b = (a >> 8) & ~ (~0U << 8);  // 另一种写法
unsigned getbits(unsigned x, int p, int n) { // 更为通用的函数
    return (x >> (p + 1 - n)) & ~ (~0 << n); // 取出x 从p到n的bit
}
/* 将 8-15 位 清零 */
b = a & ~ MASK;// 结果 0x 1234 0078
/* 将 8-15 位 设 1 */
b = a | MASK;    // 结果 0x 1234 ff78
```

有趣的取反运算

```c
/* 一个数，和自身 异或 的结果是 0 */
xorl %eax, %eax  // 寄存器如果需要0，会直接用原值进行异或运算
/* 与0异或，结果不变；与1异或，就会翻转。结合掩码MASK，就可以反转特定的bit */
unsigned b = 0x12345678 ^ (1U<<6);//0x12345638 改变第6位。4-7位0111变为0011，HEX 7变3
/* a1^a2...^an 结果是1，就说明这个序列有奇数个1（奇偶校验） */
/* x^x^y == y，因为 x^x=0, 0^y=y */
a = a ^ b; // 两数交换 tmp = a, a = b, b = tmp;
b = b ^ a; // 整数的两数交换的另一种写法。
a = a ^ b;
y = y ^ n; /* 二进制位相同，为0；反之为1。比如 010 ^ 111 得到 101 */
```

加速的移位运算：如果是乘除 2 的阶乘数，可以用移位。比乘除更快。

```c
a = b << 2; /* 将 a 左移2位，右侧的空位用 0 填补，等价于乘以 4 */
```

##### 赋值运算符

```c
int a = 1;  // 基本赋值。赋值运算符的优先级，非常低
a = b = 6;  // 实际上是 a = (b=6) ，赋值运算也有结果，b=6 的结果是 6，再赋给a变量

// 最好不要做 嵌套赋值，非常难读
int a = 6;  int b;
int c = 1 + (b=a); // 将 b 赋值 6 的同时，将 c 赋值 7
printf("a %d, b %d, c %d", a, b, c); // a 6, b 6, c 7 

// 复合赋值运算符
+=  -=  *=  /=  %=   /* 基本运算符 与 赋值运算符 结合 */
x += 1; /* 等价于 x = x + 1 其余运算符同理 */

<<=   >>=   &=   ^=  |=  /* 二进制运算符 与 赋值运算符 结合 */
```

##### 条件表达式（三元运算符）

它可以像if else 那样嵌套，但是，非常不建议这么做。

```c
if (a > b) z = a;
else       z = b;

z = (a > b) ? a : b;   /* 三目运算符。 z = max(a, b) */
z = (n > 0) ? f : n; /* 如果 f是float，n是int，那么无论结果如何，表达式的值都是float型 */
(a > b) ? (a = 0) : (printf("error\n")); /* 三目运算符也可以组建语句，而且很灵活 */
for (j=0; j < i; j++) /* 数组打印方法。 lenth = i */
    printf(
    	"%s%d%s",
    	(j==0) ? " {" : "", /* 数组开头 */
    	array[j],   /* 数组内容 */
    	(j<i-1) ? ", " : "} " /* 数组结尾和分隔符 */
);
for (j=0; j < i; j++) /* 数组打印方法。 lenth = i */
	printf("%6d%c", a[i], (i%10 == 9 || i == n-1) ? '\n' : ' '); /* 每行只放 10 个元素 */
printf("there are %d item%s.\n", n, (n==1) ? "" : "s"); /* 大于1，单词就是复数 items */
```

##### 运算符优先级

```c
()  []  ->  .
!  ~  ++  --  +  -  &  sizeof  /* 一元运算符 */
*  /  %
+   -
<<  >>
<   <=  >  >=   /* 比较运算符 */
==  !=          /* 相等性运算符 */
&
^
|
&&
||
?:              /* 条件运算符，也是一种赋值 */
=  +=  -=  *=  /=  %=  &=
^=  |=  <<=  >>=
,               /* 逗号运算符，常用于for循环 和 同等级语句并列 */
```

另外，除了要注意优先级，还有一点，除了`&&  ||  ?:  ,` ，C语言没有指定同一个运算符里面，多个操作数的计算顺序

```c
x = f() + g(); /* 究竟先执行 f() 还是 g()，不确定 */
tmp = f();
x = tmp + g(); /* 如果有依赖关系，一定要f()先执行，那必须使用临时变量 */

printf("%d %d\n", ++n, power(2,n)); /* 类似地，这种表达式也不对，参数取值顺序不定，所以 ++n 和 power()哪个先执行很难说 */
a[i] = i++; /* 函数调用、嵌套赋值、自增自减都可能产生副作用。这时候就必须小心 */
```

### 表达式 Expression

由运算符Operator 和 操作数Operand 组成的式子。表达式就是由  标识符（常量变量）、字面值、操作符构成的。

运算符有优先级Precedence，必要时可以加上括号Parenthesis

```c
5; /* 单个字面值、标识符可以成为一个表达式，虽然没有任何作用。编译器警告: statement with no effect*/
hour * 60 + minute /* 字面值、标识符、操作符构成表达式。虽然也没有任何作用 */
int total_minute;
total_minute = hour * 60 + minute /* 这也是一个表达式，赋值运算符 */
total_minute + 1 = hour * 60 /* 不合法。total_minute + 1 只能做 右值rvalue，不能做 左值lvalue*/
total_minute = 5 / 3; /* C语言的整数除法，不是ceiling 或 floor，而是 截尾*/
```

任何表达式都有值和类型两个基本属性。

任何表达式，都可以是语句。

```c
printf("%d", 2);
printf("%d", 2 / var); /* 使用某种类型的常量的地方，就可以使用同类型的更复杂的表达式 */
```





## 语句 和 控制流 statements and control flow

包括：语句、语句组（语句块`{ }`），条件判断 if…else，多路选择 switch ，终止测试在顶部的循环 while | for ，终止测试在底部的循环 do ，跳转 continue break goto

### 语句、语句块

表达式把变量与常量组合起来，生成新的值。在表达式后加一个分号`;` ，就是一个语句。

仅用  `;`  是空语句。

用花括号`{}`将声明和语句包围，就构成了复合语句（或称语句块、程序块、语句组）。注意语句块`}`后面没有分号。

- 任何允许出现语句 statement 的地方，就允许出现语句块 statement block。

  ```c
  x = 0;
  i++;
  printf("hello");
  
  {  /* 语句块以{}作为界定符号 */
      int i = 10;
      printf("%d\n", i);
  }  /* 这里没有分号 */
  
  if (1)
      printf("single statement\n"); /*单个语句以 ; 号结尾。不建议在控制语句中使用单语句*/
  
  if (1)
      ;    /*单个 ; 号表示空语句*/
  
  if (1) {  /* 语句块 以 { } 作为界定符 */
      printf("statement block\n");
  } /* 花括号的尾部，这里不需要 ; 号 */
  ```

语句块和函数一样，也会构建一个作用域。

- 进入语句块时分配存储空间，退出语句块时释放存储空间。
- 作用域内的局部变量，对作用域外无效。同时，这也会构成标识符屏蔽。

从上到下的，是顺序流。改变程序的控制流程，就需要使用特殊语句。

### 控制表达式和布尔代数 Controlling Expression & Boolean Algebra

```c
if (x != 0) {  /* x != 0 就是控制表达式 */
    printf("x is not zero.\n");
}
if (x) {...} /* 等价于 if(x != 0) */
/* 布尔类型是 int  非零为真，0为假  没有布尔值 true/false（C99有，但不常用）
 * ==  !=  >  >=  <  <= 只能比较相同类型的数。不能比较字符串 
 * 非零值 是 true ，0 是 false 所以 if (x != 0) 等价于 if (x)
 */
a == 0; /* == 和 != 是相等性运算符。Equality Operator */
a > 0; /* >  >=  <  <= 是关系运算符 Relational Operator */

/* 与运算 Logical AND 两个 &号 Ampersand */
if (a < b && b < c) {...}
/* 或运算 Logical OR 两个 |号 Pipe sign */
if (a < b | a < 0) {...}
/* 非运算 Logical NOT 一个 ！号 Exclamation Mark */
if (!(a > 10)) {...}
```

运算符优先级：记不住就多加括号。

### if conditional branching

#### if…else...

if可以带else子句Clause。if 是必须的，else if 和 else 子句都可以省略。

```c
/* 如果是单行语句，就这样 */
if (x % 2 == 0)
	printf("x is even"); /* 不要写成 return (1) */
else
    printf("x is odd");

/* 也可以用语句块 */
if (x % 2 == 0) {
	printf("x is even"); /* 不要写成 return (1) */
} else {
    printf("x is odd");
}

```

if - else if -else是写多路分支的最常用方法。

```c
if (expression_1)
  statement_1;
else if (expression_2)  // 这叫级联
  statement_2;
else if (expression_3)
  statement_3;
else
  statement; /* 作为默认操作。或者也可以用来检查错误 */
/* 常见 错误 */
if (a > 0)  // 错误1：没加 {}
    salary *= 1.2;
	printf("%f\n", salary);
if (a = b){} // 错误2：混淆 = 和 ==，赋值运算符=是有返回值的
```

还可以嵌套。在嵌套的时候，最好带上花括号{} ，并且安排好缩进，才不会出现误读。

```c
/* 嵌套 if...else if...else */
if (x > 0)
    printf("x is positive");
else if (x < 0) 
    printf("x is negative");
else
    printf("x is zero");
/* 嵌套 if {...} else { if...else } */
if (x > 0) {
    printf("x is positive");
} else {
    if (x < 0)
        printf("x is negative");
    else
        printf("x is zero");
}
```

当嵌套关系复杂的时候，最好加上{}，免得产生歧义，比如下面的 Dangling-else问题：

```c
if (A) if (B) printf("C"); else printf("D"); /* 究竟应该怎么理解？*/
/* 理解方式1 错误（怎么理解由编译器决定，编译器不管 缩进 ）*/
if (A) 
    if (B) 
        printf("C"); 
else 
    printf("D");

/* 理解方式2 事实上编译器以这种方式理解。else 总和 最近的 if 配对  */
if (A) 
    if (B) 
        printf("C"); 
    else 
        printf("D");

/* 加上 { } 要它怎样就怎样*/
if (A) {
    if (B) 
        printf("C"); 
} else {
 	printf("D");
}

if (A) {
	if (B) {
    	printf("C");
    } else {
    	printf("D");
    }
}
```

浮点数精度有限，不适合精确比较。如下例子：

```c
double tmp = 20.0;
double j = tmp / 7.0;
if (j * 7.0 == tmp)
    printf("Equal.\n");
else
    print("Not Equal.\n");
```



### switch Statement

多路判定语句，仅用于表达式的值是否与一些整数常量匹配，并且执行相应的分支动作。

```c
switch (expression){ // 只能是整数
  case EXP_1: statement; statement; // EXP_1 是常数或者常数算术，编译时就知道结果
  case EXP_2: statement; statement;
  default:statement; statement;
}
```

例子：将100分制的分数，转为成绩级别

```c
int score = 99;
if (score > 90)      printf("A\n");
else if (score > 80) printf("B\n");
else if (score > 70) printf("C\n");
else if (score > 60) printf("D\n");
else                 printf("E\n");
// 转为 switch case 语句
int grade = score / 10;
switch (score) {
    case 10: case 9: // [90, 100] 分
        printf("A\n"); break;
    case 8:
        printf("B\n"); break;
    case 7:
        printf("C\n"); break;
    case 6:
        printf("D\n"); break;
    default:
        printf("E\n"); break;
}
```

- case  x:  x必须是常量表达式（就像全局变量一样能够在编译时确定）。必须是整形，因为小数精度不够。每个case 的 x 互不相同。

- default 语句是可选的。如果没有任何匹配，也没有default ，就不执行任何语句。各case 和 default 的顺序是随意的（都是`:`语句）。但为了易读，各个case 按照逻辑顺序罗列，default 在最后。

- 执行流程：用 switch 后面的变量匹配各个case的常量表达式，匹配就执行这个case内的语句。如果没有 break 或 return 语句，匹配一个case后，不仅会执行自己的语句，还会执行余下的语句，直到遇到return、break、default 才能退出 switch。可以认为，花括号内的全是一个指令块，包含很多指令， case 关键字是指令地址，而break是指令跳转。

  ```c
  switch (day) {
  /* 没有遇到break，就一直往下执行，直到遇到break 或者 运行到末尾退出switch */
  case 1: 
  case 2:
  case 3:
  case 4:
  case 5: 
      printf("Weekday\n");
      break;
  case 6: 
  case 7:
      printf("Weekend\n");
      break;
  default:
      printf("Illegal number\n");
  }
  ```

switch 语句完全可以用 if...else... 代替。

- 有时候用 switch...case...代码更清晰。if...else...匹配片段，就像炸弹；switch...case...匹配散点，就像手枪。
- 有时候编译器会优化，switch...case... 效率更高。



### while loop

有固定次数，用for循环；必须执行一次，用 do while 循环；其他情况用 while循环。

```c
while (expression)
  statement; /* 既可以是单条语句，也可以是语句块。 */
```

执行步骤：

- 首先进行条件测试expression
- 如果为真，就执行语句/语句块statement。执行完毕后，进行下一次 测试—语句块 的循环。
- 如果为假，就不执行语句块，执行 while 之后的语句。

写循环体的时候，要特别注意边界条件。

重复，是计算机最擅长，而人类最不擅长的。每次都有一点区别的重复叫做迭代Iteration。迭代可以用递归实现，也可以用循环实现。

```c
int factorial(int n)  /* 递归实现 n!  */
{
    if (n == 0) {
        return 1;
    } else {
        int tmp = factorial(n - 1);
        int result = n * tmp;
        return result;
    }
}
int factorial(int n) /* 循环实现 n!  本质是迭代+乘法 */
{
    int result = 1;
    while (n > 0) { /* n>0 是控制表达式。如果为真，就执行循环体 */
        result = result * n; /* 变量 result 是累加器Accumulator，把每次的运算结果保存起来 */
        n = n - 1;  /* 变量 n 是循环变量 Loop variable，每次循环都改变它的值 */
    } /* 循环变量 + 控制表达式 == 控制循环次数 */
    return result;
}
```

- 递归的解决思路，是用递推关系 `n! = nX(n-1)!`；而循环的解决思路，是展开了公式`n! = nX(n-1)X...X2X1`这时候循环更容易理解。但是有时候公式展开会更麻烦，甚至不可能，这时候递归就会更直观、易理解。

- 递归时，堆栈分配释放了很多变量，但每次都只有初始化赋值，变量值没有改变过。而循环的变量每次都改变。变量的值不改变是函数式编程（Functional Programming），比如LISP、Haskell、Erlang；而每次改变是命令式编程（Imperative Programming）。

- 递归一不小心会无穷递归，循环一不小心就会死循环。`while (1) {...}`就是死循环。要小心检查 *循环变量* 和 *控制表达式*  是否会造成死循环、临界值错误（循环次数错误）。

  ```c
  /* 数学界的 3x + 1 问题并不是死循环 */
  while (n != 1) { 
      if (n % 2 == 0)
          n = n / 2;
      else
          n = n * 3 + 1;
  }
  ```

do-while、for都可以写成while循环。

### do-while loop

```c
do
  statement; /* 语句或者语句块 */
while (expression);
```

执行过程：

- 首先执行语句/语句块statement，然后进行条件测试expression
- 如果为真，就再次执行语句/语句块statement，如此循环。
- 如果为假，就不执行语句块，执行 do while 之后的语句。

while是先测试控制表达式，再执行循环体；do-while是先执行循环体，再测试控制表达式。如果控制表达式的初始值为假，while一次都不会执行循环体，do-while则执行一次循环体。

do...while 用的频率比 for 和 while 更低。

```c
/* while 循环  计算  n! */
int result = 1;
while (n > 0) {
    result = result * n; 
    n = n - 1;
}
/* do-while 循环  计算  n! */
int result = 1;
int i = 1;
do {
    result = result * i; 
    i = i + 1;
} while (i <= n)
```

例子：将整数按一位位输出。比如16，输出 1   6

```c
int main(){
    int x, mask, tmp, d;
    scanf("%d", &x);
    /* 这段是逆向输出 */
    tmp = x;
    do {
        d = tmp % 10;  tmp = tmp / 10;
        printf("%d ", d);
    } while (tmp > 0);
    printf("\n");
    /* 这段是正常输出 */
    tmp = x, mask = 1;
    while( tmp > 9){ tmp = tmp / 10;  mask = mask * 10; }
    do {
        d = x / mask; x = x % mask;  mask = mask / 10;
        printf("%d ", d);
    } while ( mask > 0);
    printf("\n");
}
```



### for iteration

循环变量累加，最常用最好用的还是for循环

```c
for (exp_1; exp_2; exp_3)
  statement;
/* 如果没有continue，等价的  while 循环  */
exp_1;
while (exp_2){
    statement;
    exp_3;
}
```

for 语句适合 初始化、步长递增、条件测试 逻辑相关的情形。这时候用 for，会比 while 更紧凑易读、紧凑。

```c
int i;
for (i=0; i<10; i++) { // for(int i=0; i<10; i++)这是 C99 写法
    printf("%d", i);
}
/* 初始化语句 i=0 仅在进入循环前 执行一次
 * 条件测试 i<10 为真 就执行循环体，再执行 步长递增 i++
 * 循环后，再次条件测试：为真，则执行循环体，再步长递增；为假，则终止循环。*/
int i, j;
for (i=0, j=10; i<j; i++, j--) { ; } // 两个递增变量。逗号运算符
for (i=0, j=10; i<5, j>5; i++, j--) { } // 这种写法，编译器会警告
for (;;) {...} // 死循环
```

for 语句的头部 ，可以是任何表达式，但是牵强地把无关的运算放到初始化、变量递增部分是不合适的，for语句头部最好是只放循环控制语句。

类似地，for循环本身，也有适用和不适用的时候。

例：改写阶乘为 for 循环

```c
/* while 循环  计算  n! */
int result = 1;
while (n > 0) {
    result = result * n; 
    n = n - 1;
}
/* for 循环  计算  n! */
int result = 1;
int i;
for (i = 1; i <= n; ++i) 
    result = result * i;
return result;
```

请注意，for循环和Java/C++ 是不同的

```c
int i;
for (i = 1; i <= n; ++i) /* C90 写法，最常见。而且后面还能读取 i 的值*/
for (int i=1; i<=n; ++i) /* C99从java、C++借鉴了这种写法，gcc编译要加上 -std=c99 */
```

for 死循环

```c
for (; 1 ;) {...} /* 让控制表达式  非零  为真 */
for (; ;) {...}  /* 如果 控制表达式 为空，默认 为真 */
```

### 逗号 `,`与逗号运算符

-  参数列表、声明中的值并不是运算符。比如 `int i, j , add(int a, int b);` 语句，声明了变量`i j`和函数 `add`，而函数中又有两个参数。它们是语法分隔符号，而不是运算符。
-  逗号运算符，是C语言中优先级最低的运算符，被逗号分隔的表达式将从左到右顺序求值，各个表达式右边的操作数的值和类型，就是结果的类型和值。一般用来合并关系非常紧密的多步运算。
  - 用在 for 循环中，同时操作多个变量。
  - 多步计算的宏
  - 数据交换动作

```c
void reverse(char s[])
{
    int tmp, i, j;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        tmp = s[i];
        s[i] = s[j];
        s[j] = tmp;
//数据交换动作，合为一句 tmp=s[i], s[i]=s[j], s[j]=tmp;
    }
}
```



### 嵌套循环

```c
/* 找出 1-100 的所有素数（只能被自己和1整除） */
#include <stdio.h>
int main(void)
{
    int i, j;
    for (i=1; i <= 100; ++i) {
        for (j=2; j < i; ++j){
            if (i % j == 0)
                break;
        }
        if (j == i)
            printf("%d\n", i);
    }
    return 0;
}
```

**最近法则**：在有多层循环或者switch的情况下，break只能跳出最内层的循环或者switch，而continue也只能跳过最内层的循环。

```c
/* 乘法表 */
    int i, j;
    for (i=1; i <= 9; ++i) {
        for (j=1; j <= i; ++j){
            printf("%dX%d=%d\t", i, j, i*j);
        }
        printf("\n");
    }

/* 打印 菱形 */
void print_diamond(int n)
{
    if (n % 2 == 0) return;
    int i, j, sign, space;
    for (i=1; i<=n; i++){
        if (i <= (n + 1) / 2){
            sign = 2 * i - 1; /* 中行之前的 符号 数目*/
            space = n + 1 - 2 * i; /* 中行之前的 空格 数目*/
        } else {
            sign = 2 * n - 2 * i + 1;  /* 中行之后的 符号 数目*/
            space = 2 * i - n - 1;  /* 中行之后的 空格 数目*/
        }
        for (j=1; j<=n+1; ++j) { /* 更新打印 */
            if (j <= space/2) {
                printf("   ");
            } else if (j <= (sign + space/2) ) {
                printf(" + ");
            }
        }
        printf("\n");
    }
    return;
}
```





### jump-statement

#### break 和 continue

- break：从switch 或 最内层的循环中立即跳出。
- continue：跳过此次循环，开始下一次循环。不用于switch语句

```c
int i;
for (i=0; i<10; i++ ){
    switch (i){
      case 6: break;  /* 这个break用来跳出 switch */
      case 5: continue; /* 这个continue，却控制for循环 */
    }
    printf("%d\n", i); 
}
```



```c
/* 找出 1-100 的所有素数（只能被自己和1整除） */
#include <stdio.h>
int is_prime(int n)
{
    int i;
    for (i=2; i < n; ++i) { /* 没有整除，就在 i==n 时结束循环 */
        if (n % i == 0) break; /* 在 1-n 之间一旦有x可以整除，就跳出循环，此时 i==x */
    }  /* if else 可以合并为 return (i == n) */
    if (i == n) return 1 /* 没有整除，就有 i==n */
    else        return 0  /* 整除，就有 i==x */
}
int main(void) 
{
    int j;
    for (j=1; j <= 100; ++j) {
        if (!is_prime(j)) /* 不是素数，就 跳过此次 循环*/
            continue;
        printf("%d\n", j);
    }
    return 0;
}
```



#### goto and Labeled Statements

goto语句作用域是整个函数，实现函数内无条件跳转。从一个函数的任意地方，跳转到标记处，无论while、for、if、switch嵌套多少层。

- 最常见的用法是终止程序在某些深度嵌套结构中的处理过程，例如跳出两层或者多层循环
- 使用了goto的代码，都可以改成没有goto的代码，只是可能需要一些额外的测试或者变量。

goto的罪恶：goto语句过于强大，滥用goto会导致程序控制流程非常复杂，可读性非常差。因为C语言没有异常处理的`try...except`语句，所以goto充当函数内的异常处理机制。setjmp 和 longjmp 可以作为函数间的异常处理机制。

千万别滥用：

```c
    int i = 1; int sum = 0;
    loop: if( i<=10 ) {  /* 实际上是 for (; i<=10; ++i){ sum = sum + i; } */
        sum = sum + i;
        i++;
        goto loop;
    }
    printf("%d", sum);
```

一般 goto 只用于异常处理。程序出现异常时，跳转到末尾的 Lable，执行错误处理后（释放资源、恢复全局变量等等），函数就结束返回。

```c
/* 单纯结束 */
for ...
    for ...
        if(breakout_condition) 
            goto final; /* Lable 也遵从标识符命名原则 */
return;  /* Lable 一定是在正常流程之外，否则 Lable 下面的语句也会被正常执行 */
final: /* 可以使用多个语句，但建议使用 { } */
    printf("\n");
    printf("\n");

/* 作为清理流程 */
void foo()
{
    if (!doA()) /* 如果步骤 A 执行错误，要马上退出 */
        goto exit;
    if (!doB()) /* 如果步骤 B 执行错误，退出前要清理自己和前一步A的遗产 */
        goto cleanupA;
    if (!doC())
        goto cleanupB;
    return; /* 所有步骤正常结束，就正常返回 */
cleanupB:
    undoB();
cleanupA:
    undoA();
exit:
    return;
}
```



Duff's  Device：

```c
void send( int * to, int * from, int count)
          //    Duff设施，有帮助的注释被有意删去了  
 {
          int n = (count + 7 ) / 8 ;
          switch (count % 8 ) {
          case 0 :    do { * to ++ = * from ++ ;
          case 7 :          * to ++ = * from ++ ;
          case 6 :          * to ++ = * from ++ ;
          case 5 :          * to ++ = * from ++ ;
          case 4 :          * to ++ = * from ++ ;
          case 3 :          * to ++ = * from ++ ;
          case 2 :          * to ++ = * from ++ ;
          case 1 :          * to ++ = * from ++ ;
                 } while ( -- n >    0 );
         }  
 } 
```

那段代码的主体还是do-while循环，但这个循环的入口点并不一定是在do那里，而是由这个switch语句根据n，把循环的 入口定在了几个case标号那里。也就是说，程序的执行流程是：程序一开始顺序执行，当它执行到了switch的时候，就会根据n的值，直接跳转到 case n那里（从此，这个swicth语句就再也没有用了）。程序继续顺序执行，再当它执行到while那里时，就会判断循环条件。若为真，则while循环开 始，程序跳转到do那里开始执行循环（这时候由于已经没有了switch，所以后面的标号就变成普通标号了，即在没有goto语句的情况下就可以忽略掉这 些标号了）；为假，则退出循环，即程序中止。

等效的「正常代码」：

```c
void my_send(int *to, int *from, int count) {
    for (int i=0 ; i != count; i++)
        *to++  =  *from++ ;
}
```

这段代码的确很简洁，也是正确的，而且生成的机器码也比send函数短很多。但是却忽略了一个因素：执行效率。计算一下就可以知 道，my_send函数里面的循环条件，即i和count的比较运算的次数，是达夫设备的8倍！

# 输入输出

```c
int a = -10;
unsigned b = 10;
long c = 10;
long long d = 10;
unsigned long long e = 10;
printf("%d %u %ld %lld %llu\n", a, b, c, d, e);

// 如果使用了错误的格式，将无法得到正常的结果。
char a = -1;
int  b = -1;
printf("%u %u\n", a, b); // 4294967295 4294967295
//  char 类型
char c; 
scaf("%c", &c);
printf("%c: %d", c, c); //输出字符，和 ASCII 码
int i; char c; scanf("%d%c"); scanf("%d %c"); // 这两种读法是不一样的。

```



